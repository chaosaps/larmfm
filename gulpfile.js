"use strict";

var gulp = require("gulp");
var del = require("del");
var replace = require("gulp-replace");
var filter = require("gulp-filter");
var runSequence = require("run-sequence");
var requirejsOptimize = require('gulp-requirejs-optimize');
var glob = require("glob");

var express = require('express');

var config = {
	npmModulePath: "./node_modules/",
	distPath: "./dist/",
	projectPath: "./src/app/Larm.web/",
	appPath: "App/",
	imagesPath: "Images/",
	ComponentsPath: "Components/",
	stylesPath: "Styles/",
	dependenciesPath: "Lib/",
	servePort: 5503
};

var server = CreateServer();

gulp.task("default", ["Build"]);

gulp.task("Build", function(callback) {
	runSequence("Clean", ["CopyDependencies", "CopyHTML", "CopyStyles", "CopyImages", "OptimizeApp"], callback);
});

gulp.task("CopyDependencies", function()
{
	return gulp.src(config.projectPath + config.dependenciesPath + "**")
		.pipe(gulp.dest(config.distPath + config.dependenciesPath));
});

gulp.task("CopyHTML", function()
{
	var defaultFilter = filter(["**/default.html"], { restore: true });

	var now = new Date();
	var version = now.getFullYear() + TwoDigits(now.getMonth() + 1) + TwoDigits(now.getDate()) + TwoDigits(now.getHours()) + TwoDigits(now.getMinutes());

	return gulp.src(config.projectPath + "*.html")
			.pipe(defaultFilter)
			.pipe(replace(/var CacheBuster = (\d+);/g, "var CacheBuster = " + version + ";"))
			.pipe(replace(/bust=(\d+)/g, "bust=" + version))
			.pipe(defaultFilter.restore)
			.pipe(gulp.dest(config.distPath));
});

gulp.task("CopyImages", function()
{
	return gulp.src(config.projectPath + config.appPath + config.imagesPath + "**")
			.pipe(gulp.dest(config.distPath + config.appPath + config.imagesPath));
});

gulp.task("CopyStyles", function () {
	return gulp.src(config.projectPath + config.appPath + config.stylesPath + "**/*.css")
			.pipe(gulp.dest(config.distPath + config.appPath + config.stylesPath));
});

gulp.task("OptimizeApp", function (callback) {
	glob("**/*.{js,html}", { cwd: config.projectPath + config.appPath }, function (error, files)
	{
		files = files.map(function(filename) { return filename.slice(-3) === ".js" ? filename.slice(0, -3) : "text!" + filename; });

		gulp.src(config.projectPath + config.appPath + "Config.js")
			.pipe(requirejsOptimize({
				paths: {
					text: "../Lib/text/text",
					jquery: "../Lib/jquery/jquery.min",
					knockout: "../Lib/knockout/knockout",
					bootstrap: "../Lib/bootstrap/js/bootstrap.min",
					crossroads: "../Lib/crossroads/crossroads.min",
					signals: "../Lib/signals/signals.min",
					PortalClient: "../Lib/Portal/PortalClient.min",
					bootstrapDatetimepicker: "../Lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min",
					moment: "../Lib/moment/moment-with-locales.min",
					vis: "empty:",
					Hls: "../Lib/hls.js/hls.min",
					bloodhound: "../Lib/typeahead.js/dist/bloodhound",
					typeahead: "../Lib/typeahead.js/dist/typeahead.jquery",
					analytics: "//www.google-analytics.com/analytics"
				},
				shim: {
					bootstrap: {
						deps: [
							"jquery"
						]
					},
					PortalClient: {
						exports: "CHAOS"
					},
					analytics: {
						exports: "ga"
					}
				},
				insertRequire: [
					"bootstrap", "KnockoutBindings/DateTimePicker", "KnockoutBindings/AutoResize", "KnockoutBindings/Element", "KnockoutBindings/ScrollTo", "KnockoutBindings/Image", "KnockoutBindings/LinedText", "KnockoutBindings/Tooltip", "Main"
				],
				include: files
			}))
		.pipe(gulp.dest(config.distPath + config.appPath))
		.on("end", callback);
	});
});

gulp.task("Clean", function (callback)
{
	del([config.distPath + "**"], callback);
});

gulp.task("Serve", function () {
	server.listen(config.servePort);
});

function CreateServer() {
	var server = express();
	server.use(express.static(config.distPath));
	server.get("/", function (req, res) {
		res.sendFile("default.html", { root: config.distPath });
	});
	server.get(/((?!\.).)*$/, function (req, res) {
		res.sendFile("default.html", { root: config.distPath });
	});

	return server;
}

function TwoDigits(number)
{
	if (number < 10)
		return "0" + number;
	return number.toString();
}