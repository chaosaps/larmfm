﻿import knockout = require("knockout");
import DisposableAction = require("Utility/DisposableAction");

abstract class DisposableComponent
{
	private _actions: DisposableAction[] = [];
	private _subscriptions: KnockoutSubscription[] = [];
	private _computed: KnockoutComputed<any>[] = [];

	protected Computed<T>(value: () => T): KnockoutComputed<T>
	{
		let computed = knockout.computed(value);
		this._computed.push(computed);

		return computed;
	}

	protected PureComputed<T>(read: () => T, write?: (value: T) => void): KnockoutComputed<T>
	{
		let computed = write == null ? knockout.pureComputed(read) : knockout.pureComputed({ read: read, write: write });
		this._computed.push(computed);

		return computed;
	}

	protected Subscribe<T>(subscribable: KnockoutSubscribable<T>, callback: (value: T) => void, autoDispose: boolean = true): () => void
	{
		let subscription = subscribable.subscribe(callback);

		if (autoDispose)
			this._subscriptions.push(subscription);

		return () => subscription.dispose();
	}

	protected SubscribeToArray<T>(subscribable: KnockoutObservableArray<T>, callback: (value: T, status: string) => void, autoDispose: boolean = true): () => void
	{
		let subscription = subscribable.subscribe((e: { value: T, status: string }[]) =>
		{
			e.forEach(v => callback(v.value, v.status));
		}, null, "arrayChange");

		if (autoDispose)
			this._subscriptions.push(subscription);

		return () => subscription.dispose();
	}

	protected SubscribeUntilChange<T>(subscribable: KnockoutSubscribable<T>, callback: (value: T) => void, autoDispose: boolean = true): () => void
	{
		let unsubscriber = this.Subscribe(subscribable, v =>
		{
			unsubscriber();
			callback(v);
		}, autoDispose);

		return unsubscriber;
	}

	protected AddAction(condition: () => boolean, action: () => void, timeout?: number): void
	{
		let disposableAction = new DisposableAction(condition, action, timeout);

		if (disposableAction.IsDisposed) return;

		this._actions.push(disposableAction);
	}

	public dispose(): void
	{
		this._actions.forEach(a => a.Dispose());
		this._subscriptions.forEach(s => s.dispose());
		this._computed.forEach(c => c.dispose());
	}

	protected IsNullOrEmpty(value: string): boolean
	{
		return value == null || value == "";
	}

	protected EscapeHtml(input: string): string
	{
		return input
			.replace(/&/g, "&amp;")
			.replace(/</g, "&lt;")
			.replace(/>/g, "&gt;")
			.replace(/"/g, "&quot;")
			.replace(/'/g, "&#039;");
	}

	protected ReplaceNewlineWithBr(input: string): string
	{
		return input
			.replace(/\r?\n/g, "<br/>");
	}
}

export = DisposableComponent;