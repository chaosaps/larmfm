﻿import knockout = require("knockout");
import DialogState = require("Managers/Dialog/DialogState");

class NotApprovedDialog
{
	private _state: DialogState<any>;

	constructor(state: DialogState<any>)
	{
		this._state = state;
	}
}

export = NotApprovedDialog;