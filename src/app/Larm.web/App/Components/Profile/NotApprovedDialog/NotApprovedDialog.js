define(["require", "exports"], function (require, exports) {
    "use strict";
    var NotApprovedDialog = (function () {
        function NotApprovedDialog(state) {
            this._state = state;
        }
        return NotApprovedDialog;
    }());
    return NotApprovedDialog;
});
