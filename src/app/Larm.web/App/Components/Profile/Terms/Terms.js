define(["require", "exports", "Managers/Profile"], function (require, exports, Profile) {
    "use strict";
    var Terms = (function () {
        function Terms(state) {
            this._state = state;
        }
        Terms.prototype.Accept = function () {
            Profile.AcceptTerms();
            this._state.Close().done(function () { return Profile.ShowDialogIfProfileIsIncomplete(true); });
        };
        return Terms;
    }());
    return Terms;
});
