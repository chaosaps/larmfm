﻿import Profile = require("Managers/Profile");
import DialogState = require("Managers/Dialog/DialogState");

class Terms
{
	private _state: DialogState<void>;

	constructor(state: DialogState<void>)
	{
		this._state = state;
	}

	public Accept():void
	{
		Profile.AcceptTerms();
		this._state.Close().done(() => Profile.ShowDialogIfProfileIsIncomplete(true))
	}
}

export = Terms;