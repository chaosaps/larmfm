﻿import knockout = require("knockout");
import Profile = require("Managers/Profile");
import DialogState = require("Managers/Dialog/DialogState");

class EditProfileDialog
{
	public ShouldShowWelcome: boolean;
	public CanSave: KnockoutComputed<boolean>;
	public Email:KnockoutObservable<string> = knockout.observable("");
	public Name: KnockoutObservable<string> = knockout.observable("");

	private _state:DialogState<boolean>;

	constructor(state: DialogState<boolean>)
	{
		this._state = state;

		this.ShouldShowWelcome = this._state.Data;

		this.Name(Profile.Name());
		this.Email(Profile.Email().indexOf("WAYF-DK-") === 0 ? "" : Profile.Email());

		this.CanSave = knockout.pureComputed(() => this.Name() !== "");
	}

	public Save():void
	{
		if (!this.CanSave()) return;

		Profile.Update(this.Name(), this.Email(), success =>
		{
			if (success) this._state.Close();
		});
	}

	public dispose():void
	{
		this.CanSave.dispose();
	}
}

export = EditProfileDialog;