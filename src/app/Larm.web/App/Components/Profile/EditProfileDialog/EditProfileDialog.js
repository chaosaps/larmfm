define(["require", "exports", "knockout", "Managers/Profile"], function (require, exports, knockout, Profile) {
    "use strict";
    var EditProfileDialog = (function () {
        function EditProfileDialog(state) {
            var _this = this;
            this.Email = knockout.observable("");
            this.Name = knockout.observable("");
            this._state = state;
            this.ShouldShowWelcome = this._state.Data;
            this.Name(Profile.Name());
            this.Email(Profile.Email().indexOf("WAYF-DK-") === 0 ? "" : Profile.Email());
            this.CanSave = knockout.pureComputed(function () { return _this.Name() !== ""; });
        }
        EditProfileDialog.prototype.Save = function () {
            var _this = this;
            if (!this.CanSave())
                return;
            Profile.Update(this.Name(), this.Email(), function (success) {
                if (success)
                    _this._state.Close();
            });
        };
        EditProfileDialog.prototype.dispose = function () {
            this.CanSave.dispose();
        };
        return EditProfileDialog;
    }());
    return EditProfileDialog;
});
