﻿import knockout = require("knockout");
import DialogState = require("Managers/Dialog/DialogState");
import DialogManager = require("Managers/Dialog/Dialog");

class Dialog
{
	public HasDialog:KnockoutComputed<boolean>;
	public State: KnockoutObservable<DialogState<any>>;
	public IsWide:KnockoutObservable<boolean>;

	constructor()
	{
		this.IsWide = DialogManager.IsWide;
		this.State = DialogManager.Current;
		this.HasDialog = knockout.computed(() => this.State() != null);
	}
}

export = Dialog;