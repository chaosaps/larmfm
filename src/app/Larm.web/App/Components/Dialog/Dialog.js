define(["require", "exports", "knockout", "Managers/Dialog/Dialog"], function (require, exports, knockout, DialogManager) {
    "use strict";
    var Dialog = (function () {
        function Dialog() {
            var _this = this;
            this.IsWide = DialogManager.IsWide;
            this.State = DialogManager.Current;
            this.HasDialog = knockout.computed(function () { return _this.State() != null; });
        }
        return Dialog;
    }());
    return Dialog;
});
