define(["require", "exports", "knockout", "Utility/DisposableAction"], function (require, exports, knockout, DisposableAction) {
    "use strict";
    var DisposableComponent = (function () {
        function DisposableComponent() {
            this._actions = [];
            this._subscriptions = [];
            this._computed = [];
        }
        DisposableComponent.prototype.Computed = function (value) {
            var computed = knockout.computed(value);
            this._computed.push(computed);
            return computed;
        };
        DisposableComponent.prototype.PureComputed = function (read, write) {
            var computed = write == null ? knockout.pureComputed(read) : knockout.pureComputed({ read: read, write: write });
            this._computed.push(computed);
            return computed;
        };
        DisposableComponent.prototype.Subscribe = function (subscribable, callback, autoDispose) {
            if (autoDispose === void 0) { autoDispose = true; }
            var subscription = subscribable.subscribe(callback);
            if (autoDispose)
                this._subscriptions.push(subscription);
            return function () { return subscription.dispose(); };
        };
        DisposableComponent.prototype.SubscribeToArray = function (subscribable, callback, autoDispose) {
            if (autoDispose === void 0) { autoDispose = true; }
            var subscription = subscribable.subscribe(function (e) {
                e.forEach(function (v) { return callback(v.value, v.status); });
            }, null, "arrayChange");
            if (autoDispose)
                this._subscriptions.push(subscription);
            return function () { return subscription.dispose(); };
        };
        DisposableComponent.prototype.SubscribeUntilChange = function (subscribable, callback, autoDispose) {
            if (autoDispose === void 0) { autoDispose = true; }
            var unsubscriber = this.Subscribe(subscribable, function (v) {
                unsubscriber();
                callback(v);
            }, autoDispose);
            return unsubscriber;
        };
        DisposableComponent.prototype.AddAction = function (condition, action, timeout) {
            var disposableAction = new DisposableAction(condition, action, timeout);
            if (disposableAction.IsDisposed)
                return;
            this._actions.push(disposableAction);
        };
        DisposableComponent.prototype.dispose = function () {
            this._actions.forEach(function (a) { return a.Dispose(); });
            this._subscriptions.forEach(function (s) { return s.dispose(); });
            this._computed.forEach(function (c) { return c.dispose(); });
        };
        DisposableComponent.prototype.IsNullOrEmpty = function (value) {
            return value == null || value == "";
        };
        DisposableComponent.prototype.EscapeHtml = function (input) {
            return input
                .replace(/&/g, "&amp;")
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;")
                .replace(/"/g, "&quot;")
                .replace(/'/g, "&#039;");
        };
        DisposableComponent.prototype.ReplaceNewlineWithBr = function (input) {
            return input
                .replace(/\r?\n/g, "<br/>");
        };
        return DisposableComponent;
    }());
    return DisposableComponent;
});
