define(["require", "exports", "Managers/Authorization", "Managers/Profile", "Managers/Dialog/Dialog", "Managers/PortChecker", "./CloseDownDialog/CloseDownDialog"], function (require, exports, Authorization, Profile, DialogManager, PortChecker, CloseDownDialog) {
    "use strict";
    var TopBar = (function () {
        function TopBar() {
            this.CanLogin = Authorization.CanLogin;
            this.CanLogOut = Authorization.CanLogOut;
            this.IsAuthenticated = Authorization.IsAuthenticated;
            this.ProfileName = Profile.Name;
            this.ChecksFailed = PortChecker.HasErrors;
            CloseDownDialog.ShowIfNeeded();
        }
        TopBar.prototype.ShowChecks = function () {
            DialogManager.Show("TopBar/ChecksDialog", {});
        };
        TopBar.prototype.ShowContact = function () {
            DialogManager.Show("TopBar/ContactDialog", {});
        };
        TopBar.prototype.ShowCloseDown = function () {
            DialogManager.Show("TopBar/CloseDownDialog", {});
        };
        TopBar.prototype.EditProfile = function () {
            Profile.EditProfile();
        };
        TopBar.prototype.WayfLogin = function () {
            Authorization.WayfLogin();
        };
        TopBar.prototype.LogOut = function () {
            Authorization.LogOut();
        };
        return TopBar;
    }());
    return TopBar;
});
