﻿import DialogState = require("Managers/Dialog/DialogState");
import Profile = require("Managers/Profile");
import DateParser = require("Utility/DateParser");

class ContactDialog
{
	private _state: DialogState<boolean>;

	public Version: number;
	public Time: string;
	public User: string | null;
	public UserAgent: string;
	public Url: string;

	constructor(state: DialogState<any>)
	{
		this._state = state;
		this.Version = CacheBuster;
		this.Time = DateParser.ToLocalUTCString(new Date());
		this.User = Profile.IsReady() ? Profile.Identifier() : null;
		this.UserAgent = window.navigator.userAgent;
		this.Url = document.location.toString();
	}
}

export = ContactDialog;