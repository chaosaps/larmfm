var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Managers/PortChecker", "Components/DisposableComponent"], function (require, exports, PortChecker, DisposableComponent) {
    "use strict";
    var ChecksDialog = (function (_super) {
        __extends(ChecksDialog, _super);
        function ChecksDialog(state) {
            var _this = _super.call(this) || this;
            _this._state = state;
            _this.HasError = PortChecker.HasErrors;
            _this.Ports = PortChecker.FailedPorts;
            _this.CanRun = _this.PureComputed(function () { return !PortChecker.IsChecking(); });
            return _this;
        }
        ChecksDialog.prototype.Run = function () {
            PortChecker.Run();
        };
        return ChecksDialog;
    }(DisposableComponent));
    return ChecksDialog;
});
