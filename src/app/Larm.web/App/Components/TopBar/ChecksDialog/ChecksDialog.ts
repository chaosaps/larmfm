﻿import knockout = require("knockout");
import DialogState = require("Managers/Dialog/DialogState");
import PortChecker = require("Managers/PortChecker");
import DisposableComponent = require("Components/DisposableComponent");

class ChecksDialog extends DisposableComponent
{
	public Ports: KnockoutObservableArray<number>;
	public CanRun: KnockoutComputed<boolean>;
	public HasError:KnockoutComputed<boolean>;

	private _state: DialogState<boolean>;

	constructor(state: DialogState<any>)
	{
		super();
		this._state = state;

		this.HasError = PortChecker.HasErrors;
		this.Ports = PortChecker.FailedPorts;
		this.CanRun = this.PureComputed(() => ! PortChecker.IsChecking());
	}

	public Run():void
	{
		PortChecker.Run();
	}
}

export = ChecksDialog;