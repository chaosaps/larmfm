define(["require", "exports", "Managers/Dialog/Dialog"], function (require, exports, DialogManager) {
    "use strict";
    var CloseDownDialog = (function () {
        function CloseDownDialog(state) {
            this._state = state;
        }
        CloseDownDialog.prototype.Close = function () {
            localStorage.setItem(CloseDownDialog.localStorageKey, Date.now().toString(10));
        };
        CloseDownDialog.ShowIfNeeded = function () {
            var lastSeen = localStorage.getItem(CloseDownDialog.localStorageKey);
            console.log(lastSeen);
            if (lastSeen !== null) {
                var lastSeenNumber = parseInt(lastSeen);
                if (lastSeenNumber !== NaN && lastSeenNumber > Date.now() - 18 * 60 * 60 * 1000)
                    return;
            }
            DialogManager.Show("TopBar/CloseDownDialog", {});
        };
        CloseDownDialog.localStorageKey = "CloseDownDialogLastSeen";
        return CloseDownDialog;
    }());
    return CloseDownDialog;
});
