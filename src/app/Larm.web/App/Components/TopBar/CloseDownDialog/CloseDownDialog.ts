﻿import DialogState = require("Managers/Dialog/DialogState");
import DialogManager = require("Managers/Dialog/Dialog");

class CloseDownDialog
{
	private static localStorageKey = "CloseDownDialogLastSeen";
	private _state: DialogState<boolean>;

	constructor(state: DialogState<any>)
	{
		this._state = state;
		
	}

	public Close(): void
	{
		localStorage.setItem(CloseDownDialog.localStorageKey, Date.now().toString(10));
	}

	public static ShowIfNeeded(): void
	{
		const lastSeen = localStorage.getItem(CloseDownDialog.localStorageKey);

		console.log(lastSeen)

		if (lastSeen !== null) {
			const lastSeenNumber = parseInt(lastSeen);

			if (lastSeenNumber !== NaN && lastSeenNumber > Date.now() - 18 * 60 * 60 * 1000)
				return;
		}

		DialogManager.Show("TopBar/CloseDownDialog", {});
	}
}

export = CloseDownDialog;