define(["require", "exports", "Managers/Profile", "Utility/DateParser"], function (require, exports, Profile, DateParser) {
    "use strict";
    var ContactDialog = (function () {
        function ContactDialog(state) {
            this._state = state;
            this.Version = CacheBuster;
            this.Time = DateParser.ToLocalUTCString(new Date());
            this.User = Profile.IsReady() ? Profile.Identifier() : null;
            this.UserAgent = window.navigator.userAgent;
            this.Url = document.location.toString();
        }
        return ContactDialog;
    }());
    return ContactDialog;
});
