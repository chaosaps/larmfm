﻿import knockout = require("knockout");
import Authorization = require("Managers/Authorization");
import Profile = require("Managers/Profile");
import DialogManager = require("Managers/Dialog/Dialog");
import PortChecker = require("Managers/PortChecker");
import CloseDownDialog = require("./CloseDownDialog/CloseDownDialog");

class TopBar
{
	public CanLogin: KnockoutObservable<boolean>;
	public CanLogOut: KnockoutObservable<boolean>;
	public IsAuthenticated: KnockoutObservable<boolean>;
	public ChecksFailed:KnockoutComputed<boolean>;

	public ProfileName:KnockoutObservable<string>;

	constructor()
	{
		this.CanLogin = Authorization.CanLogin;
		this.CanLogOut = Authorization.CanLogOut;
		this.IsAuthenticated = Authorization.IsAuthenticated;
		this.ProfileName = Profile.Name;
		this.ChecksFailed = PortChecker.HasErrors;

		CloseDownDialog.ShowIfNeeded();
	}

	public ShowChecks():void
	{
		DialogManager.Show("TopBar/ChecksDialog", {});
	}

	public ShowContact():void
	{
		DialogManager.Show("TopBar/ContactDialog", {});
	}
	
	public ShowCloseDown():void
	{
		DialogManager.Show("TopBar/CloseDownDialog", {});
	}

	public EditProfile():void
	{
		Profile.EditProfile();
	}

	public WayfLogin():void
	{
		Authorization.WayfLogin();
	}

	public LogOut():void
	{
		Authorization.LogOut();
	}
}

export = TopBar;