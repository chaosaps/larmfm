define(["require", "exports"], function (require, exports) {
    "use strict";
    var NameConventonLoader = (function () {
        function NameConventonLoader() {
        }
        NameConventonLoader.prototype.getConfig = function (componentName, callback) {
            var filePath = NameConventonLoader.GetFilePath(componentName);
            callback({
                viewModel: { require: filePath },
                template: { require: "text!" + filePath + ".html" }
            });
        };
        NameConventonLoader.GetFilePath = function (name) {
            var filePath = name + (name.lastIndexOf("/") === -1 ? "/" + name : name.substring(name.lastIndexOf("/")));
            return "Components/" + filePath;
        };
        return NameConventonLoader;
    }());
    return NameConventonLoader;
});
