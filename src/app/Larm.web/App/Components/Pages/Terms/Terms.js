define(["require", "exports", "Managers/Interface"], function (require, exports, Interface) {
    "use strict";
    var Terms = (function () {
        function Terms() {
            Interface.Hide();
        }
        return Terms;
    }());
    return Terms;
});
