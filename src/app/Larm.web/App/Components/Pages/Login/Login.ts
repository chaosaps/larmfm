﻿import knockout = require("knockout");
import Authorization = require("Managers/Authorization");
import Navigation = require("Managers/Navigation/Navigation");

class Login
{
	public CanLogin: KnockoutObservable<boolean>;
	public Email:KnockoutObservable<string> = knockout.observable("");
	public Password:KnockoutObservable<string> = knockout.observable("");
	public RememberLogin: KnockoutObservable<boolean> = knockout.observable(false);
	public IsLoginIncorrect:KnockoutObservable<boolean> = knockout.observable(false);

	constructor()
	{
		this.CanLogin = Authorization.CanLogin;
	}

	public Login():void
	{
		this.IsLoginIncorrect(false);
		Authorization.Login(this.Email(), this.Password(), this.RememberLogin(), success =>
		{
			if (success)
				Navigation.Navigate("");
			else
				this.IsLoginIncorrect(true);
		});
	}
}

export = Login;