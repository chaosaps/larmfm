define(["require", "exports", "knockout", "Managers/Authorization", "Managers/Navigation/Navigation"], function (require, exports, knockout, Authorization, Navigation) {
    "use strict";
    var Login = (function () {
        function Login() {
            this.Email = knockout.observable("");
            this.Password = knockout.observable("");
            this.RememberLogin = knockout.observable(false);
            this.IsLoginIncorrect = knockout.observable(false);
            this.CanLogin = Authorization.CanLogin;
        }
        Login.prototype.Login = function () {
            var _this = this;
            this.IsLoginIncorrect(false);
            Authorization.Login(this.Email(), this.Password(), this.RememberLogin(), function (success) {
                if (success)
                    Navigation.Navigate("");
                else
                    _this.IsLoginIncorrect(true);
            });
        };
        return Login;
    }());
    return Login;
});
