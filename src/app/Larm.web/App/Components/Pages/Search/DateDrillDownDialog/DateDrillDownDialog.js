var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Search/Search", "Managers/Portal/EZArchivePortal", "Managers/Configuration", "Managers/Notification", "Utility/DataFormat", "Components/DisposableComponent", "Managers/Search/MenuFacets"], function (require, exports, knockout, Search, EZArchivePortal, Configuration, Notification, DataFormat, DisposableComponent, MenuFacets) {
    "use strict";
    var DateDrillDownDialog = (function (_super) {
        __extends(DateDrillDownDialog, _super);
        function DateDrillDownDialog(state) {
            var _this = _super.call(this) || this;
            _this.OptionsType = knockout.observable(null);
            _this.Options = knockout.observableArray();
            _this._state = state;
            _this.SearchString = _this.PureComputed(function () { return Search.Query().Query; });
            _this.IsSearchEmpty = _this.PureComputed(function () { return _this.SearchString() === ""; });
            _this.SearchFacet = _this.PureComputed(function () {
                var query = Search.Query();
                return query.Facets !== null && query.Facets.length !== 0 ? query.Facets[0] : null;
            });
            _this.HasFacet = _this.PureComputed(function () { return _this.SearchFacet() !== null; });
            _this.FacetName = _this.PureComputed(function () { return _this.HasFacet() ? MenuFacets.GetFacetValueName(_this.SearchFacet().Key, _this.SearchFacet().Value) : null; });
            _this.ShowDecades(1920, 2029);
            _this.Breadcrumbs = _this.PureComputed(function () { return _this.CreateBreadcrumbs(); });
            return _this;
        }
        DateDrillDownDialog.prototype.ClearQuery = function () {
            var query = Search.Clone();
            query.Query = "";
            Search.Search(query);
            this.LoadCounts();
        };
        DateDrillDownDialog.prototype.ClearFacet = function () {
            var _this = this;
            var query = Search.Clone();
            query.Facets = null;
            this.SubscribeUntilChange(this.SearchFacet, function () { return _this.LoadCounts(); });
            Search.Search(query);
        };
        DateDrillDownDialog.prototype.Search = function () {
            var query = Search.Clone();
            query.From = this.From;
            query.To = this.To;
            query.Label = null;
            Search.Search(query);
            this._state.Close();
        };
        DateDrillDownDialog.prototype.ShowDecades = function (firstYear, lastYear) {
            this.From = new Date(firstYear, 0, 1);
            this.To = new Date(lastYear, 0, 1);
            this.To.setSeconds(-1);
            this.Options.removeAll();
            this.OptionsType(DateDrillDownDialog.OptionsTypeDecade);
            for (var year = firstYear; year < lastYear; year += 10)
                this.Options.push(this.CreateDecade(year));
            this.LoadCounts();
        };
        DateDrillDownDialog.prototype.ShowYears = function (firstYear, lastYear) {
            this.From = new Date(firstYear, 0, 1);
            this.To = new Date(lastYear + 1, 0, 1);
            this.To.setSeconds(-1);
            this.Options.removeAll();
            this.OptionsType(DateDrillDownDialog.OptionsTypeYear);
            for (var year = firstYear; year <= lastYear; year++)
                this.Options.push(this.CreateYear(year));
            this.LoadCounts();
        };
        DateDrillDownDialog.prototype.ShowMonths = function (year) {
            this.From = new Date(year, 0, 1);
            this.To = new Date(year + 1, 0, 1);
            this.To.setSeconds(-1);
            this.Options.removeAll();
            this.OptionsType(DateDrillDownDialog.OptionsTypeMonth);
            for (var month = 0; month < 12; month++)
                this.Options.push(this.CreateMonth(year, month));
            this.LoadCounts();
        };
        DateDrillDownDialog.prototype.ShowDays = function (year, month) {
            this.From = new Date(year, month, 1);
            this.To = new Date(year, month + 1, 1);
            this.To.setSeconds(-1);
            this.Options.removeAll();
            this.OptionsType(DateDrillDownDialog.OptionsTypeDay);
            var numberOfDays = new Date(year, month + 1, 0).getDate();
            for (var day = 1; day <= numberOfDays; day++)
                this.Options.push(this.CreateDay(year, month, day));
            this.LoadCounts();
        };
        DateDrillDownDialog.prototype.SearchOnDay = function (year, month, day) {
            this.From = new Date(year, month, day);
            this.To = new Date(year, month, day + 1);
            this.To.setSeconds(-1);
            this.Search();
        };
        DateDrillDownDialog.prototype.LoadCounts = function () {
            var _this = this;
            var gap;
            switch (this.OptionsType()) {
                case DateDrillDownDialog.OptionsTypeDecade:
                    gap = "10YEAR";
                    break;
                case DateDrillDownDialog.OptionsTypeYear:
                    gap = "1YEAR";
                    break;
                case DateDrillDownDialog.OptionsTypeMonth:
                    gap = "1MONTH";
                    break;
                case DateDrillDownDialog.OptionsTypeDay:
                    gap = "1DAY";
                    break;
            }
            var filter = this.HasFacet() ? this.SearchFacet().Key + ":\"" + this.SearchFacet().Value + "\"" : null;
            EZArchivePortal.EZFacet.Get(Search.Query().SafeQuery, filter, null, null, this.GetUTCDate(this.From), this.GetUTCDate(this.To), gap).WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to get facets: " + response.Error.Message);
                    return;
                }
                var options = _this.Options();
                response.Body.Results.filter(function (f) { return f.Header === Configuration.DrillDownFacet.Header; })[0].Fields.filter(function (f) { return f.Value === Configuration.DrillDownFacet.Field; })[0].Facets.forEach(function (f, i) { return options[i].Count(DataFormat.PrettyNumber(f.Count)); });
            });
        };
        DateDrillDownDialog.prototype.GetUTCDate = function (date) {
            return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
        };
        DateDrillDownDialog.prototype.CreateDecade = function (firstYear) {
            var _this = this;
            return { Value: firstYear, Count: knockout.observable("0"), Select: function () { return _this.ShowYears(firstYear, firstYear + 9); } };
        };
        DateDrillDownDialog.prototype.CreateYear = function (year) {
            var _this = this;
            return { Value: year, Count: knockout.observable("0"), Select: function () { return _this.ShowMonths(year); } };
        };
        DateDrillDownDialog.prototype.CreateMonth = function (year, month) {
            var _this = this;
            return { Value: month, Count: knockout.observable("0"), Select: function () { return _this.ShowDays(year, month); } };
        };
        DateDrillDownDialog.prototype.CreateDay = function (year, month, day) {
            var _this = this;
            return { Value: day, Count: knockout.observable("0"), Select: function () { return _this.SearchOnDay(year, month, day); } };
        };
        DateDrillDownDialog.prototype.CreateBreadcrumb = function (value, change) {
            if (change === void 0) { change = null; }
            return {
                Value: value,
                Change: change == null ? function () { } : change,
                CanChange: change != null
            };
        };
        DateDrillDownDialog.prototype.CreateBreadcrumbs = function () {
            var _this = this;
            var result = [];
            var optionType = this.OptionsType();
            result.push(this.CreateBreadcrumb("Alle årtier", optionType === DateDrillDownDialog.OptionsTypeDecade ? null : function () { return _this.ShowDecades(1920, 2029); }));
            if (optionType !== DateDrillDownDialog.OptionsTypeDecade) {
                var decade_1 = (Math.floor(this.From.getFullYear() / 10) * 10);
                result.push(this.CreateBreadcrumb(decade_1, optionType === DateDrillDownDialog.OptionsTypeYear ? null : function () { return _this.ShowYears(decade_1, decade_1 + 9); }));
                if (optionType !== DateDrillDownDialog.OptionsTypeYear) {
                    result.push(this.CreateBreadcrumb(this.From.getFullYear(), optionType === DateDrillDownDialog.OptionsTypeMonth ? null : function () { return _this.ShowMonths(_this.From.getFullYear()); }));
                    if (optionType !== DateDrillDownDialog.OptionsTypeMonth) {
                        result.push(this.CreateBreadcrumb(this.From.getMonth()));
                    }
                }
            }
            return result;
        };
        DateDrillDownDialog.OptionsTypeDecade = "Decade";
        DateDrillDownDialog.OptionsTypeYear = "Year";
        DateDrillDownDialog.OptionsTypeMonth = "Month";
        DateDrillDownDialog.OptionsTypeDay = "Day";
        return DateDrillDownDialog;
    }(DisposableComponent));
    return DateDrillDownDialog;
});
