﻿import knockout = require("knockout");
import Search = require("Managers/Search/Search");
import DialogState = require("Managers/Dialog/DialogState");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Configuration = require("Managers/Configuration");
import Notification = require("Managers/Notification");
import DataFormat = require("Utility/DataFormat");
import DisposableComponent = require("Components/DisposableComponent");
import MenuFacets = require("Managers/Search/MenuFacets");

type DrillDownOption = { Value: number, Count: KnockoutObservable<string>, Select: () => void };
type Breadcrumb = { Value: string | number, Change: () => void, CanChange:boolean };
type Facet = { Key: string; Value: string };

class DateDrillDownDialog extends DisposableComponent
{
	private static OptionsTypeDecade:string = "Decade";
	private static OptionsTypeYear: string = "Year";
	private static OptionsTypeMonth:string = "Month";
	private static OptionsTypeDay: string = "Day";

	public OptionsType: KnockoutObservable<string> = knockout.observable(null);
	public Options: KnockoutObservableArray<DrillDownOption> = knockout.observableArray<DrillDownOption>();
	public SearchString: KnockoutComputed<string>;
	public IsSearchEmpty: KnockoutComputed<boolean>;
	public SearchFacet: KnockoutComputed<Facet | null>;
	public HasFacet: KnockoutComputed<boolean>;
	public FacetName: KnockoutComputed<string | null>;
	public Breadcrumbs: KnockoutComputed<Breadcrumb[]>;

	public From:Date;
	public To: Date;

	private _state: DialogState<boolean>;

	constructor(state: DialogState<any>)
	{
		super();
		this._state = state;

		this.SearchString = this.PureComputed(() => Search.Query().Query);
		this.IsSearchEmpty = this.PureComputed(() => this.SearchString() === "");
		this.SearchFacet = this.PureComputed(() =>
		{
			const query = Search.Query();

			return query.Facets !== null && query.Facets.length !== 0 ? query.Facets[0] : null;
		});
		this.HasFacet = this.PureComputed(() => this.SearchFacet() !== null);
		this.FacetName = this.PureComputed(() => this.HasFacet() ? MenuFacets.GetFacetValueName(this.SearchFacet().Key, this.SearchFacet().Value) : null);

		this.ShowDecades(1920, 2029);

		this.Breadcrumbs = this.PureComputed(() => this.CreateBreadcrumbs());
	}

	public ClearQuery(): void
	{
		var query = Search.Clone();

		query.Query = "";

		Search.Search(query);

		this.LoadCounts();
	}
	
	public ClearFacet(): void
	{
		const query = Search.Clone();

		query.Facets = null;

		this.SubscribeUntilChange(this.SearchFacet, () => this.LoadCounts());

		Search.Search(query);
	}

	public Search(): void
	{
		var query = Search.Clone();
		query.From = this.From;
		query.To = this.To;
		query.Label = null;
		Search.Search(query);

		this._state.Close();
	}

	public ShowDecades(firstYear:number, lastYear:number):void
	{
		this.From = new Date(firstYear, 0, 1);
		this.To = new Date(lastYear, 0, 1);
		this.To.setSeconds(-1);

		this.Options.removeAll();
		this.OptionsType(DateDrillDownDialog.OptionsTypeDecade);

		for (let year = firstYear; year < lastYear; year += 10)
			this.Options.push(this.CreateDecade(year));

		this.LoadCounts();
	}

	public ShowYears(firstYear:number, lastYear:number):void
	{
		this.From = new Date(firstYear, 0, 1);
		this.To = new Date(lastYear + 1, 0, 1);
		this.To.setSeconds(-1);

		this.Options.removeAll();
		this.OptionsType(DateDrillDownDialog.OptionsTypeYear);

		for (let year = firstYear; year <= lastYear; year++)
			this.Options.push(this.CreateYear(year));

		this.LoadCounts();
	}

	public ShowMonths(year:number):void
	{
		this.From = new Date(year, 0, 1);
		this.To = new Date(year + 1, 0, 1);
		this.To.setSeconds(-1);

		this.Options.removeAll();
		this.OptionsType(DateDrillDownDialog.OptionsTypeMonth);

		for (let month = 0; month < 12; month++)
			this.Options.push(this.CreateMonth(year, month));

		this.LoadCounts();
	}

	public ShowDays(year:number, month:number):void
	{
		this.From = new Date(year, month, 1);
		this.To = new Date(year, month + 1, 1);
		this.To.setSeconds(-1);

		this.Options.removeAll();
		this.OptionsType(DateDrillDownDialog.OptionsTypeDay);

		var numberOfDays = new Date(year, month + 1, 0).getDate();

		for (let day = 1; day <= numberOfDays; day++)
			this.Options.push(this.CreateDay(year, month, day));

		this.LoadCounts();
	}

	private SearchOnDay(year:number, month:number, day:number):void
	{
		this.From = new Date(year, month, day);
		this.To = new Date(year, month, day + 1);
		this.To.setSeconds(-1);

		this.Search();
	}

	private LoadCounts():void
	{
		let gap: string;

		switch (this.OptionsType())
		{
			case DateDrillDownDialog.OptionsTypeDecade:
				gap = "10YEAR";
				break;
			case DateDrillDownDialog.OptionsTypeYear:
				gap = "1YEAR";
				break;
			case DateDrillDownDialog.OptionsTypeMonth:
				gap = "1MONTH";
				break;
			case DateDrillDownDialog.OptionsTypeDay:
				gap = "1DAY";
				break;
		}

		const filter = this.HasFacet() ? `${this.SearchFacet().Key}:"${this.SearchFacet().Value}"` : null;

		EZArchivePortal.EZFacet.Get(Search.Query().SafeQuery, filter, null, null, this.GetUTCDate(this.From), this.GetUTCDate(this.To), gap).WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error("Failed to get facets: " + response.Error.Message);
				return;
			}

			var options = this.Options();

			response.Body.Results.filter(f => f.Header === Configuration.DrillDownFacet.Header)[0].Fields.filter(f => f.Value === Configuration.DrillDownFacet.Field)[0].Facets.forEach((f, i) => options[i].Count(DataFormat.PrettyNumber(f.Count)));
		});
	}

	private GetUTCDate(date:Date):Date
	{
		return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
	}

	private CreateDecade(firstYear:number):DrillDownOption
	{
		return { Value: firstYear, Count: knockout.observable("0"), Select: () => this.ShowYears(firstYear, firstYear + 9) };
	}

	private CreateYear(year: number): DrillDownOption
	{
		return { Value: year, Count: knockout.observable("0"), Select: () => this.ShowMonths(year) };
	}

	private CreateMonth(year: number, month: number): DrillDownOption
	{
		return { Value: month, Count: knockout.observable("0"), Select: () => this.ShowDays(year, month) };
	}

	private CreateDay(year: number, month: number, day: number): DrillDownOption
	{
		return { Value: day, Count: knockout.observable("0"), Select: () => this.SearchOnDay(year, month, day) };
	}

	private CreateBreadcrumb(value: string|number, change: () => void = null): Breadcrumb
	{
		return {
			Value: value,
			Change: change == null ? () => {} : change,
			CanChange: change != null
		}
	}

	private CreateBreadcrumbs(): Breadcrumb[]
	{
		const result: Breadcrumb[] = [];
		const optionType = this.OptionsType();

		result.push(this.CreateBreadcrumb("Alle årtier", optionType === DateDrillDownDialog.OptionsTypeDecade ? null : () => this.ShowDecades(1920, 2029)));

		if (optionType !== DateDrillDownDialog.OptionsTypeDecade)
		{
			const decade = (Math.floor(this.From.getFullYear() / 10) * 10);
			result.push(this.CreateBreadcrumb(decade, optionType === DateDrillDownDialog.OptionsTypeYear ? null : () => this.ShowYears(decade, decade + 9)));

			if (optionType !== DateDrillDownDialog.OptionsTypeYear)
			{
				result.push(this.CreateBreadcrumb(this.From.getFullYear(), optionType === DateDrillDownDialog.OptionsTypeMonth ? null : () => this.ShowMonths(this.From.getFullYear())));

				if (optionType !== DateDrillDownDialog.OptionsTypeMonth)
				{
					result.push(this.CreateBreadcrumb(this.From.getMonth()));
				}
			}
		}
			
		return result;
	}
}

export = DateDrillDownDialog
