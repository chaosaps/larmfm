var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Portal/EZArchivePortal", "Managers/Notification", "Managers/Search/Search", "Managers/Configuration", "Managers/Dragging", "Managers/Permissions", "Components/DisposableComponent", "Utility/DateParser", "Managers/Interface", "Managers/Authorization", "Utility/FileSaver"], function (require, exports, knockout, EZArchivePortal, Notification, SearchManager, Configuration, Dragging, Permissions, DisposableComponent, DateParser, Interface, Authorization, FileSaver_1) {
    "use strict";
    var Search = (function (_super) {
        __extends(Search, _super);
        function Search(data) {
            var _this = _super.call(this) || this;
            _this.SearchFields = knockout.observableArray();
            _this.SearchResults = knockout.observableArray();
            _this.TotalResultCount = knockout.observable(0);
            _this.DragElement = knockout.observable();
            _this.DraggingResult = knockout.observable();
            _this.IsDraggingTopElement = knockout.observable(false);
            _this._searchCount = 0;
            _this._valueParsers = _this.CreateParsers();
            SearchManager.Parse(data.Query());
            _this.Query = SearchManager.Query;
            _this.Search = function (q) { return SearchManager.Search(q); };
            _this.CanDisassociateFromLabel = _this.PureComputed(function () { return _this.Query().Label != null; });
            _this.CanAssociateWithLabel = Permissions.CanAssociateWithLabel;
            _this.CanExport = _this.PureComputed(function () { return Authorization.IsAuthenticated() && _this.SearchResults().length !== 0; });
            _this.AddAction(function () { return SearchManager.IsReady(); }, function () { return _this.Initialize(); });
            Interface.Show();
            _this.Subscribe(data.Query, function (query) {
                SearchManager.Parse(query);
                _this.InnerSearch();
            });
            return _this;
        }
        Search.prototype.Export = function () {
            if (!this.CanExport)
                return;
            var link = window.location.protocol + "//" + window.location.host + "/Asset/";
            var data = this.SearchResults().map(function (s) {
                var row = s.Fields.map(function (f) { return f.Value; });
                row.push(link + s.Identifier);
                return row;
            });
            FileSaver_1.SaveAsCsv(data, "SøgeResultat.csv");
        };
        Search.prototype.Initialize = function () {
            var _a;
            (_a = this.SearchFields).push.apply(_a, this.CreateSearchFields(SearchManager.SearchFields));
            this.InnerSearch();
        };
        Search.prototype.CreateParsers = function () {
            return {
                "Datetime": function (v, t) { return DateParser.ToFormattedStringByAssetType(DateParser.ParseLocalUTCString(v), t); },
                "Date": function (v, t) { return DateParser.ToFormattedDateString(DateParser.ParseLocalUTCString(v)); },
                "String": function (v, t) { return DateParser.ToFormattedDateRange(v, t); }
            };
        };
        Search.prototype.InnerSearch = function () {
            var query = SearchManager.Query();
            var queryString = query.SafeQuery;
            var filterString = "";
            var betweenString = null;
            var sortString = query.SortField == null ? null : query.SortField + " " + (query.IsSortDirectionDescending ? "desc" : "asc");
            var facetString = query.Facets == null ? null : query.Facets.map(function (f) { return f.Key + ":" + f.Value; }).join(",");
            var pageSize = query.PageSize == null ? Configuration.DefaultPageSize : query.PageSize;
            var pageIndex = query.PageNumber == null ? 0 : query.PageNumber - 1;
            if (query.From != null && query.To != null)
                betweenString = JSON.stringify({ From: DateParser.ToLocalUTCString(query.From), To: DateParser.ToLocalUTCString(query.To) });
            if (query.Label != null)
                filterString += Configuration.LabelFilter + ":EXACT " + query.Label;
            if (filterString === "")
                filterString = null;
            this.CallSearch(queryString, filterString, betweenString, facetString, sortString, pageIndex, pageSize, ++this._searchCount);
        };
        Search.prototype.CallSearch = function (queryString, filterString, betweenString, facetString, sortString, pageIndex, pageSize, searchCount) {
            var _this = this;
            EZArchivePortal.Search.Get(queryString, filterString, betweenString, null, facetString, sortString, pageIndex, pageSize).WithCallback(function (response) {
                var _a;
                if (searchCount !== _this._searchCount)
                    return;
                if (response.Error !== null) {
                    Notification.Error("Failed to search: " + response.Error.Message);
                    return;
                }
                _this.TotalResultCount(response.Body.TotalCount);
                _this.SearchResults.removeAll();
                (_a = _this.SearchResults).push.apply(_a, response.Body.Results.map(function (r) { return _this.CreateSearchResult(r); }));
            });
        };
        Search.prototype.CreateSearchFields = function (fields) {
            return fields
                .filter(function (f) { return !Configuration.SearchResultReplaceMap.hasOwnProperty(f.DisplayName); })
                .map(function (f) {
                var isSorting = knockout.computed(function () { return SearchManager.Query().SortField === f.DisplayName; });
                var isDescending = knockout.computed(function () { return SearchManager.Query().IsSortDirectionDescending; });
                return {
                    Title: f.DisplayName,
                    Sort: function () {
                        if (!f.IsSortable)
                            return;
                        var query = SearchManager.Clone();
                        if (query.SortField === f.DisplayName)
                            query.IsSortDirectionDescending = !query.IsSortDirectionDescending;
                        else {
                            query.SortField = f.DisplayName;
                            query.IsSortDirectionDescending = false;
                        }
                        SearchManager.Search(query);
                    },
                    CanSort: f.IsSortable,
                    IsSorting: isSorting,
                    IsDescending: isDescending
                };
            });
        };
        Search.prototype.CreateSearchResult = function (result) {
            var _this = this;
            var mapped = {};
            result.Fields.forEach(function (f) { return (mapped[f.Key] = f.Value); });
            var fields = SearchManager.SearchFields.map(function (f) {
                return _this.GetSearchFieldValue(result.TypeId, f.DisplayName, f.Type, mapped.hasOwnProperty(f.DisplayName)
                    ? mapped[f.DisplayName]
                    : null);
            });
            this.ApplyReplaceMap(fields);
            this.ApplyReplaceValueMap(fields);
            return {
                Identifier: result.Identifier,
                TypeId: result.TypeId,
                Fields: fields,
                Remove: function (r) { return _this.Remove(r); },
                StartDrag: function (r, e) { return _this.StartDrag(r, e.originalEvent); }
            };
        };
        Search.prototype.ApplyReplaceMap = function (fields) {
            var replaced = [];
            var _loop_1 = function (replaceKey) {
                var key = Configuration.SearchResultReplaceMap[replaceKey];
                var replaceIndex = -1;
                var replaceValueIndex = -1;
                fields.forEach(function (f, index) {
                    switch (SearchManager.SearchFields[index].DisplayName) {
                        case key:
                            replaceIndex = index;
                            return;
                        case replaceKey:
                            replaceValueIndex = index;
                            return;
                    }
                });
                if (replaceIndex !== -1 && replaceValueIndex !== -1) {
                    var value = fields[replaceValueIndex].Value;
                    if (value !== "")
                        fields[replaceIndex] = fields[replaceValueIndex];
                    replaced.unshift(replaceValueIndex);
                }
            };
            for (var replaceKey in Configuration.SearchResultReplaceMap) {
                _loop_1(replaceKey);
            }
            replaced.forEach(function (index) { return fields.splice(index, 1); });
        };
        Search.prototype.ApplyReplaceValueMap = function (fields) {
            for (var i = 0; i < fields.length; i++) {
                if (!Configuration.SearchResultReplaceValueMap.hasOwnProperty(SearchManager.SearchFields[i].DisplayName))
                    continue;
                var replacementMap = Configuration.SearchResultReplaceValueMap[SearchManager.SearchFields[i].DisplayName];
                for (var replaceValueKey in replacementMap) {
                    if (typeof fields[i].Value === "string" && fields[i].Value.substring(0, replaceValueKey.length) !== replaceValueKey)
                        continue;
                    fields[i].Value = replacementMap[replaceValueKey];
                    break;
                }
            }
        };
        Search.prototype.Remove = function (result) {
            var _this = this;
            this.SearchResults.remove(result);
            EZArchivePortal.EZLabel.DisassociateWith(this.Query().Label, result.Identifier).WithCallback(function (response) {
                if (response.Error != null)
                    Notification.Error("Failed to disassociate asset from label: " + response.Error.Message);
                _this.InnerSearch();
            });
        };
        Search.prototype.StartDrag = function (result, event) {
            var _this = this;
            if (!this.CanAssociateWithLabel())
                return false;
            Dragging.StartSearchResultDrag(event, result.Identifier);
            if (event.dataTransfer.setDragImage !== undefined) {
                this.IsDraggingTopElement(event.clientY < 200);
                this.DraggingResult(result);
                event.dataTransfer.setDragImage(this.DragElement(), -7, -7);
                setInterval(function () { return _this.DraggingResult(null); });
            }
            return true;
        };
        Search.prototype.GetSearchFieldValue = function (assetType, fieldName, fieldType, value) {
            if (fieldName === "Udsendelsesdato_ddmmyyyy")
                fieldType = DateParser.DateTypeName;
            var parsedValue = "";
            var imagePath = null;
            if (value != null) {
                var isImage = Configuration.SearchImageMaps.indexOf(fieldName) !== -1;
                parsedValue = !isImage && this._valueParsers.hasOwnProperty(fieldType) ? this._valueParsers[fieldType](value, assetType) : value;
                if (isImage)
                    imagePath = "" + Configuration.SearchImagePath + fieldName + "/" + parsedValue.replace(/ /g, "_") + ".png";
            }
            else if (Configuration.SearchResultDefaultValue.hasOwnProperty(assetType) && Configuration.SearchResultDefaultValue[assetType].hasOwnProperty(fieldName))
                parsedValue = Configuration.SearchResultDefaultValue[assetType][fieldName];
            return { Value: parsedValue, ImagePath: imagePath };
        };
        return Search;
    }(DisposableComponent));
    return Search;
});
