﻿import knockout = require("knockout");
import Query = require("Managers/Search/Query");
import Configuration = require("Managers/Configuration");
import DisposableComponent = require("Components/DisposableComponent");

type Page = {Number:number, Size:number, Link:KnockoutComputed<string>, NavigateTo:() => void, IsActive:KnockoutComputed<boolean> };

class Pagination extends DisposableComponent
{
	public IsReady: KnockoutComputed<boolean>;

	public Pages: KnockoutObservableArray<Page> = knockout.observableArray<Page>();
	public FirstPage: KnockoutComputed<Page>;
	public LastPage: KnockoutComputed<Page>;
	public PageSizes:KnockoutComputed<Page[]>;

	public TotalResultCount: KnockoutObservable<number>;
	public HasResults: KnockoutComputed<boolean>;
	public CurrentPageNumber: KnockoutComputed<number>;
	public CurrentPageSize: KnockoutComputed<number>;
	public LastPageNumber: KnockoutComputed<number>;

	private  _query: KnockoutObservable<Query>;
	
	private _searchCallback: (query: Query) => void;

	constructor(data: { Query: KnockoutObservable<Query>, TotalResultCount: KnockoutObservable<number>, Search:(query:Query)=>void})
	{
		super();
		this._query = data.Query;
		this.TotalResultCount = data.TotalResultCount;
		this._searchCallback = data.Search;

		this.HasResults = this.PureComputed(() => this.TotalResultCount() !== 0);
		this.CurrentPageNumber = this.Computed(() => this._query().PageNumber == null ? 1 : this._query().PageNumber);
		this.CurrentPageSize = this.Computed(() => this._query().PageSize);
		this.LastPageNumber = this.Computed(() => Math.ceil(this.TotalResultCount() / (this._query().PageSize != null ? this._query().PageSize : Configuration.DefaultPageSize)));

		this.FirstPage = this.PureComputed(() => this.CreatePage(1, this.CurrentPageSize()));
		this.LastPage = this.PureComputed(() => this.CreatePage(this.LastPageNumber(), this.CurrentPageSize()));
		this.PageSizes = this.PureComputed(() => Configuration.PageSizes.map(s => this.CreatePage(this.CurrentPageNumber(), s)));

		this.TotalResultCount.subscribe(() => this.CreatePages());
		this._query.subscribe(() => this.CreatePages());

		this.CreatePages();
	}

	private CreatePages():void
	{
		this.Pages.removeAll();

		var half = Configuration.MaxNumberOfPages / 2;
		var current = this.CurrentPageNumber();

		var start = Math.max(1, Math.ceil(current - half));
		var end = Math.min(this.LastPageNumber(), start + Configuration.MaxNumberOfPages - 1);

		if (start !== 1 && end === this.LastPageNumber())
			start = Math.max(1, end - Configuration.MaxNumberOfPages + 1);

		for (let i = start; i <= end; i++)
			this.Pages.push(this.CreatePage(i, this.CurrentPageSize()));
	}

	private CreatePage(pageNumber:number, pageSize:number):Page
	{
		var defaultedPageSize = pageSize === Configuration.DefaultPageSize ? null : pageSize;

		var createQuery = () =>
		{
			var query = this._query().Clone();
			query.PageNumber = pageNumber;
			query.PageSize = defaultedPageSize;
			return query;
		}

		return {
			Number: pageNumber,
			Size: pageSize,
			Link: knockout.pureComputed(() => createQuery().ToString()),
			NavigateTo: () => this._searchCallback(createQuery()),
			IsActive: knockout.pureComputed(() => this.CurrentPageNumber() === pageNumber && defaultedPageSize === this.CurrentPageSize())
	};
	}
}

export = Pagination;