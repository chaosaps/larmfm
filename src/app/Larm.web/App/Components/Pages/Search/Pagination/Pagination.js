var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Configuration", "Components/DisposableComponent"], function (require, exports, knockout, Configuration, DisposableComponent) {
    "use strict";
    var Pagination = (function (_super) {
        __extends(Pagination, _super);
        function Pagination(data) {
            var _this = _super.call(this) || this;
            _this.Pages = knockout.observableArray();
            _this._query = data.Query;
            _this.TotalResultCount = data.TotalResultCount;
            _this._searchCallback = data.Search;
            _this.HasResults = _this.PureComputed(function () { return _this.TotalResultCount() !== 0; });
            _this.CurrentPageNumber = _this.Computed(function () { return _this._query().PageNumber == null ? 1 : _this._query().PageNumber; });
            _this.CurrentPageSize = _this.Computed(function () { return _this._query().PageSize; });
            _this.LastPageNumber = _this.Computed(function () { return Math.ceil(_this.TotalResultCount() / (_this._query().PageSize != null ? _this._query().PageSize : Configuration.DefaultPageSize)); });
            _this.FirstPage = _this.PureComputed(function () { return _this.CreatePage(1, _this.CurrentPageSize()); });
            _this.LastPage = _this.PureComputed(function () { return _this.CreatePage(_this.LastPageNumber(), _this.CurrentPageSize()); });
            _this.PageSizes = _this.PureComputed(function () { return Configuration.PageSizes.map(function (s) { return _this.CreatePage(_this.CurrentPageNumber(), s); }); });
            _this.TotalResultCount.subscribe(function () { return _this.CreatePages(); });
            _this._query.subscribe(function () { return _this.CreatePages(); });
            _this.CreatePages();
            return _this;
        }
        Pagination.prototype.CreatePages = function () {
            this.Pages.removeAll();
            var half = Configuration.MaxNumberOfPages / 2;
            var current = this.CurrentPageNumber();
            var start = Math.max(1, Math.ceil(current - half));
            var end = Math.min(this.LastPageNumber(), start + Configuration.MaxNumberOfPages - 1);
            if (start !== 1 && end === this.LastPageNumber())
                start = Math.max(1, end - Configuration.MaxNumberOfPages + 1);
            for (var i = start; i <= end; i++)
                this.Pages.push(this.CreatePage(i, this.CurrentPageSize()));
        };
        Pagination.prototype.CreatePage = function (pageNumber, pageSize) {
            var _this = this;
            var defaultedPageSize = pageSize === Configuration.DefaultPageSize ? null : pageSize;
            var createQuery = function () {
                var query = _this._query().Clone();
                query.PageNumber = pageNumber;
                query.PageSize = defaultedPageSize;
                return query;
            };
            return {
                Number: pageNumber,
                Size: pageSize,
                Link: knockout.pureComputed(function () { return createQuery().ToString(); }),
                NavigateTo: function () { return _this._searchCallback(createQuery()); },
                IsActive: knockout.pureComputed(function () { return _this.CurrentPageNumber() === pageNumber && defaultedPageSize === _this.CurrentPageSize(); })
            };
        };
        return Pagination;
    }(DisposableComponent));
    return Pagination;
});
