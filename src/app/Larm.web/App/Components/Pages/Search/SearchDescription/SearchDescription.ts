﻿import knockout = require("knockout");
import Query = require("Managers/Search/Query");
import MenuFacets = require("Managers/Search/MenuFacets");
import ProjectManager = require("Managers/ProjectManager");
import DisposableComponent = require("Components/DisposableComponent");
import DateParser = require("Utility/DateParser");
import DataFormat = require("Utility/DataFormat");

class SearchDescription extends DisposableComponent
{
	public HasQuery:KnockoutComputed<boolean>;
	public HasFacet:KnockoutComputed<boolean>;
	public HasLabel:KnockoutComputed<boolean>;
	public HasTime:KnockoutComputed<boolean>;

	public QuerylessLink: KnockoutComputed<string>;
	public FacetlessLink: KnockoutComputed<string>;
	public LabellessLink: KnockoutComputed<string>;
	public TimelessLink: KnockoutComputed<string>;

	public Query: KnockoutComputed<string>;
	public FacetName: KnockoutComputed<string>;
	public LabelName: KnockoutComputed<string>;
	public From:KnockoutComputed<string>;
	public To:KnockoutComputed<string>;

	public TotalResultCount: KnockoutComputed<string>;

	private _query: KnockoutObservable<Query>;
	private _searchPath:string = "/Search";

	constructor(data: { Query: KnockoutObservable<Query>, TotalResultCount: KnockoutObservable<number>, Search: (query: Query) => void })
	{
		super();

		this._query = data.Query;
		this.TotalResultCount = this.PureComputed(() => DataFormat.PrettyNumber(data.TotalResultCount()));

		this.HasQuery = this.PureComputed(() => { return this._query().Query !== ""; });
		this.HasFacet = this.PureComputed(() => { return this._query().Facets != null; });
		this.HasLabel = this.PureComputed(() => { return this._query().Label != null; });
		this.HasTime = this.PureComputed(() => { return this._query().From != null && this._query().To != null; });

		this.QuerylessLink = this.CreateLessLink(q => q.Query = "");
		this.FacetlessLink = this.CreateLessLink(q => q.Facets = null);
		this.LabellessLink = this.CreateLessLink(q => q.Label = null);
		this.TimelessLink = this.CreateLessLink(q =>
		{
			q.From = null;
			q.To = null;
		});

		this.Query = this.PureComputed(() => { return this._query().Query; });
		this.FacetName = this.PureComputed(() =>
		{
			var facet = MenuFacets.SearchFacet();
			return facet == null ? null : MenuFacets.GetFacetValueName(facet.Facet.Value, facet.Value.Key);
		});
		this.LabelName = this.PureComputed(() =>
		{
			var label = ProjectManager.SearchLabel();
			return label == null ? null : label.Name;
		});
		this.From = this.PureComputed(() => this.HasTime() ? DateParser.ToFormattedStringByAssetType(this._query().From) : null);
		this.To = this.PureComputed(() => this.HasTime() ? DateParser.ToFormattedStringByAssetType(this._query().To) : null);
	}

	private CreateLessLink(updater:(query:Query)=>void):KnockoutComputed<string>
	{
		return this.PureComputed(() =>
		{
			var query = this._query().Clone();
			updater(query);
			return this._searchPath + query.ToString()
		});
	}
}

export = SearchDescription;