var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Managers/Search/MenuFacets", "Managers/ProjectManager", "Components/DisposableComponent", "Utility/DateParser", "Utility/DataFormat"], function (require, exports, MenuFacets, ProjectManager, DisposableComponent, DateParser, DataFormat) {
    "use strict";
    var SearchDescription = (function (_super) {
        __extends(SearchDescription, _super);
        function SearchDescription(data) {
            var _this = _super.call(this) || this;
            _this._searchPath = "/Search";
            _this._query = data.Query;
            _this.TotalResultCount = _this.PureComputed(function () { return DataFormat.PrettyNumber(data.TotalResultCount()); });
            _this.HasQuery = _this.PureComputed(function () { return _this._query().Query !== ""; });
            _this.HasFacet = _this.PureComputed(function () { return _this._query().Facets != null; });
            _this.HasLabel = _this.PureComputed(function () { return _this._query().Label != null; });
            _this.HasTime = _this.PureComputed(function () { return _this._query().From != null && _this._query().To != null; });
            _this.QuerylessLink = _this.CreateLessLink(function (q) { return q.Query = ""; });
            _this.FacetlessLink = _this.CreateLessLink(function (q) { return q.Facets = null; });
            _this.LabellessLink = _this.CreateLessLink(function (q) { return q.Label = null; });
            _this.TimelessLink = _this.CreateLessLink(function (q) {
                q.From = null;
                q.To = null;
            });
            _this.Query = _this.PureComputed(function () { return _this._query().Query; });
            _this.FacetName = _this.PureComputed(function () {
                var facet = MenuFacets.SearchFacet();
                return facet == null ? null : MenuFacets.GetFacetValueName(facet.Facet.Value, facet.Value.Key);
            });
            _this.LabelName = _this.PureComputed(function () {
                var label = ProjectManager.SearchLabel();
                return label == null ? null : label.Name;
            });
            _this.From = _this.PureComputed(function () { return _this.HasTime() ? DateParser.ToFormattedStringByAssetType(_this._query().From) : null; });
            _this.To = _this.PureComputed(function () { return _this.HasTime() ? DateParser.ToFormattedStringByAssetType(_this._query().To) : null; });
            return _this;
        }
        SearchDescription.prototype.CreateLessLink = function (updater) {
            var _this = this;
            return this.PureComputed(function () {
                var query = _this._query().Clone();
                updater(query);
                return _this._searchPath + query.ToString();
            });
        };
        return SearchDescription;
    }(DisposableComponent));
    return SearchDescription;
});
