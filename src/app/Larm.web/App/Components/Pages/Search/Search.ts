﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Notification = require("Managers/Notification");
import SearchManager = require("Managers/Search/Search");
import Configuration = require("Managers/Configuration");
import Query = require("Managers/Search/Query");
import Dragging = require("Managers/Dragging");
import Permissions = require("Managers/Permissions");
import DisposableComponent = require("Components/DisposableComponent");
import DateParser = require("Utility/DateParser");
import Interface = require("Managers/Interface");
import Authorization = require("Managers/Authorization");
import { SaveAsCsv } from "Utility/FileSaver";

type SearchResultField = {Value:string, ImagePath:string}
type SearchResult = { Identifier: string, TypeId: string, Fields: SearchResultField[], Remove: (result: SearchResult) => void, StartDrag: (result:SearchResult, event:JQueryEventObject)=>boolean };
type SearchField = { Title: string, Sort: () => void, CanSort:boolean, IsSorting: KnockoutComputed<boolean>, IsDescending: KnockoutComputed<boolean> };
type QueryParameters = {[key: string]: any}

class Search extends DisposableComponent
{
	public SearchFields = knockout.observableArray<SearchField>();
	public SearchResults = knockout.observableArray<SearchResult>();
	public TotalResultCount = knockout.observable(0);
	public Query: KnockoutObservable<Query>;
	public Search: (query: Query) => void;
	public CanDisassociateFromLabel:KnockoutComputed<boolean>;
	public CanAssociateWithLabel: KnockoutComputed<boolean>;
    public CanExport: KnockoutComputed<boolean>;

	public DragElement = knockout.observable<HTMLElement>();
	public DraggingResult = knockout.observable<SearchResult>();
	public IsDraggingTopElement = knockout.observable(false);

	private _valueParsers: { [key: string]: (value: string, format: string) => string };

	private _searchCount = 0;

	constructor(data: { Query: KnockoutObservable<QueryParameters>})
	{
		super();

		this._valueParsers = this.CreateParsers();

		SearchManager.Parse(data.Query());
		this.Query = SearchManager.Query;

		this.Search = q => SearchManager.Search(q);
		this.CanDisassociateFromLabel = this.PureComputed(() => this.Query().Label != null);
        this.CanAssociateWithLabel = Permissions.CanAssociateWithLabel;
		this.CanExport = this.PureComputed(() => Authorization.IsAuthenticated() && this.SearchResults().length !== 0);
		this.AddAction(() => SearchManager.IsReady(), () => this.Initialize());
		Interface.Show();

		this.Subscribe(data.Query,
			query =>
			{
				SearchManager.Parse(query);
				this.InnerSearch();
			});
	}

    public Export(): void {
        if (!this.CanExport)
			return;

        const link = `${window.location.protocol}//${window.location.host}/Asset/`;

		const data = this.SearchResults().map(s => {
			const row = s.Fields.map(f => f.Value);
			row.push(link + s.Identifier);
			return row;
		});

        SaveAsCsv(data, "SøgeResultat.csv");
    }

	private Initialize():void
	{
		this.SearchFields.push(...this.CreateSearchFields(SearchManager.SearchFields));

		this.InnerSearch();
	}

	private CreateParsers(): { [index: string]: (value: string, assetTypeId:string) => string; }
	{
		return {
			"Datetime": (v, t) => DateParser.ToFormattedStringByAssetType(DateParser.ParseLocalUTCString(v), t),
			"Date": (v, t) => DateParser.ToFormattedDateString(DateParser.ParseLocalUTCString(v)),
			"String": (v, t) => DateParser.ToFormattedDateRange(v, t)
		}
	}

	private InnerSearch():void
	{
		const query = SearchManager.Query();
		const queryString = query.SafeQuery;
		let filterString = "";
		let betweenString: string = null;
		const sortString = query.SortField == null ? null : query.SortField + " " + (query.IsSortDirectionDescending ? "desc" : "asc");
		const facetString = query.Facets == null ? null : query.Facets.map(f => `${f.Key}:${f.Value}`).join(",");
		const pageSize = query.PageSize == null ? Configuration.DefaultPageSize : query.PageSize;
		const pageIndex = query.PageNumber == null ? 0 : query.PageNumber - 1;

		if (query.From != null && query.To != null)
			betweenString = JSON.stringify({ From: DateParser.ToLocalUTCString(query.From), To: DateParser.ToLocalUTCString(query.To) });

		if (query.Label != null)
			filterString += Configuration.LabelFilter + ":EXACT " + query.Label;

		if (filterString === "")
			filterString = null;

		this.CallSearch(queryString, filterString, betweenString, facetString, sortString, pageIndex, pageSize, ++this._searchCount);
	}

	private CallSearch(queryString:string, filterString:string, betweenString: string, facetString:string, sortString:string, pageIndex:number, pageSize:number, searchCount:number):void
	{
		EZArchivePortal.Search.Get(queryString, filterString, betweenString, null, facetString, sortString, pageIndex, pageSize).WithCallback(response =>
		{
			if (searchCount !== this._searchCount)
				return; //Another search has been started since this

			if (response.Error !== null)
			{
				Notification.Error("Failed to search: " + response.Error.Message);
				return;
			}
			this.TotalResultCount(response.Body.TotalCount);
			this.SearchResults.removeAll();
			this.SearchResults.push(...response.Body.Results.map(r => this.CreateSearchResult(r)));
		});
	}

	private CreateSearchFields(fields: EZArchivePortal.IEZSearchDefinitionField[]): SearchField[]
	{
		return fields
			.filter(f => !Configuration.SearchResultReplaceMap.hasOwnProperty(f.DisplayName))
			.map(f =>
			{
				const isSorting = knockout.computed(() => SearchManager.Query().SortField === f.DisplayName);
				const isDescending = knockout.computed(() => SearchManager.Query().IsSortDirectionDescending);

				return {
					Title: f.DisplayName,
					Sort: () =>
					{
						if (!f.IsSortable) return;
						const query = SearchManager.Clone();
						if (query.SortField === f.DisplayName)
							query.IsSortDirectionDescending = !query.IsSortDirectionDescending;
						else
						{
							query.SortField = f.DisplayName;
							query.IsSortDirectionDescending = false;
						}
						SearchManager.Search(query);
					},
					CanSort: f.IsSortable,
					IsSorting: isSorting,
					IsDescending: isDescending
				}
			});
	}

	private CreateSearchResult(result: EZArchivePortal.ISearchResult): SearchResult
	{
        var mapped: { [key: string]: string } = {};

		result.Fields.forEach(f => (mapped[f.Key] = f.Value));
		const fields = SearchManager.SearchFields.map(f =>
			this.GetSearchFieldValue(result.TypeId, f.DisplayName, f.Type,
				mapped.hasOwnProperty(f.DisplayName)
					? mapped[f.DisplayName]
					: null));

		this.ApplyReplaceMap(fields);
		this.ApplyReplaceValueMap(fields);

		return {
			Identifier: result.Identifier,
			TypeId: result.TypeId,
			Fields: fields,
			Remove: (r:SearchResult) => this.Remove(r),
			StartDrag: (r: SearchResult, e: JQueryEventObject) => this.StartDrag(r, <DragEvent>e.originalEvent)
		};
	}

	private ApplyReplaceMap(fields: SearchResultField[]): void
	{
		const replaced: number[] = [];
		for (let replaceKey in Configuration.SearchResultReplaceMap) {
			const key = Configuration.SearchResultReplaceMap[replaceKey];
			let replaceIndex = -1;
			let replaceValueIndex = -1;

			fields.forEach((f, index) => {
				switch (SearchManager.SearchFields[index].DisplayName) {
				case key:
					replaceIndex = index;
					return;
				case replaceKey:
					replaceValueIndex = index;
					return;
				}
			});

			if (replaceIndex !== -1 && replaceValueIndex !== -1) {
				const value = fields[replaceValueIndex].Value;
				if (value !== "")
					fields[replaceIndex] = fields[replaceValueIndex];

				replaced.unshift(replaceValueIndex);
			}
		}

		replaced.forEach(index => fields.splice(index, 1));
	}

	private ApplyReplaceValueMap(fields: SearchResultField[]): void
	{
		for (let i = 0; i < fields.length; i++) {
			if (!Configuration.SearchResultReplaceValueMap.hasOwnProperty(SearchManager.SearchFields[i].DisplayName))
				continue;

			const replacementMap = Configuration.SearchResultReplaceValueMap[SearchManager.SearchFields[i].DisplayName];

			for (let replaceValueKey in replacementMap) {
				if (typeof fields[i].Value === "string" && fields[i].Value.substring(0, replaceValueKey.length) !== replaceValueKey)
					continue;

				fields[i].Value = replacementMap[replaceValueKey];
				break;
			}
		}
	}

	private Remove(result: SearchResult): void
	{
		this.SearchResults.remove(result);

		EZArchivePortal.EZLabel.DisassociateWith(this.Query().Label, result.Identifier).WithCallback(response =>
		{
			if (response.Error != null)
				Notification.Error("Failed to disassociate asset from label: " + response.Error.Message);

			this.InnerSearch();
		});
	}

	private StartDrag(result: SearchResult, event: DragEvent):boolean
	{
		if (!this.CanAssociateWithLabel()) return false;


		Dragging.StartSearchResultDrag(event, result.Identifier);

		if (event.dataTransfer.setDragImage !== undefined)
		{
			this.IsDraggingTopElement(event.clientY < 200);
			this.DraggingResult(result);
			event.dataTransfer.setDragImage(this.DragElement(), -7, -7);
			setInterval(() => this.DraggingResult(null)); //Remove dragelement from sceen after it's been used for dragImage
		}

		return true;
	}

	private GetSearchFieldValue(assetType: string, fieldName: string, fieldType: string, value: string): SearchResultField
	{
		if (fieldName === "Udsendelsesdato_ddmmyyyy")
			fieldType = DateParser.DateTypeName;

		let parsedValue = "";
		let imagePath: string = null;

		if (value != null) {
		    const isImage = Configuration.SearchImageMaps.indexOf(fieldName) !== -1;

		    parsedValue = !isImage && this._valueParsers.hasOwnProperty(fieldType) ? this._valueParsers[fieldType](value, assetType) : value;

		    if (isImage)
				imagePath = `${Configuration.SearchImagePath}${fieldName}/${parsedValue.replace(/ /g, "_") }.png`;
		}
		else if (Configuration.SearchResultDefaultValue.hasOwnProperty(assetType) && Configuration.SearchResultDefaultValue[assetType].hasOwnProperty(fieldName))
			parsedValue = Configuration.SearchResultDefaultValue[assetType][fieldName];

		return { Value: parsedValue, ImagePath: imagePath};
	}
}

export = Search;
