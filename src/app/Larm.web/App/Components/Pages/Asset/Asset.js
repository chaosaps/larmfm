var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Portal/EZArchivePortal", "Managers/Portal/Portal", "Managers/Portal/DataDefinition", "Managers/Notification", "Components/DisposableComponent", "Utility/DataExtractor", "Managers/Configuration", "Components/Pages/Asset/Annotations/AnnotationHandler", "Components/Pages/Asset/Players/State", "Managers/Permissions", "Managers/Tracking", "Managers/Copyright", "Managers/Navigation/Navigation", "Managers/Title", "Managers/Dragging", "Managers/Interface", "Managers/Dialog/Dialog"], function (require, exports, knockout, EZArchivePortal, LarmPortal, DataDefinition, Notification, DisposableComponent, DataExtractor, Configuration, AnnotationHandler, PlayerState, Permissions, Tracking, Copyright, Navigation, Title, Dragging, Interface, DialogManager) {
    "use strict";
    var Asset = (function (_super) {
        __extends(Asset, _super);
        function Asset(data) {
            var _this = _super.call(this) || this;
            _this.TypeId = knockout.observable(null);
            _this.IsLoading = knockout.observable(true);
            _this.Title = knockout.observable("");
            _this.Data = knockout.observableArray();
            _this.VisibleData = knockout.observableArray();
            _this.DragElement = knockout.observable();
            _this.IsDragging = knockout.observable(false);
            Interface.Hide();
            _this.Id = data.Id;
            _this.Title(_this.Id);
            _this._dataExtractor = new DataExtractor(_this.Id, _this.TypeId, _this.Data);
            _this.DataUpdatedCallback = function () {
                _this._dataExtractor.Update();
                _this.Title(_this._dataExtractor.Title);
                Title.Set(_this.Title());
            };
            _this.PlayerState = new PlayerState();
            _this._annotationHandler = new AnnotationHandler(_this.Id, _this.PlayerState);
            _this.AnnotationGroups = _this._annotationHandler.AnnotationGroups;
            _this.SelectedAnnotation = _this._annotationHandler.SelectedAnnotation;
            _this.GetActiveGroup = function () { return _this._annotationHandler.GetActiveGroup(); };
            _this.IsAuthenticated = _this.PureComputed(function () { return LarmPortal.IsAuthenticated(); });
            _this.MediaRequiresLogin = _this.PureComputed(function () { return !LarmPortal.IsAuthenticated(); });
            _this.MediaIsCopyrightBlocked = _this.PureComputed(function () { return !Copyright.IsCleared(_this.Data()); });
            _this.CanAssociateWithLabel = _this.PureComputed(function () { return LarmPortal.IsAuthenticated(); });
            _this.Link = window.location.protocol + "//" + window.location.host + "/Asset/" + _this.Id;
            _this.AddTracking();
            _this.LoadAsset(_this.Id);
            _this.Subscribe(LarmPortal.IsAuthenticated, function () { if (!_this.IsLoading())
                _this.LoadFiles(); });
            _this.Subscribe(_this.SelectedAnnotation, function (a) { return Navigation.Navigate("Asset/" + _this.Id + (a == null || a.Identifier == null ? "" : "/" + a.Identifier)); });
            _this.Subscribe(data.AnnotationId, function (id) { return _this._annotationHandler.SelectAnnotation(id); });
            if (data.AnnotationId() != null) {
                _this._annotationHandler.SelectAnnotation(data.AnnotationId());
                if (_this._annotationHandler.SelectedAnnotation() == null) {
                    _this.SubscribeUntilChange(_this._annotationHandler.SelectedAnnotation, function (annotation) {
                        if (annotation.IsTimed)
                            _this.PlayerState.Position(annotation.Start().valueOf());
                    });
                }
            }
            return _this;
        }
        Asset.prototype.ShowAddToFolder = function () {
            DialogManager.Show("Pages/Asset/AddAssetToFolderDialog", { Id: this.Id, Title: this.Title() });
        };
        Asset.prototype.AddTracking = function () {
            var _this = this;
            this.Subscribe(this.PlayerState.IsPlaying, function (v) {
                if (v)
                    Tracking.TrackEvent("Player", _this.PlayerState.Type(), "Playing");
            });
            this.SubscribeUntilChange(this.TypeId, function (type) { return Tracking.TrackEvent("Asset", "Open", Configuration.TrackingType[type]); });
        };
        Asset.prototype.StartDrag = function (viewModel, jqueryEvent) {
            var _this = this;
            if (!this.CanAssociateWithLabel())
                return false;
            var event = jqueryEvent.originalEvent;
            Dragging.StartSearchResultDrag(event, this.Id);
            if (event.dataTransfer.setDragImage !== undefined) {
                this.IsDragging(true);
                event.dataTransfer.setDragImage(this.DragElement(), -7, -7);
                setInterval(function () { return _this.IsDragging(false); });
            }
            return true;
        };
        Asset.prototype.LoadAsset = function (id) {
            var _this = this;
            this.GetAsset(id, function (asset) {
                var _a, _b;
                _this.TypeId(asset.TypeId);
                if (asset.Data.length > 0) {
                    var ordered = new Array();
                    Configuration.SchemaOrder.forEach(function (name) {
                        for (var i = 0; i < asset.Data.length; i++) {
                            if (asset.Data[i].Name === name) {
                                ordered.push(asset.Data[i]);
                                asset.Data.splice(i, 1);
                                return;
                            }
                        }
                    });
                    (_a = _this.Data).push.apply(_a, ordered.concat(asset.Data));
                    (_b = _this.VisibleData).push.apply(_b, _this.Data()
                        .filter(function (d) { return Configuration.IsSchemaVisible(d.Name, Permissions.CanSeeOffsetMetadata()); })
                        .sort(function (a, b) { return a.Name.localeCompare(b.Name); }));
                    _this.AddMissingDatas();
                    _this.DataUpdatedCallback();
                    if (!Permissions.CanSeeOffsetMetadata())
                        _this.SubscribeUntilChange(Permissions.CanSeeOffsetMetadata, function (canSee) {
                            var _a;
                            (_a = _this.VisibleData).push.apply(_a, _this.Data().filter(function (d) { return d.Name === Configuration.OffsetSchema; }));
                            _this.AddMissingDatas();
                            _this.VisibleData.sort(function (a, b) { return a.Name.localeCompare(b.Name); });
                        });
                }
                _this._annotationHandler.Load(asset.TypeId, asset.Annotations, _this._dataExtractor.BroadcastWalltimeStart);
                _this.PlayerState.Update(asset.Files, _this.Data(), Permissions.CanStreamContent());
                _this.IsLoading(false);
            });
        };
        Asset.prototype.LoadFiles = function () {
            var _this = this;
            this.GetAsset(this.Id, function (asset) {
                try {
                    _this.PlayerState.Update(asset.Files, _this.Data(), Permissions.CanStreamContent());
                }
                catch (e) {
                    Notification.Error("Failed to update files: " + e.message);
                }
            });
        };
        Asset.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this._annotationHandler.dispose();
            this.PlayerState.dispose();
        };
        Asset.prototype.GetAsset = function (id, callback) {
            this.AddAction(LarmPortal.IsReady, function () {
                EZArchivePortal.Asset.Get(id).WithCallback(function (response) {
                    if (response.Error !== null) {
                        Notification.Error("Failed to get asset: " + response.Error.Message);
                        return;
                    }
                    if (response.Body.Results.length === 0) {
                        Notification.Error("No asset data returned");
                        return;
                    }
                    callback(response.Body.Results[0]);
                });
            });
        };
        Asset.prototype.AddMissingDatas = function () {
            var _this = this;
            this.AddAction(DataDefinition.IsReady, function () {
                var _a;
                var datas = _this.Data();
                var missing = DataDefinition.GetByAssetType(_this.TypeId()).filter(function (definition) { return Configuration.IsSchemaVisible(definition.Name, Permissions.CanSeeOffsetMetadata()) && !datas.some(function (data) { return data.Name === definition.Name; }); }).map(function (d) { return ({ Name: d.Name, Fields: {} }); });
                if (missing.length > 0)
                    (_a = _this.VisibleData).push.apply(_a, missing);
            });
        };
        return Asset;
    }(DisposableComponent));
    return Asset;
});
