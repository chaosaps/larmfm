﻿import knockout = require("knockout");
import Notification = require("Managers/Notification");
import DialogState = require("Managers/Dialog/DialogState");
import DisposableComponent = require("Components/DisposableComponent");
import PlayerState = require("Components/Pages/Asset/Players/State");

type State = { State: PlayerState };

class EditDialog extends DisposableComponent
{
	public Element = knockout.observable<HTMLAudioElement | null>(null);
	private _state: DialogState<State>;

	public get State(): PlayerState
	{
		return this._state.Data.State;
	}

	constructor(state: DialogState<State>)
	{
		super();
		this._state = state;
		this._state.Data.State.IsPlaying(false);
	}
}

export = EditDialog