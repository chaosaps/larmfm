var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Hls", "knockout", "Managers/Notification", "Components/DisposableComponent", "Utility/StreamTransformer"], function (require, exports, Hls, knockout, Notification, DisposableComponent, StreamTransformer_1) {
    "use strict";
    var EditDialog = (function (_super) {
        __extends(EditDialog, _super);
        function EditDialog(state) {
            var _this = _super.call(this) || this;
            _this.Element = knockout.observable(null);
            _this._hls = null;
            _this._state = state;
            _this._state.Data.State.IsPlaying(false);
            _this.SubscribeUntilChange(_this.Element, function (element) { return _this.InitializeAudio(element, _this._state.Data.State.FirstFile().Destinations[0].Url); });
            return _this;
        }
        EditDialog.prototype.InitializeAudio = function (element, url) {
            url = StreamTransformer_1.default.UpdateAndGetHls(url);
            if (Hls.isSupported()) {
                this._hls = new Hls();
                this._hls.loadSource(url);
                this._hls.attachMedia(element);
            }
            else if (element.canPlayType("application/vnd.apple.mpegURL; codecs=\"mp3\"") !== "") {
                var sourceElement = window.document.createElement("source");
                sourceElement.src = url;
                sourceElement.type = "application/vnd.apple.mpegURL; codecs=\"mp3\"";
                element.appendChild(sourceElement);
            }
            else {
                Notification.Debug("Audio codec not supported");
            }
        };
        return EditDialog;
    }(DisposableComponent));
    return EditDialog;
});
