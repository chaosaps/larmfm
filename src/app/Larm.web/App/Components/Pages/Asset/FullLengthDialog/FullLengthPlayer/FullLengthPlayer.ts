﻿import Hls = require("Hls");
import knockout = require("knockout");
import Notification = require("Managers/Notification");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import DataFormat = require("Utility/DataFormat");
import DisposableComponent = require("Components/DisposableComponent");
import StreamTransformer from "Utility/StreamTransformer"

type Offset = { Start: number, End: number, File: EZArchivePortal.IFile, FileDuration: KnockoutObservable<number> };

class EditDialog extends DisposableComponent
{
	public readonly Element = knockout.observable<HTMLAudioElement | null>(null);
	public readonly Offset: Offset;
	public readonly OffsetLength: KnockoutComputed<number | null>;
	private _hls: Hls | null = null;

	public get PrettyStart(): string | null
	{
		return this.Offset.Start !== null ? DataFormat.MillieSecondsToPrettyTime(this.Offset.Start) : null;
	}

	public get PrettyEnd(): string | null {
		return this.Offset.End !== null ? DataFormat.MillieSecondsToPrettyTime(this.Offset.End) : null;
	}

	public get PrettyOffsetLength(): string | null {
		return this.OffsetLength() !== null ? DataFormat.MillieSecondsToPrettyTime(this.OffsetLength()) : null;
	}

	public get PrettyDuration(): string | null {
		return this.Offset.FileDuration() !== null ? DataFormat.MillieSecondsToPrettyTime(this.Offset.FileDuration()) : null;
	}

	constructor(data: Offset)
	{
		super();
		this.Offset = data;
		this.OffsetLength = this.PureComputed(() =>
		{
			const start = this.Offset.Start !== null ? this.Offset.Start : 0;
			const end = this.Offset.End !== null
				? this.Offset.End
				: this.Offset.FileDuration() !== null
				? this.Offset.FileDuration()
				: null;

			return end !== null ? end - start : null;
		});

		this.SubscribeUntilChange(this.Element, element =>
		{
			if (this.Offset.File.Destinations.length === 0)
				Notification.Error("Missing destination for " + this.Offset.File.Identifier);
			else
				this.InitializeAudio(element, this.Offset.File.Destinations[0].Url);
		});
	}

	public GetEmailLink(email: string, subject: string, body: string): KnockoutComputed<string>
	{
		const replaceFileId = /\$fileId/g;
		const replaceOffsetStart = /\$offsetStart/g;
		const replaceOffsetEnd = /\$offsetEnd/g;
		const replaceAssetLink = /\$assetLink/g;
		const assetLink = window.location.toString().replace(/(Asset\/.+)\/.+$/, "$1");

		return this.PureComputed(() =>
		{
			let start = this.PrettyStart;
			let end = this.PrettyEnd;

			if (start === null)
				start = "00:00:00";

			if (end === null)
				end = this.PrettyDuration;

			subject = subject
				.replace(replaceFileId, this.Offset.File.Identifier)
				.replace(replaceOffsetStart, start)
				.replace(replaceOffsetEnd, end)
				.replace(replaceAssetLink, assetLink);
			body = body.replace(replaceFileId, this.Offset.File.Identifier)
				.replace(replaceOffsetStart, start)
				.replace(replaceOffsetEnd, end)
				.replace(replaceAssetLink, assetLink);

			return `mailto:${email}?subject=${encodeURIComponent(subject)}&body=${encodeURIComponent(body)}`;
		});
	}

	public GotoStart(): void
	{
		if (this.Offset.Start === null || this.Element() === null)
			return;

		this.Element().currentTime = this.Offset.Start / 1000;
	}

	public GotoEnd(): void {
		if (this.Offset.End === null || this.Element() === null)
			return;

		this.Element().currentTime = this.Offset.End / 1000;
	}

	public dispose(): void
	{
		super.dispose();

		if (this._hls !== null)
			this._hls.destroy();

		if (this.Element() !== null) {
			this.Element().pause();
			this.Element(null);
		}
	}

	private InitializeAudio(element: HTMLAudioElement, url: string): void
	{
		url = StreamTransformer.UpdateAndGetHls(url);

		if (Hls.isSupported()) {
			this._hls = new Hls();
			this._hls.loadSource(url);
			this._hls.attachMedia(element as any);
		} else if (element.canPlayType("application/vnd.apple.mpegURL; codecs=\"mp3\"") !== "") {
			const sourceElement = window.document.createElement("source");
			sourceElement.src = url;
			sourceElement.type = "application/vnd.apple.mpegURL; codecs=\"mp3\"";
			element.appendChild(sourceElement);
		} else {
			Notification.Debug("Audio codec not supported");
		}
	}
}

export = EditDialog