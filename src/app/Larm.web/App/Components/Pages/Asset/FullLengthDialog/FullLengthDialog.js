var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/DisposableComponent"], function (require, exports, knockout, DisposableComponent) {
    "use strict";
    var EditDialog = (function (_super) {
        __extends(EditDialog, _super);
        function EditDialog(state) {
            var _this = _super.call(this) || this;
            _this.Element = knockout.observable(null);
            _this._state = state;
            _this._state.Data.State.IsPlaying(false);
            return _this;
        }
        Object.defineProperty(EditDialog.prototype, "State", {
            get: function () {
                return this._state.Data.State;
            },
            enumerable: true,
            configurable: true
        });
        return EditDialog;
    }(DisposableComponent));
    return EditDialog;
});
