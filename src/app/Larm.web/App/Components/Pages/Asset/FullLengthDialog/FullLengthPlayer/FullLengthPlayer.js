var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Hls", "knockout", "Managers/Notification", "Utility/DataFormat", "Components/DisposableComponent", "Utility/StreamTransformer"], function (require, exports, Hls, knockout, Notification, DataFormat, DisposableComponent, StreamTransformer_1) {
    "use strict";
    var EditDialog = (function (_super) {
        __extends(EditDialog, _super);
        function EditDialog(data) {
            var _this = _super.call(this) || this;
            _this.Element = knockout.observable(null);
            _this._hls = null;
            _this.Offset = data;
            _this.OffsetLength = _this.PureComputed(function () {
                var start = _this.Offset.Start !== null ? _this.Offset.Start : 0;
                var end = _this.Offset.End !== null
                    ? _this.Offset.End
                    : _this.Offset.FileDuration() !== null
                        ? _this.Offset.FileDuration()
                        : null;
                return end !== null ? end - start : null;
            });
            _this.SubscribeUntilChange(_this.Element, function (element) {
                if (_this.Offset.File.Destinations.length === 0)
                    Notification.Error("Missing destination for " + _this.Offset.File.Identifier);
                else
                    _this.InitializeAudio(element, _this.Offset.File.Destinations[0].Url);
            });
            return _this;
        }
        Object.defineProperty(EditDialog.prototype, "PrettyStart", {
            get: function () {
                return this.Offset.Start !== null ? DataFormat.MillieSecondsToPrettyTime(this.Offset.Start) : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditDialog.prototype, "PrettyEnd", {
            get: function () {
                return this.Offset.End !== null ? DataFormat.MillieSecondsToPrettyTime(this.Offset.End) : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditDialog.prototype, "PrettyOffsetLength", {
            get: function () {
                return this.OffsetLength() !== null ? DataFormat.MillieSecondsToPrettyTime(this.OffsetLength()) : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditDialog.prototype, "PrettyDuration", {
            get: function () {
                return this.Offset.FileDuration() !== null ? DataFormat.MillieSecondsToPrettyTime(this.Offset.FileDuration()) : null;
            },
            enumerable: true,
            configurable: true
        });
        EditDialog.prototype.GetEmailLink = function (email, subject, body) {
            var _this = this;
            var replaceFileId = /\$fileId/g;
            var replaceOffsetStart = /\$offsetStart/g;
            var replaceOffsetEnd = /\$offsetEnd/g;
            var replaceAssetLink = /\$assetLink/g;
            var assetLink = window.location.toString().replace(/(Asset\/.+)\/.+$/, "$1");
            return this.PureComputed(function () {
                var start = _this.PrettyStart;
                var end = _this.PrettyEnd;
                if (start === null)
                    start = "00:00:00";
                if (end === null)
                    end = _this.PrettyDuration;
                subject = subject
                    .replace(replaceFileId, _this.Offset.File.Identifier)
                    .replace(replaceOffsetStart, start)
                    .replace(replaceOffsetEnd, end)
                    .replace(replaceAssetLink, assetLink);
                body = body.replace(replaceFileId, _this.Offset.File.Identifier)
                    .replace(replaceOffsetStart, start)
                    .replace(replaceOffsetEnd, end)
                    .replace(replaceAssetLink, assetLink);
                return "mailto:" + email + "?subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(body);
            });
        };
        EditDialog.prototype.GotoStart = function () {
            if (this.Offset.Start === null || this.Element() === null)
                return;
            this.Element().currentTime = this.Offset.Start / 1000;
        };
        EditDialog.prototype.GotoEnd = function () {
            if (this.Offset.End === null || this.Element() === null)
                return;
            this.Element().currentTime = this.Offset.End / 1000;
        };
        EditDialog.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            if (this._hls !== null)
                this._hls.destroy();
            if (this.Element() !== null) {
                this.Element().pause();
                this.Element(null);
            }
        };
        EditDialog.prototype.InitializeAudio = function (element, url) {
            url = StreamTransformer_1.default.UpdateAndGetHls(url);
            if (Hls.isSupported()) {
                this._hls = new Hls();
                this._hls.loadSource(url);
                this._hls.attachMedia(element);
            }
            else if (element.canPlayType("application/vnd.apple.mpegURL; codecs=\"mp3\"") !== "") {
                var sourceElement = window.document.createElement("source");
                sourceElement.src = url;
                sourceElement.type = "application/vnd.apple.mpegURL; codecs=\"mp3\"";
                element.appendChild(sourceElement);
            }
            else {
                Notification.Debug("Audio codec not supported");
            }
        };
        return EditDialog;
    }(DisposableComponent));
    return EditDialog;
});
