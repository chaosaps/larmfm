﻿import knockout = require("knockout");
import Configuration = require("Managers/Configuration");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import DisposableComponent = require("Components/DisposableComponent");

type Offset = {Start:number, End:number, File:EZArchivePortal.IFile, FileDuration:KnockoutObservable<number>};

class State extends DisposableComponent
{
	public IsPlaying: KnockoutObservable<boolean> = knockout.observable(false);
    public IsReady: KnockoutObservable<boolean> = knockout.observable(false);
	public IsSideBySide = knockout.observable(false);
	public AreFilesReady = knockout.observable(false);
	public Duration: KnockoutObservable<number> = knockout.observable(null);
	public Position: KnockoutObservable<number> = knockout.observable(null);
	public Volume = knockout.observable(100);
	public Files: KnockoutObservable<EZArchivePortal.IFile[]> = knockout.observable<EZArchivePortal.IFile[]>(null);
	public Offsets: Offset[];

	public FirstFile: KnockoutComputed<EZArchivePortal.IFile>;
	public HasPlayer: KnockoutComputed<boolean>;
	public HasFiles: KnockoutComputed<boolean>;
	public Type: KnockoutComputed<string>;
	public IsTimed: KnockoutComputed<boolean>;
	public IsInline:KnockoutComputed<boolean>;
	public IsBig:KnockoutComputed<boolean>;
	public UsesTimedTools:KnockoutComputed<boolean>;

	constructor()
	{
		super();
		this.FirstFile = this.PureComputed(() => this.Files() == null || this.Files().length === 0 ? null : this.Files()[0]);

		this.Type = this.PureComputed(() =>
		{
			if (this.FirstFile() == null) return null;

			switch (this.FirstFile().Type)
			{
				case "Audio":
					return "Audio";
				case "Video":
					return "Video";
				case "Image":
					return "Image";
				case "Other":
					return "PDF";
				default:
					return "Download";
			}
		});
		this.IsTimed = this.PureComputed(() => this.Type() === "Audio" || this.Type() === "Video");
		this.IsInline = this.PureComputed(() => this.Type() === "Audio" || this.Type() === "Video");
		this.IsBig = this.PureComputed(() => this.Type() !== "Audio");
		this.UsesTimedTools = this.PureComputed(() => this.Type() === "Audio" || this.Type() === "Video");
        this.HasPlayer = this.Computed(() => this.Type() != null);
        this.HasFiles = this.Computed(() => this.Files() != null);
	}

    public Update(files: EZArchivePortal.IFile[], data: EZArchivePortal.IData[], canStream:boolean): void
    {
		this.LoadFiles(files, data);
		this.AreFilesReady(canStream);	
    }

    private LoadFiles(files: EZArchivePortal.IFile[], data: EZArchivePortal.IData[]): void
    {
        var offsetData: string[][] = null;
        const offsetDatas = data.filter(d => d.Name === Configuration.OffsetSchema);

        if (offsetDatas.length > 0)
            offsetData = offsetDatas[0].Fields["Files"] as string[][];

        this.FixOffsets(offsetData);

        if (this.Files() == null)
		{
			this.Offsets = files.map(f =>
			{
				var offset: Offset = { Start: 0, End: null, File: f, FileDuration: knockout.observable(null) };

				if (offsetData != null)
				{
					for (let data of offsetData)
					{
						if (f.Destinations[0].Url.indexOf(data[0]) === -1) continue;

						offset.Start = parseInt(data[1]);
						offset.End = parseInt(data[2]);

						if (offset.Start < 0)
							offset.Start = 0;
						if (offset.End <= 0)
							offset.End = null;
					}
				}

				return offset;
			});
			this.Files(files);
		} else
		{
			if (this.Files().length !== files.length)
				throw new Error(`Number of files has changed from: ${this.Files().length} to ${files.length}`);

			for (let existingFile of this.Files())
			{
				existingFile.Destinations = null;
				for (let newFile of files)
				{
					if (newFile.Identifier !== existingFile.Identifier) continue;

					existingFile.Destinations = newFile.Destinations;
					break;
				}

				if (existingFile.Destinations == null)
					throw new Error("Unable to find updated destinations for file " + existingFile.Identifier);
			}
		}
    }

    private FixOffsets(offsetData: string[][]): void
    {
	    if (offsetData === null)
		    return;
	    const extraFiles: string[][] = [];

	    for (const offset of offsetData) {
			if (offset.length === 6)
				extraFiles.push(offset.splice(3, 3));
		}

		offsetData.push(...extraFiles);
    }
}

export = State;