var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Utility/Communicator", "Components/Pages/Asset/Players/BasePlayer", "Utility/DataFormat"], function (require, exports, knockout, Communicator, BasePlayer, DataFormat) {
    "use strict";
    var InlineVideo = (function (_super) {
        __extends(InlineVideo, _super);
        function InlineVideo(data) {
            var _this = _super.call(this, data) || this;
            _this._volumeBeforeMute = 100;
            _this.IsReady = _this.State.IsReady;
            _this.IsSideBySide = _this.State.IsSideBySide;
            _this.IsPlaying = _this.State.IsPlaying;
            _this.Volume = _this.State.Volume;
            _this.Position = knockout.pureComputed(function () { return DataFormat.MillieSecondsToPrettyTime(_this.State.Position()); });
            _this.Duration = knockout.pureComputed(function () { return DataFormat.MillieSecondsToPrettyTime(_this.State.Duration()); });
            _this.IsMuted = _this.Computed(function () { return _this.Volume() === 0; });
            _this._communicator = new Communicator("PlayerState", function (v) {
                if (v === "Playing" && _this.IsPlaying())
                    _this.IsPlaying(false);
            });
            return _this;
        }
        InlineVideo.prototype.ToggleMute = function () {
            if (this.IsMuted())
                this.Volume(this._volumeBeforeMute);
            else {
                this._volumeBeforeMute = this.Volume();
                this.Volume(0);
            }
        };
        InlineVideo.prototype.ToggleSideBySide = function () {
            this.State.IsSideBySide(!this.State.IsSideBySide());
        };
        InlineVideo.prototype.Toggle = function () {
            if (!this.IsPlaying() && this.State.Position() >= this.State.Duration())
                this.State.Position(0);
            this.IsPlaying(!this.IsPlaying());
        };
        InlineVideo.prototype.dispose = function () {
            this._communicator.Dispose();
        };
        InlineVideo.prototype.GetTwoDigits = function (value) {
            return value < 10 ? "0" + value : value.toString();
        };
        return InlineVideo;
    }(BasePlayer));
    return InlineVideo;
});
