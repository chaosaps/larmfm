var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/Pages/Asset/Players/BasePlayer"], function (require, exports, knockout, BasePlayer) {
    "use strict";
    var PDF = (function (_super) {
        __extends(PDF, _super);
        function PDF(data) {
            var _this = _super.call(this, data) || this;
            _this.IsBig = knockout.observable(false);
            _this.HasMultiplePages = false;
            _this.Pages = data.State.Files().map(function (f) {
                var page = { Name: "Ukendt", File: f, Select: null };
                if (f.Name != null) {
                    page.Name = f.Name.replace(/\.\w{1,4}$/, "").replace(/_|-/g, " ");
                }
                else if (f.Destinations.length > 0) {
                    var match = /.+\/(.+)\..+/.exec(f.Destinations[0].Url);
                    if (match.length > 0)
                        page.Name = match[1];
                }
                page.Select = function () { return _this.ActivePage(page); };
                return page;
            }).sort(function (a, b) { return a.Name.localeCompare(b.Name); });
            _this.ActivePage = knockout.observable(_this.Pages.length === 0 ? null : _this.Pages[0]);
            _this.Link = _this.PureComputed(function () { return _this.ActivePage() == null ? null : _this.ActivePage().File.Destinations[0].Url; });
            _this.HasMultiplePages = _this.Pages.length > 1;
            return _this;
        }
        PDF.prototype.ToggleBig = function () {
            this.IsBig(!this.IsBig());
        };
        return PDF;
    }(BasePlayer));
    return PDF;
});
