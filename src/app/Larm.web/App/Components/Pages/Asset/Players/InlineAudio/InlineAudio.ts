﻿import knockout = require("knockout");
import AudioHandler from "Utility/HlsAudio";
import Communicator = require("Utility/Communicator");
import DataFormat = require("Utility/DataFormat");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import PlayerState = require("Components/Pages/Asset/Players/State");
import BasePlayer = require("Components/Pages/Asset/Players/BasePlayer");

type AudioPart = { Start: number, End: number, Duration:number, PartStart:number, Audio: AudioHandler };
type Offset = { Start: number, End: number, File: EZArchivePortal.IFile, FileDuration: KnockoutObservable<number> };

class InlineAudio extends BasePlayer
{
	public IsReady: KnockoutComputed<boolean>;
	public IsMediaUnsupported: KnockoutComputed<boolean>;
	public HasErrorOccured: KnockoutComputed<boolean>;
	public IsPlaying: KnockoutComputed<boolean>;
	public IsMuted: KnockoutComputed<boolean>;
	public Position: KnockoutComputed<string>;
	public RelativePosition: KnockoutComputed<number>;
	public Duration: KnockoutComputed<string>;
	public Volume: KnockoutObservable<number>;

	private readonly  _parts: AudioPart[];
	private _activePart: AudioPart;
	private readonly  _communicator: Communicator;
	private _volumeBeforeMute: number = 100;

	constructor(data:{ State: PlayerState })
	{
		super(data);

		this._parts = this.State.Offsets.map(o => this.CreatePart(o));
		this._activePart = this._parts[0];
		this._activePart.PartStart = 0;

		this.IsReady = this.Computed(() => this._parts.every(p => p.Audio.IsReady()));
		this.IsPlaying = this.Computed(() => this._parts.some(p => p.Audio.IsPlaying()));
		this.IsMediaUnsupported = this.Computed(() => this._parts.some(p => p.Audio.IsMediaUnsupported()));
		this.HasErrorOccured = this.Computed(() => this._parts.some(p => p.Audio.HasErrorOccured()));

		this.Position = knockout.pureComputed(() => DataFormat.MillieSecondsToPrettyTime(this.State.Position()));
		this.Duration = knockout.pureComputed(() => DataFormat.MillieSecondsToPrettyTime(this.State.Duration()));

		this.UpdateDuration();
		this.InitializePosition();
		this.InitializeVolume();
		this.InitializeIsPlaying();
		this.IsReady.subscribe(v => this.State.IsReady(v));

		this.IsMuted = this.Computed(() => this.Volume() === 0);

		this._communicator = new Communicator("PlayerState", v =>
		{
			if (v === "Playing" && this.IsPlaying()) this._activePart.Audio.Pause();
		});
	}

	public ToggleMute():void
	{
		if (this.IsMuted())
			this.Volume(this._volumeBeforeMute);
		else
		{
			this._volumeBeforeMute = this.Volume();
			this.Volume(0);
		}
	}

	public Toggle(): void
	{
		if (!this.IsPlaying() && this.State.Position() >= this.State.Duration())
			this.State.Position(0);

		this._activePart.Audio.TogglePlay();
	}

	public dispose():void
	{
		this._parts.forEach(p => p.Audio.dispose());
		this._communicator.Dispose();
	}

	private CreatePart(offset: Offset):AudioPart
	{
		var audio = new AudioHandler(offset.File.Destinations[0].Url);
		var part = {
			Start: offset.Start,
			End: offset.End,
			Duration: offset.End == null ? null : (offset.End - offset.Start),
			PartStart: <number>null,
			Audio: audio
		};

		this.Subscribe(audio.Duration, v =>
		{
			part.End = offset.End == null ? v : Math.min(offset.End, v);
			part.Duration = part.End - part.Start;
			offset.FileDuration(v);
			this.UpdateDuration();
		});

		this.Subscribe(audio.IsFinished, v =>
		{
			if (v)
				this.JumpToNextPart();
		});

		this.SubscribeUntilChange(audio.IsReady, () => audio.Position(offset.Start));

		return part;
	}

	private UpdateDuration():void
	{
		for (let i = 0; i < this._parts.length - 1; i++)
		{
			let part = this._parts[i];

			if (part.Duration == null) break;

			this._parts[i + 1].PartStart = part.PartStart + part.Duration;
		}

		if (this._parts.some(p => p.Duration == null)) return;

		this.State.Duration(this._parts.reduce((d, p) => d + p.Duration, 0));
	}

	private InitializePosition():void
	{
		var isUpdatingPosition = false;

		this._parts.forEach(p => p.Audio.Position.subscribe(v =>
		{
			if (isUpdatingPosition || p !== this._activePart || p.Duration == null) return;
			isUpdatingPosition = true;
			var newPosition = v - p.Start;

			if (newPosition >= p.Duration - 70)
			{
				this.JumpToNextPart();
			}
			else if (newPosition < -1)
			{
				newPosition = 0;
				setTimeout(() => p.Audio.Position(p.Start), 0);
			}

			this.State.Position(newPosition + p.PartStart);
			isUpdatingPosition = false;
		}));

		this.State.Position.subscribe(v =>
		{
			if (isUpdatingPosition) return;
			isUpdatingPosition = true;
			var part = this.GetPart(v);

			if (part != null)
			{
				part.Audio.Position(v + part.Start - part.PartStart);

				if (part !== this._activePart)
				{
					if (this.IsPlaying())
						part.Audio.Play();

					this._activePart.Audio.Pause();
					this._activePart = part;
				}
			}

			isUpdatingPosition = false;
		});

		this.RelativePosition = this.PureComputed(() =>
			{
				const duration = this.State.Duration();
				const position = this.State.Position();

				if (duration === null || duration === 0 || position === null)
					return 0;

				return position / duration;
			}, value =>
			{
				value = parseFloat(value as any as string);
				const duration = this.State.Duration();

				this.State.Position(duration * value);
			});
	}

	private InitializeVolume(): any
	{
		this.Volume = this._parts[0].Audio.Volume;

		if (this._parts.length > 1)
		{
			this.Volume.subscribe(v =>
			{
				for (let i = 1; i < this._parts.length; i++)
					this._parts[i].Audio.Volume(v);
			});
		}
	}

	private JumpToNextPart():void
	{
		for (let i = 0; i < this._parts.length; i++)
		{
			if (this._parts[i] !== this._activePart) continue;

			if (i === this._parts.length - 1)
			{
				this._activePart.Audio.Pause();
				this._activePart = this._parts[0];
				this._activePart.Audio.Position(this._activePart.Start);
			} else
			{
				const newPart = this._parts[i + 1];

				newPart.Audio.Position(newPart.Start);
				newPart.Audio.Play();
				this._activePart.Audio.Pause();
				this._activePart = newPart;
			}
			break;
		}
	}

	private GetPart(position:number):AudioPart
	{
		let total = 0;

		for (let part of this._parts)
		{
			total += part.End - part.Start;

			if (total >= position) return part;
		}

		return null;
	}

	private InitializeIsPlaying():void
	{
		var isUpdatingPlaying = false;

		this.IsPlaying.subscribe(v =>
		{
			if (v) this._communicator.Send("Playing");

			if (isUpdatingPlaying) return;
			isUpdatingPlaying = true;
			this.State.IsPlaying(v);
			isUpdatingPlaying = false;
		});

		this.State.IsPlaying.subscribe(v =>
		{
			if (isUpdatingPlaying || v === this._activePart.Audio.IsPlaying()) return;
			isUpdatingPlaying = true;
			if (v)
				this._activePart.Audio.Play();
			else
				this._activePart.Audio.Pause();
			isUpdatingPlaying = false;
		});
	}
}

export = InlineAudio;