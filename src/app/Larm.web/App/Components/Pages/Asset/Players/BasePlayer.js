var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Components/DisposableComponent"], function (require, exports, DisposableComponent) {
    "use strict";
    var BasePlayer = (function (_super) {
        __extends(BasePlayer, _super);
        function BasePlayer(data) {
            var _this = _super.call(this) || this;
            _this._state = data.State;
            return _this;
        }
        Object.defineProperty(BasePlayer.prototype, "State", {
            get: function () {
                return this._state;
            },
            enumerable: true,
            configurable: true
        });
        return BasePlayer;
    }(DisposableComponent));
    return BasePlayer;
});
