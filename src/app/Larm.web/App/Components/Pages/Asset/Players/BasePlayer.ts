﻿import DisposableComponent = require("Components/DisposableComponent");
import PlayerState = require("Components/Pages/Asset/Players/State");

abstract class BasePlayer extends DisposableComponent
{
	private _state: PlayerState;

	constructor(data: { State: PlayerState})
	{
		super();
		this._state = data.State;
	}

	protected get State():PlayerState
	{
		return this._state;
	}
}

export = BasePlayer;