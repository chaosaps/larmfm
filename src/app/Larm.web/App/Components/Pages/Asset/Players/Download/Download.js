var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Components/Pages/Asset/Players/BasePlayer"], function (require, exports, BasePlayer) {
    "use strict";
    var Download = (function (_super) {
        __extends(Download, _super);
        function Download(data) {
            var _this = _super.call(this, data) || this;
            _this.Link = _this.PureComputed(function () { return _this.State.FirstFile() == null ? null : _this.State.FirstFile().Destinations[0].Url; });
            return _this;
        }
        return Download;
    }(BasePlayer));
    return Download;
});
