var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Utility/HlsAudio", "Utility/Communicator", "Utility/DataFormat", "Components/Pages/Asset/Players/BasePlayer"], function (require, exports, knockout, HlsAudio_1, Communicator, DataFormat, BasePlayer) {
    "use strict";
    var InlineAudio = (function (_super) {
        __extends(InlineAudio, _super);
        function InlineAudio(data) {
            var _this = _super.call(this, data) || this;
            _this._volumeBeforeMute = 100;
            _this._parts = _this.State.Offsets.map(function (o) { return _this.CreatePart(o); });
            _this._activePart = _this._parts[0];
            _this._activePart.PartStart = 0;
            _this.IsReady = _this.Computed(function () { return _this._parts.every(function (p) { return p.Audio.IsReady(); }); });
            _this.IsPlaying = _this.Computed(function () { return _this._parts.some(function (p) { return p.Audio.IsPlaying(); }); });
            _this.IsMediaUnsupported = _this.Computed(function () { return _this._parts.some(function (p) { return p.Audio.IsMediaUnsupported(); }); });
            _this.HasErrorOccured = _this.Computed(function () { return _this._parts.some(function (p) { return p.Audio.HasErrorOccured(); }); });
            _this.Position = knockout.pureComputed(function () { return DataFormat.MillieSecondsToPrettyTime(_this.State.Position()); });
            _this.Duration = knockout.pureComputed(function () { return DataFormat.MillieSecondsToPrettyTime(_this.State.Duration()); });
            _this.UpdateDuration();
            _this.InitializePosition();
            _this.InitializeVolume();
            _this.InitializeIsPlaying();
            _this.IsReady.subscribe(function (v) { return _this.State.IsReady(v); });
            _this.IsMuted = _this.Computed(function () { return _this.Volume() === 0; });
            _this._communicator = new Communicator("PlayerState", function (v) {
                if (v === "Playing" && _this.IsPlaying())
                    _this._activePart.Audio.Pause();
            });
            return _this;
        }
        InlineAudio.prototype.ToggleMute = function () {
            if (this.IsMuted())
                this.Volume(this._volumeBeforeMute);
            else {
                this._volumeBeforeMute = this.Volume();
                this.Volume(0);
            }
        };
        InlineAudio.prototype.Toggle = function () {
            if (!this.IsPlaying() && this.State.Position() >= this.State.Duration())
                this.State.Position(0);
            this._activePart.Audio.TogglePlay();
        };
        InlineAudio.prototype.dispose = function () {
            this._parts.forEach(function (p) { return p.Audio.dispose(); });
            this._communicator.Dispose();
        };
        InlineAudio.prototype.CreatePart = function (offset) {
            var _this = this;
            var audio = new HlsAudio_1.default(offset.File.Destinations[0].Url);
            var part = {
                Start: offset.Start,
                End: offset.End,
                Duration: offset.End == null ? null : (offset.End - offset.Start),
                PartStart: null,
                Audio: audio
            };
            this.Subscribe(audio.Duration, function (v) {
                part.End = offset.End == null ? v : Math.min(offset.End, v);
                part.Duration = part.End - part.Start;
                offset.FileDuration(v);
                _this.UpdateDuration();
            });
            this.Subscribe(audio.IsFinished, function (v) {
                if (v)
                    _this.JumpToNextPart();
            });
            this.SubscribeUntilChange(audio.IsReady, function () { return audio.Position(offset.Start); });
            return part;
        };
        InlineAudio.prototype.UpdateDuration = function () {
            for (var i = 0; i < this._parts.length - 1; i++) {
                var part = this._parts[i];
                if (part.Duration == null)
                    break;
                this._parts[i + 1].PartStart = part.PartStart + part.Duration;
            }
            if (this._parts.some(function (p) { return p.Duration == null; }))
                return;
            this.State.Duration(this._parts.reduce(function (d, p) { return d + p.Duration; }, 0));
        };
        InlineAudio.prototype.InitializePosition = function () {
            var _this = this;
            var isUpdatingPosition = false;
            this._parts.forEach(function (p) { return p.Audio.Position.subscribe(function (v) {
                if (isUpdatingPosition || p !== _this._activePart || p.Duration == null)
                    return;
                isUpdatingPosition = true;
                var newPosition = v - p.Start;
                if (newPosition >= p.Duration - 70) {
                    _this.JumpToNextPart();
                }
                else if (newPosition < -1) {
                    newPosition = 0;
                    setTimeout(function () { return p.Audio.Position(p.Start); }, 0);
                }
                _this.State.Position(newPosition + p.PartStart);
                isUpdatingPosition = false;
            }); });
            this.State.Position.subscribe(function (v) {
                if (isUpdatingPosition)
                    return;
                isUpdatingPosition = true;
                var part = _this.GetPart(v);
                if (part != null) {
                    part.Audio.Position(v + part.Start - part.PartStart);
                    if (part !== _this._activePart) {
                        if (_this.IsPlaying())
                            part.Audio.Play();
                        _this._activePart.Audio.Pause();
                        _this._activePart = part;
                    }
                }
                isUpdatingPosition = false;
            });
            this.RelativePosition = this.PureComputed(function () {
                var duration = _this.State.Duration();
                var position = _this.State.Position();
                if (duration === null || duration === 0 || position === null)
                    return 0;
                return position / duration;
            }, function (value) {
                value = parseFloat(value);
                var duration = _this.State.Duration();
                _this.State.Position(duration * value);
            });
        };
        InlineAudio.prototype.InitializeVolume = function () {
            var _this = this;
            this.Volume = this._parts[0].Audio.Volume;
            if (this._parts.length > 1) {
                this.Volume.subscribe(function (v) {
                    for (var i = 1; i < _this._parts.length; i++)
                        _this._parts[i].Audio.Volume(v);
                });
            }
        };
        InlineAudio.prototype.JumpToNextPart = function () {
            for (var i = 0; i < this._parts.length; i++) {
                if (this._parts[i] !== this._activePart)
                    continue;
                if (i === this._parts.length - 1) {
                    this._activePart.Audio.Pause();
                    this._activePart = this._parts[0];
                    this._activePart.Audio.Position(this._activePart.Start);
                }
                else {
                    var newPart = this._parts[i + 1];
                    newPart.Audio.Position(newPart.Start);
                    newPart.Audio.Play();
                    this._activePart.Audio.Pause();
                    this._activePart = newPart;
                }
                break;
            }
        };
        InlineAudio.prototype.GetPart = function (position) {
            var total = 0;
            for (var _i = 0, _a = this._parts; _i < _a.length; _i++) {
                var part = _a[_i];
                total += part.End - part.Start;
                if (total >= position)
                    return part;
            }
            return null;
        };
        InlineAudio.prototype.InitializeIsPlaying = function () {
            var _this = this;
            var isUpdatingPlaying = false;
            this.IsPlaying.subscribe(function (v) {
                if (v)
                    _this._communicator.Send("Playing");
                if (isUpdatingPlaying)
                    return;
                isUpdatingPlaying = true;
                _this.State.IsPlaying(v);
                isUpdatingPlaying = false;
            });
            this.State.IsPlaying.subscribe(function (v) {
                if (isUpdatingPlaying || v === _this._activePart.Audio.IsPlaying())
                    return;
                isUpdatingPlaying = true;
                if (v)
                    _this._activePart.Audio.Play();
                else
                    _this._activePart.Audio.Pause();
                isUpdatingPlaying = false;
            });
        };
        return InlineAudio;
    }(BasePlayer));
    return InlineAudio;
});
