var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Hls", "Components/Pages/Asset/Players/BasePlayer", "Managers/Notification"], function (require, exports, knockout, Hls, BasePlayer, Notification) {
    "use strict";
    var Video = (function (_super) {
        __extends(Video, _super);
        function Video(data) {
            var _this = _super.call(this, data) || this;
            _this.VideoElement = knockout.observable(null);
            _this.HasMediaError = knockout.observable(false);
            _this._hls = null;
            var source = _this.State.FirstFile().Destinations[0].Url;
            _this.VideoElement.subscribe(function (v) {
                _this.LoadPlayer(v, source);
            });
            return _this;
        }
        Video.prototype.LoadPlayer = function (element, source) {
            var _this = this;
            if (element.canPlayType("application/vnd.apple.mpegURL; codecs=\"mp4a.40\"") !== "") {
                var sourceElement = document.createElement("source");
                sourceElement.src = source;
                sourceElement.type = "application/vnd.apple.mpegURL";
                element.appendChild(sourceElement);
            }
            else if (Hls.isSupported()) {
                this._hls = new Hls();
                this._hls.loadSource(source);
                this._hls.attachMedia(element);
                this.HandleHlsError(this._hls);
            }
            else {
                throw new Error("HLS play not supported");
            }
            element.addEventListener("loadeddata", function (event) {
                _this.State.IsReady(true);
            });
            this.InitializeDuration();
            this.InitializePosition();
            this.InitializeVolume();
            this.InitializeState();
        };
        Video.prototype.HandleHlsError = function (hls) {
            var _this = this;
            var fatalHasOccured = false;
            var fatalReported = false;
            this._hls.on(Hls.Events.ERROR, function (event, data) {
                if (data.fatal) {
                    if (fatalHasOccured) {
                        if (!fatalReported) {
                            fatalReported = true;
                            Notification.Error("Error playing back media: " + data.details);
                        }
                        return;
                    }
                    fatalHasOccured = true;
                    switch (data.type) {
                        case Hls.ErrorTypes.NETWORK_ERROR:
                            hls.startLoad();
                            break;
                        case Hls.ErrorTypes.MEDIA_ERROR:
                            hls.recoverMediaError();
                            break;
                        default:
                            fatalReported = true;
                            Notification.Error("Error playing back media: " + data.details);
                            break;
                    }
                }
                else if (data.type === Hls.ErrorTypes.MEDIA_ERROR) {
                    _this.HasMediaError(true);
                }
            });
        };
        Video.prototype.InitializePosition = function () {
            var _this = this;
            var isUpdatingPosition = false;
            var element = this.VideoElement();
            element.addEventListener("timeupdate", function (event) {
                isUpdatingPosition = true;
                _this.State.Position(element.currentTime * 1000);
                isUpdatingPosition = false;
            });
            this.Subscribe(this.State.Position, function (v) {
                if (isUpdatingPosition)
                    return;
                element.currentTime = v / 1000;
            });
        };
        Video.prototype.InitializeDuration = function () {
            var _this = this;
            var element = this.VideoElement();
            element.addEventListener("durationchange", function (event) {
                _this.State.Duration(element.duration * 1000);
                _this.State.Offsets[0].FileDuration(element.duration * 1000);
            });
        };
        Video.prototype.InitializeVolume = function () {
            var _this = this;
            var element = this.VideoElement();
            var isChanging = false;
            element.addEventListener("volumechange", function (event) {
                isChanging = true;
                _this.State.Volume(element.muted ? 0 : element.volume * 100);
                isChanging = false;
            });
            this.Subscribe(this.State.Volume, function (value) {
                if (isChanging)
                    return;
                element.volume = value / 100;
            });
        };
        Video.prototype.InitializeState = function () {
            var _this = this;
            var element = this.VideoElement();
            var isChanging = false;
            element.addEventListener("playing", function (event) {
                isChanging = true;
                _this.State.IsPlaying(true);
                isChanging = false;
            });
            element.addEventListener("pause", function (event) {
                isChanging = true;
                _this.State.IsPlaying(false);
                isChanging = false;
            });
            element.addEventListener("ended", function (event) {
                isChanging = true;
                _this.State.IsPlaying(false);
                isChanging = false;
            });
            this.Subscribe(this.State.IsPlaying, function (v) {
                if (isChanging)
                    return;
                if (v)
                    element.play();
                else
                    element.pause();
            });
        };
        Video.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            if (this._hls != null) {
                this._hls.destroy();
                this._hls = null;
            }
        };
        return Video;
    }(BasePlayer));
    return Video;
});
