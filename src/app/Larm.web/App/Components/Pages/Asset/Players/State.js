var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Configuration", "Components/DisposableComponent"], function (require, exports, knockout, Configuration, DisposableComponent) {
    "use strict";
    var State = (function (_super) {
        __extends(State, _super);
        function State() {
            var _this = _super.call(this) || this;
            _this.IsPlaying = knockout.observable(false);
            _this.IsReady = knockout.observable(false);
            _this.IsSideBySide = knockout.observable(false);
            _this.AreFilesReady = knockout.observable(false);
            _this.Duration = knockout.observable(null);
            _this.Position = knockout.observable(null);
            _this.Volume = knockout.observable(100);
            _this.Files = knockout.observable(null);
            _this.FirstFile = _this.PureComputed(function () { return _this.Files() == null || _this.Files().length === 0 ? null : _this.Files()[0]; });
            _this.Type = _this.PureComputed(function () {
                if (_this.FirstFile() == null)
                    return null;
                switch (_this.FirstFile().Type) {
                    case "Audio":
                        return "Audio";
                    case "Video":
                        return "Video";
                    case "Image":
                        return "Image";
                    case "Other":
                        return "PDF";
                    default:
                        return "Download";
                }
            });
            _this.IsTimed = _this.PureComputed(function () { return _this.Type() === "Audio" || _this.Type() === "Video"; });
            _this.IsInline = _this.PureComputed(function () { return _this.Type() === "Audio" || _this.Type() === "Video"; });
            _this.IsBig = _this.PureComputed(function () { return _this.Type() !== "Audio"; });
            _this.UsesTimedTools = _this.PureComputed(function () { return _this.Type() === "Audio" || _this.Type() === "Video"; });
            _this.HasPlayer = _this.Computed(function () { return _this.Type() != null; });
            _this.HasFiles = _this.Computed(function () { return _this.Files() != null; });
            return _this;
        }
        State.prototype.Update = function (files, data, canStream) {
            this.LoadFiles(files, data);
            this.AreFilesReady(canStream);
        };
        State.prototype.LoadFiles = function (files, data) {
            var offsetData = null;
            var offsetDatas = data.filter(function (d) { return d.Name === Configuration.OffsetSchema; });
            if (offsetDatas.length > 0)
                offsetData = offsetDatas[0].Fields["Files"];
            this.FixOffsets(offsetData);
            if (this.Files() == null) {
                this.Offsets = files.map(function (f) {
                    var offset = { Start: 0, End: null, File: f, FileDuration: knockout.observable(null) };
                    if (offsetData != null) {
                        for (var _i = 0, offsetData_1 = offsetData; _i < offsetData_1.length; _i++) {
                            var data_1 = offsetData_1[_i];
                            if (f.Destinations[0].Url.indexOf(data_1[0]) === -1)
                                continue;
                            offset.Start = parseInt(data_1[1]);
                            offset.End = parseInt(data_1[2]);
                            if (offset.Start < 0)
                                offset.Start = 0;
                            if (offset.End <= 0)
                                offset.End = null;
                        }
                    }
                    return offset;
                });
                this.Files(files);
            }
            else {
                if (this.Files().length !== files.length)
                    throw new Error("Number of files has changed from: " + this.Files().length + " to " + files.length);
                for (var _i = 0, _a = this.Files(); _i < _a.length; _i++) {
                    var existingFile = _a[_i];
                    existingFile.Destinations = null;
                    for (var _b = 0, files_1 = files; _b < files_1.length; _b++) {
                        var newFile = files_1[_b];
                        if (newFile.Identifier !== existingFile.Identifier)
                            continue;
                        existingFile.Destinations = newFile.Destinations;
                        break;
                    }
                    if (existingFile.Destinations == null)
                        throw new Error("Unable to find updated destinations for file " + existingFile.Identifier);
                }
            }
        };
        State.prototype.FixOffsets = function (offsetData) {
            if (offsetData === null)
                return;
            var extraFiles = [];
            for (var _i = 0, offsetData_2 = offsetData; _i < offsetData_2.length; _i++) {
                var offset = offsetData_2[_i];
                if (offset.length === 6)
                    extraFiles.push(offset.splice(3, 3));
            }
            offsetData.push.apply(offsetData, extraFiles);
        };
        return State;
    }(DisposableComponent));
    return State;
});
