﻿import knockout = require("knockout");
import Communicator = require("Utility/Communicator");
import PlayerState = require("Components/Pages/Asset/Players/State");
import BasePlayer = require("Components/Pages/Asset/Players/BasePlayer");
import DataFormat = require("Utility/DataFormat");

class InlineVideo extends BasePlayer
{
	public IsReady: KnockoutObservable<boolean>;
	public IsSideBySide: KnockoutObservable<boolean>;
	public IsPlaying: KnockoutObservable<boolean>;
	public Volume: KnockoutObservable<number>;

	public IsMuted: KnockoutComputed<boolean>;
	public Position: KnockoutComputed<string>;
	public Duration: KnockoutComputed<string>;

	private _communicator: Communicator;
	private _volumeBeforeMute:number = 100;

	constructor(data:{ State: PlayerState })
	{
		super(data);

		this.IsReady = this.State.IsReady;
		this.IsSideBySide = this.State.IsSideBySide;
		this.IsPlaying = this.State.IsPlaying;
		this.Volume = this.State.Volume;

		this.Position = knockout.pureComputed(() => DataFormat.MillieSecondsToPrettyTime(this.State.Position()));
		this.Duration = knockout.pureComputed(() => DataFormat.MillieSecondsToPrettyTime(this.State.Duration()));
		this.IsMuted = this.Computed(() => this.Volume() === 0);

		this._communicator = new Communicator("PlayerState", v =>
		{
			if (v === "Playing" && this.IsPlaying())
				this.IsPlaying(false);
		});
	}

	public ToggleMute():void
	{
		if (this.IsMuted())
			this.Volume(this._volumeBeforeMute);
		else
		{
			this._volumeBeforeMute = this.Volume();
			this.Volume(0);
		}
	}

	public ToggleSideBySide(): void
	{
		this.State.IsSideBySide(!this.State.IsSideBySide());
	}

	public Toggle(): void
	{
		if (!this.IsPlaying() && this.State.Position() >= this.State.Duration())
			this.State.Position(0);

		this.IsPlaying(!this.IsPlaying());
	}

	public dispose():void
	{
		this._communicator.Dispose();
	}

	private GetTwoDigits(value: number): string
	{
		return value < 10 ? "0" + value : value.toString();
	}
}

export = InlineVideo;