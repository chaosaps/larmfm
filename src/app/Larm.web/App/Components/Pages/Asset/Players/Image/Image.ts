﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import PlayerState = require("Components/Pages/Asset/Players/State");
import BasePlayer = require("Components/Pages/Asset/Players/BasePlayer");
import PDFPlayer = require("Components/Pages/Asset/Players/PDF/PDF");

type Page = { Name: string, File: EZArchivePortal.IFile, Select:()=>void}

class Image extends PDFPlayer
{
	constructor(data: { State: PlayerState })
	{
		super(data);
	}
}

export = Image;