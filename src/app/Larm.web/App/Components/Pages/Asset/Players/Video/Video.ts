﻿import knockout = require("knockout");
import Hls = require("Hls");
import PlayerState = require("Components/Pages/Asset/Players/State");
import BasePlayer = require("Components/Pages/Asset/Players/BasePlayer");
import Notification = require("Managers/Notification");

class Video extends BasePlayer
{
	public VideoElement = knockout.observable<HTMLVideoElement>(null);
	public HasMediaError = knockout.observable(false);

	private _hls:Hls = null;

	constructor(data: { State: PlayerState })
	{
		super(data);

		var source = this.State.FirstFile().Destinations[0].Url;
		this.VideoElement.subscribe(v => {
			this.LoadPlayer(v, source);
		});
	}

	private LoadPlayer(element: HTMLVideoElement, source: string):void
	{
		if (element.canPlayType("application/vnd.apple.mpegURL; codecs=\"mp4a.40\"") !== "") {
			const sourceElement = document.createElement("source");
			sourceElement.src = source;
			sourceElement.type = "application/vnd.apple.mpegURL";
			element.appendChild(sourceElement);
		} else if (Hls.isSupported()) {
			this._hls = new Hls();
			this._hls.loadSource(source);
			this._hls.attachMedia(element);
			this.HandleHlsError(this._hls);
		} else {
			throw new Error("HLS play not supported");
		}

		element.addEventListener("loadeddata", event => {
			this.State.IsReady(true);
		});

		this.InitializeDuration();
		this.InitializePosition();
		this.InitializeVolume();
		this.InitializeState();
	}

	private HandleHlsError(hls: Hls): void {
		let fatalHasOccured = false;
		let fatalReported = false;

		this._hls.on(Hls.Events.ERROR, (event, data) => {
			if (data.fatal) {
				if (fatalHasOccured) {
					if (!fatalReported) {
						fatalReported = true;
						Notification.Error("Error playing back media: " + data.details);
					}
					return;
				}
				fatalHasOccured = true;

				switch (data.type) {
					case Hls.ErrorTypes.NETWORK_ERROR:
						hls.startLoad();
						break;
					case Hls.ErrorTypes.MEDIA_ERROR:
						hls.recoverMediaError();
						break;
					default:
						fatalReported = true;
						Notification.Error("Error playing back media: " + data.details);
						break;
				}
			} else if(data.type === Hls.ErrorTypes.MEDIA_ERROR) {
				this.HasMediaError(true);
			}
		});
	}

	private InitializePosition(): void
	{
		let isUpdatingPosition = false;
		const element = this.VideoElement();

		element.addEventListener("timeupdate", event => {
			isUpdatingPosition = true;
			this.State.Position(element.currentTime * 1000);
			isUpdatingPosition = false;
		});

		this.Subscribe(this.State.Position,
			v => {
				if (isUpdatingPosition)
					return;
				element.currentTime = v / 1000
			});
	}

	private InitializeDuration(): void
	{
		const element = this.VideoElement();

		element.addEventListener("durationchange", event => {
			this.State.Duration(element.duration * 1000);
			this.State.Offsets[0].FileDuration(element.duration * 1000);
		});
	}

	private InitializeVolume(): void
	{
		const element = this.VideoElement();
		let isChanging = false;

		element.addEventListener("volumechange", event => {
			isChanging = true;
			this.State.Volume(element.muted ? 0 : element.volume * 100);
			isChanging = false;
		});

		this.Subscribe(this.State.Volume, value => {
				if (isChanging)
					return;

				element.volume = value / 100;
			});
	}

	private InitializeState(): void
	{
		const element = this.VideoElement();
		let isChanging = false;

		element.addEventListener("playing", event => {
			isChanging = true;
			this.State.IsPlaying(true);
			isChanging = false;
		});
		element.addEventListener("pause", event => {
			isChanging = true;
			this.State.IsPlaying(false);
			isChanging = false;
		});
		element.addEventListener("ended", event => {
			isChanging = true;
			this.State.IsPlaying(false);
			isChanging = false;
		});

		this.Subscribe(this.State.IsPlaying, v => {
				if (isChanging)
				return;

				if (v)
					element.play();
				else
					element.pause();
			});
	}

	public dispose(): void
	{
		super.dispose();

		if (this._hls != null) {
			this._hls.destroy();
			this._hls = null;
		}
	}
}

export = Video;