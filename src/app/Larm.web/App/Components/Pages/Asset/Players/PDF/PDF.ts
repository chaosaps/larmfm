﻿import knockout = require("knockout");
import PlayerState = require("Components/Pages/Asset/Players/State");
import BasePlayer = require("Components/Pages/Asset/Players/BasePlayer");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");

type Page = { Name: string, File: EZArchivePortal.IFile, Select:()=>void}

class PDF extends BasePlayer
{
	public IsBig = knockout.observable(false);
	public Link: KnockoutComputed<string>;
	public Pages: Page[];
	public ActivePage: KnockoutObservable<Page>;
	public HasMultiplePages:boolean = false;

	constructor(data: { State: PlayerState })
	{
		super(data);

		this.Pages = data.State.Files().map(f =>
		{
			var page: Page = { Name: "Ukendt", File: f, Select: null };
			if (f.Name != null)
			{
				page.Name = f.Name.replace(/\.\w{1,4}$/, "").replace(/_|-/g, " ");
			}
			else if (f.Destinations.length > 0)
			{
				var match = /.+\/(.+)\..+/.exec(f.Destinations[0].Url);

				if (match.length > 0)
					page.Name = match[1];
			}

			page.Select = () => this.ActivePage(page);
			return page;
		}).sort((a,b) => a.Name.localeCompare(b.Name));
		this.ActivePage = knockout.observable(this.Pages.length === 0 ? null : this.Pages[0]);

		this.Link = this.PureComputed(() => this.ActivePage() == null ? null : this.ActivePage().File.Destinations[0].Url);

		this.HasMultiplePages = this.Pages.length > 1;
	}

	public ToggleBig():void
	{
		this.IsBig(!this.IsBig());
	}
}

export = PDF;