﻿import PlayerState = require("Components/Pages/Asset/Players/State");
import BasePlayer = require("Components/Pages/Asset/Players/BasePlayer");

class Download extends BasePlayer
{
	public Link:KnockoutComputed<string>;

	constructor(data: { State: PlayerState })
	{
		super(data);

		this.Link = this.PureComputed(() => this.State.FirstFile() == null ? null : this.State.FirstFile().Destinations[0].Url);
	}
}

export = Download;