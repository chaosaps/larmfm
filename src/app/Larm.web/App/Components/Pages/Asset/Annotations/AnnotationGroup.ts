﻿import knockout = require("knockout");
import Annotation = require("Components/Pages/Asset/Annotations/Annotation");
import AnnotationDefinition = require("Managers/Portal/AnnotationDefinition");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import DisposableComponent = require("Components/DisposableComponent");
import Profile = require("Managers/Profile");

type Owner = { Name: string, Id: string };

class AnnotationGroup extends DisposableComponent
{
	public Name: string;
	public IsTimed: boolean;
	public Definition: EZArchivePortal.IEZAnnotationDefinition;
	public Annotations: KnockoutObservableArray<Annotation> = knockout.observableArray<Annotation>();
	public Owners: KnockoutComputed<Owner[]>;
	public FilteredOwner: KnockoutObservable<Owner>;
	
	private _addedCallback: (annotation: Annotation, group: AnnotationGroup) => void;
	private _visibleCheck: (ownerId: string) => boolean;

	constructor(annotationGroup: EZArchivePortal.IAnnotationGroup,
		addedCallback: (annotation: Annotation, group: AnnotationGroup) => void,
		visibleCheck: (ownerId: string) => boolean,
		filteredOwner: KnockoutObservable<Owner>,
		owners: KnockoutComputed<Owner[]>)
	{
		super();
		this.Name = annotationGroup.Name;
		this.Definition = AnnotationDefinition.GetById(annotationGroup.DefinitionId);
		this._addedCallback = addedCallback;
		this._visibleCheck = visibleCheck;
		this.IsTimed = AnnotationDefinition.IsTimed(this.Definition);
		this.FilteredOwner = filteredOwner;
		this.Owners = owners;

		annotationGroup.Data.forEach(a => this.AddAnnotationFromPortal(a));
	}

	public CreateAnnotation(setCurrentUserAsOwner:boolean):Annotation
	{
		const annotation = new Annotation(this.Definition.Fields, this.IsTimed, this._visibleCheck);

		if (setCurrentUserAsOwner)
		{
			annotation.OwnerId(Profile.Identifier());
			annotation.Owner(Profile.Name());
			annotation.Permission(EZArchivePortal.AnnotationPermission.All);
			annotation.Visibility("Public");
		}

		return annotation;
	}

	public AddAnnotation(annotation: Annotation):void
	{
		this.Annotations.push(annotation);
		this._addedCallback(annotation, this);
	}

	private AddAnnotationFromPortal(annotation: EZArchivePortal.IAnnotation):void
	{
		const result = this.CreateAnnotation(false);

		result.ParsePortalAnnotation(annotation);
		result.IsServerSide(true);
		
		this.AddAnnotation(result);
	}
}

export = AnnotationGroup;