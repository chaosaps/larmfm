var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "moment", "Managers/Portal/EZArchivePortal", "Managers/Permissions", "Managers/Configuration", "Components/DisposableComponent", "Managers/Dialog/Dialog", "Managers/Profile"], function (require, exports, knockout, moment, EZArchivePortal, Permissions, Configuration, DisposableComponent, DialogManager, Profile) {
    "use strict";
    var Annotation = (function (_super) {
        __extends(Annotation, _super);
        function Annotation(fieldTypes, isTimed, visibleCheck) {
            var _this = _super.call(this) || this;
            _this.Identifier = null;
            _this.Start = knockout.observable();
            _this.End = knockout.observable();
            _this.IsEditing = knockout.observable(false);
            _this.IsCollapsed = knockout.observable(true);
            _this.IsSelected = knockout.observable(false);
            _this.IsServerSide = knockout.observable(false);
            _this.Owner = knockout.observable("");
            _this.OwnerId = knockout.observable(null);
            _this.Permission = knockout.observable(EZArchivePortal.AnnotationPermission.Owner);
            _this.Visibility = knockout.observable();
            _this._playCallbacks = [];
            _this._saveCallbacks = [];
            _this._deleteCallbacks = [];
            _this.IsTimed = isTimed;
            _this.Fields = _this.CreateFields(fieldTypes, _this.IsTimed);
            _this.Title = _this.PureComputed(function () {
                for (var _i = 0, _a = _this.Fields; _i < _a.length; _i++) {
                    var field = _a[_i];
                    if (field.Value())
                        return field.Value().toString();
                }
                return "";
            });
            _this.IsLocked = _this.PureComputed(function () { return _this.Permission() === EZArchivePortal.AnnotationPermission.Owner; }, function (v) { return _this.Permission(v ? EZArchivePortal.AnnotationPermission.Owner : EZArchivePortal.AnnotationPermission.All); });
            _this.IsPrivate = _this.PureComputed(function () { return _this.Visibility() === "Private"; }, function (v) { return _this.Visibility(v ? "Private" : "Public"); });
            _this.CanEdit = _this.PureComputed(function () { return Permissions.CanEditAnnotations && (!_this.IsLocked() || _this.OwnerId() === Profile.Identifier()); });
            _this.CanEditLock = _this.PureComputed(function () { return Permissions.CanEditAnnotations && _this.OwnerId() === Profile.Identifier(); });
            _this.CanEditVisibility = _this.PureComputed(function () { return Permissions.CanEditAnnotations && _this.OwnerId() === Profile.Identifier(); });
            _this.IsVisible = _this.PureComputed(function () { return visibleCheck(_this.OwnerId()); });
            _this._internalId = Annotation.Count++;
            _this.PrettyStart = knockout.pureComputed(function () { return _this.Start().utc().format("HH:mm:ss"); });
            _this.PrettyEnd = knockout.pureComputed(function () { return _this.End().utc().format("HH:mm:ss"); });
            _this.Duration = knockout.pureComputed(function () { return moment(_this.End().diff(_this.Start())).utc().format("HH:mm:ss"); });
            return _this;
        }
        Annotation.prototype.HtmlEscape = function (str, replaceNewLines) {
            if (replaceNewLines === void 0) { replaceNewLines = false; }
            if (str == null)
                return "";
            str = str
                .replace(/&/g, "&amp;")
                .replace(/"/g, "&quot;")
                .replace(/'/g, "&#39;")
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;");
            if (replaceNewLines)
                str = str.replace(/[\n\r]/g, "<br/>");
            return str;
        };
        Annotation.prototype.ToggleCollapsed = function () {
            this.IsCollapsed(!this.IsCollapsed());
            if (!this.IsCollapsed())
                this.IsSelected(true);
        };
        Object.defineProperty(Annotation.prototype, "Id", {
            get: function () {
                return this._internalId;
            },
            enumerable: true,
            configurable: true
        });
        Annotation.prototype.AddPlayCallback = function (callback) {
            this._playCallbacks.push(callback);
        };
        Annotation.prototype.AddSaveCallback = function (callback) {
            this._saveCallbacks.push(callback);
        };
        Annotation.prototype.AddDeleteCallback = function (callback) {
            this._deleteCallbacks.push(callback);
        };
        Annotation.prototype.Play = function () {
            var _this = this;
            this._playCallbacks.forEach(function (c) { return c(_this); });
        };
        Annotation.prototype.Save = function () {
            var _this = this;
            this.IsEditing(false);
            this.Fields.forEach(function (f) { return f.OriginalValue = f.Value(); });
            this._saveCallbacks.forEach(function (c) { return c(_this); });
        };
        Annotation.prototype.RequestDelete = function () {
            DialogManager.Show("Pages/Asset/Annotations/DeleteDialog", this);
        };
        Annotation.prototype.Delete = function () {
            var _this = this;
            this._deleteCallbacks.forEach(function (c) { return c(_this); });
        };
        Annotation.prototype.ParsePortalAnnotation = function (annotation) {
            this.Identifier = annotation.Identifier;
            this.Owner(annotation.__Owner);
            this.OwnerId(annotation.__OwnerId);
            this.Permission(annotation.__Permission);
            this.Visibility(annotation.__Visibility);
            if (this.IsTimed) {
                this.Start(moment(parseInt(annotation[Annotation.StartField])));
                this.End(moment(parseInt(annotation[Annotation.EndField])));
            }
            this.Fields.forEach(function (f) {
                f.OriginalValue = annotation[f.Name];
                f.Value(f.OriginalValue);
            });
        };
        Annotation.prototype.ToPortalAnnotation = function () {
            var annotation = {};
            if (this.Identifier != null)
                annotation.Identifier = this.Identifier;
            if (this.IsTimed) {
                annotation[Annotation.StartField] = this.Start().valueOf().toString();
                annotation[Annotation.EndField] = this.End().valueOf().toString();
            }
            this.Fields.forEach(function (f) { return annotation[f.Name] = f.Value(); });
            return annotation;
        };
        Annotation.prototype.Edit = function () {
            if (!this.CanEdit())
                return;
            this.IsEditing(true);
            this.IsCollapsed(false);
        };
        Annotation.prototype.Cancel = function () {
            if (this.IsServerSide()) {
                this.Fields.forEach(function (f) { return f.Value(f.OriginalValue); });
                this.IsEditing(false);
            }
            else
                this.Delete();
        };
        Annotation.prototype.CreateFields = function (types, isTimed) {
            var result = [];
            for (var key in types) {
                if (isTimed && (key === Annotation.StartField || key === Annotation.EndField))
                    continue;
                result.push({ Name: key, Type: types[key], OriginalValue: "", Value: knockout.observable(""), IsVisible: Configuration.HiddenAnnotationProperties.indexOf(key) === -1 });
            }
            return result;
        };
        Annotation.StartField = "StartTime";
        Annotation.EndField = "EndTime";
        Annotation.Count = 0;
        return Annotation;
    }(DisposableComponent));
    return Annotation;
});
