var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "moment", "vis", "jquery", "Components/DisposableComponent", "Managers/Permissions"], function (require, exports, knockout, moment, vis, jquery, DisposableComponent, Permissions) {
    "use strict";
    var Timeline = (function (_super) {
        __extends(Timeline, _super);
        function Timeline(data) {
            var _this = _super.call(this) || this;
            _this.TimelineElement = knockout.observable();
            _this.IsDraggingTimeline = knockout.observable(false);
            _this._isAdding = false;
            _this._annotationGroups = data.Groups;
            _this._getActiveGroup = data.GetActiveGroup;
            _this._playerState = data.PlayerState;
            _this._data = new vis.DataSet(_this.GetAllAnnotations(_this._annotationGroups()).map(function (a) { return _this.ToTimelineAnnotation(a, true); }), { type: { start: "Moment", end: "Moment" } });
            _this.InitializeOptions();
            _this.InitializeEvents();
            return _this;
        }
        Timeline.prototype.GetAllAnnotations = function (groups) {
            var _a;
            var lists = groups.filter(function (g) { return g.IsTimed; }).map(function (g) { return g.Annotations(); });
            switch (lists.length) {
                case 0:
                    return [];
                case 1:
                    return lists[0];
                default:
                    return (_a = new Array()).concat.apply(_a, lists);
            }
        };
        Timeline.prototype.InitializeOptions = function () {
            var _this = this;
            var duration = this._playerState.Duration();
            if (!this.isNumberValid(duration))
                duration = 1000 * 60 * 30;
            this._options = {
                min: 0,
                max: duration,
                start: 0,
                end: duration,
                selectable: true,
                showCurrentTime: false,
                zoomMin: 1000,
                showMajorLabels: false,
                clickToUse: false,
                format: {
                    minorLabels: {
                        millisecond: "mm:ss.S",
                        second: "mm:ss",
                        minute: "mm:ss",
                        hour: "HH:mm",
                        weekday: "HH:mm",
                        day: "HH:mm",
                        month: "HH:mm",
                        year: "HH:mm"
                    },
                    majorLabels: {
                        millisecond: "HH:mm",
                        second: "HH:mm",
                        minute: "HH:mm",
                        hour: "HH:mm",
                        weekday: "HH:mm",
                        day: "HH:mm",
                        month: "HH:mm",
                        year: "HH:mm"
                    }
                },
                maxHeight: "300px",
                editable: Permissions.CanEditAnnotations(),
                snap: null,
                moment: function (date) { return moment(date).utc(); },
                onAdd: function (i, c) { return _this.AddAnnotation(i, c); },
                onUpdate: function (i, c) { return _this.EditAnnotation(i, c); },
                onMove: function (i, c) { return _this.MoveAnnotation(i, c); },
                onRemove: function (i, c) { return _this.RemoveAnnotation(i, c); }
            };
        };
        Timeline.prototype.InitializeEvents = function () {
            var _this = this;
            this.SubscribeUntilChange(this.TimelineElement, function (e) { return _this.InitializeTimeline(e); });
            this._annotationGroups().forEach(function (g) {
                if (g.IsTimed)
                    _this.SubscribeToAnnotationGroup(g);
            });
            this.SubscribeToArray(this._annotationGroups, function (group, status) {
                if (status === "added" && group.IsTimed) {
                    _this.SubscribeToAnnotationGroup(group);
                    if (group.Annotations().length > 0) {
                        _this._data.add(group.Annotations().map(function (a) { return _this.ToTimelineAnnotation(a, true); }));
                        _this.UpdateDeleteButton();
                    }
                }
            });
            this.Subscribe(this._playerState.Duration, function (v) { return _this.UpdateDuration(v); });
            this.SubscribeUntilChange(this._playerState.IsReady, function (v) { return _this.InitializePosition(); });
            this.Subscribe(Permissions.CanEditAnnotations, function (v) {
                _this._options.editable = v;
                _this.UpdateOptions();
            });
        };
        Timeline.prototype.SubscribeToAnnotationGroup = function (group) {
            var _this = this;
            this.SubscribeToArray(group.Annotations, function (annotation, status) {
                if (!_this._isAdding && status === "added") {
                    _this._data.add(_this.ToTimelineAnnotation(annotation, true));
                    _this.UpdateDeleteButton();
                }
            });
        };
        Timeline.prototype.InitializePosition = function () {
            var _this = this;
            if (this._timeline == null || !this._playerState.IsReady())
                return;
            var startPosition = this._playerState.Position() != null ? this._playerState.Position() : 0;
            this._timeline.addCustomTime(startPosition, "PlayerPosition");
            var isUpdatingPosition = false;
            var isDraggingTime = false;
            this._playerState.Position.subscribe(function (v) {
                if (isUpdatingPosition || isDraggingTime || v == null)
                    return;
                isUpdatingPosition = true;
                _this._timeline.setCustomTime(v, "PlayerPosition");
                isUpdatingPosition = false;
            });
            this._timeline.on("timechanged", function (e) {
                isDraggingTime = false;
                if (isUpdatingPosition)
                    return;
                isUpdatingPosition = true;
                _this._playerState.Position(e.time.getTime());
                isUpdatingPosition = false;
            });
            this._timeline.on("timechange", function (e) {
                isDraggingTime = true;
            });
            this._timeline.on("rangechange", function (e) {
                _this.IsDraggingTimeline(true);
            });
            this._timeline.on("rangechanged", function (e) {
                _this.IsDraggingTimeline(false);
            });
        };
        Timeline.prototype.InitializeTimeline = function (element) {
            var _this = this;
            this._timeline = new vis.Timeline(element, this._data, this._options);
            this._timeline.on("select", function (e) {
                if (e.items.length === 1) {
                    _this.GetAnnotation(e.items[0]).IsSelected(true);
                    _this.UpdateDeleteButton();
                }
            });
            this.InitializePosition();
        };
        Timeline.prototype.UpdateOptions = function () {
            if (this._timeline != null)
                this._timeline.setOptions(this._options);
        };
        Timeline.prototype.AddAnnotation = function (item, callback) {
            this._isAdding = true;
            item.content = "";
            item.end = moment(item.start);
            item.end.add(30, "s");
            item.editable = true;
            var group = this._getActiveGroup() || this._annotationGroups()[0];
            var annotation = group.CreateAnnotation(true);
            this.UpdateAnnotationWithTimelineItem(annotation, item);
            group.AddAnnotation(annotation);
            item.id = annotation.Id;
            this.AddCallbacks(annotation);
            setTimeout(function () {
                annotation.Edit();
                annotation.IsSelected(true);
            }, 0);
            callback(item);
            this._isAdding = false;
            this.UpdateDeleteButton();
        };
        Timeline.prototype.EditAnnotation = function (item, callback) {
            var annotation = this.GetAnnotation(item.id);
            annotation.Edit();
            callback(null);
        };
        Timeline.prototype.MoveAnnotation = function (item, callback) {
            this.UpdateAnnotationWithTimelineItem(this.GetAnnotation(item.id), item).Save();
            callback(item);
        };
        Timeline.prototype.RemoveAnnotation = function (item, callback) {
            callback(null);
            this.GetAnnotation(item.id).RequestDelete();
        };
        Timeline.prototype.UpdateAnnotationWithTimelineItem = function (annotation, item) {
            if (item.start instanceof Date)
                item.start = moment(item.start);
            if (item.end instanceof Date)
                item.end = moment(item.end);
            if (item.start.valueOf() < 0)
                item.start = moment(0);
            else if (item.start.valueOf() > this._options.max)
                item.start = moment(this._options.max - 1000);
            if (item.end.valueOf() < 0)
                item.end = moment(1000);
            else if (item.end.valueOf() > this._options.max)
                item.end = moment(this._options.max);
            annotation.Start(item.start);
            annotation.End(item.end);
            return annotation;
        };
        Timeline.prototype.UpdateTimelineItemWithAnnotation = function (item, annotation) {
            item.id = annotation.Id;
            item.start = annotation.Start();
            item.end = annotation.End();
            item.content = annotation.Title();
            item.editable = annotation.CanEdit();
            return item;
        };
        Timeline.prototype.ToTimelineAnnotation = function (annotation, addCallback) {
            if (addCallback)
                this.AddCallbacks(annotation);
            return this.UpdateTimelineItemWithAnnotation({}, annotation);
        };
        Timeline.prototype.AddCallbacks = function (annotation) {
            var _this = this;
            annotation.AddSaveCallback(function (a) { return _this.AnnotationUpdated(a); });
            annotation.AddDeleteCallback(function (a) { return _this.AnnotationDeleted(a); });
            this.Subscribe(annotation.IsSelected, function (v) {
                if (v)
                    _this.AnnotationSelected(annotation);
            });
            this.Subscribe(annotation.IsVisible, function (v) {
                if (v) {
                    _this._data.add(_this.ToTimelineAnnotation(annotation, false));
                    _this.UpdateDeleteButton();
                }
                else
                    _this._data.remove(annotation.Id);
            });
        };
        Timeline.prototype.GetAnnotation = function (id) {
            this._annotationGroups().forEach(function (g) { return g.Annotations; });
            var groups = this._annotationGroups();
            for (var _i = 0, groups_1 = groups; _i < groups_1.length; _i++) {
                var group = groups_1[_i];
                var annotations = group.Annotations();
                for (var _a = 0, annotations_1 = annotations; _a < annotations_1.length; _a++) {
                    var annotation = annotations_1[_a];
                    if (annotation.Id === id)
                        return annotation;
                }
            }
            throw new Error("Annotation with id: " + id + " not found");
        };
        Timeline.prototype.AnnotationUpdated = function (annotation) {
            this._data.update(this.ToTimelineAnnotation(annotation, false));
        };
        Timeline.prototype.AnnotationDeleted = function (annotation) {
            this._data.remove(annotation.Id);
        };
        Timeline.prototype.AnnotationSelected = function (annotation) {
            this._timeline.setSelection(annotation.Id, { focus: true });
        };
        Timeline.prototype.UpdateDuration = function (duration) {
            if (!this.isNumberValid(duration))
                return;
            this._options.max = duration;
            this._options.end = duration;
            this.UpdateOptions();
        };
        Timeline.prototype.isNumberValid = function (value) {
            return typeof value === "number" && isFinite(value);
        };
        Timeline.prototype.UpdateDeleteButton = function () {
            var _this = this;
            setTimeout(function () {
                jquery(_this.TimelineElement())
                    .find(".vis-delete")
                    .each(function (index, deleteElement) { return deleteElement.title = "Slet annotation"; });
            });
        };
        return Timeline;
    }(DisposableComponent));
    return Timeline;
});
