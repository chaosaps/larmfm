var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "jquery", "knockout", "Managers/Portal/EZArchivePortal", "Managers/Portal/AnnotationDefinition", "Components/Pages/Asset/Annotations/AnnotationGroup", "Managers/Notification", "Components/DisposableComponent", "Utility/DateParser"], function (require, exports, jquery, knockout, EZArchivePortal, AnnotationDefinition, AnnotationGroup, Notification, DisposableComponent, DateParser) {
    "use strict";
    var AnnotationHandler = (function (_super) {
        __extends(AnnotationHandler, _super);
        function AnnotationHandler(assetId, playerState) {
            var _this = _super.call(this) || this;
            _this.AnnotationGroups = knockout.observableArray();
            _this.SelectedAnnotation = knockout.observable(null);
            _this._broadcastWalltimeStart = null;
            _this._annotationToSelectOnLoad = null;
            _this._assetId = assetId;
            _this._playerState = playerState;
            _this.AllDurationsAreReady = _this.PureComputed(function () { return _this._playerState.AreFilesReady() && _this._playerState.Files() != null && _this._playerState.Offsets != null && _this._playerState.Offsets.every(function (o) { return o.FileDuration() != null; }); });
            _this.Offsets = _this.PureComputed(function () { return _this.AllDurationsAreReady() ? _this.GetOffsets(_this._playerState) : null; });
            _this.Owners = _this.PureComputed(function () {
                var result = [{ Name: "Alle", Id: null }];
                var userHash = {};
                for (var _i = 0, _a = _this.AnnotationGroups(); _i < _a.length; _i++) {
                    var group = _a[_i];
                    for (var _b = 0, _c = group.Annotations(); _b < _c.length; _b++) {
                        var annotaion = _c[_b];
                        if (userHash.hasOwnProperty(annotaion.OwnerId()))
                            continue;
                        userHash[annotaion.OwnerId()] = true;
                        result.push({ Name: annotaion.Owner(), Id: annotaion.OwnerId() });
                    }
                }
                return result;
            });
            _this.FilteredOwner = knockout.observable(_this.Owners()[0]);
            return _this;
        }
        AnnotationHandler.prototype.Load = function (assetTypeId, annotations, broadcastWalltimeStart) {
            var _this = this;
            this._broadcastWalltimeStart = broadcastWalltimeStart;
            this.AnnotationGroups.removeAll();
            this.AddAction(function () { return AnnotationDefinition.IsReady() && _this._playerState.HasFiles() && (!_this._playerState.UsesTimedTools() || _this.AllDurationsAreReady()); }, function () { return _this.InnerLoad(assetTypeId, annotations); });
        };
        AnnotationHandler.prototype.GetActiveGroup = function () {
            var id = this.GetActiveDefinition();
            if (id == null)
                return null;
            for (var _i = 0, _a = this.AnnotationGroups(); _i < _a.length; _i++) {
                var g = _a[_i];
                if (g.Definition.Identifier === id)
                    return g;
            }
            return null;
        };
        AnnotationHandler.prototype.SelectAnnotation = function (annotationIdentifier) {
            if (this.SelectedAnnotation() != null && this.SelectedAnnotation().Identifier === annotationIdentifier)
                return;
            if (this.AnnotationGroups().length === 0) {
                this._annotationToSelectOnLoad = annotationIdentifier;
                return;
            }
            if (annotationIdentifier == null)
                this.SelectedAnnotation(null);
            for (var _i = 0, _a = this.AnnotationGroups(); _i < _a.length; _i++) {
                var group = _a[_i];
                for (var _b = 0, _c = group.Annotations(); _b < _c.length; _b++) {
                    var annotation = _c[_b];
                    if (annotation.Identifier === annotationIdentifier) {
                        annotation.IsSelected(true);
                        return;
                    }
                }
            }
        };
        AnnotationHandler.prototype.InnerLoad = function (assetTypeId, groups) {
            var _this = this;
            var _a;
            this.AdjustFromFileOffset(groups, this._playerState);
            var newGroups = AnnotationDefinition.GetByAssetType(assetTypeId)
                .filter(function (d) { return !groups.some(function (g) { return g.DefinitionId === d.Identifier; }); })
                .map(function (d) { return _this.CreateEmptyAnnotationGroup(d); });
            newGroups.push.apply(newGroups, groups.map(function (g) { return _this.CreateAnnotationGroup(g); }));
            newGroups.sort(function (a, b) { return a.Name.localeCompare(b.Name); });
            (_a = this.AnnotationGroups).push.apply(_a, newGroups);
            if (this._annotationToSelectOnLoad != null) {
                this.SelectAnnotation(this._annotationToSelectOnLoad);
                this._annotationToSelectOnLoad = null;
            }
        };
        AnnotationHandler.prototype.CreateAnnotationGroup = function (data) {
            var _this = this;
            return new AnnotationGroup(data, function (a, ag) { return _this.AnnotationAdded(a, ag); }, function (ownerId) { return _this.FilteredOwner().Id === null || _this.FilteredOwner().Id === ownerId; }, this.FilteredOwner, this.Owners);
        };
        AnnotationHandler.prototype.CreateEmptyAnnotationGroup = function (definition) {
            return this.CreateAnnotationGroup({ DefinitionId: definition.Identifier, Name: definition.Name, Data: [] });
        };
        AnnotationHandler.prototype.AnnotationAdded = function (annotation, group) {
            var _this = this;
            this.ShowList(group);
            annotation.AddSaveCallback(function (a) { return _this.SaveAnnotation(a, group); });
            annotation.AddDeleteCallback(function (a) { return _this.DeleteAnnotation(a, group); });
            annotation.AddPlayCallback(function (a) { return _this.PlayAnnotation(a); });
            this.Subscribe(annotation.Permission, function (v) {
                EZArchivePortal.Annotation.SetPermission(_this._assetId, annotation.Identifier, v)
                    .WithCallback(function (response) {
                    if (response.Error != null) {
                        Notification.Error("Failed to set annotation permission: " + response.Error.Message);
                        return;
                    }
                });
            });
            this.Subscribe(annotation.Visibility, function (v) { annotation.Save(); });
            this.Subscribe(annotation.IsSelected, function (v) {
                var currentSelection = _this.SelectedAnnotation();
                if (v && currentSelection !== annotation) {
                    if (currentSelection != null)
                        currentSelection.IsSelected(false);
                    _this.SelectedAnnotation(annotation);
                    annotation.IsSelected(true);
                    _this.ShowList(group);
                }
            });
        };
        AnnotationHandler.prototype.SaveAnnotation = function (annotation, group) {
            EZArchivePortal.Annotation.Set(this._assetId, group.Definition.Identifier, this.AdjustToFileOffsets(annotation.ToPortalAnnotation(), annotation.IsTimed, this._playerState), { Scope: annotation.Visibility() })
                .WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to save annotation: " + response.Error.Message);
                    return;
                }
                annotation.Identifier = response.Body.Results[0].Identifier;
                annotation.IsServerSide(true);
            });
        };
        AnnotationHandler.prototype.DeleteAnnotation = function (annotation, group) {
            if (annotation.IsServerSide()) {
                EZArchivePortal.Annotation.Delete(this._assetId, annotation.Identifier).WithCallback(function (response) {
                    if (response.Error != null)
                        Notification.Error("Failed to delete annotation: " + response.Error.Message);
                });
            }
            group.Annotations.remove(annotation);
        };
        AnnotationHandler.prototype.PlayAnnotation = function (annotation) {
            this._playerState.Position(annotation.Start().valueOf());
            this._playerState.IsPlaying(true);
        };
        AnnotationHandler.prototype.GetActiveDefinition = function () {
            var value = jquery(".TabContainer .nav-tabs li.active a").attr("href");
            if (value == undefined)
                return null;
            var result = /#(.+)Tab/.exec(value);
            if (result == null)
                return null;
            return result[1];
        };
        AnnotationHandler.prototype.ShowList = function (group) {
            jquery(".TabContainer .nav-tabs a[href=\"#" + group.Definition.Identifier + "Tab\"]").tab("show");
        };
        AnnotationHandler.prototype.AdjustToFileOffsets = function (annotation, isTimed, state) {
            if (!isTimed)
                return annotation;
            var offsets = this.Offsets();
            this.AddOffset(annotation, AnnotationDefinition.StartField, offsets);
            this.AddOffset(annotation, AnnotationDefinition.EndField, offsets);
            if (this._broadcastWalltimeStart != null) {
                this.AddjustToWallTime(annotation, AnnotationDefinition.StartField);
                this.AddjustToWallTime(annotation, AnnotationDefinition.EndField);
            }
            return annotation;
        };
        AnnotationHandler.prototype.AddOffset = function (annotation, field, offsets) {
            var time = this.GetTime(annotation, field);
            for (var i = offsets.length - 1; i >= 0; i--) {
                if (offsets[i].ProgramPosition <= time) {
                    annotation[field] = (time + offsets[i].Offset).toString();
                    break;
                }
            }
        };
        AnnotationHandler.prototype.GetTime = function (annotation, field) {
            return parseInt(annotation[field]);
        };
        AnnotationHandler.prototype.AdjustFromFileOffset = function (groups, playerState) {
            var offsets = this.Offsets();
            if (this._broadcastWalltimeStart != null && offsets != null)
                this._broadcastWalltimeStart.setMilliseconds(this._broadcastWalltimeStart.getMilliseconds() - offsets[0].Offset);
            for (var _i = 0, groups_1 = groups; _i < groups_1.length; _i++) {
                var group = groups_1[_i];
                if (!AnnotationDefinition.IsTimed(AnnotationDefinition.GetById(group.DefinitionId)))
                    continue;
                for (var _a = 0, _b = group.Data; _a < _b.length; _a++) {
                    var annotation = _b[_a];
                    if (this._broadcastWalltimeStart != null) {
                        this.AddjustFromWallTime(annotation, AnnotationDefinition.StartField);
                        this.AddjustFromWallTime(annotation, AnnotationDefinition.EndField);
                    }
                    this.RemoveOffset(annotation, AnnotationDefinition.StartField, offsets);
                    this.RemoveOffset(annotation, AnnotationDefinition.EndField, offsets);
                }
            }
        };
        AnnotationHandler.prototype.RemoveOffset = function (annotation, field, offsets) {
            var time = this.GetTime(annotation, field);
            for (var i = offsets.length - 1; i >= 0; i--) {
                if (offsets[i].ProgramPosition <= time - offsets[i].Offset) {
                    annotation[field] = (time - offsets[i].Offset).toString();
                    break;
                }
            }
            if (time - offsets[0].Offset < 0)
                annotation[field] = "0";
        };
        AnnotationHandler.prototype.AddjustFromWallTime = function (annotation, field) {
            annotation[field] = (DateParser.ParseLocalUTCString(annotation[field]).getTime() - this._broadcastWalltimeStart.getTime()).toString();
        };
        AnnotationHandler.prototype.AddjustToWallTime = function (annotation, field) {
            annotation[field] = DateParser.ToLocalUTCString(new Date(this._broadcastWalltimeStart.getTime() + this.GetTime(annotation, field)), true);
        };
        AnnotationHandler.prototype.GetOffsets = function (state) {
            if (!this.AllDurationsAreReady())
                throw new Error("Durations are not ready");
            var result = [];
            var runningPosition = 0;
            var runningOffset = 0;
            for (var _i = 0, _a = state.Offsets; _i < _a.length; _i++) {
                var offset = _a[_i];
                runningOffset += offset.Start;
                result.push({ ProgramPosition: runningPosition, Offset: runningOffset });
                runningPosition += offset.FileDuration() - offset.Start - offset.End;
                runningOffset += offset.End;
            }
            return result;
        };
        return AnnotationHandler;
    }(DisposableComponent));
    return AnnotationHandler;
});
