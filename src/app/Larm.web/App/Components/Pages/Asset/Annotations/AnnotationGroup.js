var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/Pages/Asset/Annotations/Annotation", "Managers/Portal/AnnotationDefinition", "Managers/Portal/EZArchivePortal", "Components/DisposableComponent", "Managers/Profile"], function (require, exports, knockout, Annotation, AnnotationDefinition, EZArchivePortal, DisposableComponent, Profile) {
    "use strict";
    var AnnotationGroup = (function (_super) {
        __extends(AnnotationGroup, _super);
        function AnnotationGroup(annotationGroup, addedCallback, visibleCheck, filteredOwner, owners) {
            var _this = _super.call(this) || this;
            _this.Annotations = knockout.observableArray();
            _this.Name = annotationGroup.Name;
            _this.Definition = AnnotationDefinition.GetById(annotationGroup.DefinitionId);
            _this._addedCallback = addedCallback;
            _this._visibleCheck = visibleCheck;
            _this.IsTimed = AnnotationDefinition.IsTimed(_this.Definition);
            _this.FilteredOwner = filteredOwner;
            _this.Owners = owners;
            annotationGroup.Data.forEach(function (a) { return _this.AddAnnotationFromPortal(a); });
            return _this;
        }
        AnnotationGroup.prototype.CreateAnnotation = function (setCurrentUserAsOwner) {
            var annotation = new Annotation(this.Definition.Fields, this.IsTimed, this._visibleCheck);
            if (setCurrentUserAsOwner) {
                annotation.OwnerId(Profile.Identifier());
                annotation.Owner(Profile.Name());
                annotation.Permission(EZArchivePortal.AnnotationPermission.All);
                annotation.Visibility("Public");
            }
            return annotation;
        };
        AnnotationGroup.prototype.AddAnnotation = function (annotation) {
            this.Annotations.push(annotation);
            this._addedCallback(annotation, this);
        };
        AnnotationGroup.prototype.AddAnnotationFromPortal = function (annotation) {
            var result = this.CreateAnnotation(false);
            result.ParsePortalAnnotation(annotation);
            result.IsServerSide(true);
            this.AddAnnotation(result);
        };
        return AnnotationGroup;
    }(DisposableComponent));
    return AnnotationGroup;
});
