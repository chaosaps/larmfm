﻿import jquery = require("jquery");
import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import AnnotationDefinition = require("Managers/Portal/AnnotationDefinition");
import Annotation = require("Components/Pages/Asset/Annotations/Annotation");
import AnnotationGroup = require("Components/Pages/Asset/Annotations/AnnotationGroup");
import Notification = require("Managers/Notification");
import PlayerState = require("Components/Pages/Asset/Players/State");
import DisposableComponent = require("Components/DisposableComponent");
import DateParser = require("Utility/DateParser");

type OffsetMark = { ProgramPosition: number, Offset: number };
type Owner = { Name: string, Id: string };

class AnnotationHandler extends DisposableComponent
{
	public AnnotationGroups: KnockoutObservableArray<AnnotationGroup> = knockout.observableArray<AnnotationGroup>();
	public SelectedAnnotation: KnockoutObservable<Annotation> = knockout.observable<Annotation>(null);
	public AllDurationsAreReady: KnockoutComputed<boolean>;
	public Offsets: KnockoutComputed<OffsetMark[]>;
	public Owners: KnockoutComputed<Owner[]>;
	public FilteredOwner: KnockoutObservable<Owner>;

	private _broadcastWalltimeStart:Date = null;
	private _assetId: string;
	private _playerState: PlayerState;

	private _annotationToSelectOnLoad:string = null;

	constructor(assetId:string, playerState:PlayerState)
	{
		super();
		this._assetId = assetId;
		this._playerState = playerState;
		this.AllDurationsAreReady = this.PureComputed(() => this._playerState.AreFilesReady() && this._playerState.Files() != null && this._playerState.Offsets != null && this._playerState.Offsets.every(o => o.FileDuration() != null));
		this.Offsets = this.PureComputed(() => this.AllDurationsAreReady() ? this.GetOffsets(this._playerState) : null);

		this.Owners = this.PureComputed(() =>
		{
			const result: Owner[] = [{ Name: "Alle", Id: null }];
			const userHash: { [key: string]: boolean } = {}

			for (const group of this.AnnotationGroups())
			{
				for (let annotaion of group.Annotations())
				{
					if (userHash.hasOwnProperty(annotaion.OwnerId()))
						continue;

					userHash[annotaion.OwnerId()] = true;

					result.push({ Name: annotaion.Owner(), Id: annotaion.OwnerId() });
				}
			}

			return result;
		});
		this.FilteredOwner = knockout.observable(this.Owners()[0]);
	}

	public Load(assetTypeId: string, annotations: EZArchivePortal.IAnnotationGroup[], broadcastWalltimeStart:Date):void
	{
		this._broadcastWalltimeStart = broadcastWalltimeStart;
		this.AnnotationGroups.removeAll();

		this.AddAction(() => AnnotationDefinition.IsReady() && this._playerState.HasFiles() && (!this._playerState.UsesTimedTools() || this.AllDurationsAreReady()), () => this.InnerLoad(assetTypeId, annotations));
	}

	public GetActiveGroup():AnnotationGroup
	{
		var id = this.GetActiveDefinition();

		if (id == null) return null;

		for (let g of this.AnnotationGroups())
		{
			if (g.Definition.Identifier === id)
				return g;
		}

		return null;
	}

	public SelectAnnotation(annotationIdentifier: string)
	{
		if (this.SelectedAnnotation() != null && this.SelectedAnnotation().Identifier === annotationIdentifier) return;

		if (this.AnnotationGroups().length === 0)
		{
			this._annotationToSelectOnLoad = annotationIdentifier;
			return;
		}

		if (annotationIdentifier == null)
			this.SelectedAnnotation(null);

		for (let group of this.AnnotationGroups())
		{
			for (let annotation of group.Annotations())
			{
				if (annotation.Identifier === annotationIdentifier)
				{
					annotation.IsSelected(true);
					return;
				}
			}
		}
	}

	private InnerLoad(assetTypeId:string, groups:EZArchivePortal.IAnnotationGroup[]):void
	{
		this.AdjustFromFileOffset(groups, this._playerState);

		const newGroups = AnnotationDefinition.GetByAssetType(assetTypeId)
			.filter(d => !groups.some(g => g.DefinitionId === d.Identifier))
			.map(d => this.CreateEmptyAnnotationGroup(d));

		newGroups.push(...groups.map(g => this.CreateAnnotationGroup(g)));
		newGroups.sort((a, b) => a.Name.localeCompare(b.Name));

		this.AnnotationGroups.push(...newGroups);

		if (this._annotationToSelectOnLoad != null)
		{
			this.SelectAnnotation(this._annotationToSelectOnLoad);
			this._annotationToSelectOnLoad = null;
		}
	}

	private CreateAnnotationGroup(data:EZArchivePortal.IAnnotationGroup):AnnotationGroup
	{
		return new AnnotationGroup(data,
			(a, ag) => this.AnnotationAdded(a, ag),
			ownerId => this.FilteredOwner().Id === null || this.FilteredOwner().Id === ownerId,
			this.FilteredOwner,
			this.Owners);
	}

	private CreateEmptyAnnotationGroup(definition:EZArchivePortal.IEZAnnotationDefinition): AnnotationGroup
	{
		return this.CreateAnnotationGroup({ DefinitionId: definition.Identifier, Name: definition.Name, Data: [] });
	}

	private AnnotationAdded(annotation:Annotation, group:AnnotationGroup):void
	{
		this.ShowList(group);

		annotation.AddSaveCallback(a => this.SaveAnnotation(a, group));
		annotation.AddDeleteCallback(a => this.DeleteAnnotation(a, group));
		annotation.AddPlayCallback(a => this.PlayAnnotation(a));

		this.Subscribe(annotation.Permission,
			v =>
			{
				EZArchivePortal.Annotation.SetPermission(this._assetId, annotation.Identifier, v)
					.WithCallback(response =>
						{
							if (response.Error != null)
							{
								Notification.Error("Failed to set annotation permission: " + response.Error.Message);
								return;
							}
						});
			});

		this.Subscribe(annotation.Visibility, v => { annotation.Save(); });

		this.Subscribe(annotation.IsSelected,
			v =>
			{
				var currentSelection = this.SelectedAnnotation();

				if (v && currentSelection !== annotation)
				{
					if (currentSelection != null)
						currentSelection.IsSelected(false);
					this.SelectedAnnotation(annotation);
					annotation.IsSelected(true);

					this.ShowList(group);
				}
			});
	}

	private SaveAnnotation(annotation: Annotation, group:AnnotationGroup): void
	{
		EZArchivePortal.Annotation.Set(this._assetId, 
				group.Definition.Identifier,
				this.AdjustToFileOffsets(annotation.ToPortalAnnotation(), annotation.IsTimed, this._playerState),
				{ Scope: annotation.Visibility()})
			.WithCallback(response =>
				{
				if (response.Error != null)
				{
					Notification.Error("Failed to save annotation: " + response.Error.Message);
					return;
				}

				annotation.Identifier = response.Body.Results[0].Identifier;
				annotation.IsServerSide(true);
			});
	}

	private DeleteAnnotation(annotation: Annotation, group: AnnotationGroup): void
	{
		if (annotation.IsServerSide())
		{
			EZArchivePortal.Annotation.Delete(this._assetId, annotation.Identifier).WithCallback(response =>
			{
				if (response.Error != null)
					Notification.Error("Failed to delete annotation: " + response.Error.Message);
			});
		}

		group.Annotations.remove(annotation);
	}

	private PlayAnnotation(annotation: Annotation): void
	{
		this._playerState.Position(annotation.Start().valueOf());
		this._playerState.IsPlaying(true);
	}

	private GetActiveDefinition():string
	{
		var value = jquery(".TabContainer .nav-tabs li.active a").attr("href");

		if (value == undefined) return null;

		var result = /#(.+)Tab/.exec(value);

		if (result == null) return null;

		return result[1];
	}

	private ShowList(group:AnnotationGroup): void
	{
		jquery(`.TabContainer .nav-tabs a[href="#${group.Definition.Identifier}Tab"]`).tab("show");
	}

	private AdjustToFileOffsets(annotation: EZArchivePortal.IAnnotation, isTimed:boolean, state: PlayerState): EZArchivePortal.IAnnotation
	{
		if (!isTimed) return annotation;

		var offsets = this.Offsets();

		this.AddOffset(annotation, AnnotationDefinition.StartField, offsets);
		this.AddOffset(annotation, AnnotationDefinition.EndField, offsets);

		if (this._broadcastWalltimeStart != null)
		{
			this.AddjustToWallTime(annotation, AnnotationDefinition.StartField);
			this.AddjustToWallTime(annotation, AnnotationDefinition.EndField);
		}

		return annotation;
	}

	private AddOffset(annotation:EZArchivePortal.IAnnotation, field:string, offsets:OffsetMark[]):void
	{
		const time = this.GetTime(annotation, field);
		for (let i = offsets.length - 1; i >= 0; i--)
		{
			if (offsets[i].ProgramPosition <= time)
			{
				annotation[field] = (time + offsets[i].Offset).toString();
				break;
			}
		}
	}

	private GetTime(annotation:EZArchivePortal.IAnnotation, field:string):number
	{
		return parseInt(annotation[field]);
	}

	private AdjustFromFileOffset(groups:EZArchivePortal.IAnnotationGroup[], playerState:PlayerState):void
	{
		var offsets = this.Offsets();

		if (this._broadcastWalltimeStart != null && offsets != null)
			this._broadcastWalltimeStart.setMilliseconds(this._broadcastWalltimeStart.getMilliseconds() - offsets[0].Offset);

		for (let group of groups)
		{
			if(!AnnotationDefinition.IsTimed(AnnotationDefinition.GetById(group.DefinitionId)))
				continue;
			for (let annotation of group.Data)
			{
				if (this._broadcastWalltimeStart != null)
				{
					this.AddjustFromWallTime(annotation, AnnotationDefinition.StartField);
					this.AddjustFromWallTime(annotation, AnnotationDefinition.EndField);
				}

				this.RemoveOffset(annotation, AnnotationDefinition.StartField, offsets);
				this.RemoveOffset(annotation, AnnotationDefinition.EndField, offsets);
			}
		}
	}

	private RemoveOffset(annotation: EZArchivePortal.IAnnotation, field: string, offsets: OffsetMark[]): void
	{
		const time = this.GetTime(annotation, field);

		for (let i = offsets.length - 1; i >= 0; i--)
		{
			if (offsets[i].ProgramPosition <= time - offsets[i].Offset)
			{
				annotation[field] = (time - offsets[i].Offset).toString();
				break;
			}
		}

		if (time - offsets[0].Offset < 0)
			annotation[field] = "0";
	}

	private AddjustFromWallTime(annotation: EZArchivePortal.IAnnotation, field: string): void
	{
		annotation[field] = (DateParser.ParseLocalUTCString(annotation[field]).getTime() - this._broadcastWalltimeStart.getTime()).toString();
	}

	private AddjustToWallTime(annotation: EZArchivePortal.IAnnotation, field: string): void
	{
		annotation[field] = DateParser.ToLocalUTCString(new Date(this._broadcastWalltimeStart.getTime() + this.GetTime(annotation, field)), true);
	}

	private GetOffsets(state:PlayerState):OffsetMark[]
	{
		if (!this.AllDurationsAreReady()) throw new Error("Durations are not ready");

		var result: OffsetMark[] = [];
		var runningPosition: number = 0;
		var runningOffset:number = 0;

		for (let offset of state.Offsets)
		{
			runningOffset += offset.Start;

			result.push({ ProgramPosition: runningPosition, Offset: runningOffset });

			runningPosition += offset.FileDuration() - offset.Start - offset.End;
			runningOffset += offset.End;
		}

		return result;
	}
}

interface IArrayChangeEvent<T>
{
	index: number;
	status: string,
	value: T;
}

export = AnnotationHandler;