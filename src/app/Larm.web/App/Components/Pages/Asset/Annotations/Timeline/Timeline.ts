﻿import knockout = require("knockout");
import moment = require("moment");
import vis = require("vis");
import jquery = require("jquery");
import Annotation = require("Components/Pages/Asset/Annotations/Annotation");
import DisposableComponent = require("Components/DisposableComponent")
import PlayerState = require("Components/Pages/Asset/Players/State");
import Permissions = require("Managers/Permissions");
import AnnotationGroup = require("Components/Pages/Asset/Annotations/AnnotationGroup");

class Timeline extends DisposableComponent
{
	public TimelineElement: KnockoutObservable<Element> = knockout.observable<Element>();
	public IsDraggingTimeline = knockout.observable(false);
	
	private _annotationGroups: KnockoutObservableArray<AnnotationGroup>;
	private _timeline: vis.Timeline;
	private _data: vis.DataSet;
	private _options:vis.ITimelineOptions;
	private _isAdding: boolean = false;
	private _playerState: PlayerState;
	private _getActiveGroup:()=>AnnotationGroup;

	constructor(data: { Groups: KnockoutObservableArray<AnnotationGroup>, GetActiveGroup:()=>AnnotationGroup, PlayerState: PlayerState})
	{
		super();
		
		this._annotationGroups = data.Groups;
		this._getActiveGroup = data.GetActiveGroup;
		this._playerState = data.PlayerState;
		this._data = new vis.DataSet(this.GetAllAnnotations(this._annotationGroups()).map(a => this.ToTimelineAnnotation(a, true)), { type: { start: "Moment", end: "Moment" } });
		this.InitializeOptions();
		this.InitializeEvents();
	}

	private GetAllAnnotations(groups:AnnotationGroup[]):Annotation[]
	{
		const lists = groups.filter(g => g.IsTimed).map(g => g.Annotations());

		switch (lists.length)
		{
			case 0:
				return [];
			case 1:
				return lists[0];
			default:
				return new Array<Annotation>().concat(...lists);
		}
	}

	private InitializeOptions():void
	{
		let duration = this._playerState.Duration();
		if (!this.isNumberValid(duration)) duration = 1000 * 60 * 30;

		this._options = {
			min: 0,
			max: duration,
			start: 0,
			end: duration,
			selectable: true,
			showCurrentTime: false,
			zoomMin: 1000,
			showMajorLabels: false,
			clickToUse: false,
			format: {
				minorLabels: {
					millisecond: "mm:ss.S",
					second: "mm:ss",
					minute: "mm:ss",
					hour: "HH:mm",
					weekday: "HH:mm",
					day: "HH:mm",
					month: "HH:mm",
					year: "HH:mm"
				},
				majorLabels: {
					millisecond: "HH:mm",
					second: "HH:mm",
					minute: "HH:mm",
					hour: "HH:mm",
					weekday: "HH:mm",
					day: "HH:mm",
					month: "HH:mm",
					year: "HH:mm"
				}
			},
			maxHeight: "300px",
			editable: Permissions.CanEditAnnotations(),
			snap: null,
			moment: (date: Date) => moment(date).utc(),
			onAdd: (i, c) => this.AddAnnotation(i, c),
			onUpdate: (i, c) => this.EditAnnotation(i, c),
			onMove: (i, c) => this.MoveAnnotation(i, c),
			onRemove: (i, c) => this.RemoveAnnotation(i, c)
		};
	}

	private InitializeEvents():void
	{
		this.SubscribeUntilChange(this.TimelineElement, e => this.InitializeTimeline(e));

		this._annotationGroups().forEach(g =>
		{
			if (g.IsTimed)
				this.SubscribeToAnnotationGroup(g);
		});
		this.SubscribeToArray(this._annotationGroups, (group, status) =>
		{
			if (status === "added" && group.IsTimed)
			{
				this.SubscribeToAnnotationGroup(group);

				if (group.Annotations().length > 0)
				{
					this._data.add(group.Annotations().map(a => this.ToTimelineAnnotation(a, true)));
					this.UpdateDeleteButton();
				}
			}
		});

		this.Subscribe(this._playerState.Duration, v => this.UpdateDuration(v));
		this.SubscribeUntilChange(this._playerState.IsReady, v => this.InitializePosition());
		this.Subscribe(Permissions.CanEditAnnotations, v =>
		{
			this._options.editable = v;
			this.UpdateOptions();
		});
	}

	private SubscribeToAnnotationGroup(group: AnnotationGroup):void
	{
		this.SubscribeToArray(group.Annotations, (annotation, status) =>
		{
			if (!this._isAdding && status === "added") {
				this._data.add(this.ToTimelineAnnotation(annotation, true));
				this.UpdateDeleteButton();
			}
		});
	}

	private InitializePosition(): void
	{
		if (this._timeline == null || !this._playerState.IsReady()) return;

		var startPosition = this._playerState.Position() != null ? this._playerState.Position() : 0;
		this._timeline.addCustomTime(startPosition, "PlayerPosition");

		var isUpdatingPosition = false;
		var isDraggingTime = false;
		this._playerState.Position.subscribe(v =>
		{
			if (isUpdatingPosition || isDraggingTime || v == null) return;
			isUpdatingPosition = true;
			this._timeline.setCustomTime(v, "PlayerPosition");
			isUpdatingPosition = false;
		});
		this._timeline.on("timechanged", e =>
		{
			isDraggingTime = false;
			if (isUpdatingPosition) return;
			isUpdatingPosition = true;
			this._playerState.Position(e.time.getTime());
			isUpdatingPosition = false;
		});
		this._timeline.on("timechange", e =>
		{
			isDraggingTime = true;
		});
		this._timeline.on("rangechange", e =>
		{
			this.IsDraggingTimeline(true);
		});
		this._timeline.on("rangechanged", e =>
		{
			this.IsDraggingTimeline(false);
		});
	}

	private InitializeTimeline(element:Element):void
	{
		this._timeline = new vis.Timeline(element, this._data, this._options);
		this._timeline.on("select", (e) =>
		{
			if (e.items.length === 1)
			{
				this.GetAnnotation(e.items[0]).IsSelected(true);
				this.UpdateDeleteButton();
			}
		});
		this.InitializePosition();
	}

	private UpdateOptions():void
	{
		if (this._timeline != null)
			this._timeline.setOptions(this._options);
	}

	private AddAnnotation(item: ITimelineAnnotation, callback: (item: any) => void): void
	{
		this._isAdding = true;
		item.content = "";
		item.end = moment(item.start);
		item.end.add(30, "s");
		item.editable = true;

		const group = this._getActiveGroup() || this._annotationGroups()[0];
		var annotation = group.CreateAnnotation(true);
		this.UpdateAnnotationWithTimelineItem(annotation, item);
		group.AddAnnotation(annotation);

		item.id = annotation.Id;

		this.AddCallbacks(annotation);

		setTimeout(() =>
			{
				annotation.Edit();
				annotation.IsSelected(true);
			},
			0);

		callback(item);

		this._isAdding = false;

		this.UpdateDeleteButton();
	}

	private EditAnnotation(item: ITimelineAnnotation, callback: (item: any) => void): void
	{
		const annotation = this.GetAnnotation(item.id);

		annotation.Edit();
		callback(null);
	}

	private MoveAnnotation(item: ITimelineAnnotation, callback: (item: any) => void): void
	{
		this.UpdateAnnotationWithTimelineItem(this.GetAnnotation(item.id), item).Save();

		callback(item);
	}

	private RemoveAnnotation(item: ITimelineAnnotation, callback: (item: any) => void): void
	{
		callback(null);
		this.GetAnnotation(item.id).RequestDelete();
	}

	private UpdateAnnotationWithTimelineItem(annotation: Annotation, item: ITimelineAnnotation): Annotation
	{
		if (item.start instanceof Date)
			item.start = moment(item.start);
			
		if (item.end instanceof Date)
			item.end = moment(item.end);

		if (item.start.valueOf() < 0)
			item.start = moment(0);
		else if (item.start.valueOf() > this._options.max)
			item.start = moment(this._options.max - 1000);

		if (item.end.valueOf() < 0)
			item.end = moment(1000);
		else if (item.end.valueOf() > this._options.max)
			item.end = moment(this._options.max);

		annotation.Start(item.start);
		annotation.End(item.end);

		return annotation;
	}

	private UpdateTimelineItemWithAnnotation(item: ITimelineAnnotation, annotation: Annotation): ITimelineAnnotation
	{
		item.id = annotation.Id;
		item.start = annotation.Start();
		item.end = annotation.End();
		item.content = annotation.Title();
		item.editable = annotation.CanEdit();

		return item;
	}

	private ToTimelineAnnotation(annotation: Annotation, addCallback: boolean):ITimelineAnnotation
	{
		if (addCallback)
			this.AddCallbacks(annotation);

		return this.UpdateTimelineItemWithAnnotation(<any>{}, annotation);
	}

	private AddCallbacks(annotation:Annotation):void
	{
		annotation.AddSaveCallback(a => this.AnnotationUpdated(a));
		annotation.AddDeleteCallback(a => this.AnnotationDeleted(a));
		this.Subscribe(annotation.IsSelected,
			v =>
			{
				if (v)
					this.AnnotationSelected(annotation);
			});
		this.Subscribe(annotation.IsVisible,
			v =>
			{
				if (v)
				{
					this._data.add(this.ToTimelineAnnotation(annotation, false));
					this.UpdateDeleteButton();
				}
				else
					this._data.remove(annotation.Id);
			});
	}

	private GetAnnotation(id:number):Annotation
	{
		this._annotationGroups().forEach(g => g.Annotations);

		let groups = this._annotationGroups();
		for (let group of groups)
		{
			let annotations = group.Annotations();
			for (let annotation of annotations)
			{
				if (annotation.Id === id)
					return annotation;
			}
		}

		throw new Error(`Annotation with id: ${id} not found`);
	}

	private AnnotationUpdated(annotation: Annotation): void
	{
		this._data.update(this.ToTimelineAnnotation(annotation, false));
	}

	private AnnotationDeleted(annotation:Annotation):void
	{
		this._data.remove(annotation.Id);
	}

	private AnnotationSelected(annotation: Annotation): void
	{
		this._timeline.setSelection(annotation.Id, {focus: true});
	}

	private UpdateDuration(duration: number): void
	{
		if (!this.isNumberValid(duration))
			return;

		this._options.max = duration;
		this._options.end = duration;

		this.UpdateOptions();
	}

	private isNumberValid(value: number): boolean
	{
		return typeof value === "number" && isFinite(value);
	}

	private UpdateDeleteButton(): void
	{
		setTimeout(() =>
		{
			jquery(this.TimelineElement())
				.find(".vis-delete")
				.each((index, deleteElement) => (<any>deleteElement).title = "Slet annotation");
		});
	}
}

interface ITimelineAnnotation
{
	id: number;
	start: moment.Moment;
	end: moment.Moment;
	content: string;
	editable: boolean;
}

export = Timeline;