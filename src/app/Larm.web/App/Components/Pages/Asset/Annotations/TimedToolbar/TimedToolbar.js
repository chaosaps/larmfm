var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "moment", "Components/DisposableComponent", "Managers/Permissions", "Managers/Configuration", "Managers/Profile", "Managers/Dialog/Dialog"], function (require, exports, knockout, moment, DisposableComponent, Permissions, Configuration, Profile, DialogManager) {
    "use strict";
    var TimedToolbar = (function (_super) {
        __extends(TimedToolbar, _super);
        function TimedToolbar(data) {
            var _this = _super.call(this) || this;
            _this.JumpLength = knockout.observable(Configuration.TimedToolbar.DefaultJumpLength);
            _this.JumpLengths = knockout.observableArray();
            _this.IsLoopingAnnotation = knockout.observable(false);
            _this.CanCreate = Permissions.CanEditAnnotations;
            _this.CanEdit = Permissions.CanEditAnnotations;
            _this.CanShowFullLength = _this.PureComputed(function () { return _this._playerState.Type() === "Audio"; });
            _this._selectedAnnotation = data.SelectedAnnotation;
            _this._annotationGroups = data.Groups;
            _this._getActiveGroup = data.GetActiveGroup;
            _this._playerState = data.PlayerState;
            _this.InitializePosition();
            _this.UpdateJumpLengths();
            _this.IsFilterReady = _this.PureComputed(function () { return data.Groups().length !== 0; });
            _this.AddAction(_this.IsFilterReady, function () {
                _this.FilteredOwner = data.Groups()[0].FilteredOwner;
                _this.Owners = data.Groups()[0].Owners;
                _this.CanFilterOnUser = _this.PureComputed(function () {
                    var identifier = Profile.Identifier();
                    return identifier != null && _this.Owners().some(function (o) { return o.Id === identifier; });
                });
            });
            _this.Subscribe(_this._playerState.Duration, function () { return _this.UpdateJumpLengths(); });
            return _this;
        }
        TimedToolbar.prototype.FilterOnUser = function () {
            var identifier = Profile.Identifier();
            for (var _i = 0, _a = this.Owners(); _i < _a.length; _i++) {
                var owner = _a[_i];
                if (owner.Id == null || owner.Id !== identifier)
                    continue;
                this.FilteredOwner(owner);
            }
        };
        TimedToolbar.prototype.Create = function () {
            var group = this.GetGroup();
            if (group.IsTimed && this._playerState.Position() == null)
                return;
            var annotation = group.CreateAnnotation(true);
            if (annotation.IsTimed) {
                annotation.Start(moment(this._playerState.Position()));
                var end = moment(annotation.Start());
                end.add(30, "s");
                annotation.End(end);
            }
            group.AddAnnotation(annotation);
            annotation.Edit();
            annotation.IsSelected(true);
        };
        TimedToolbar.prototype.SetIn = function () {
            var position = this._playerState.Position();
            if (position == null)
                return;
            var selection = this._selectedAnnotation();
            if (selection == null)
                return;
            var newTime = moment(position);
            if (newTime.isAfter(selection.End()))
                return;
            selection.Start(newTime);
            selection.Save();
        };
        TimedToolbar.prototype.SetOut = function () {
            var position = this._playerState.Position();
            if (position == null)
                return;
            var selection = this._selectedAnnotation();
            if (selection == null)
                return;
            var newTime = moment(position);
            if (newTime.isBefore(selection.Start()))
                return;
            selection.End(newTime);
            selection.Save();
        };
        TimedToolbar.prototype.JumpBackward = function () {
            this._playerState.Position(Math.max(0, this._playerState.Position() - this.JumpLength()));
        };
        TimedToolbar.prototype.JumpForward = function () {
            this._playerState.Position(Math.min(this._playerState.Duration(), this._playerState.Position() + this.JumpLength()));
        };
        TimedToolbar.prototype.ToggleLoopAnnotation = function () {
            if (this._selectedAnnotation() == null)
                return;
            this.IsLoopingAnnotation(!this.IsLoopingAnnotation());
        };
        TimedToolbar.prototype.ShowFullLength = function () {
            DialogManager.Show("Pages/Asset/FullLengthDialog", { State: this._playerState }, true, true);
        };
        TimedToolbar.prototype.InitializePosition = function () {
            var _this = this;
            var isUpdating = false;
            this.Subscribe(this._playerState.Position, function (v) {
                if (!isUpdating && _this.IsLoopingAnnotation()) {
                    var selection = _this._selectedAnnotation();
                    if (selection != null) {
                        var start = selection.Start().valueOf();
                        var end = selection.End().valueOf();
                        if (v < start - 100) {
                            isUpdating = true;
                            setTimeout(function () {
                                _this._playerState.Position(start);
                                isUpdating = false;
                            }, 10);
                        }
                        else if (v > end) {
                            isUpdating = true;
                            setTimeout(function () {
                                _this._playerState.Position(start);
                                isUpdating = false;
                            }, 10);
                        }
                    }
                }
            });
        };
        TimedToolbar.prototype.UpdateJumpLengths = function () {
            this.JumpLengths.removeAll();
            var duration = this._playerState.Duration();
            if (duration == null || duration === 0)
                return;
            for (var _i = 0, _a = Configuration.TimedToolbar.JumpLengths; _i < _a.length; _i++) {
                var jumpLength = _a[_i];
                if (duration < jumpLength)
                    break;
                var isSecondSized = jumpLength < 60000;
                this.JumpLengths.push({ Value: jumpLength, Text: isSecondSized ? jumpLength / 1000 + Configuration.TimedToolbar.JumpLengthSecondText : jumpLength / 60000 + Configuration.TimedToolbar.JumpLengthMinuteText });
            }
            this.JumpLength(Configuration.TimedToolbar.DefaultJumpLength);
        };
        TimedToolbar.prototype.GetGroup = function () {
            var activeGroup = this._getActiveGroup();
            if (activeGroup && activeGroup.IsTimed)
                return activeGroup;
            var firstTimedGroup = this.GetFirstTimedGroup(this._annotationGroups());
            if (firstTimedGroup && firstTimedGroup.IsTimed)
                return firstTimedGroup;
            if (activeGroup)
                return activeGroup;
            return this._annotationGroups()[0];
        };
        TimedToolbar.prototype.GetFirstTimedGroup = function (groups) {
            for (var _i = 0, groups_1 = groups; _i < groups_1.length; _i++) {
                var group = groups_1[_i];
                if (group.IsTimed)
                    return group;
            }
            return null;
        };
        return TimedToolbar;
    }(DisposableComponent));
    return TimedToolbar;
});
