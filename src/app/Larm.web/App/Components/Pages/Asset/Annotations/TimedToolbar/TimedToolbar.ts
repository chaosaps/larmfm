﻿import knockout = require("knockout");
import moment = require("moment");
import Annotation = require("Components/Pages/Asset/Annotations/Annotation");
import AnnotationGroup = require("Components/Pages/Asset/Annotations/AnnotationGroup");
import DisposableComponent = require("Components/DisposableComponent")
import PlayerState = require("Components/Pages/Asset/Players/State");
import Permissions = require("Managers/Permissions");
import Configuration = require("Managers/Configuration");
import Profile = require("Managers/Profile");
import DialogManager = require("Managers/Dialog/Dialog");

type Option = { Value: number; Text: string };
type Owner = { Name: string, Id: string };

class TimedToolbar extends DisposableComponent
{
	public CanEdit: KnockoutComputed<boolean>;
	public CanCreate: KnockoutComputed<boolean>;
	public CanShowFullLength: KnockoutComputed<boolean>;
	public JumpLength = knockout.observable(Configuration.TimedToolbar.DefaultJumpLength);
	public JumpLengths = knockout.observableArray<Option>();
	public IsLoopingAnnotation: KnockoutObservable<boolean> = knockout.observable(false);

	public IsFilterReady: KnockoutComputed<boolean>;
	public Owners: KnockoutComputed<Owner[]>;
	public FilteredOwner: KnockoutObservable<Owner>;
	public CanFilterOnUser: KnockoutComputed<boolean>;

	private _selectedAnnotation: KnockoutObservable<Annotation>;
	private _playerState: PlayerState;
	private _annotationGroups: KnockoutObservableArray<AnnotationGroup>;
	private _getActiveGroup: () => AnnotationGroup;

	constructor(data: { SelectedAnnotation: KnockoutObservable<Annotation>, Groups: KnockoutObservableArray<AnnotationGroup>, GetActiveGroup: () => AnnotationGroup, PlayerState: PlayerState })
	{
		super();
		this.CanCreate = Permissions.CanEditAnnotations;
		this.CanEdit = Permissions.CanEditAnnotations;
		this.CanShowFullLength = this.PureComputed(() => this._playerState.Type() === "Audio");
		this._selectedAnnotation = data.SelectedAnnotation;
		this._annotationGroups = data.Groups;
		this._getActiveGroup = data.GetActiveGroup;
		this._playerState = data.PlayerState;
		this.InitializePosition();
		this.UpdateJumpLengths();

		this.IsFilterReady = this.PureComputed(() => data.Groups().length !== 0);

		this.AddAction(this.IsFilterReady, () =>
		{
			this.FilteredOwner = data.Groups()[0].FilteredOwner;
			this.Owners = data.Groups()[0].Owners;
			this.CanFilterOnUser = this.PureComputed(() =>
			{
				var identifier = Profile.Identifier();
				return identifier != null && this.Owners().some(o => o.Id === identifier);
			});
		});		

		this.Subscribe(this._playerState.Duration, () => this.UpdateJumpLengths());
	}

	public FilterOnUser(): void
	{
		var identifier = Profile.Identifier();

		for (const owner of this.Owners())
		{
			if (owner.Id == null || owner.Id !== identifier)
				continue;

			this.FilteredOwner(owner);
		}
	}

	public Create():void
	{
		var group = this.GetGroup();

		if (group.IsTimed && this._playerState.Position() == null) return;

		var annotation = group.CreateAnnotation(true);
		
		if (annotation.IsTimed)
		{
			annotation.Start(moment(this._playerState.Position()));

			let end = moment(annotation.Start());
			end.add(30, "s");

			annotation.End(end);
		}
		
		group.AddAnnotation(annotation);
		annotation.Edit();
		annotation.IsSelected(true);
	}

	public SetIn(): void
	{
		var position = this._playerState.Position();

		if (position == null) return;

		var selection = this._selectedAnnotation();

		if (selection == null) return;

		var newTime = moment(position);

		if (newTime.isAfter(selection.End())) return;

		selection.Start(newTime);
		selection.Save();
	}

	public SetOut(): void
	{
		var position = this._playerState.Position();

		if (position == null) return;

		var selection = this._selectedAnnotation();

		if (selection == null) return;

		var newTime = moment(position);

		if (newTime.isBefore(selection.Start())) return;

		selection.End(newTime);
		selection.Save();
	}

	public JumpBackward():void
	{
		this._playerState.Position(Math.max(0, this._playerState.Position() - this.JumpLength()));
	}

	public JumpForward(): void
	{
		this._playerState.Position(Math.min(this._playerState.Duration(), this._playerState.Position() + this.JumpLength()));
	}

	public ToggleLoopAnnotation():void
	{
		if (this._selectedAnnotation() == null)
			return;

		this.IsLoopingAnnotation(!this.IsLoopingAnnotation());
	}

	public ShowFullLength(): void
	{
		DialogManager.Show("Pages/Asset/FullLengthDialog", { State: this._playerState }, true, true);
	}

	private InitializePosition():any
	{
		var isUpdating = false;
		this.Subscribe(this._playerState.Position, v =>
		{
			if (!isUpdating && this.IsLoopingAnnotation())
			{
				var selection = this._selectedAnnotation();

				if (selection != null)
				{
					var start = selection.Start().valueOf();
					var end = selection.End().valueOf();
					if (v < start - 100)
					{
						isUpdating = true;
						setTimeout(() =>
						{
							this._playerState.Position(start);
							isUpdating = false;
						}, 10);
					}
					else if (v > end)
					{
						isUpdating = true;
						setTimeout(() =>
						{
							this._playerState.Position(start);
							isUpdating = false;
						}, 10);
					}
				}
			}
		});
	}

	private UpdateJumpLengths(): void
	{
		this.JumpLengths.removeAll();

		const duration = this._playerState.Duration();

		if (duration == null || duration === 0) return;

		for (let jumpLength of Configuration.TimedToolbar.JumpLengths)
		{
			if (duration < jumpLength) break;

			let isSecondSized = jumpLength < 60000;

			this.JumpLengths.push({ Value: jumpLength, Text: isSecondSized ? jumpLength / 1000 + Configuration.TimedToolbar.JumpLengthSecondText : jumpLength / 60000 + Configuration.TimedToolbar.JumpLengthMinuteText });
		}

		this.JumpLength(Configuration.TimedToolbar.DefaultJumpLength);
	}

	private GetGroup(): AnnotationGroup
	{
		const activeGroup = this._getActiveGroup();

		if (activeGroup && activeGroup.IsTimed)
			return activeGroup;

		const firstTimedGroup = this.GetFirstTimedGroup(this._annotationGroups());

		if (firstTimedGroup && firstTimedGroup.IsTimed)
			return firstTimedGroup;

		if (activeGroup)
			return activeGroup;

		return this._annotationGroups()[0];
	}

	private GetFirstTimedGroup(groups: AnnotationGroup[]): AnnotationGroup
	{
		for (let group of groups)
		{
			if (group.IsTimed)
				return group;
		}

		return null;
	}
}

export = TimedToolbar;
