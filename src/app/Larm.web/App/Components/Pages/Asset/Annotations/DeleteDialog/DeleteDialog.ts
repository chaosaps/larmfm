﻿import knockout = require("knockout");
import DialogState = require("Managers/Dialog/DialogState");
import PortChecker = require("Managers/PortChecker");
import DisposableComponent = require("Components/DisposableComponent");
import Annotation = require("Components/Pages/Asset/Annotations/Annotation");

class DeleteDialog extends DisposableComponent
{
	public Title: KnockoutComputed<string>;

	private _state: DialogState<Annotation>;

	constructor(state: DialogState<Annotation>)
	{
		super();
		this._state = state;

		this.Title = this._state.Data.Title;
	}

	public Delete(): void
	{
		this._state.Data.Delete();
		this._state.Close();
	}
}

export = DeleteDialog;