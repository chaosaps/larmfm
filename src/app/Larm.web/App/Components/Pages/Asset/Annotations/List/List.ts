﻿import moment = require("moment");
import Annotation = require("Components/Pages/Asset/Annotations/Annotation");
import AnnotationGroup = require("Components/Pages/Asset/Annotations/AnnotationGroup");
import Permissions = require("Managers/Permissions");
import DisposableComponent = require("Components/DisposableComponent")
import { SaveAsCsv } from "Utility/FileSaver";

type Owner = { Name: string, Id: string };

class AnnotationList extends DisposableComponent
{
	public Annotations: KnockoutComputed<Annotation[]>;
	public IsTimed: boolean;
	public CanEdit: KnockoutObservable<boolean>;
	public Name: string;
	public Owners: KnockoutComputed<Owner[]>;
    public FilteredOwner: KnockoutObservable<Owner>;
    public CanExport: KnockoutComputed<boolean>;

	private _group: AnnotationGroup;
	private _link: string;

	constructor(data: { Group: AnnotationGroup, Link: string })
	{
		super();
		this._group = data.Group;
		this._link = data.Link;
		this.CanEdit = Permissions.CanEditAnnotations;
		this.Name = this._group.Name;

		this.IsTimed = this._group.IsTimed;
		this.Owners = data.Group.Owners;
        this.FilteredOwner = data.Group.FilteredOwner;

	    this.CanExport = this.PureComputed(() => this.Annotations().filter(a => a.IsVisible()).length !== 0);
		
		this.Annotations = this.PureComputed(() => this.IsTimed
				? this._group.Annotations().slice(0).sort((a, b) => a.Start().valueOf() - b.Start().valueOf())
				: this._group.Annotations());
	}

	public Add():void
	{
		var annotation = this._group.CreateAnnotation(true);

		if (this.IsTimed)
		{
			annotation.Start(moment(0));
			annotation.End(moment(30000));
		}

		this._group.AddAnnotation(annotation);
		annotation.Edit();
		annotation.IsSelected(true);
    }

	public Export(): void {
	    const data = this.Annotations().filter(a => a.IsVisible()).map(a => this.GetAnnotationExportData(a));

        SaveAsCsv(data, "Annotationer.csv");
    }

	private GetAnnotationExportData(annotation: Annotation): string[] {
		const data = annotation.Fields.filter(f => f.IsVisible).map(f => f.Value());

		if (annotation.IsTimed)
			data.unshift(annotation.PrettyStart(), annotation.PrettyEnd(), annotation.Duration());

		data.push(`${this._link}/${annotation.Identifier}`);

		return data;
	}
}

export = AnnotationList;