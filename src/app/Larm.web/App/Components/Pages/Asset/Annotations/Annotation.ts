﻿import knockout = require("knockout");
import moment = require("moment");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Permissions = require("Managers/Permissions");
import Configuration = require("Managers/Configuration");
import DisposableComponent = require("Components/DisposableComponent");
import DialogManager = require("Managers/Dialog/Dialog");
import Profile = require("Managers/Profile");

type Field = { Name:string, OriginalValue: any, Value:KnockoutObservable<any>, Type:EZArchivePortal.IEZDataDefinitionType, IsVisible:boolean };

class Annotation extends DisposableComponent
{
	private static StartField: string = "StartTime";
	private static EndField: string = "EndTime";

	public Identifier:string = null;

	public Start: KnockoutObservable<moment.Moment> = knockout.observable<moment.Moment>();
	public End: KnockoutObservable<moment.Moment> = knockout.observable<moment.Moment>();

	public IsEditing: KnockoutObservable<boolean> = knockout.observable<boolean>(false);
	public CanEdit:KnockoutComputed<boolean>;
	public IsCollapsed: KnockoutObservable<boolean> = knockout.observable<boolean>(true);
	public IsSelected: KnockoutObservable<boolean> = knockout.observable<boolean>(false);

	public Fields: Field[];
	public IsServerSide = knockout.observable(false);
	public IsTimed: boolean;
	
	public Title: KnockoutComputed<string>;
	public Owner = knockout.observable("");
	public OwnerId = knockout.observable<string>(null);
	public Permission = knockout.observable(EZArchivePortal.AnnotationPermission.Owner);
	public Visibility = knockout.observable<EZArchivePortal.AnnotationVisibility>();
	public IsLocked: KnockoutComputed<boolean>;
	public IsPrivate: KnockoutComputed<boolean>;
	public CanEditLock: KnockoutComputed<boolean>;
	public CanEditVisibility: KnockoutComputed<boolean>;
	public IsVisible: KnockoutComputed<boolean>;

	public PrettyStart: KnockoutComputed<string>;
	public PrettyEnd:KnockoutComputed<string>;
	public Duration: KnockoutComputed<string>;

	private _playCallbacks:Array<(annotaion:Annotation)=>void> = [];
	private _saveCallbacks:Array<(annotaion:Annotation)=>void> = [];
	private _deleteCallbacks: Array<(annotaion: Annotation) => void> = [];
	private _internalId: number;

	private static Count: number = 0;

	constructor(fieldTypes: { [key: string]: EZArchivePortal.IEZDataDefinitionType}, isTimed:boolean, visibleCheck:(ownerId:string)=>boolean)
	{
		super();
		this.IsTimed = isTimed;

		this.Fields = this.CreateFields(fieldTypes, this.IsTimed);

		this.Title = this.PureComputed(() =>
		{
			for (let field of this.Fields)
			{
				if (field.Value()) return <string>field.Value().toString();
			}

			return "";
		});

		this.IsLocked = this.PureComputed(() => this.Permission() === EZArchivePortal.AnnotationPermission.Owner,
			v => this.Permission(v ? EZArchivePortal.AnnotationPermission.Owner : EZArchivePortal.AnnotationPermission.All));

		this.IsPrivate = this.PureComputed(() => this.Visibility() === "Private",
			v => this.Visibility(v ? "Private" : "Public"));

		this.CanEdit = this.PureComputed(() => Permissions.CanEditAnnotations && (!this.IsLocked() || this.OwnerId() === Profile.Identifier()));
		this.CanEditLock = this.PureComputed(() => Permissions.CanEditAnnotations && this.OwnerId() === Profile.Identifier());
		this.CanEditVisibility = this.PureComputed(() => Permissions.CanEditAnnotations && this.OwnerId() === Profile.Identifier());
		this.IsVisible = this.PureComputed(() => visibleCheck(this.OwnerId()));
		this._internalId = Annotation.Count++;
		this.PrettyStart = knockout.pureComputed(() => this.Start().utc().format("HH:mm:ss"));
		this.PrettyEnd = knockout.pureComputed(() => this.End().utc().format("HH:mm:ss"));
		this.Duration = knockout.pureComputed(() => moment(this.End().diff(this.Start())).utc().format("HH:mm:ss"));
	}

	private HtmlEscape(str:string, replaceNewLines:boolean = false):string
	{
		if (str == null) return "";

		str = str
			.replace(/&/g, "&amp;")
			.replace(/"/g, "&quot;")
			.replace(/'/g, "&#39;")
			.replace(/</g, "&lt;")
			.replace(/>/g, "&gt;");

		if (replaceNewLines)
			str = str.replace(/[\n\r]/g, "<br/>");

		return str;
	}

	public ToggleCollapsed():void
	{
		this.IsCollapsed(!this.IsCollapsed());

		if (!this.IsCollapsed())
			this.IsSelected(true);
	}

	public get Id():number
	{
		return this._internalId;
	}

	public AddPlayCallback(callback: (annotation: Annotation) => void): void
	{
		this._playCallbacks.push(callback);
	}

	public AddSaveCallback(callback:(annotation:Annotation) => void):void
	{
		this._saveCallbacks.push(callback);
	}

	public AddDeleteCallback(callback: (annotation: Annotation) => void): void
	{
		this._deleteCallbacks.push(callback);
	}

	public Play():void
	{
		this._playCallbacks.forEach(c => c(this));
	}

	public Save():void
	{
		this.IsEditing(false);

		this.Fields.forEach(f => f.OriginalValue = f.Value());

		this._saveCallbacks.forEach(c => c(this));
	}

	public RequestDelete():void
	{
		DialogManager.Show("Pages/Asset/Annotations/DeleteDialog", this);
	}

	public Delete(): void
	{
		this._deleteCallbacks.forEach(c => c(this));
	}

	public ParsePortalAnnotation(annotation:EZArchivePortal.IAnnotation):void
	{
		this.Identifier = annotation.Identifier;
		this.Owner(annotation.__Owner);
		this.OwnerId(annotation.__OwnerId);
		this.Permission(annotation.__Permission);
		this.Visibility(annotation.__Visibility);

		if (this.IsTimed)
		{
			this.Start(moment(parseInt(annotation[Annotation.StartField])));
			this.End(moment(parseInt(annotation[Annotation.EndField])));
		}

		this.Fields.forEach(f =>
		{
			f.OriginalValue = annotation[f.Name];
			f.Value(f.OriginalValue);
		});
	}

	public ToPortalAnnotation():EZArchivePortal.IAnnotation
	{
		var annotation: EZArchivePortal.IAnnotation = {};

		if (this.Identifier != null)
			annotation.Identifier = this.Identifier;

		if (this.IsTimed)
		{
			annotation[Annotation.StartField] = this.Start().valueOf().toString();
			annotation[Annotation.EndField] = this.End().valueOf().toString();
		}

		this.Fields.forEach(f => annotation[f.Name] = f.Value());

		return annotation;
	}

	public Edit():any
	{
		if (!this.CanEdit()) return;

		this.IsEditing(true);
		this.IsCollapsed(false);
	}

	public Cancel():void
	{
		if (this.IsServerSide())
		{
			this.Fields.forEach(f => f.Value(f.OriginalValue));
			this.IsEditing(false);
		}
		else
			this.Delete();
	}

	private CreateFields(types: { [index: string]: EZArchivePortal.IEZDataDefinitionType; }, isTimed:boolean): Field[]
	{
		var result: Field[] = [];

		for (let key in types)
		{
			if (isTimed && (key === Annotation.StartField || key === Annotation.EndField)) continue;

			result.push({ Name: key, Type: types[key], OriginalValue: "", Value: knockout.observable(""), IsVisible: Configuration.HiddenAnnotationProperties.indexOf(key) === -1});
		}

		return result;
	}
}

export = Annotation