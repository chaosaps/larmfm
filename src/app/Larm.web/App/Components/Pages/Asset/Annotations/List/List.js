var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "moment", "Managers/Permissions", "Components/DisposableComponent", "Utility/FileSaver"], function (require, exports, moment, Permissions, DisposableComponent, FileSaver_1) {
    "use strict";
    var AnnotationList = (function (_super) {
        __extends(AnnotationList, _super);
        function AnnotationList(data) {
            var _this = _super.call(this) || this;
            _this._group = data.Group;
            _this._link = data.Link;
            _this.CanEdit = Permissions.CanEditAnnotations;
            _this.Name = _this._group.Name;
            _this.IsTimed = _this._group.IsTimed;
            _this.Owners = data.Group.Owners;
            _this.FilteredOwner = data.Group.FilteredOwner;
            _this.CanExport = _this.PureComputed(function () { return _this.Annotations().filter(function (a) { return a.IsVisible(); }).length !== 0; });
            _this.Annotations = _this.PureComputed(function () { return _this.IsTimed
                ? _this._group.Annotations().slice(0).sort(function (a, b) { return a.Start().valueOf() - b.Start().valueOf(); })
                : _this._group.Annotations(); });
            return _this;
        }
        AnnotationList.prototype.Add = function () {
            var annotation = this._group.CreateAnnotation(true);
            if (this.IsTimed) {
                annotation.Start(moment(0));
                annotation.End(moment(30000));
            }
            this._group.AddAnnotation(annotation);
            annotation.Edit();
            annotation.IsSelected(true);
        };
        AnnotationList.prototype.Export = function () {
            var _this = this;
            var data = this.Annotations().filter(function (a) { return a.IsVisible(); }).map(function (a) { return _this.GetAnnotationExportData(a); });
            FileSaver_1.SaveAsCsv(data, "Annotationer.csv");
        };
        AnnotationList.prototype.GetAnnotationExportData = function (annotation) {
            var data = annotation.Fields.filter(function (f) { return f.IsVisible; }).map(function (f) { return f.Value(); });
            if (annotation.IsTimed)
                data.unshift(annotation.PrettyStart(), annotation.PrettyEnd(), annotation.Duration());
            data.push(this._link + "/" + annotation.Identifier);
            return data;
        };
        return AnnotationList;
    }(DisposableComponent));
    return AnnotationList;
});
