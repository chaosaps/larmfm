var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "moment", "Components/Pages/Asset/Data/DataFieldBase", "Managers/Configuration", "Utility/DateParser"], function (require, exports, knockout, moment, DataFieldBase, Configuration, DateParser) {
    "use strict";
    var Datetime = (function (_super) {
        __extends(Datetime, _super);
        function Datetime(info) {
            var _this = _super.call(this, info) || this;
            _this.IsEditing = knockout.observable(false);
            _this.DateValue = knockout.observable(_this.GetValue());
            var replacementMap = Configuration.AssetDataReplaceValues.hasOwnProperty("Datetime")
                ? Configuration.AssetDataReplaceValues["Datetime"]
                : null;
            _this.PrettyValue = knockout.computed(function () {
                var date = _this.DateValue();
                var result = date != null ?
                    Configuration.AssetUseDateFormatFieldNames.indexOf(_this.Type.DisplayName) !== -1 ?
                        DateParser.ToFormattedStringByAssetType(date, _this._assetTypeId, !_this.IsArchive) :
                        moment(date).format("DD/MM/YYYY HH:mm") :
                    "";
                if (replacementMap !== null)
                    for (var key in replacementMap)
                        if (result.indexOf(key) === 0) {
                            result = replacementMap[key];
                            break;
                        }
                return result;
            });
            return _this;
        }
        Datetime.prototype.Save = function () {
            var value = this.DateValue();
            this.Value(value !== null ? DateParser.ToLocalUTCString(value) : null);
            _super.prototype.Save.call(this);
        };
        Datetime.prototype.Cancel = function () {
            _super.prototype.Cancel.call(this);
            this.DateValue(this.GetValue());
        };
        Datetime.prototype.dispose = function () {
            this.PrettyValue.dispose();
        };
        Datetime.prototype.GetValue = function () {
            var value = this.Value();
            return value != null && value !== "" ? DateParser.ParseLocalUTCString(value) : null;
        };
        return Datetime;
    }(DataFieldBase));
    return Datetime;
});
