﻿import DataFieldBase = require("Components/Pages/Asset/Data/DataFieldBase");

class DataString extends DataFieldBase<string>
{
	constructor(info:any)
	{
		super(info);
	}
}

export = DataString;