var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/Pages/Asset/Data/DataFieldBase", "Managers/Portal/EZArchivePortal", "Managers/Notification", "Managers/Configuration", "jquery", "bloodhound", "typeahead"], function (require, exports, knockout, DataFieldBase, EZArchivePortal, Notification, Configuration, jquery, Bloodhound) {
    "use strict";
    var ClosedList = (function (_super) {
        __extends(ClosedList, _super);
        function ClosedList(info) {
            var _this = _super.call(this, info) || this;
            _this.Input = knockout.observable(null);
            _this.HasValidValue = knockout.observable(false);
            var filter = _this.Type.Options.TypeFacet + ":" + _this._assetTypeId;
            EZArchivePortal.EZFacet.Get(null, filter, null, null, null, null, null, 0, 999).WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to get facets: " + response.Error.Message);
                    return;
                }
                var facets = response.Body.Results.filter(function (f) { return f.Header === Configuration.MenuFacetHeader; });
                if (facets.length === 0)
                    return;
                var valueFacet = facets[0].Fields.filter(function (f) { return f.Value === _this.Type.Options.Facet; });
                if (valueFacet.length === 0)
                    return;
                var values = valueFacet[0].Facets.filter(function (f) { return f.Count !== 0; }).map(function (f) { return f.Key; }).sort();
                _this.AddAction(function () { return _this.Input() !== null; }, function () { return _this.InitializeTypeahead(_this.Input(), values); });
            });
            return _this;
        }
        ClosedList.prototype.Save = function () {
            var value = this.UpdateValid();
            if (!this.HasValidValue())
                return;
            this.Value(value);
            _super.prototype.Save.call(this);
        };
        ClosedList.prototype.UpdateValid = function () {
            var value = this._typeAhead.typeahead("val");
            this.HasValidValue(value === "" || this._values.indexOf(value) !== -1);
            return value;
        };
        ClosedList.prototype.InitializeTypeahead = function (input, values) {
            var _this = this;
            this._values = values;
            var suggestions = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: this._values
            });
            input.value = this.Value();
            this._typeAhead = jquery(input);
            this._typeAhead.typeahead({
                hint: true,
                highligh: true,
                minLength: 0
            }, { name: "suggestions", source: suggestions });
            this._typeAhead.bind("typeahead:select", function (event, value) { return _this.UpdateValid(); });
            this._typeAhead.bind("typeahead:autocomplete", function (event, value) { return _this.UpdateValid(); });
            this._typeAhead.bind("typeahead:cursorchange ", function (event, value) { return _this.UpdateValid(); });
            this._typeAhead.bind("typeahead:change  ", function (event, value) { return _this.UpdateValid(); });
        };
        return ClosedList;
    }(DataFieldBase));
    return ClosedList;
});
