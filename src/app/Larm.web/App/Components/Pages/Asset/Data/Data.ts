﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import DataDefinition = require("Managers/Portal/DataDefinition");
import DisposableComponent = require("Components/DisposableComponent");
import Notification = require("Managers/Notification");
import Permissions = require("Managers/Permissions");
import Configuration = require("Managers/Configuration");

type DataFieldInfo = { Value: KnockoutObservable<string>; AssetTypeId: string; Name:string, SchemaName:string; Type:EZArchivePortal.IEZDataDefinitionType; CanEdit:KnockoutComputed<boolean>; Save:(callback:(success:boolean)=>void)=>void };

class Data extends DisposableComponent
{
	public readonly Title: KnockoutObservable<string> = knockout.observable("");
	public readonly Fields: KnockoutObservableArray<DataFieldInfo> = knockout.observableArray<DataFieldInfo>();
	public readonly CanEdit: KnockoutComputed<boolean>;

	private readonly _updateCallback: ()=>void;

	constructor(data: { Id: string, AssetTypeId:string, Data:EZArchivePortal.IData, UpdateCallback:()=>void})
	{
		super();

		this.CanEdit = this.Computed(() =>
		{
			if (!DataDefinition.IsReady()) return false;

			var definition = DataDefinition.Get(data.Data.Name);

			return Permissions.CanEditMetadata() && definition != null && (definition.IsEditable || (Configuration.OffsetSchema === data.Data.Name && Permissions.CanEditOffsetMetadata()));
		});

		this._updateCallback = data.UpdateCallback;

		this.AddAction(DataDefinition.IsReady, () => this.Initialize(data.Id, data.AssetTypeId, data.Data.Name, data.Data.Fields));
	}

	private Initialize(id: string, assetTypeId: string, definitionName:string, fields:{[key:string]:string; }):void
	{
		const definition = DataDefinition.Get(definitionName);

		if (definition == null)
		{
			Notification.Error(`DataDefinition not found: ${definitionName}`);
			return;
		}

		this.Title(definition.Name);

		const listFieldData = Configuration.SchemaListField.Schemas.hasOwnProperty(definition.Name)
			? Configuration.SchemaListField.Schemas[definition.Name]
			: null;

		for (let key in definition.Fields)
		{
			let type = definition.Fields[key];

			if (listFieldData !== null && listFieldData.Field === key)
				type = this.CreateClosedListType(type, listFieldData);

			this.Fields.push(this.CreateField(id, type, assetTypeId, definition, key, fields.hasOwnProperty(key) ? fields[key] : "", (key,value) =>
			{
				fields[key] = value;
				this._updateCallback();
			}));
		}
	}

	private CreateField(id: string, type: EZArchivePortal.IEZDataDefinitionType, assetTypeId: string, definition:EZArchivePortal.IEZDataDefinition, key:string, value:string, saveCallback:(key:string, value:string)=>void):DataFieldInfo
	{
		const valueObservable = knockout.observable(value);

		return {
			Value: valueObservable,
			AssetTypeId: assetTypeId,
			Type: type,
			Name: key,
			SchemaName: definition.Name,
			CanEdit: this.CanEdit,
			Save: (callback) =>
			{
				var data: { [key: string]: string } = {};
				var newValue = valueObservable();
				data[`${definition.Name}.${key}`] = newValue;

				saveCallback(key, newValue);

				EZArchivePortal.Asset.Set(data, id).WithCallback(response =>
				{
					if (response.Error != null)
					{
						Notification.Error(`Failed to save data: ${response.Error.Message}`);
						callback(false);
					} else
						callback(true);
				});
			}
		}
	}

	private CreateClosedListType(originalType: EZArchivePortal.IEZDataDefinitionType, options: { Field: string; Facet: string }): EZArchivePortal.IEZDataDefinitionType
	{
		return {
			DisplayName: originalType.DisplayName,
			Type: "ClosedList",
			Options: { Facet: options.Facet, TypeFacet: Configuration.SchemaListField.TypeFacet },
			IsRequired: originalType.IsRequired,
			Placeholder: originalType.Placeholder
		}
	}
}

export = Data;