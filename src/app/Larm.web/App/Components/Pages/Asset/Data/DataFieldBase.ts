﻿import knockout = require("knockout");
import DisposableComponent = require("Components/DisposableComponent");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");

type DataFieldInfo = { Value: KnockoutObservable<any>; AssetTypeId: string; Name:string, SchemaName:string; Type: EZArchivePortal.IEZDataDefinitionType; CanEdit: KnockoutComputed<boolean>; Save:(callback:(success:boolean) => void) => void };

abstract class DataFieldBase<T> extends DisposableComponent
{
	private _save: (callback: (success: boolean) => void) => void;
	protected OriginalValue: T;
	protected Name: string;
	protected SchemaName:string;
	protected readonly IsArchive: boolean;
	public Value: KnockoutObservable<T>;
	public Type: EZArchivePortal.IEZDataDefinitionType;
	public IsEditing: KnockoutObservable<boolean> = knockout.observable(false);
	public CanEdit: KnockoutComputed<boolean>;

	protected _assetTypeId:string;

	constructor(info: DataFieldInfo)
	{
		super();
		this.Value = info.Value;
		this._assetTypeId = info.AssetTypeId;
		this.OriginalValue = this.Value();
		this.Name = info.Name;
		this.SchemaName = info.SchemaName;
		this.Type = info.Type;
		this.CanEdit = info.CanEdit;
		this._save = <any>info.Save;
		this.IsArchive = this.SchemaName.indexOf("Arkiv") !== -1;
	}

	public Edit(): void
	{
		if (!this.CanEdit()) return;
		this.IsEditing(true);
	}

	public Save(): void
	{
		this.IsEditing(false);

		this._save(success =>
		{
			if (success)
				this.OriginalValue = this.Value();
			else
				this.IsEditing(true);
		});
	}

	public Cancel():void
	{
		this.IsEditing(false);

		this.Value(this.OriginalValue);
	}
}

export = DataFieldBase;