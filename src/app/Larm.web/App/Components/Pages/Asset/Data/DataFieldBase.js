var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/DisposableComponent"], function (require, exports, knockout, DisposableComponent) {
    "use strict";
    var DataFieldBase = (function (_super) {
        __extends(DataFieldBase, _super);
        function DataFieldBase(info) {
            var _this = _super.call(this) || this;
            _this.IsEditing = knockout.observable(false);
            _this.Value = info.Value;
            _this._assetTypeId = info.AssetTypeId;
            _this.OriginalValue = _this.Value();
            _this.Name = info.Name;
            _this.SchemaName = info.SchemaName;
            _this.Type = info.Type;
            _this.CanEdit = info.CanEdit;
            _this._save = info.Save;
            _this.IsArchive = _this.SchemaName.indexOf("Arkiv") !== -1;
            return _this;
        }
        DataFieldBase.prototype.Edit = function () {
            if (!this.CanEdit())
                return;
            this.IsEditing(true);
        };
        DataFieldBase.prototype.Save = function () {
            var _this = this;
            this.IsEditing(false);
            this._save(function (success) {
                if (success)
                    _this.OriginalValue = _this.Value();
                else
                    _this.IsEditing(true);
            });
        };
        DataFieldBase.prototype.Cancel = function () {
            this.IsEditing(false);
            this.Value(this.OriginalValue);
        };
        return DataFieldBase;
    }(DisposableComponent));
    return DataFieldBase;
});
