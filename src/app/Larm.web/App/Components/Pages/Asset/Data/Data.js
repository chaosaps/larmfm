var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Portal/EZArchivePortal", "Managers/Portal/DataDefinition", "Components/DisposableComponent", "Managers/Notification", "Managers/Permissions", "Managers/Configuration"], function (require, exports, knockout, EZArchivePortal, DataDefinition, DisposableComponent, Notification, Permissions, Configuration) {
    "use strict";
    var Data = (function (_super) {
        __extends(Data, _super);
        function Data(data) {
            var _this = _super.call(this) || this;
            _this.Title = knockout.observable("");
            _this.Fields = knockout.observableArray();
            _this.CanEdit = _this.Computed(function () {
                if (!DataDefinition.IsReady())
                    return false;
                var definition = DataDefinition.Get(data.Data.Name);
                return Permissions.CanEditMetadata() && definition != null && (definition.IsEditable || (Configuration.OffsetSchema === data.Data.Name && Permissions.CanEditOffsetMetadata()));
            });
            _this._updateCallback = data.UpdateCallback;
            _this.AddAction(DataDefinition.IsReady, function () { return _this.Initialize(data.Id, data.AssetTypeId, data.Data.Name, data.Data.Fields); });
            return _this;
        }
        Data.prototype.Initialize = function (id, assetTypeId, definitionName, fields) {
            var _this = this;
            var definition = DataDefinition.Get(definitionName);
            if (definition == null) {
                Notification.Error("DataDefinition not found: " + definitionName);
                return;
            }
            this.Title(definition.Name);
            var listFieldData = Configuration.SchemaListField.Schemas.hasOwnProperty(definition.Name)
                ? Configuration.SchemaListField.Schemas[definition.Name]
                : null;
            for (var key in definition.Fields) {
                var type = definition.Fields[key];
                if (listFieldData !== null && listFieldData.Field === key)
                    type = this.CreateClosedListType(type, listFieldData);
                this.Fields.push(this.CreateField(id, type, assetTypeId, definition, key, fields.hasOwnProperty(key) ? fields[key] : "", function (key, value) {
                    fields[key] = value;
                    _this._updateCallback();
                }));
            }
        };
        Data.prototype.CreateField = function (id, type, assetTypeId, definition, key, value, saveCallback) {
            var valueObservable = knockout.observable(value);
            return {
                Value: valueObservable,
                AssetTypeId: assetTypeId,
                Type: type,
                Name: key,
                SchemaName: definition.Name,
                CanEdit: this.CanEdit,
                Save: function (callback) {
                    var data = {};
                    var newValue = valueObservable();
                    data[definition.Name + "." + key] = newValue;
                    saveCallback(key, newValue);
                    EZArchivePortal.Asset.Set(data, id).WithCallback(function (response) {
                        if (response.Error != null) {
                            Notification.Error("Failed to save data: " + response.Error.Message);
                            callback(false);
                        }
                        else
                            callback(true);
                    });
                }
            };
        };
        Data.prototype.CreateClosedListType = function (originalType, options) {
            return {
                DisplayName: originalType.DisplayName,
                Type: "ClosedList",
                Options: { Facet: options.Facet, TypeFacet: Configuration.SchemaListField.TypeFacet },
                IsRequired: originalType.IsRequired,
                Placeholder: originalType.Placeholder
            };
        };
        return Data;
    }(DisposableComponent));
    return Data;
});
