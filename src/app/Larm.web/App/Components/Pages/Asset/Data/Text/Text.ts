﻿import DataFieldBase = require("Components/Pages/Asset/Data/DataFieldBase");

class Text extends DataFieldBase<string>
{
	constructor(info:any)
	{
		super(info);
	}
}

export = Text;