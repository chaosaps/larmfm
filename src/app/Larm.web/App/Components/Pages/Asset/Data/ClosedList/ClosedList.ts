﻿import knockout = require("knockout");
import DataFieldBase = require("Components/Pages/Asset/Data/DataFieldBase");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Notification = require("Managers/Notification");
import Configuration = require("Managers/Configuration");
import jquery = require("jquery");
import "typeahead";
import * as Bloodhound from "bloodhound"

class ClosedList extends DataFieldBase<string>
{
	public Input = knockout.observable<HTMLInputElement | null>(null);
	public HasValidValue = knockout.observable(false);
	private _typeAhead: any;
	private _values: string[];

	constructor(info:any)
	{
		super(info);

		const filter = this.Type.Options.TypeFacet + ":" + this._assetTypeId;
		EZArchivePortal.EZFacet.Get(null, filter, null, null, null, null, null, 0, 999).WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error(`Failed to get facets: ${response.Error.Message}`);
				return;
			}

			const facets = response.Body.Results.filter(f => f.Header === Configuration.MenuFacetHeader);
			if (facets.length === 0)
				return;

			const valueFacet = facets[0].Fields.filter(f => f.Value === this.Type.Options.Facet);

			if (valueFacet.length === 0)
				return;

			const values = valueFacet[0].Facets.filter(f => f.Count !== 0).map(f => f.Key).sort();

			this.AddAction(() => this.Input() !== null, () => this.InitializeTypeahead(this.Input(), values));
		});
	}

	public Save(): void
	{
		const value = this.UpdateValid();

		if (!this.HasValidValue())
			return;

		this.Value(value);

		super.Save();
	}
	
	private UpdateValid(): string
	{
		const value = this._typeAhead.typeahead("val");
		this.HasValidValue(value === "" || this._values.indexOf(value) !== -1);

		return value;
	}

	private InitializeTypeahead(input: HTMLInputElement, values: string[]): void
	{
		this._values = values;
		const suggestions = new Bloodhound<string>({
			datumTokenizer: Bloodhound.tokenizers.whitespace,
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			local: this._values
		});

		input.value = this.Value();
		this._typeAhead = (<any>jquery(input));
		this._typeAhead.typeahead({
				hint: true,
				highligh: true,
				minLength: 0
			},
			{ name: "suggestions", source: suggestions });

		this._typeAhead.bind("typeahead:select", (event: any, value: any) => this.UpdateValid());
		this._typeAhead.bind("typeahead:autocomplete", (event: any, value: any) => this.UpdateValid());
		this._typeAhead.bind("typeahead:cursorchange ", (event: any, value: any) => this.UpdateValid());
		this._typeAhead.bind("typeahead:change  ", (event: any, value: any) => this.UpdateValid());
	}
}

export = ClosedList;