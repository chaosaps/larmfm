﻿import jquery = require("jquery");
import knockout = require("knockout");
import Configuration = require("Managers/Configuration");
import DataFieldBase = require("Components/Pages/Asset/Data/DataFieldBase");

type CollapsableData = { IsCollapsed: boolean, Data: string, CollapsedWidth?: number, Toggle?: () => void };
type CollapsableRow = { CanCollapse: boolean, Data: CollapsableData[], CollapsedData?: string[], IsCollapsed?: KnockoutObservable<boolean>, Toggle?: () => void };

class DataString extends DataFieldBase<string[][]>
{
	public Columns: string[];

	public HasItems: KnockoutComputed<boolean>;
	public CollapseData = knockout.observableArray<CollapsableRow>();

	public TableElement = knockout.observable<HTMLTableElement | null>(null);
	public ShowOverlayHeader = knockout.observable(false);
	public HeaderColumns:KnockoutObservable<number>[] = [];

	private _scrollHandler:()=>void;
	private _resizeHandler:()=>void;

	constructor(info: any)
	{
		super(info);

		this.HasItems = knockout.pureComputed(() => this.Value() != null && this.Value().length !== 0);
		this.Columns = this.GetVisibleColumns(this.Name, this.SchemaName, this.Type.Options);
		this.Columns.forEach(() => this.HeaderColumns.push(knockout.observable(0)));

		this.Sort(this.Name, this.SchemaName, this.Value());
		this.CreateCollapseStates(this.Name, this.SchemaName, this.Value());

		this.SubscribeUntilChange(this.TableElement, table => this.InitializeScrolling(table));
	}

	private InitializeScrolling(tableElement: HTMLTableElement): void
	{
		const table = jquery(tableElement);
		const scrollContent = jquery(".TabContainer > .tab-content");
		const contentTop = scrollContent.position().top;
		let headerRow: JQuery;

		const updateRows = () =>
		{
			if (!headerRow)
				return;
			headerRow.each((index, element) =>
			{
				this.HeaderColumns[index](jquery(element).outerWidth());
			});
		}

		const initializeRows = () =>
		{
			headerRow = table.find("thead tr th");
			
			updateRows();
		}

		this._scrollHandler = () =>
		{
			var position = table.position();

			this.ShowOverlayHeader(position.top - contentTop < 0 && position.top - contentTop > -table.height());
		}

		this._resizeHandler = () =>
		{
			updateRows();
		}

		scrollContent.one("scroll", initializeRows);

		scrollContent.scroll(this._scrollHandler);
		jquery(window).resize(this._resizeHandler);
	}

	public dispose(): void
	{
		jquery(".TabContainer > .tab-content").off("scroll", this._scrollHandler);
		jquery(window).off("resize", this._resizeHandler);

		super.dispose();
	}

	public Save(): void {
		this.Value(this.CollapseData().map(row => row.Data.map(data => data.Data)));
		super.Save();
	}

	public Cancel(): void {
		super.Cancel();
		this.CreateCollapseStates(this.Name, this.SchemaName, this.Value());
	}

	private Sort(name:string, schemaName: string, data: string[][]): void
	{
		const schemaSort = Configuration.SchemaTableSort[schemaName];

		if (!schemaSort)
			return;

		const fieldSort = schemaSort[name];

		if (!fieldSort)
			return;

		data.sort((rowA, rowB) =>
		{
			for (let sortIndex of fieldSort) {
				const result = rowA[sortIndex].localeCompare(rowB[sortIndex]);
				if (result !== 0)
					return result;
			}
			return 0;
		});
	}

	private CreateCollapseStates(name: string, schemaName: string, data: string[][]): void
	{
		const schemaCollapse = Configuration.SchemaTableCollapse[schemaName];
		const fieldCollapse = schemaCollapse ? schemaCollapse[name] : undefined;
		const hasCollapse = fieldCollapse != undefined;
		this.CollapseData.removeAll();

		for (let row of data)
		{
			const rowCollapseState: CollapsableRow = {
				CanCollapse: hasCollapse,
				Data: []
			};

			if (rowCollapseState.CanCollapse)
			{
				rowCollapseState.IsCollapsed = knockout.observable(true);
				rowCollapseState.CollapsedData = [];
				rowCollapseState.Toggle = () => rowCollapseState.IsCollapsed(!rowCollapseState.IsCollapsed());
			}

			for (let key in row)
			{
				const isCollapsed = hasCollapse && fieldCollapse.hasOwnProperty(key);

				if (!isCollapsed || fieldCollapse[key] !== 0)
				{
					const state: CollapsableData = {
						IsCollapsed: isCollapsed,
						Data: row[key]
					};
					rowCollapseState.Data.push(state);

					if (state.IsCollapsed)
					{
						state.CollapsedWidth = fieldCollapse[key];
						state.Toggle = rowCollapseState.Toggle;
					}
				}

				if (isCollapsed)
					rowCollapseState.CollapsedData.push(row[key]);
			}

			this.CollapseData.push(rowCollapseState);
		}
	}

	private GetVisibleColumns(name: string, schemaName: string, columnData: string[]): string[]
	{
		const schemaCollapse = Configuration.SchemaTableCollapse[schemaName];
		const fieldCollapse = schemaCollapse ? schemaCollapse[name] : undefined;
		const hasCollapse = fieldCollapse != undefined;

		if (!hasCollapse)
			return columnData;

		return columnData.filter((column, index) => fieldCollapse[index] !== 0);
	}
}

export = DataString;