var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "jquery", "knockout", "Managers/Configuration", "Components/Pages/Asset/Data/DataFieldBase"], function (require, exports, jquery, knockout, Configuration, DataFieldBase) {
    "use strict";
    var DataString = (function (_super) {
        __extends(DataString, _super);
        function DataString(info) {
            var _this = _super.call(this, info) || this;
            _this.CollapseData = knockout.observableArray();
            _this.TableElement = knockout.observable(null);
            _this.ShowOverlayHeader = knockout.observable(false);
            _this.HeaderColumns = [];
            _this.HasItems = knockout.pureComputed(function () { return _this.Value() != null && _this.Value().length !== 0; });
            _this.Columns = _this.GetVisibleColumns(_this.Name, _this.SchemaName, _this.Type.Options);
            _this.Columns.forEach(function () { return _this.HeaderColumns.push(knockout.observable(0)); });
            _this.Sort(_this.Name, _this.SchemaName, _this.Value());
            _this.CreateCollapseStates(_this.Name, _this.SchemaName, _this.Value());
            _this.SubscribeUntilChange(_this.TableElement, function (table) { return _this.InitializeScrolling(table); });
            return _this;
        }
        DataString.prototype.InitializeScrolling = function (tableElement) {
            var _this = this;
            var table = jquery(tableElement);
            var scrollContent = jquery(".TabContainer > .tab-content");
            var contentTop = scrollContent.position().top;
            var headerRow;
            var updateRows = function () {
                if (!headerRow)
                    return;
                headerRow.each(function (index, element) {
                    _this.HeaderColumns[index](jquery(element).outerWidth());
                });
            };
            var initializeRows = function () {
                headerRow = table.find("thead tr th");
                updateRows();
            };
            this._scrollHandler = function () {
                var position = table.position();
                _this.ShowOverlayHeader(position.top - contentTop < 0 && position.top - contentTop > -table.height());
            };
            this._resizeHandler = function () {
                updateRows();
            };
            scrollContent.one("scroll", initializeRows);
            scrollContent.scroll(this._scrollHandler);
            jquery(window).resize(this._resizeHandler);
        };
        DataString.prototype.dispose = function () {
            jquery(".TabContainer > .tab-content").off("scroll", this._scrollHandler);
            jquery(window).off("resize", this._resizeHandler);
            _super.prototype.dispose.call(this);
        };
        DataString.prototype.Save = function () {
            this.Value(this.CollapseData().map(function (row) { return row.Data.map(function (data) { return data.Data; }); }));
            _super.prototype.Save.call(this);
        };
        DataString.prototype.Cancel = function () {
            _super.prototype.Cancel.call(this);
            this.CreateCollapseStates(this.Name, this.SchemaName, this.Value());
        };
        DataString.prototype.Sort = function (name, schemaName, data) {
            var schemaSort = Configuration.SchemaTableSort[schemaName];
            if (!schemaSort)
                return;
            var fieldSort = schemaSort[name];
            if (!fieldSort)
                return;
            data.sort(function (rowA, rowB) {
                for (var _i = 0, fieldSort_1 = fieldSort; _i < fieldSort_1.length; _i++) {
                    var sortIndex = fieldSort_1[_i];
                    var result = rowA[sortIndex].localeCompare(rowB[sortIndex]);
                    if (result !== 0)
                        return result;
                }
                return 0;
            });
        };
        DataString.prototype.CreateCollapseStates = function (name, schemaName, data) {
            var schemaCollapse = Configuration.SchemaTableCollapse[schemaName];
            var fieldCollapse = schemaCollapse ? schemaCollapse[name] : undefined;
            var hasCollapse = fieldCollapse != undefined;
            this.CollapseData.removeAll();
            var _loop_1 = function (row) {
                var rowCollapseState = {
                    CanCollapse: hasCollapse,
                    Data: []
                };
                if (rowCollapseState.CanCollapse) {
                    rowCollapseState.IsCollapsed = knockout.observable(true);
                    rowCollapseState.CollapsedData = [];
                    rowCollapseState.Toggle = function () { return rowCollapseState.IsCollapsed(!rowCollapseState.IsCollapsed()); };
                }
                for (var key in row) {
                    var isCollapsed = hasCollapse && fieldCollapse.hasOwnProperty(key);
                    if (!isCollapsed || fieldCollapse[key] !== 0) {
                        var state = {
                            IsCollapsed: isCollapsed,
                            Data: row[key]
                        };
                        rowCollapseState.Data.push(state);
                        if (state.IsCollapsed) {
                            state.CollapsedWidth = fieldCollapse[key];
                            state.Toggle = rowCollapseState.Toggle;
                        }
                    }
                    if (isCollapsed)
                        rowCollapseState.CollapsedData.push(row[key]);
                }
                this_1.CollapseData.push(rowCollapseState);
            };
            var this_1 = this;
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var row = data_1[_i];
                _loop_1(row);
            }
        };
        DataString.prototype.GetVisibleColumns = function (name, schemaName, columnData) {
            var schemaCollapse = Configuration.SchemaTableCollapse[schemaName];
            var fieldCollapse = schemaCollapse ? schemaCollapse[name] : undefined;
            var hasCollapse = fieldCollapse != undefined;
            if (!hasCollapse)
                return columnData;
            return columnData.filter(function (column, index) { return fieldCollapse[index] !== 0; });
        };
        return DataString;
    }(DataFieldBase));
    return DataString;
});
