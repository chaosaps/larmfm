﻿import knockout = require("knockout");
import moment = require("moment");
import DataFieldBase = require("Components/Pages/Asset/Data/DataFieldBase");
import Configuration = require("Managers/Configuration");
import DateParser = require("Utility/DateParser");

class Datetime extends DataFieldBase<string>
{
	public PrettyValue: KnockoutComputed<string>;
	public DateValue: KnockoutObservable<Date | null>;
	public IsEditing: KnockoutObservable<boolean> = knockout.observable(false);

	constructor(info:any)
	{
		super(info);

		this.DateValue = knockout.observable(this.GetValue());

		const replacementMap = Configuration.AssetDataReplaceValues.hasOwnProperty("Datetime")
			? Configuration.AssetDataReplaceValues["Datetime"]
			: null;

		this.PrettyValue = knockout.computed(() =>
		{
			const date = this.DateValue();

			let result = date != null ?
				Configuration.AssetUseDateFormatFieldNames.indexOf(this.Type.DisplayName) !== -1 ?
					DateParser.ToFormattedStringByAssetType(date, this._assetTypeId, !this.IsArchive) :
					moment(date).format("DD/MM/YYYY HH:mm") :
				"";

			if (replacementMap !== null)
				for (const key in replacementMap)
					if (result.indexOf(key) === 0) {
						result = replacementMap[key];
						break;
					}

			return result;
		});
	}

	public Save(): void
	{
		const value = this.DateValue();
		this.Value(value !== null ? DateParser.ToLocalUTCString(value) : null);
		super.Save();
	}

	public Cancel(): void
	{
		super.Cancel();
		this.DateValue(this.GetValue());
	}

	public dispose(): void
	{
		this.PrettyValue.dispose();
	}

	private GetValue(): Date
	{
		const value = this.Value();
		return value != null && value !== "" ? DateParser.ParseLocalUTCString(value) : null;
	}
}

export = Datetime;