﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import LarmPortal = require("Managers/Portal/Portal");
import DataDefinition = require("Managers/Portal/DataDefinition");
import Notification = require("Managers/Notification");
import DisposableComponent = require("Components/DisposableComponent");
import DataExtractor = require("Utility/DataExtractor");
import Configuration = require("Managers/Configuration");
import Annotation = require("Components/Pages/Asset/Annotations/Annotation");
import AnnotationGroup = require("Components/Pages/Asset/Annotations/AnnotationGroup");
import AnnotationHandler = require("Components/Pages/Asset/Annotations/AnnotationHandler");
import PlayerState = require("Components/Pages/Asset/Players/State");
import Permissions = require("Managers/Permissions");
import Tracking = require("Managers/Tracking");
import Copyright = require("Managers/Copyright");
import Navigation = require("Managers/Navigation/Navigation");
import Title = require("Managers/Title");
import Dragging = require("Managers/Dragging");
import Interface = require("Managers/Interface");
import DialogManager = require("Managers/Dialog/Dialog");

class Asset extends DisposableComponent
{
	public Id: string;
	public TypeId = knockout.observable<string>(null);
	public IsLoading:KnockoutObservable<boolean> = knockout.observable(true);
	public Title: KnockoutObservable<string> = knockout.observable("");
	public Data: KnockoutObservableArray<EZArchivePortal.IData> = knockout.observableArray<EZArchivePortal.IData>();
	public VisibleData: KnockoutObservableArray<EZArchivePortal.IData> = knockout.observableArray<EZArchivePortal.IData>();
	public AnnotationGroups: KnockoutObservableArray<AnnotationGroup>;
	public SelectedAnnotation: KnockoutObservable<Annotation>;
	public GetActiveGroup:()=>AnnotationGroup;
	public PlayerState: PlayerState;
	public IsAuthenticated: KnockoutComputed<boolean>;
	public MediaRequiresLogin: KnockoutComputed<boolean>;
	public MediaIsCopyrightBlocked: KnockoutComputed<boolean>;
	public CanAssociateWithLabel: KnockoutComputed<boolean>;
	public DataUpdatedCallback: () => void;
	public DragElement = knockout.observable<HTMLElement>();
	public IsDragging = knockout.observable(false);
	public Link: string;

	private _annotationHandler: AnnotationHandler;
	private _dataExtractor: DataExtractor;

	constructor(data: { Id:string; AnnotationId:KnockoutObservable<string> })
	{
		super();
		Interface.Hide();
		this.Id = data.Id;
		this.Title(this.Id);
		this._dataExtractor = new DataExtractor(this.Id, this.TypeId, this.Data);

		this.DataUpdatedCallback = () =>
		{
			this._dataExtractor.Update();
			this.Title(this._dataExtractor.Title);
			Title.Set(this.Title());
		};

		this.PlayerState = new PlayerState();
		this._annotationHandler = new AnnotationHandler(this.Id, this.PlayerState);
		this.AnnotationGroups = this._annotationHandler.AnnotationGroups;
		this.SelectedAnnotation = this._annotationHandler.SelectedAnnotation;
		this.GetActiveGroup = () => this._annotationHandler.GetActiveGroup();
		this.IsAuthenticated = this.PureComputed(() => LarmPortal.IsAuthenticated());
		this.MediaRequiresLogin = this.PureComputed(() => !LarmPortal.IsAuthenticated());
		this.MediaIsCopyrightBlocked = this.PureComputed(() => !Copyright.IsCleared(this.Data()));
		this.CanAssociateWithLabel = this.PureComputed(() => LarmPortal.IsAuthenticated());
		this.Link = `${window.location.protocol}//${window.location.host}/Asset/${this.Id}`;

		this.AddTracking();
		this.LoadAsset(this.Id);
        this.Subscribe(LarmPortal.IsAuthenticated, () => { if (!this.IsLoading()) this.LoadFiles(); });
		this.Subscribe(this.SelectedAnnotation, a => Navigation.Navigate(`Asset/${this.Id}${a == null || a.Identifier == null ? "" : `/${a.Identifier}`}`));
		this.Subscribe(data.AnnotationId, id => this._annotationHandler.SelectAnnotation(id));

		if (data.AnnotationId() != null)
		{
			this._annotationHandler.SelectAnnotation(data.AnnotationId());

			if (this._annotationHandler.SelectedAnnotation() == null)
			{
				this.SubscribeUntilChange(this._annotationHandler.SelectedAnnotation, annotation =>
				{
					if (annotation.IsTimed)
						this.PlayerState.Position(annotation.Start().valueOf());
				});
			}
		}
	}

	public ShowAddToFolder(): void
	{
		DialogManager.Show("Pages/Asset/AddAssetToFolderDialog", {Id: this.Id, Title: this.Title()});
	}

	private AddTracking(): void
	{
		this.Subscribe(this.PlayerState.IsPlaying, v =>
		{
			if (v) Tracking.TrackEvent("Player", this.PlayerState.Type(), "Playing");
		});

		this.SubscribeUntilChange(this.TypeId, type => Tracking.TrackEvent("Asset", "Open", Configuration.TrackingType[type]));
	}

	private StartDrag(viewModel: any, jqueryEvent: JQueryEventObject): boolean
	{
		if (!this.CanAssociateWithLabel()) return false;

		const event = jqueryEvent.originalEvent as DragEvent;

		Dragging.StartSearchResultDrag(event, this.Id);

		if (event.dataTransfer.setDragImage !== undefined)
		{
			this.IsDragging(true);

			event.dataTransfer.setDragImage(this.DragElement(), -7, -7);
			setInterval(() => this.IsDragging(false));
		}

		return true;
	}

	private LoadAsset(id:string):void
	{
        this.GetAsset(id, asset => 
        {
			this.TypeId(asset.TypeId);

			if (asset.Data.length > 0)
			{
				var ordered = new Array<EZArchivePortal.IData>();

				Configuration.SchemaOrder.forEach(name =>
				{
					for (let i = 0; i < asset.Data.length; i++)
					{
						if (asset.Data[i].Name === name)
						{
							ordered.push(asset.Data[i]);
							asset.Data.splice(i, 1);
							return;
						}
					}
				});

				this.Data.push(...ordered, ...asset.Data);
				this.VisibleData.push(...this.Data()
					.filter(d => Configuration.IsSchemaVisible(d.Name, Permissions.CanSeeOffsetMetadata()))
					.sort((a, b) => a.Name.localeCompare(b.Name)));
				this.AddMissingDatas();
				this.DataUpdatedCallback();

				if (!Permissions.CanSeeOffsetMetadata())
					this.SubscribeUntilChange(Permissions.CanSeeOffsetMetadata, canSee =>
					{
						this.VisibleData.push(...this.Data().filter(d => d.Name === Configuration.OffsetSchema));
						this.AddMissingDatas();
						this.VisibleData.sort((a, b) => a.Name.localeCompare(b.Name));
					});
			}

			this._annotationHandler.Load(asset.TypeId, asset.Annotations, this._dataExtractor.BroadcastWalltimeStart);
			this.PlayerState.Update(asset.Files, this.Data(), Permissions.CanStreamContent());

			this.IsLoading(false);
		});
	}

    public LoadFiles(): void
    {
        this.GetAsset(this.Id, asset => {
	        try
	        {
				this.PlayerState.Update(asset.Files, this.Data(), Permissions.CanStreamContent());
	        } catch (e)
			{
				Notification.Error("Failed to update files: " + e.message);
	        }
        });
    }

	public dispose():void
	{
		super.dispose();

		this._annotationHandler.dispose();
		this.PlayerState.dispose();
    }

    private GetAsset(id: string, callback: (asset: EZArchivePortal.IAsset) => void): void
    {
		this.AddAction(LarmPortal.IsReady, () =>
		{
			EZArchivePortal.Asset.Get(id).WithCallback(response =>
			{
				if (response.Error !== null)
				{
					Notification.Error("Failed to get asset: " + response.Error.Message);
					return;
				}

				if (response.Body.Results.length === 0)
				{
					Notification.Error("No asset data returned");
					return;
				}

				callback(response.Body.Results[0]);
			});
		});
    }

	private AddMissingDatas():any
	{
		this.AddAction(DataDefinition.IsReady, () =>
		{
			var datas = this.Data();
			var missing = DataDefinition.GetByAssetType(this.TypeId()).filter(definition => Configuration.IsSchemaVisible(definition.Name, Permissions.CanSeeOffsetMetadata()) && !datas.some(data => data.Name === definition.Name)).map(d => (<EZArchivePortal.IData>{ Name: d.Name, Fields: {} }));

			if (missing.length > 0)
				this.VisibleData.push(...missing);
		});
	}
}

export = Asset;