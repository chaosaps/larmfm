﻿import DisposableComponent = require("Components/DisposableComponent");
import LabelFolderData from "Components/Menu/Projects/Project/LabelFolderData";
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Notification = require("Managers/Notification");
import IEZLabel = EZArchivePortal.IEZLabel;

class Label extends DisposableComponent
{
	public readonly Data: LabelFolderData;
	public readonly AssetId: string;

	constructor(data: { Data: LabelFolderData, AssetId: string })
	{
		super();

		this.Data = data.Data;
		this.AssetId = data.AssetId;
	}

	public GetBottomName(label: IEZLabel): string
	{
		const levels = LabelFolderData.GetLevels(label);
		return levels[levels.length - 1];
	}

	public Add(label: IEZLabel): void
	{
		EZArchivePortal.EZLabel.AssociateWith(label.Identifier, this.AssetId).WithCallback(response =>
		{
			if (response.Error != null)
				Notification.Error("Failed to associate asset with label: " + response.Error.Message);
		});
	}
}

export = Label;