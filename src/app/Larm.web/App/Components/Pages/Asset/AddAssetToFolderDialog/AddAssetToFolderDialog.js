var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/DisposableComponent", "Managers/ProjectManager", "Components/Menu/Projects/Project/LabelFolderData"], function (require, exports, knockout, DisposableComponent, ProjectManager, LabelFolderData_1) {
    "use strict";
    var AddAssetToFolderDialog = (function (_super) {
        __extends(AddAssetToFolderDialog, _super);
        function AddAssetToFolderDialog(state) {
            var _this = _super.call(this) || this;
            _this._state = state;
            _this.AssetId = _this._state.Data.Id;
            _this.Title = _this._state.Data.Title;
            _this.Projects = ProjectManager.Projects().filter(function (p) { return p.Labels && p.Labels.length > 0; }).map(function (p) { return new Project(p); });
            return _this;
        }
        return AddAssetToFolderDialog;
    }(DisposableComponent));
    var Project = (function (_super) {
        __extends(Project, _super);
        function Project(project) {
            var _this = _super.call(this) || this;
            _this.Labels = knockout.observableArray();
            _this.Identifier = project.Identifier;
            _this.Name = project.Name;
            for (var _i = 0, _a = project.Labels; _i < _a.length; _i++) {
                var label = _a[_i];
                _this.HandleLabel(label);
            }
            return _this;
        }
        Project.prototype.HandleLabel = function (label) {
            var labelDatas = this.Labels();
            for (var _i = 0, labelDatas_1 = labelDatas; _i < labelDatas_1.length; _i++) {
                var data = labelDatas_1[_i];
                if (data.AddLabel(label))
                    return;
            }
            var labelFolder = new LabelFolderData_1.default(this.Identifier, function (l, f, r) { }, label);
            LabelFolderData_1.default.AlphabeticAddFolder(this.Labels, labelFolder);
        };
        return Project;
    }(DisposableComponent));
    return AddAssetToFolderDialog;
});
