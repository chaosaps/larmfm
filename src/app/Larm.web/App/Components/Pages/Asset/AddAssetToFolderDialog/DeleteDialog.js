var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Components/DisposableComponent"], function (require, exports, DisposableComponent) {
    "use strict";
    var DeleteDialog = (function (_super) {
        __extends(DeleteDialog, _super);
        function DeleteDialog(state) {
            var _this = _super.call(this) || this;
            _this._state = state;
            _this.Title = _this._state.Data.Title;
            return _this;
        }
        DeleteDialog.prototype.Delete = function () {
            this._state.Data.Delete();
            this._state.Close();
        };
        return DeleteDialog;
    }(DisposableComponent));
    return DeleteDialog;
});
