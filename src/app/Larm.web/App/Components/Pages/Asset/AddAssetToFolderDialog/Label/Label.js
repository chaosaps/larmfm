var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Components/DisposableComponent", "Components/Menu/Projects/Project/LabelFolderData", "Managers/Portal/EZArchivePortal", "Managers/Notification"], function (require, exports, DisposableComponent, LabelFolderData_1, EZArchivePortal, Notification) {
    "use strict";
    var Label = (function (_super) {
        __extends(Label, _super);
        function Label(data) {
            var _this = _super.call(this) || this;
            _this.Data = data.Data;
            _this.AssetId = data.AssetId;
            return _this;
        }
        Label.prototype.GetBottomName = function (label) {
            var levels = LabelFolderData_1.default.GetLevels(label);
            return levels[levels.length - 1];
        };
        Label.prototype.Add = function (label) {
            EZArchivePortal.EZLabel.AssociateWith(label.Identifier, this.AssetId).WithCallback(function (response) {
                if (response.Error != null)
                    Notification.Error("Failed to associate asset with label: " + response.Error.Message);
            });
        };
        return Label;
    }(DisposableComponent));
    return Label;
});
