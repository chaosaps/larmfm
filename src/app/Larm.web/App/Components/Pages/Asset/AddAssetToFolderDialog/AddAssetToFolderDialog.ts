﻿import knockout = require("knockout");
import DialogState = require("Managers/Dialog/DialogState");
import DisposableComponent = require("Components/DisposableComponent");
import ProjectManager = require("Managers/ProjectManager");
import LabelFolderData from "Components/Menu/Projects/Project/LabelFolderData";
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import IEZLabel = EZArchivePortal.IEZLabel;

type options = { Id: string, Title: string };

class AddAssetToFolderDialog extends DisposableComponent
{
	private _state: DialogState<options>;

	public Title: string;
	public AssetId: string;
	public Projects: Project[];

	constructor(state: DialogState<options>)
	{
		super();
		this._state = state;
		this.AssetId = this._state.Data.Id;
		this.Title = this._state.Data.Title;

		this.Projects = ProjectManager.Projects().filter(p => p.Labels && p.Labels.length > 0).map(p => new Project(p));
	}
}

class Project extends DisposableComponent {
	public Identifier: string;
	public Name: string;
	public Labels = knockout.observableArray<LabelFolderData>();
	
	constructor(project: EZArchivePortal.IEZProject)
	{
		super();

		this.Identifier = project.Identifier;
		this.Name = project.Name;

		for (const label of project.Labels)
			this.HandleLabel(label);
	}

	private HandleLabel(label: IEZLabel): void
	{
		const labelDatas = this.Labels();
		for (const data of labelDatas) {
			if (data.AddLabel(label))
				return;
		}

		const labelFolder: LabelFolderData = new LabelFolderData(this.Identifier, (l, f, r) => {}, label);
		LabelFolderData.AlphabeticAddFolder(this.Labels, labelFolder);
	}
}

export = AddAssetToFolderDialog;