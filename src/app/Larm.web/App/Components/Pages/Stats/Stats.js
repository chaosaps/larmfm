var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/DisposableComponent"], function (require, exports, knockout, DisposableComponent) {
    "use strict";
    var Stats = (function (_super) {
        __extends(Stats, _super);
        function Stats() {
            var _this = _super.call(this) || this;
            _this.Institutes = knockout.observableArray();
            return _this;
        }
        Stats.prototype.FileSelected = function (viewModel, event) {
            var _this = this;
            var input = event.target;
            if (input.files.length === 0)
                return;
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.LoadDate(event.target.result);
            };
            reader.readAsText(input.files[0]);
        };
        Stats.prototype.LoadDate = function (data) {
            var parser = new DOMParser();
            var xmlDocument = parser.parseFromString(data, "text/xml");
            var tags = xmlDocument.getElementsByTagName("EZArchive.Portal.Module.Core.EzUser");
            var institutes = {};
            for (var i = 0; i < tags.length; i++) {
                var wayfTag = tags[i].getElementsByTagName("WayfAttributes");
                if (wayfTag.length === 0 || wayfTag[0].textContent === "")
                    continue;
                var wayfData = JSON.parse(wayfTag[0].textContent);
                var instituteName = wayfData.schacHomeOrganization[0];
                var roleName = wayfData.eduPersonPrimaryAffiliation[0];
                if (!institutes.hasOwnProperty(instituteName))
                    institutes[instituteName] = { Name: instituteName, Roles: [], RolesHash: {} };
                if (!institutes[instituteName].RolesHash.hasOwnProperty(roleName)) {
                    var role = { Name: roleName, Count: 1 };
                    institutes[instituteName].RolesHash[roleName] = role;
                    institutes[instituteName].Roles.push(role);
                    institutes[instituteName].Roles = institutes[instituteName].Roles.sort(function (a, b) { return a.Name > b.Name ? 0 : 1; });
                }
                else {
                    institutes[instituteName].RolesHash[roleName].Count++;
                }
            }
            console.log(institutes);
            for (var key in institutes) {
                console.log(key);
                this.Institutes.push(institutes[key]);
            }
        };
        return Stats;
    }(DisposableComponent));
    return Stats;
});
