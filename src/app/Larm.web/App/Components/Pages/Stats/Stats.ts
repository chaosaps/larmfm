﻿import knockout = require("knockout");
import Notification = require("Managers/Notification");
import DisposableComponent = require("Components/DisposableComponent");

type Institute = { Name: string, Roles: { Name: string, Count: number }[], RolesHash: { [key: string]: { Name: string, Count: number } } };

class Stats extends DisposableComponent
{
	public Institutes:KnockoutObservableArray<any> = knockout.observableArray();

	constructor()
	{
		super();
	}

	public FileSelected(viewModel: any, event: JQueryInputEventObject): void
	{
		var input = <HTMLInputElement>event.target;

		if (input.files.length === 0)
			return;

		var reader = new FileReader();

		reader.onload = (event:Event) =>
		{
			this.LoadDate((<any>event.target).result);
		};

		reader.readAsText(input.files[0]);
	}

	private LoadDate(data: string): void
	{
		let parser = new DOMParser();
		let xmlDocument = parser.parseFromString(data, "text/xml");
		let tags = xmlDocument.getElementsByTagName("EZArchive.Portal.Module.Core.EzUser");

		let institutes: { [key: string]:Institute } = {};

		for (let i = 0; i < tags.length; i++)
		{
			let wayfTag = tags[i].getElementsByTagName("WayfAttributes");

			if (wayfTag.length === 0 || wayfTag[0].textContent === "")
				continue;

			let wayfData = JSON.parse(wayfTag[0].textContent);
			let instituteName = wayfData.schacHomeOrganization[0];
			let roleName = wayfData.eduPersonPrimaryAffiliation[0];

			if (!institutes.hasOwnProperty(instituteName))
				institutes[instituteName] = { Name: instituteName, Roles: [], RolesHash: {} };

			if (!institutes[instituteName].RolesHash.hasOwnProperty(roleName))
			{
				let role = { Name: roleName, Count: 1 };

				institutes[instituteName].RolesHash[roleName] = role;
				institutes[instituteName].Roles.push(role);
				institutes[instituteName].Roles = institutes[instituteName].Roles.sort((a, b) => a.Name > b.Name ? 0 : 1);
			} else
			{
				institutes[instituteName].RolesHash[roleName].Count++;
			}
		}

		console.log(institutes)

		for (var key in institutes)
		{
			console.log(key)
			this.Institutes.push(institutes[key]);
		}
			

	}
}

export = Stats;