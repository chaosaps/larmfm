﻿import knockout = require("knockout");
import Notification = require("Managers/Notification");
import Authorization = require("Managers/Authorization");
import DisposableComponent = require("Components/DisposableComponent");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import StreamTransformer from "Utility/StreamTransformer";
import Hls = require("Hls");

class Test extends DisposableComponent
{
	public Element = knockout.observable<HTMLDivElement>();
	public Output = knockout.observable("Waiting...")

	constructor()
	{
		super();

		this.AddAction(Authorization.IsAuthenticated, () => this.GetData());
	}

	private GetData(): void
	{
		EZArchivePortal.Asset.Get("9bddef5d-6ab0-40aa-ac94-b129648eb167").WithCallback(response =>
		{
			if (response.Error !== null)
				throw new Error(response.Error.Message);

			this.PlayUrl(StreamTransformer.UpdateAndGetHls(response.Body.Results[0].Files[0].Destinations[0].Url));
		});
	}

	private PlayUrl(url: string): void
	{
		this.CreatePlayer(element =>
		{
			this.Output("CanPlay: " + element.canPlayType("application/vnd.apple.mpegURL; codecs=\"mp3\"") + " - " + element.canPlayType("application/vnd.apple.mpegURL") + " - " + Hls.isSupported());

			const sourceElement = document.createElement("source");
			sourceElement.src = url;
			sourceElement.type = "application/vnd.apple.mpegURL; codecs=\"mp3\"";
			element.appendChild(sourceElement);
		});

		this.CreatePlayer(element =>
		{
			const sourceElement = document.createElement("source");
			sourceElement.src = url;
			sourceElement.type = "application/vnd.apple.mpegURL; codecs=mp";
			element.appendChild(sourceElement);
		});

		this.CreatePlayer(element =>
		{
			var hls = new Hls();
			hls.loadSource(url);
			hls.attachMedia(element as any);
		});
	}

	private CreatePlayer(callback: (element: HTMLAudioElement) => void): void
	{
		const element = document.createElement("audio") as HTMLAudioElement;
		element.setAttribute("controls", "");

		callback(element);

		const breakElement = document.createElement("br");

		this.Element().appendChild(breakElement);
		this.Element().appendChild(element);
	}
}

export = Test