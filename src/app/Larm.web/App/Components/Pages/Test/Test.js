var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Authorization", "Components/DisposableComponent", "Managers/Portal/EZArchivePortal", "Utility/StreamTransformer", "Hls"], function (require, exports, knockout, Authorization, DisposableComponent, EZArchivePortal, StreamTransformer_1, Hls) {
    "use strict";
    var Test = (function (_super) {
        __extends(Test, _super);
        function Test() {
            var _this = _super.call(this) || this;
            _this.Element = knockout.observable();
            _this.Output = knockout.observable("Waiting...");
            _this.AddAction(Authorization.IsAuthenticated, function () { return _this.GetData(); });
            return _this;
        }
        Test.prototype.GetData = function () {
            var _this = this;
            EZArchivePortal.Asset.Get("9bddef5d-6ab0-40aa-ac94-b129648eb167").WithCallback(function (response) {
                if (response.Error !== null)
                    throw new Error(response.Error.Message);
                _this.PlayUrl(StreamTransformer_1.default.UpdateAndGetHls(response.Body.Results[0].Files[0].Destinations[0].Url));
            });
        };
        Test.prototype.PlayUrl = function (url) {
            var _this = this;
            this.CreatePlayer(function (element) {
                _this.Output("CanPlay: " + element.canPlayType("application/vnd.apple.mpegURL; codecs=\"mp3\"") + " - " + element.canPlayType("application/vnd.apple.mpegURL") + " - " + Hls.isSupported());
                var sourceElement = document.createElement("source");
                sourceElement.src = url;
                sourceElement.type = "application/vnd.apple.mpegURL; codecs=\"mp3\"";
                element.appendChild(sourceElement);
            });
            this.CreatePlayer(function (element) {
                var sourceElement = document.createElement("source");
                sourceElement.src = url;
                sourceElement.type = "application/vnd.apple.mpegURL; codecs=mp";
                element.appendChild(sourceElement);
            });
            this.CreatePlayer(function (element) {
                var hls = new Hls();
                hls.loadSource(url);
                hls.attachMedia(element);
            });
        };
        Test.prototype.CreatePlayer = function (callback) {
            var element = document.createElement("audio");
            element.setAttribute("controls", "");
            callback(element);
            var breakElement = document.createElement("br");
            this.Element().appendChild(breakElement);
            this.Element().appendChild(element);
        };
        return Test;
    }(DisposableComponent));
    return Test;
});
