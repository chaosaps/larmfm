var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Notification", "Utility/DataFormat", "Components/DisposableComponent"], function (require, exports, knockout, Notification, DataFormat, DisposableComponent) {
    "use strict";
    var Tools = (function (_super) {
        __extends(Tools, _super);
        function Tools() {
            var _this = _super.call(this) || this;
            _this.TimeCode = knockout.observable("");
            _this.Milliseconds = knockout.observable("");
            return _this;
        }
        Tools.prototype.ConvertToMilliseconds = function () {
            try {
                var result = DataFormat.PrettyTimeToMillieSeconds(this.TimeCode());
                if (result === null)
                    Notification.Error("Failed to convert timecode");
                else
                    this.Milliseconds(result.toString(10));
            }
            catch (error) {
                Notification.Error("Failed to convert: " + error.message);
            }
        };
        Tools.prototype.ConvertToTimeCode = function () {
            try {
                var milliseconds = parseInt(this.Milliseconds(), 10);
                if (milliseconds === null || isNaN(milliseconds))
                    throw new Error("Failed to parse milliseconds");
                this.TimeCode(DataFormat.MillieSecondsToPrettyTime(milliseconds));
            }
            catch (error) {
                Notification.Error("Failed to convert: " + error.message);
            }
        };
        return Tools;
    }(DisposableComponent));
    return Tools;
});
