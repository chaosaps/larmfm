﻿import knockout = require("knockout");
import Notification = require("Managers/Notification");
import DataFormat = require("Utility/DataFormat");
import DisposableComponent = require("Components/DisposableComponent");

class Tools extends DisposableComponent
{
	public readonly TimeCode = knockout.observable("");
	public readonly Milliseconds = knockout.observable("");

	constructor()
	{
		super();
	}

	public ConvertToMilliseconds(): void
	{
		try {
			const result = DataFormat.PrettyTimeToMillieSeconds(this.TimeCode());

			if (result === null)
				Notification.Error("Failed to convert timecode");
			else
				this.Milliseconds(result.toString(10));
		} catch (error) {
			Notification.Error("Failed to convert: " + error.message);
		}
	}

	public ConvertToTimeCode(): void
	{
		try {
			const milliseconds = parseInt(this.Milliseconds(), 10);

			if (milliseconds === null || isNaN(milliseconds))
				throw new Error("Failed to parse milliseconds");

			this.TimeCode(DataFormat.MillieSecondsToPrettyTime(milliseconds));
		} catch (error) {
			Notification.Error("Failed to convert: " + error.message);
		}
	}
}

export = Tools