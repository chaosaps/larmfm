﻿import knockout = require("knockout");
import Notification = require("Managers/Notification");
import DisposableComponent = require("Components/DisposableComponent");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import ISearchResult = EZArchivePortal.ISearchResult;

const UnkownYear = "Ukendt";

type YearCount = { Year: string, Count: KnockoutObservable<number> };

class Analyse extends DisposableComponent
{
	public DetailsLoaded: KnockoutComputed<number>;

	public Total = knockout.observable<number | string>("...");
	public Loaded = knockout.observable<number | string>("...");
	public AffectedYears = knockout.observableArray<YearCount>();
	public Items = knockout.observableArray<Item>();

	public IsGettingDetails = knockout.observable(false);

	private _pageSize = 999;
	private _nextDetailLoadIndex = 0;
	private _maxConcurrentDetailLoads = 3;
	private _currentActiveDetailLoads = 0;

	constructor()
	{
		super();
		this.LoadMore(0);

		this.DetailsLoaded = this.PureComputed(() => this.Items().filter(i => i.HasLoadedDetails()).length);
	}

	public ToggleGetDetails(): void
	{
		this.IsGettingDetails(!this.IsGettingDetails());

		if (this.IsGettingDetails())
			this.GetMoreDetails();
	}

	private GetMoreDetails(): void
	{
		if (this._currentActiveDetailLoads >= this._maxConcurrentDetailLoads || !this.IsGettingDetails())
			return;

		if (this._nextDetailLoadIndex >= this.Items().length)
		{
			this.IsGettingDetails(false);
			return;
		}

		const item = this.Items()[this._nextDetailLoadIndex++];

		if (!item.HasStartedLoading())
		{
			this._currentActiveDetailLoads++;
			item.Load(() =>
			{
				this._currentActiveDetailLoads--;
				this.GetMoreDetails();
			});
		}
		
		this.GetMoreDetails();
	}

	private LoadMore(pageIndex: number):void
	{
		EZArchivePortal.Search.Get(".xml", null, null, null, "{Document}.TypeId:86", null, pageIndex, this._pageSize).WithCallback(response =>
		{
			if (response.Error !== null)
			{
				Notification.Error("Failed to search: " + response.Error.Message);
				return;
			}

			if (response.Body.TotalCount > (pageIndex + 1) * this._pageSize)
				this.LoadMore(pageIndex + 1);

			this.Total(response.Body.TotalCount);
			this.Items.push(...response.Body.Results.map(r =>
			{
				const item = new Item(r);

				this.SubscribeUntilChange(item.Year, year => this.AddYear(year));

				return item;
			}));
			this.Loaded(this.Items().length);
		});
	}

	private AddYear(year: string)
	{
		for (const count of this.AffectedYears())
		{
			if (count.Year !== year)
				continue;

			count.Count(count.Count() + 1);

			return;
		}

		this.AffectedYears.push({ Year: year, Count: knockout.observable(1) });

		this.AffectedYears.sort((y1, y2) => y1.Year.localeCompare(y2.Year));
	}
}

class Item
{
	public Identifier: string;
	public TypeId: string;
	public Title: string | null = null;
	public NumberOfFiles = knockout.observable<number | null>(null);
	public NumberOfMissingFiles = knockout.observable<number | null>(null);
	public MissingFiles = knockout.observableArray<string>();
	public HasStartedLoading = knockout.observable(false);
	public HasLoadedDetails = knockout.observable(false);
	public Year = knockout.observable<string | null>(null);

	constructor(data: ISearchResult)
	{
		this.Identifier = data.Identifier;
		this.TypeId = data.TypeId;

		for (const field of data.Fields)
		{
			if (field.Key !== "Titel")
				continue;

			this.Title = field.Value;
			break;
		}
	}

	public Load(callback?:()=>void): void
	{
		if (this.HasStartedLoading())
			return;

		this.HasStartedLoading(true);

		EZArchivePortal.Asset.Get(this.Identifier).WithCallback(response =>
		{
			if (response.Error !== null)
			{
				Notification.Error("Failed to load asset: " + response.Error.Message);
				this.HasLoadedDetails(true);
				if (callback)
					callback();
				return;
			}

			if (response.Body.Results.length === 0)
				Notification.Error("Asset not found: " + this.Identifier);
			else
				this.LoadData(response.Body.Results[0]);

			this.HasLoadedDetails(true);

			if (callback)
				callback();
		});
	}

	private LoadData(result: EZArchivePortal.IAsset): void
	{
		this.NumberOfFiles(result.Files.length);
		
		for (const data of result.Data)
		{
			if (data.Name !== "Programoversigt Arkiv metadata")
				continue;

			if (data.Fields.hasOwnProperty("RawText"))
			{
				const raw = data.Fields["RawText"] as Array<Array<any>>;
				const missing = raw.filter(file => file.length >= 3 && file[2].indexOf(".xml not found.") !== -1);
				this.NumberOfMissingFiles(missing.length);

				this.MissingFiles.push(...missing.map(m => m[2]));
			}

			if (data.Fields.hasOwnProperty("Date") && data.Fields["Date"].length === 20)
			{
				this.Year((data.Fields["Date"] as string).substr(0, 4));
			} else
				this.Year(UnkownYear);

			if (this.NumberOfMissingFiles() === null || this.NumberOfMissingFiles() === 0)
			{
				console.log("Not missing: " + this.Identifier, this.NumberOfMissingFiles());
			}

			break;
		}

	}
}

export = Analyse