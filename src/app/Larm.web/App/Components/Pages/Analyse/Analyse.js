var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Notification", "Components/DisposableComponent", "Managers/Portal/EZArchivePortal"], function (require, exports, knockout, Notification, DisposableComponent, EZArchivePortal) {
    "use strict";
    var UnkownYear = "Ukendt";
    var Analyse = (function (_super) {
        __extends(Analyse, _super);
        function Analyse() {
            var _this = _super.call(this) || this;
            _this.Total = knockout.observable("...");
            _this.Loaded = knockout.observable("...");
            _this.AffectedYears = knockout.observableArray();
            _this.Items = knockout.observableArray();
            _this.IsGettingDetails = knockout.observable(false);
            _this._pageSize = 999;
            _this._nextDetailLoadIndex = 0;
            _this._maxConcurrentDetailLoads = 3;
            _this._currentActiveDetailLoads = 0;
            _this.LoadMore(0);
            _this.DetailsLoaded = _this.PureComputed(function () { return _this.Items().filter(function (i) { return i.HasLoadedDetails(); }).length; });
            return _this;
        }
        Analyse.prototype.ToggleGetDetails = function () {
            this.IsGettingDetails(!this.IsGettingDetails());
            if (this.IsGettingDetails())
                this.GetMoreDetails();
        };
        Analyse.prototype.GetMoreDetails = function () {
            var _this = this;
            if (this._currentActiveDetailLoads >= this._maxConcurrentDetailLoads || !this.IsGettingDetails())
                return;
            if (this._nextDetailLoadIndex >= this.Items().length) {
                this.IsGettingDetails(false);
                return;
            }
            var item = this.Items()[this._nextDetailLoadIndex++];
            if (!item.HasStartedLoading()) {
                this._currentActiveDetailLoads++;
                item.Load(function () {
                    _this._currentActiveDetailLoads--;
                    _this.GetMoreDetails();
                });
            }
            this.GetMoreDetails();
        };
        Analyse.prototype.LoadMore = function (pageIndex) {
            var _this = this;
            EZArchivePortal.Search.Get(".xml", null, null, null, "{Document}.TypeId:86", null, pageIndex, this._pageSize).WithCallback(function (response) {
                var _a;
                if (response.Error !== null) {
                    Notification.Error("Failed to search: " + response.Error.Message);
                    return;
                }
                if (response.Body.TotalCount > (pageIndex + 1) * _this._pageSize)
                    _this.LoadMore(pageIndex + 1);
                _this.Total(response.Body.TotalCount);
                (_a = _this.Items).push.apply(_a, response.Body.Results.map(function (r) {
                    var item = new Item(r);
                    _this.SubscribeUntilChange(item.Year, function (year) { return _this.AddYear(year); });
                    return item;
                }));
                _this.Loaded(_this.Items().length);
            });
        };
        Analyse.prototype.AddYear = function (year) {
            for (var _i = 0, _a = this.AffectedYears(); _i < _a.length; _i++) {
                var count = _a[_i];
                if (count.Year !== year)
                    continue;
                count.Count(count.Count() + 1);
                return;
            }
            this.AffectedYears.push({ Year: year, Count: knockout.observable(1) });
            this.AffectedYears.sort(function (y1, y2) { return y1.Year.localeCompare(y2.Year); });
        };
        return Analyse;
    }(DisposableComponent));
    var Item = (function () {
        function Item(data) {
            this.Title = null;
            this.NumberOfFiles = knockout.observable(null);
            this.NumberOfMissingFiles = knockout.observable(null);
            this.MissingFiles = knockout.observableArray();
            this.HasStartedLoading = knockout.observable(false);
            this.HasLoadedDetails = knockout.observable(false);
            this.Year = knockout.observable(null);
            this.Identifier = data.Identifier;
            this.TypeId = data.TypeId;
            for (var _i = 0, _a = data.Fields; _i < _a.length; _i++) {
                var field = _a[_i];
                if (field.Key !== "Titel")
                    continue;
                this.Title = field.Value;
                break;
            }
        }
        Item.prototype.Load = function (callback) {
            var _this = this;
            if (this.HasStartedLoading())
                return;
            this.HasStartedLoading(true);
            EZArchivePortal.Asset.Get(this.Identifier).WithCallback(function (response) {
                if (response.Error !== null) {
                    Notification.Error("Failed to load asset: " + response.Error.Message);
                    _this.HasLoadedDetails(true);
                    if (callback)
                        callback();
                    return;
                }
                if (response.Body.Results.length === 0)
                    Notification.Error("Asset not found: " + _this.Identifier);
                else
                    _this.LoadData(response.Body.Results[0]);
                _this.HasLoadedDetails(true);
                if (callback)
                    callback();
            });
        };
        Item.prototype.LoadData = function (result) {
            var _a;
            this.NumberOfFiles(result.Files.length);
            for (var _i = 0, _b = result.Data; _i < _b.length; _i++) {
                var data = _b[_i];
                if (data.Name !== "Programoversigt Arkiv metadata")
                    continue;
                if (data.Fields.hasOwnProperty("RawText")) {
                    var raw = data.Fields["RawText"];
                    var missing = raw.filter(function (file) { return file.length >= 3 && file[2].indexOf(".xml not found.") !== -1; });
                    this.NumberOfMissingFiles(missing.length);
                    (_a = this.MissingFiles).push.apply(_a, missing.map(function (m) { return m[2]; }));
                }
                if (data.Fields.hasOwnProperty("Date") && data.Fields["Date"].length === 20) {
                    this.Year(data.Fields["Date"].substr(0, 4));
                }
                else
                    this.Year(UnkownYear);
                if (this.NumberOfMissingFiles() === null || this.NumberOfMissingFiles() === 0) {
                    console.log("Not missing: " + this.Identifier, this.NumberOfMissingFiles());
                }
                break;
            }
        };
        return Item;
    }());
    return Analyse;
});
