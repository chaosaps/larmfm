var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/DisposableComponent"], function (require, exports, knockout, DisposableComponent) {
    "use strict";
    var Range = (function (_super) {
        __extends(Range, _super);
        function Range(data) {
            var _this = _super.call(this) || this;
            _this.IsDragging = knockout.observable(false);
            var lastValue = data.Value();
            _this.Position = _this.PureComputed(function () {
                if (_this.IsDragging())
                    return lastValue;
                lastValue = data.Value();
                return lastValue;
            }, function (value) {
                data.Value(value);
                _this.EndDrag();
            });
            return _this;
        }
        Range.prototype.StartDrag = function () {
            this.IsDragging(true);
            return true;
        };
        Range.prototype.EndDrag = function () {
            this.IsDragging(false);
            return true;
        };
        return Range;
    }(DisposableComponent));
    return Range;
});
