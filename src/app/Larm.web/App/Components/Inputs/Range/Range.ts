﻿import knockout = require("knockout");
import DisposableComponent = require("Components/DisposableComponent");

class Range extends DisposableComponent {
	public Position: KnockoutComputed<number>;
	public IsDragging = knockout.observable(false);

	constructor(data: {Value: KnockoutComputed<number>}) {
		super();

		let lastValue = data.Value();
		this.Position = this.PureComputed(() =>
		{
			if (this.IsDragging())
				return lastValue;

			lastValue = data.Value();
			return lastValue;
		}, value =>
			{
				data.Value(value);
				this.EndDrag();
			});
	}

	public StartDrag(): boolean
	{
		this.IsDragging(true);
		return true;
	}

	public EndDrag(): boolean
	{
		this.IsDragging(false);
		return true;
	}
}

export = Range