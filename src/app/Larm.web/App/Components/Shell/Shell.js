define(["require", "exports", "knockout", "Managers/Navigation/Navigation", "Managers/Notification", "Managers/Storage", "Managers/Interface", "Managers/Tracking"], function (require, exports, knockout, Navigation, Notification, Storage, Interface, Tracking) {
    "use strict";
    var Shell = (function () {
        function Shell() {
            this.IsCookieDialogVisible = knockout.observable(false);
            this.HideCookieDialogKey = "HideCookieDialog";
            this.CookiesKey = "Cookies";
            this.CookiesAcceptValue = "Accept";
            this.CookiesDenyValue = "Deny";
            this.Page = Navigation.Current;
            this.Notifications = Notification.Notifications;
            if (this.Page() !== null && this.Page().Name.indexOf("Asset") === 0)
                Interface.Hide();
            this.InitializeTracking();
        }
        Shell.prototype.InitializeTracking = function () {
            var oldValue = Storage.Get(this.HideCookieDialogKey);
            if (oldValue !== null) {
                Storage.Remove(this.HideCookieDialogKey);
                Storage.Set(this.CookiesKey, this.CookiesAcceptValue);
            }
            var cookies = Storage.Get(this.CookiesKey);
            this.IsCookieDialogVisible(cookies === null);
            if (cookies === this.CookiesAcceptValue)
                Tracking.StartTracking();
        };
        Shell.prototype.CloseAndAcceptCookieDialog = function () {
            this.IsCookieDialogVisible(false);
            Storage.Set(this.CookiesKey, this.CookiesAcceptValue);
            Tracking.StartTracking();
        };
        Shell.prototype.CloseCookieDialog = function () {
            this.IsCookieDialogVisible(false);
            Storage.Set(this.CookiesKey, this.CookiesDenyValue);
        };
        return Shell;
    }());
    return Shell;
});
