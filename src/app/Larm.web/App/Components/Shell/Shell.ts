﻿import knockout = require("knockout");
import Navigation = require("Managers/Navigation/Navigation");
import Notification = require("Managers/Notification");
import Storage = require("Managers/Storage");
import Interface = require("Managers/Interface");
import Tracking = require("Managers/Tracking");

class Shell
{
	public Page:KnockoutObservable<IPage>;
	public Notifications: KnockoutObservableArray<INotification>;
	public IsCookieDialogVisible = knockout.observable(false);

	private HideCookieDialogKey = "HideCookieDialog";
	private CookiesKey = "Cookies";
	private CookiesAcceptValue = "Accept";
	private CookiesDenyValue = "Deny";

	constructor()
	{
		this.Page = Navigation.Current;
		this.Notifications = Notification.Notifications;

		if (this.Page() !== null && this.Page().Name.indexOf("Asset") === 0)
			Interface.Hide();

		this.InitializeTracking();
	}

	private InitializeTracking(): void
	{
		const oldValue = Storage.Get(this.HideCookieDialogKey);

		if (oldValue !== null)
		{
			Storage.Remove(this.HideCookieDialogKey);
			Storage.Set(this.CookiesKey, this.CookiesAcceptValue);
		}

		const cookies = Storage.Get(this.CookiesKey);

		this.IsCookieDialogVisible(cookies === null);

		if (cookies === this.CookiesAcceptValue)
			Tracking.StartTracking();
	}

	public CloseAndAcceptCookieDialog(): void
	{
		this.IsCookieDialogVisible(false);
		Storage.Set(this.CookiesKey, this.CookiesAcceptValue);
		Tracking.StartTracking();
	}

	public CloseCookieDialog(): void
	{
		this.IsCookieDialogVisible(false);
		Storage.Set(this.CookiesKey, this.CookiesDenyValue);
	}
}

export = Shell;