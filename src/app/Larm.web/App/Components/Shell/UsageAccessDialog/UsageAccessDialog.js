var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Components/DisposableComponent", "Managers/Dialog/Dialog", "Managers/Storage", "Managers/Navigation/Navigation", "Managers/Portal/Portal"], function (require, exports, DisposableComponent, DialogManager, Storage, Navigation, Portal) {
    "use strict";
    var UsageAccessDialog = (function (_super) {
        __extends(UsageAccessDialog, _super);
        function UsageAccessDialog(state) {
            var _this = _super.call(this) || this;
            _this._state = state;
            return _this;
        }
        UsageAccessDialog.prototype.ViewTerms = function () {
            UsageAccessDialog.SetAccepted();
            Navigation.Navigate("Terms");
        };
        UsageAccessDialog.prototype.Ok = function () {
            UsageAccessDialog.SetAccepted();
        };
        UsageAccessDialog.SetAccepted = function () {
            Storage.Set(UsageAccessDialog.UsageAcceptedKey, true.toString());
        };
        UsageAccessDialog.Check = function () {
            if (Storage.Get(this.UsageAcceptedKey) === true.toString())
                return;
            var open = function () {
                if (Portal.IsAuthenticated()) {
                    UsageAccessDialog.SetAccepted();
                    return;
                }
                DialogManager.Show("Shell/UsageAccessDialog", null, false, false);
            };
            if (Portal.HasCheckedAuthorization())
                open();
            else {
                var subscription_1 = Portal.HasCheckedAuthorization.subscribe(function () {
                    subscription_1.dispose();
                    open();
                });
            }
        };
        UsageAccessDialog.UsageAcceptedKey = "UsageAccepted";
        return UsageAccessDialog;
    }(DisposableComponent));
    return UsageAccessDialog;
});
