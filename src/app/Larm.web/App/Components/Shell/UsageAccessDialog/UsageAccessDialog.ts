﻿import DialogState = require("Managers/Dialog/DialogState");
import DisposableComponent = require("Components/DisposableComponent");
import DialogManager = require("Managers/Dialog/Dialog");
import Storage = require("Managers/Storage");
import Navigation = require("Managers/Navigation/Navigation");
import Portal = require("Managers/Portal/Portal");

class UsageAccessDialog extends DisposableComponent
{
	private static UsageAcceptedKey = "UsageAccepted";

	private _state: DialogState<void>;

	constructor(state: DialogState<void>)
	{
		super();
		this._state = state;
	}

	public ViewTerms(): void
	{
		UsageAccessDialog.SetAccepted();
		Navigation.Navigate("Terms");
	}

	public Ok(): void
	{
		UsageAccessDialog.SetAccepted();
	}

	private static SetAccepted(): void
	{
		Storage.Set(UsageAccessDialog.UsageAcceptedKey, true.toString());
	}

	public static Check(): void
	{
		if (Storage.Get(this.UsageAcceptedKey) === true.toString())
			return;
		const open = () =>
		{
			if (Portal.IsAuthenticated()) {
				UsageAccessDialog.SetAccepted();
				return;
			}
			DialogManager.Show("Shell/UsageAccessDialog", null, false, false);
		};
		if (Portal.HasCheckedAuthorization())
			open();
		else {
			const subscription = Portal.HasCheckedAuthorization.subscribe(() =>
			{
				subscription.dispose();
				open();
			});
		}
	}
}

export = UsageAccessDialog;