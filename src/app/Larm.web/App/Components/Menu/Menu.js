var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Search/Search", "Managers/Search/Query", "Managers/Portal/Portal", "Managers/Search/MenuFacets", "Managers/Interface", "Components/DisposableComponent"], function (require, exports, knockout, Search, Query, LarmPortal, MenuFacets, Interface, DisposableComponent) {
    "use strict";
    var Menu = (function (_super) {
        __extends(Menu, _super);
        function Menu() {
            var _this = _super.call(this) || this;
            _this.Query = knockout.observable("");
            _this.QueryHasFocus = knockout.observable(true);
            _this.SearchIsCollapsed = knockout.observable(false);
            _this.IsCollapsed = Interface.IsMenuCollapsed;
            _this._isUpdating = false;
            _this.Facets = MenuFacets.Facets;
            var queryUpdater = function (query) {
                _this.LoadFromQuery(query);
            };
            _this.IsAuthenticated = LarmPortal.IsAuthenticated;
            _this.Subscribe(_this.QueryHasFocus, function (v) {
                if (!v && _this.Query() !== Search.Query().Query)
                    _this.Search();
            });
            _this.Subscribe(Search.Query, queryUpdater);
            queryUpdater(Search.Query());
            return _this;
        }
        Menu.Show = function () {
            Interface.Show();
        };
        Menu.Hide = function () {
            Interface.Hide();
        };
        Menu.prototype.Search = function () {
            var query = Search.Clone();
            query.Query = this.Query();
            query.PageNumber = null;
            Search.Search(query);
        };
        Menu.prototype.ClearSearch = function () {
            Search.Search(new Query());
        };
        Menu.prototype.ToggleMenu = function () {
            this.IsCollapsed(!this.IsCollapsed());
        };
        Menu.prototype.LoadFromQuery = function (query) {
            this._isUpdating = true;
            this.Query(query.Query);
            this._isUpdating = false;
            if ((query.From != null || query.To != null) || (query.Facets != null && query.Facets.length !== 0))
                this.SearchIsCollapsed(false);
        };
        return Menu;
    }(DisposableComponent));
    return Menu;
});
