﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Configuration = require("Managers/Configuration");
import Search = require("Managers/Search/Search");
import SessionStorage = require("Managers/SessionStorage");
import DisposableComponent = require("Components/DisposableComponent");
import DataFormat = require("Utility/DataFormat");

type Field = { Key: string; HasIcon:boolean, IconPath:string, Count: string, IsActive: KnockoutComputed<boolean>, Toggle: () => void };
type FaceData = { Key: string, Value: string };

class Facet extends DisposableComponent
{
	public Name: string;
	public Fields: KnockoutObservable<Field[]>;
	public IsCollapsed = knockout.observable(false);
	public IsShowingAll = knockout.observable(false);
	public CanShowMore = false;

	private static readonly CollapsedStorageKey = "MenuFacetCollapsed_";
	private static readonly ShowingAllStorageKey = "MenuFacetShowingAll_";
	private _allFields: Field[];
	private _fewFields: Field[];
	private _key: string;
	private _map: { Name: string, HasIcon:boolean, Values: { [key: string]: string } };

	constructor(data: EZArchivePortal.IEZFacetField)
	{
		super();

		this._key = data.Value;
		this._map = Configuration.FacetKeys.hasOwnProperty(this._key) ? Configuration.FacetKeys[this._key] : null;
		this.Name = this._map != null && this._map.Name != null ? this._map.Name : this._key;
		var matching = this.PureComputed(() => this.GetMatching(Search.Query().Facets)) ;

		this._allFields = data.Facets.map(f => this.CreateField(f, matching));
		this._fewFields = this._allFields.slice(0, Math.min(Configuration.FacetValuesVisible, this._allFields.length));
		this.CanShowMore = this._allFields.length !== this._fewFields.length;
		this.Fields = knockout.observable(this._fewFields);

		this.InitializeCollapsed();
	}

	public ToggleShowAll(): void
	{
		var isShowingAll = this.IsShowingAll();

		this.IsShowingAll(!isShowingAll);

		this.Fields(isShowingAll ? this._fewFields : this._allFields);
	}

	private InitializeCollapsed(): void
	{
		this.Subscribe(this.IsCollapsed, isCollapsed => SessionStorage.Set(collapsedStorageKey, isCollapsed.toString()));
		this.Subscribe(this.IsShowingAll, isShowingAll => SessionStorage.Set(showingAllStorageKey, isShowingAll.toString()));

		const collapsedStorageKey = Facet.CollapsedStorageKey + this._key;
		const showingAllStorageKey = Facet.ShowingAllStorageKey + this._key;
		const isCollapsed = SessionStorage.Get(collapsedStorageKey);

		if (isCollapsed !== null) {
			this.IsCollapsed(isCollapsed === "true");

			const isShowingAll = SessionStorage.Get(showingAllStorageKey);
			this.IsShowingAll(isShowingAll === "true");
		} else if (this._allFields.some(f => f.IsActive())) {
			this.IsCollapsed(false);

			if (!this._fewFields.some(f => f.IsActive()) && !this.IsShowingAll())
				this.ToggleShowAll();
		}
	}

	private GetMatching(facets: FaceData[]): FaceData
	{
		if (facets == null) return null;

		var matching = facets.filter(f => f.Key === this._key);

		return matching.length === 0 ? null : matching[0];
	}

	private CreateField(facetField: EZArchivePortal.IEZFacetFieldValue, matching: KnockoutComputed<FaceData>): Field
	{
		var isActive = this.PureComputed(() => matching() != null && matching().Value === facetField.Key);

		return {
			Key: this._map != null && this._map.Values != null && this._map.Values.hasOwnProperty(facetField.Key) ? this._map.Values[facetField.Key] : facetField.Key,
			HasIcon: this._map != null && this._map.HasIcon,
			IconPath: this._map != null && this._map.HasIcon ? `/App/Images/SearchFields/${this.Name}/${facetField.Key}.png` : null,
			Count: DataFormat.PrettyNumber(facetField.Count),
			IsActive: isActive,
			Toggle: () =>
			{
				var query = Search.Clone();

				query.Facets = isActive() ? null : [{ Key: this._key, Value: facetField.Key }];
				query.PageNumber = null;

				Search.Search(query);
			}
		};
	}
}

export = Facet;