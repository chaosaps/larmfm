var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Configuration", "Managers/Search/Search", "Managers/SessionStorage", "Components/DisposableComponent", "Utility/DataFormat"], function (require, exports, knockout, Configuration, Search, SessionStorage, DisposableComponent, DataFormat) {
    "use strict";
    var Facet = (function (_super) {
        __extends(Facet, _super);
        function Facet(data) {
            var _this = _super.call(this) || this;
            _this.IsCollapsed = knockout.observable(false);
            _this.IsShowingAll = knockout.observable(false);
            _this.CanShowMore = false;
            _this._key = data.Value;
            _this._map = Configuration.FacetKeys.hasOwnProperty(_this._key) ? Configuration.FacetKeys[_this._key] : null;
            _this.Name = _this._map != null && _this._map.Name != null ? _this._map.Name : _this._key;
            var matching = _this.PureComputed(function () { return _this.GetMatching(Search.Query().Facets); });
            _this._allFields = data.Facets.map(function (f) { return _this.CreateField(f, matching); });
            _this._fewFields = _this._allFields.slice(0, Math.min(Configuration.FacetValuesVisible, _this._allFields.length));
            _this.CanShowMore = _this._allFields.length !== _this._fewFields.length;
            _this.Fields = knockout.observable(_this._fewFields);
            _this.InitializeCollapsed();
            return _this;
        }
        Facet.prototype.ToggleShowAll = function () {
            var isShowingAll = this.IsShowingAll();
            this.IsShowingAll(!isShowingAll);
            this.Fields(isShowingAll ? this._fewFields : this._allFields);
        };
        Facet.prototype.InitializeCollapsed = function () {
            this.Subscribe(this.IsCollapsed, function (isCollapsed) { return SessionStorage.Set(collapsedStorageKey, isCollapsed.toString()); });
            this.Subscribe(this.IsShowingAll, function (isShowingAll) { return SessionStorage.Set(showingAllStorageKey, isShowingAll.toString()); });
            var collapsedStorageKey = Facet.CollapsedStorageKey + this._key;
            var showingAllStorageKey = Facet.ShowingAllStorageKey + this._key;
            var isCollapsed = SessionStorage.Get(collapsedStorageKey);
            if (isCollapsed !== null) {
                this.IsCollapsed(isCollapsed === "true");
                var isShowingAll = SessionStorage.Get(showingAllStorageKey);
                this.IsShowingAll(isShowingAll === "true");
            }
            else if (this._allFields.some(function (f) { return f.IsActive(); })) {
                this.IsCollapsed(false);
                if (!this._fewFields.some(function (f) { return f.IsActive(); }) && !this.IsShowingAll())
                    this.ToggleShowAll();
            }
        };
        Facet.prototype.GetMatching = function (facets) {
            var _this = this;
            if (facets == null)
                return null;
            var matching = facets.filter(function (f) { return f.Key === _this._key; });
            return matching.length === 0 ? null : matching[0];
        };
        Facet.prototype.CreateField = function (facetField, matching) {
            var _this = this;
            var isActive = this.PureComputed(function () { return matching() != null && matching().Value === facetField.Key; });
            return {
                Key: this._map != null && this._map.Values != null && this._map.Values.hasOwnProperty(facetField.Key) ? this._map.Values[facetField.Key] : facetField.Key,
                HasIcon: this._map != null && this._map.HasIcon,
                IconPath: this._map != null && this._map.HasIcon ? "/App/Images/SearchFields/" + this.Name + "/" + facetField.Key + ".png" : null,
                Count: DataFormat.PrettyNumber(facetField.Count),
                IsActive: isActive,
                Toggle: function () {
                    var query = Search.Clone();
                    query.Facets = isActive() ? null : [{ Key: _this._key, Value: facetField.Key }];
                    query.PageNumber = null;
                    Search.Search(query);
                }
            };
        };
        Facet.CollapsedStorageKey = "MenuFacetCollapsed_";
        Facet.ShowingAllStorageKey = "MenuFacetShowingAll_";
        return Facet;
    }(DisposableComponent));
    return Facet;
});
