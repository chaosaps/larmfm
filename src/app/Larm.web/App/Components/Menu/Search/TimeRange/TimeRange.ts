﻿import knockout = require("knockout");
import Search = require("Managers/Search/Search");
import Query = require("Managers/Search/Query");
import DialogManager = require("Managers/Dialog/Dialog");
import DisposableComponent = require("Components/DisposableComponent");

class TimeRange extends DisposableComponent
{
	public From: KnockoutObservable<Date> = knockout.observable<Date>(null);
	public To: KnockoutObservable<Date> = knockout.observable<Date>(null);
	public IsCollapsed = knockout.observable(false);
	private _isUpdating: boolean = false;

	constructor()
	{
		super();

		this.LoadFromQuery(Search.Query());

		Search.Query.subscribe(q => this.LoadFromQuery(q));

		this.Subscribe(this.From, v =>
		{
			if (!this._isUpdating && !(v != null && this.To() == null)) this.Search();
		});
		this.Subscribe(this.To, v =>
		{
			if (!this._isUpdating && !(v != null && this.From() == null)) this.Search();
		});
	}

	public Search(): void
	{
		var query = Search.Clone();
		query.From = this.From();
		query.To = this.To();
		query.PageNumber = null;
		query.Label = null;

		Search.Search(query);
	}

	private LoadFromQuery(query: Query): void
	{
		this._isUpdating = true;
		this.From(query.From);
		this.To(query.To);
		this._isUpdating = false;

		if (query.From != null || query.To != null)
			this.IsCollapsed(false);
	}

	public ShowDrillDown(): void
	{
		DialogManager.Show("Pages/Search/DateDrillDownDialog", {});
	}

	public ResetTimeRange(): void
	{
		this.From(null);
		this.To(null);
		this.Search();
	}
}

export = TimeRange;