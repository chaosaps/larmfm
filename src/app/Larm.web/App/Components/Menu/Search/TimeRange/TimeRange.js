var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Search/Search", "Managers/Dialog/Dialog", "Components/DisposableComponent"], function (require, exports, knockout, Search, DialogManager, DisposableComponent) {
    "use strict";
    var TimeRange = (function (_super) {
        __extends(TimeRange, _super);
        function TimeRange() {
            var _this = _super.call(this) || this;
            _this.From = knockout.observable(null);
            _this.To = knockout.observable(null);
            _this.IsCollapsed = knockout.observable(false);
            _this._isUpdating = false;
            _this.LoadFromQuery(Search.Query());
            Search.Query.subscribe(function (q) { return _this.LoadFromQuery(q); });
            _this.Subscribe(_this.From, function (v) {
                if (!_this._isUpdating && !(v != null && _this.To() == null))
                    _this.Search();
            });
            _this.Subscribe(_this.To, function (v) {
                if (!_this._isUpdating && !(v != null && _this.From() == null))
                    _this.Search();
            });
            return _this;
        }
        TimeRange.prototype.Search = function () {
            var query = Search.Clone();
            query.From = this.From();
            query.To = this.To();
            query.PageNumber = null;
            query.Label = null;
            Search.Search(query);
        };
        TimeRange.prototype.LoadFromQuery = function (query) {
            this._isUpdating = true;
            this.From(query.From);
            this.To(query.To);
            this._isUpdating = false;
            if (query.From != null || query.To != null)
                this.IsCollapsed(false);
        };
        TimeRange.prototype.ShowDrillDown = function () {
            DialogManager.Show("Pages/Search/DateDrillDownDialog", {});
        };
        TimeRange.prototype.ResetTimeRange = function () {
            this.From(null);
            this.To(null);
            this.Search();
        };
        return TimeRange;
    }(DisposableComponent));
    return TimeRange;
});
