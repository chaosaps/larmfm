﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Search = require("Managers/Search/Search");
import ProjectManager = require("Managers/ProjectManager");
import DisposableComponent = require("Components/DisposableComponent");

class Projects extends DisposableComponent
{
	public Projects: KnockoutObservableArray<EZArchivePortal.IEZProject>;
	public IsCollapsed = knockout.observable(false);
	public CanCreate = knockout.observable(true);

	public CreatedCallback: (project: EZArchivePortal.IEZProject) => void;
	public DragEnterHandle: (event: DragEvent) => boolean;

	constructor()
	{
		super();

		this.CreatedCallback = () => this.CanCreate(true);
		this.DragEnterHandle = event =>
		{
			this.IsCollapsed(false);
			return true;
		};

		this.Projects = ProjectManager.Projects;

		if (Search.Query().Label != null)
			this.IsCollapsed(false);

		this.Subscribe(Search.Query, q =>
		{
			if (q.Label != null)
				this.IsCollapsed(false);
		});

	}

	public Create():boolean
	{
		this.Projects.push({ Name: "" });
		this.IsCollapsed(false);
		this.CanCreate(false);

		return true;
	}
}

export = Projects;