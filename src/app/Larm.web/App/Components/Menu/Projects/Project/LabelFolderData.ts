﻿import knockout = require("knockout");
import DisposableComponent = require("Components/DisposableComponent");
import EzArchivePortal = require("Managers/Portal/EZArchivePortal");
import IEZLabel = EzArchivePortal.IEZLabel;

export default class LabelFolderData extends DisposableComponent
{
	public readonly Name = knockout.observable("");
	public readonly Folders = knockout.observableArray<LabelFolderData>();
	public readonly Label = knockout.observable<IEZLabel | null>(null);
	public readonly ExtraLabels = knockout.observableArray<IEZLabel>();
	public readonly HasLabel: KnockoutComputed<boolean>;

	public readonly ExpandTrigger = knockout.observable(0);

	public readonly  ProjectId: string;
	public readonly  Delete: (label: IEZLabel, reAdd: boolean) => void;

	private readonly _level: number;
	private readonly _deleteCallback: (label: IEZLabel, folder: LabelFolderData | null, reAdd: boolean) => void;

	constructor(projectId: string, deleteCallback: (label: IEZLabel, folder: LabelFolderData | null, reAdd: boolean) => void, label: IEZLabel, level: number = 0)
	{
		super();

		this.ProjectId = projectId;
		this._deleteCallback = deleteCallback;
		this._level = level;

		this.Delete = (label, reAdd) => this.InnerDelete(label, reAdd);

		const levels = LabelFolderData.GetLevels(label);

		this.Name(levels[level]);
		this.HasLabel = this.PureComputed(() => this.Label() !== null);

		this.HandleLabel(label, levels);
	}

	public AddLabel(label: IEZLabel): boolean
	{
		const levels = LabelFolderData.GetLevels(label);

		if (levels[this._level] !== this.Name())
			return false;

		this.HandleLabel(label, levels);

		return true;
	}

	public TriggerExpand(): void
	{
		this.ExpandTrigger(this.ExpandTrigger() + 1);
	}

	private HandleLabel(label: IEZLabel, levels: string[]): void
	{
		const nextLevel = this._level + 1;

		if (nextLevel === levels.length) {
			if (this.HasLabel())
				this.ExtraLabels.push(label);
			else
				this.Label(label);
		} else {
			for (const folder of this.Folders())
				if (folder.AddLabel(label))
					return;

			LabelFolderData.AlphabeticAddFolder(this.Folders, (new LabelFolderData(this.ProjectId, (label, folder, reAdd) =>
			{
				if (folder !== null)
					this.Folders.remove(folder);
				this.CallDelete(label, reAdd);
				
			}, label, nextLevel)));
		}
	}

	private InnerDelete(label: EzArchivePortal.IEZLabel, reAdd: boolean): void {
		if (this.Label().Identifier === label.Identifier)
			this.Label(this.ExtraLabels().length > 0
				? this.ExtraLabels.shift()
				: null);
		else
			this.ExtraLabels.remove(label);

		this.CallDelete(label, reAdd);
	}

	private CallDelete(label: EzArchivePortal.IEZLabel, reAdd: boolean): void
	{
		this._deleteCallback(label, this.Label() === null && this.Folders().length === 0
			? this
			: null,
			reAdd);
	}

	public static GetLevels(label: IEZLabel): string[]
	{
		const separator = "/";
		const repeatPattern = new RegExp(separator + "{2,}" , "g");
		let name = label.Name.trim();

		name = name.replace(repeatPattern, separator);

		if (name.length > 0 && name[0] === separator)
			name = name.substring(1);

		if (name.length > 0 && name[name.length - 1] === separator)
			name = name.substring(0, name.length - 1);

		return name.split(separator);
	}

	public static AlphabeticAddFolder(list: KnockoutObservableArray<LabelFolderData>, folder: LabelFolderData): void
	{
		const listInstance = list();

		for (let i = 0; i < listInstance.length; i++)
			if (listInstance[i].Name().localeCompare(folder.Name()) > 0) {
				list.splice(i, 0, folder);
				return;
			}

		list.push(folder);
	}
}