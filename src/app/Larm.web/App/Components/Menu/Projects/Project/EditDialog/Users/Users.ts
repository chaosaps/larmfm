﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Notification = require("Managers/Notification");
import DisposableComponent = require("Components/DisposableComponent");
import ProjectManager = require("Managers/ProjectManager")

type User = { Name:string, Add:() => void, Remove:() => void, CanRemove: KnockoutComputed<boolean>, Added: boolean };

class Users extends DisposableComponent
{
	public Users = knockout.observableArray<User>();
	public SearchResults = knockout.observableArray<User>();
	public Search = knockout.observable("");
	public CanLeave:KnockoutComputed<boolean>;

	private _project: EZArchivePortal.IEZProject;
	private _leave: ()=>void;
	private _userId = knockout.observable("");

	private _maxSearchResults:number = 10;

	constructor(data:{Project:EZArchivePortal.IEZProject, Leave:()=>void})
	{
		super();

		this._project = data.Project;
		this._leave = data.Leave;
		this.CanLeave = this.PureComputed(() => this.Users().length > 1);

		if (this._project.Users.length !== 0)
			this.Users.push(... this._project.Users.map(u => this.CreateUser(u, false)));

		this.Search.extend({ rateLimit: { timeout: 200, method: "notifyWhenChangesStop" } });
		this.Search.subscribe(v => this.SearchUsers(v));

		EZArchivePortal.EZUser.Me().WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error("Failed to get current user");
				return;
			}

			this._userId(response.Body.Results[0].Identifier);

			this.SearchUsers(this.Search());
		});
	}

	public Leave(): void
	{
		this.AddAction(() => this._userId() !== "", () => ProjectManager.RemoveUser(this._project, this._userId()));
		this._leave();
	}

	private SearchUsers(query:string):void
	{
		EZArchivePortal.EZUser.Search(query, this._project.Users.length + this._maxSearchResults).WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error("Failed to search for users: " + response.Error.Message);
				return;
			}

			this.SearchResults.removeAll();
			
			if (response.Body.Results.length !== 0)
			{
				var currentMembers: { [key: string]: boolean } = {};
				this._project.Users.forEach(u => currentMembers[u.Identifier] = true);

				let results = response.Body.Results.filter(u => !currentMembers.hasOwnProperty(u.Identifier));
				if (results.length > this._maxSearchResults)
					results = results.slice(0, this._maxSearchResults);

				this.SearchResults.push(...results.map(u => this.CreateUser(u, false)));
			}
		});
	}

	private CreateUser(user:EZArchivePortal.IEZUser, added: boolean):User
	{
		var result:User = {
			Name: user.Name,
			Add: null,
			Remove: null,
			CanRemove: this.PureComputed(() => this._userId() !== "" && this._userId() !== user.Identifier),
			Added: added
		}

		result.Add = () => this.Add(result, user);
		result.Remove = () => this.Remove(result, user);

		return result;
	}

	private Add(user:User, ezUser:EZArchivePortal.IEZUser):void
	{
		ProjectManager.AddUser(this._project, ezUser);
		user.Added = true;
		this.Users.push(user);
		this.SearchResults.remove(user);
	}

	private Remove(user: User, ezUser: EZArchivePortal.IEZUser): void
	{
		ProjectManager.RemoveUser(this._project, ezUser.Identifier);
		this.Users.remove(user);
	}
}

export = Users;