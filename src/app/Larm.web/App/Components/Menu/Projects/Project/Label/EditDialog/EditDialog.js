define(["require", "exports", "knockout"], function (require, exports, knockout) {
    "use strict";
    var EditDialog = (function () {
        function EditDialog(state) {
            this.Label = state.Data.Label;
            this.Name = knockout.observable(this.Label.Name);
            this.NewName = knockout.observable(this.Label.Name);
            this._state = state;
        }
        EditDialog.prototype.UpdateName = function () {
            this.Name(this.NewName());
            this.Label.Name = this.Name();
            this._state.Data.Save();
            this._state.Close();
        };
        EditDialog.prototype.Delete = function () {
            this._state.Data.Delete();
            this._state.Close();
        };
        return EditDialog;
    }());
    return EditDialog;
});
