var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "jquery", "Managers/Portal/EZArchivePortal", "Managers/Notification", "Components/DisposableComponent", "Managers/Search/Search", "Managers/Dialog/Dialog", "Managers/Dragging", "../LabelFolderData"], function (require, exports, knockout, jquery, EZArchivePortal, Notification, DisposableComponent, Search, DialogManager, Dragging, LabelFolderData_1) {
    "use strict";
    var Label = (function (_super) {
        __extends(Label, _super);
        function Label(data) {
            var _this = _super.call(this) || this;
            _this._identifier = knockout.observable(null);
            _this.Name = knockout.observable("");
            _this.IsEditing = knockout.observable(false);
            _this.HasFocus = knockout.observable(false);
            _this.HasAssociated = knockout.observable(false);
            _this.CanEdit = knockout.observable(false);
            _this.IsHoveringDrop = knockout.observable(false);
            _this._projectId = data.ProjectId;
            _this._label = data.Label;
            _this._deleteCallback = data.Delete;
            if (_this._label.Identifier)
                _this._identifier(_this._label.Identifier);
            _this.Name(_this._label.Name);
            _this.IsEditing(!_this._label.Identifier);
            _this.HasFocus(_this.IsEditing());
            _this.CanEdit(!_this.IsEditing());
            _this.DisplayName = _this.PureComputed(function () {
                var levels = LabelFolderData_1.default.GetLevels(_this._label);
                return levels[levels.length - 1];
            });
            _this.Link = _this.PureComputed(function () {
                if (_this._identifier() == null)
                    return null;
                var query = Search.Clone();
                var fullId = _this._identifier();
                query.Label = (query.Label === fullId ? null : fullId);
                query.PageNumber = null;
                query.From = null;
                query.To = null;
                return "/Search" + query.ToString();
            });
            _this.IsActive = _this.PureComputed(function () {
                if (_this._identifier() == null)
                    return false;
                return _this._identifier() === Search.Query().Label;
            });
            _this.Subscribe(Dragging.IsDraggingSearchResult, function (v) {
                if (!v)
                    _this.IsHoveringDrop(false);
            });
            return _this;
        }
        Label.prototype.Save = function () {
            var _this = this;
            this.IsEditing(false);
            this._label.Name = this.Name();
            EZArchivePortal.EZLabel.Set(this._projectId, this._label).WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to set Label: " + response.Error.Message);
                    return;
                }
                var newLabel = response.Body.Results[0];
                _this._label.Identifier = newLabel.Identifier;
                _this._identifier(_this._label.Identifier);
                _this.CanEdit(true);
                _this._deleteCallback(_this._label, true);
            });
        };
        Label.prototype.Edit = function () {
            var _this = this;
            DialogManager.Show("Menu/Projects/Project/Label/EditDialog", { Label: this._label, Save: function () {
                    _this.Name(_this._label.Name);
                    _this.Save();
                },
                Delete: function () {
                    _this._deleteCallback(_this._label, false);
                } });
        };
        Label.prototype.DragEnter = function (target, event) {
            if (!Dragging.IsDraggingSearchResult())
                return true;
            this.IsHoveringDrop(true);
            return false;
        };
        Label.prototype.DragLeave = function (target, event) {
            if (jquery.contains(event.currentTarget, event.relatedTarget))
                return true;
            this.IsHoveringDrop(false);
            return false;
        };
        Label.prototype.DragOver = function (target, event) {
            return !Dragging.IsDraggingSearchResult();
        };
        Label.prototype.DragDrop = function (target, event) {
            var _this = this;
            if (!Dragging.IsDraggingSearchResult())
                return true;
            this.HasAssociated(false);
            this.HasAssociated(true);
            setTimeout(function () { return _this.HasAssociated(false); }, 1000);
            EZArchivePortal.EZLabel.AssociateWith(this._label.Identifier, Dragging.DragData()).WithCallback(function (response) {
                if (response.Error != null)
                    Notification.Error("Failed to associate asset with label: " + response.Error.Message);
            });
            return false;
        };
        return Label;
    }(DisposableComponent));
    return Label;
});
