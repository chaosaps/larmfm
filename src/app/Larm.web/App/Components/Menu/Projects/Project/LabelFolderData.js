var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/DisposableComponent"], function (require, exports, knockout, DisposableComponent) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var LabelFolderData = (function (_super) {
        __extends(LabelFolderData, _super);
        function LabelFolderData(projectId, deleteCallback, label, level) {
            if (level === void 0) { level = 0; }
            var _this = _super.call(this) || this;
            _this.Name = knockout.observable("");
            _this.Folders = knockout.observableArray();
            _this.Label = knockout.observable(null);
            _this.ExtraLabels = knockout.observableArray();
            _this.ExpandTrigger = knockout.observable(0);
            _this.ProjectId = projectId;
            _this._deleteCallback = deleteCallback;
            _this._level = level;
            _this.Delete = function (label, reAdd) { return _this.InnerDelete(label, reAdd); };
            var levels = LabelFolderData.GetLevels(label);
            _this.Name(levels[level]);
            _this.HasLabel = _this.PureComputed(function () { return _this.Label() !== null; });
            _this.HandleLabel(label, levels);
            return _this;
        }
        LabelFolderData.prototype.AddLabel = function (label) {
            var levels = LabelFolderData.GetLevels(label);
            if (levels[this._level] !== this.Name())
                return false;
            this.HandleLabel(label, levels);
            return true;
        };
        LabelFolderData.prototype.TriggerExpand = function () {
            this.ExpandTrigger(this.ExpandTrigger() + 1);
        };
        LabelFolderData.prototype.HandleLabel = function (label, levels) {
            var _this = this;
            var nextLevel = this._level + 1;
            if (nextLevel === levels.length) {
                if (this.HasLabel())
                    this.ExtraLabels.push(label);
                else
                    this.Label(label);
            }
            else {
                for (var _i = 0, _a = this.Folders(); _i < _a.length; _i++) {
                    var folder = _a[_i];
                    if (folder.AddLabel(label))
                        return;
                }
                LabelFolderData.AlphabeticAddFolder(this.Folders, (new LabelFolderData(this.ProjectId, function (label, folder, reAdd) {
                    if (folder !== null)
                        _this.Folders.remove(folder);
                    _this.CallDelete(label, reAdd);
                }, label, nextLevel)));
            }
        };
        LabelFolderData.prototype.InnerDelete = function (label, reAdd) {
            if (this.Label().Identifier === label.Identifier)
                this.Label(this.ExtraLabels().length > 0
                    ? this.ExtraLabels.shift()
                    : null);
            else
                this.ExtraLabels.remove(label);
            this.CallDelete(label, reAdd);
        };
        LabelFolderData.prototype.CallDelete = function (label, reAdd) {
            this._deleteCallback(label, this.Label() === null && this.Folders().length === 0
                ? this
                : null, reAdd);
        };
        LabelFolderData.GetLevels = function (label) {
            var separator = "/";
            var repeatPattern = new RegExp(separator + "{2,}", "g");
            var name = label.Name.trim();
            name = name.replace(repeatPattern, separator);
            if (name.length > 0 && name[0] === separator)
                name = name.substring(1);
            if (name.length > 0 && name[name.length - 1] === separator)
                name = name.substring(0, name.length - 1);
            return name.split(separator);
        };
        LabelFolderData.AlphabeticAddFolder = function (list, folder) {
            var listInstance = list();
            for (var i = 0; i < listInstance.length; i++)
                if (listInstance[i].Name().localeCompare(folder.Name()) > 0) {
                    list.splice(i, 0, folder);
                    return;
                }
            list.push(folder);
        };
        return LabelFolderData;
    }(DisposableComponent));
    exports.default = LabelFolderData;
});
