var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Portal/EZArchivePortal", "Managers/Notification", "Components/DisposableComponent", "Managers/ProjectManager"], function (require, exports, knockout, EZArchivePortal, Notification, DisposableComponent, ProjectManager) {
    "use strict";
    var Users = (function (_super) {
        __extends(Users, _super);
        function Users(data) {
            var _a;
            var _this = _super.call(this) || this;
            _this.Users = knockout.observableArray();
            _this.SearchResults = knockout.observableArray();
            _this.Search = knockout.observable("");
            _this._userId = knockout.observable("");
            _this._maxSearchResults = 10;
            _this._project = data.Project;
            _this._leave = data.Leave;
            _this.CanLeave = _this.PureComputed(function () { return _this.Users().length > 1; });
            if (_this._project.Users.length !== 0)
                (_a = _this.Users).push.apply(_a, _this._project.Users.map(function (u) { return _this.CreateUser(u, false); }));
            _this.Search.extend({ rateLimit: { timeout: 200, method: "notifyWhenChangesStop" } });
            _this.Search.subscribe(function (v) { return _this.SearchUsers(v); });
            EZArchivePortal.EZUser.Me().WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to get current user");
                    return;
                }
                _this._userId(response.Body.Results[0].Identifier);
                _this.SearchUsers(_this.Search());
            });
            return _this;
        }
        Users.prototype.Leave = function () {
            var _this = this;
            this.AddAction(function () { return _this._userId() !== ""; }, function () { return ProjectManager.RemoveUser(_this._project, _this._userId()); });
            this._leave();
        };
        Users.prototype.SearchUsers = function (query) {
            var _this = this;
            EZArchivePortal.EZUser.Search(query, this._project.Users.length + this._maxSearchResults).WithCallback(function (response) {
                var _a;
                if (response.Error != null) {
                    Notification.Error("Failed to search for users: " + response.Error.Message);
                    return;
                }
                _this.SearchResults.removeAll();
                if (response.Body.Results.length !== 0) {
                    var currentMembers = {};
                    _this._project.Users.forEach(function (u) { return currentMembers[u.Identifier] = true; });
                    var results = response.Body.Results.filter(function (u) { return !currentMembers.hasOwnProperty(u.Identifier); });
                    if (results.length > _this._maxSearchResults)
                        results = results.slice(0, _this._maxSearchResults);
                    (_a = _this.SearchResults).push.apply(_a, results.map(function (u) { return _this.CreateUser(u, false); }));
                }
            });
        };
        Users.prototype.CreateUser = function (user, added) {
            var _this = this;
            var result = {
                Name: user.Name,
                Add: null,
                Remove: null,
                CanRemove: this.PureComputed(function () { return _this._userId() !== "" && _this._userId() !== user.Identifier; }),
                Added: added
            };
            result.Add = function () { return _this.Add(result, user); };
            result.Remove = function () { return _this.Remove(result, user); };
            return result;
        };
        Users.prototype.Add = function (user, ezUser) {
            ProjectManager.AddUser(this._project, ezUser);
            user.Added = true;
            this.Users.push(user);
            this.SearchResults.remove(user);
        };
        Users.prototype.Remove = function (user, ezUser) {
            ProjectManager.RemoveUser(this._project, ezUser.Identifier);
            this.Users.remove(user);
        };
        return Users;
    }(DisposableComponent));
    return Users;
});
