﻿import knockout = require("knockout");
import Search = require("Managers/Search/Search");
import Query = require("Managers/Search/Query");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Notification = require("Managers/Notification");
import DialogManager = require("Managers/Dialog/Dialog");
import DisposableComponent = require("Components/DisposableComponent");
import Dragging = require("Managers/Dragging");
import ProjectManager = require("Managers/ProjectManager");
import IEZLabel = EZArchivePortal.IEZLabel;
import LabelFolderData from "./LabelFolderData";

class Project extends DisposableComponent
{
	public IsDeleted = knockout.observable(false);
	public IsEditing = knockout.observable(false);
	public IsCollapsed = knockout.observable(true);
	public CanEdit = knockout.observable(false);
	public HasFocus = knockout.observable(false);
	public IsDefaultHeaderVisible:KnockoutComputed<boolean>;

	public Identifier: string;
	public Name = knockout.observable("");
	public Labels = knockout.observableArray<LabelFolderData>();

	public DragStartHandle: (event: DragEvent) => boolean;
	public DragEnterHandle: (event: DragEvent) => boolean;
	public DragOverHandle: (event: DragEvent) => boolean;
	public DragDropHandle: (event: DragEvent) => boolean;

	private _project: EZArchivePortal.IEZProject;
	private _createdCallback: (project:EZArchivePortal.IEZProject)=>void;

	constructor(data: { Project: EZArchivePortal.IEZProject, Created: (project: EZArchivePortal.IEZProject)=>void})
	{
		super();

		this._project = data.Project;
		this._createdCallback = data.Created;

		this.DragStartHandle = event => this.HandleStartDrag(event);
		this.DragEnterHandle = event => this.HandleEnterDrag(event);
		this.DragOverHandle = event => this.HandleOverDrag(event);
		this.DragDropHandle = event => this.HandleDragDrop(event);

		this.Identifier = this._project.Identifier;
		this.Name(this._project.Name);
		this.IsEditing(!this.Identifier);
		this.HasFocus(this.IsEditing());
		this.CanEdit(!this.IsEditing());
		this.IsDefaultHeaderVisible = this.PureComputed(() => !this.IsEditing());

		if (this._project.Labels && this._project.Labels.length > 0)
			for (const label of this._project.Labels)
				this.HandleLabel(label);

		this.ExpandIfProjectHasSelectedLabel(Search.Query());

		this.Subscribe(Search.Query, q => this.ExpandIfProjectHasSelectedLabel(q));
	}

	public Delete(clientSideOnly:boolean):void
	{
		this.IsDeleted(true);

		if (!clientSideOnly)
		{
			EZArchivePortal.EZProject.Delete(this.Identifier).WithCallback(response =>
			{
				if (response.Error != null)
				{
					Notification.Error("Failed to delete project: " + response.Error.Message);
					return;
				}
			});
		}
	}

	public CreateLabel():void
	{
		this.IsCollapsed(false);

		this.HandleLabel({ Name: "" });
	}

	public Save():void
	{
		this.IsEditing(false);
		this._project.Name = this.Name();

		EZArchivePortal.EZProject.Set({ Identifier: this.Identifier, Name: this.Name()}).WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error("Failed to set project: " + response.Error.Message);
				return;
			}

			const project = response.Body.Results[0];

			this._project.Identifier = project.Identifier;
			this._project.Users = project.Users;
			this._project.Name = project.Name;
			this.Identifier = this._project.Identifier;

			if (this._createdCallback != null)
				this._createdCallback(this._project);

			this.CanEdit(true);
		});
	}

	public Edit():void
	{
		DialogManager.Show("Menu/Projects/Project/EditDialog", { Project: this._project, Name: this.Name, Delete: (clientSideOnly: boolean) => this.Delete(clientSideOnly)});
	}

	public ToggleCollapsed(): void
	{
		this.IsCollapsed(!this.IsCollapsed());
	}

	private HandleLabel(label: IEZLabel): void
	{
		const labelDatas = this.Labels();
		for (const data of labelDatas) {
			if (data.AddLabel(label))
				return;
		}

		const labelFolder: LabelFolderData = new LabelFolderData(this.Identifier, (l, f, r) => this.InnerDeleteLabel(l, f, r), label);
		LabelFolderData.AlphabeticAddFolder(this.Labels, labelFolder);
	}

	private HandleStartDrag(event: DragEvent): boolean
	{
		Dragging.StartProjectDrag(event, this.Identifier);

		return true;
	}

	private HandleEnterDrag(event: DragEvent): boolean
	{
		if (Dragging.IsDraggingSearchResult())
			this.IsCollapsed(false);
		else if (Dragging.IsDraggingProject() && Dragging.DragData() !== this.Identifier)
			return false;

		return true;
	}

	private HandleOverDrag(event: DragEvent): boolean
	{
		return !Dragging.IsDraggingProject() || Dragging.DragData() === this.Identifier;
	}

	private HandleDragDrop(event: DragEvent): boolean
	{
		if (Dragging.IsDraggingProject() && Dragging.DragData() !== this.Identifier) {
			ProjectManager.MoveProjectNextTo(this.Identifier, Dragging.DragData());
			return false;
		}

		return true;
	}

	private InnerDeleteLabel(label: EZArchivePortal.IEZLabel, labelFolder: LabelFolderData | null, reAdd: boolean):void
	{
		if  (labelFolder !== null)
			this.Labels.remove(labelFolder);

		if (reAdd) {
			this.HandleLabel(label);
			return;
		}

		for (let i = 0; i < this._project.Labels.length; i++)
			if (this._project.Labels[i].Identifier === label.Identifier) {
				this._project.Labels.splice(i, 1);
				break;
			}

		if (this.Labels().length === 0)
			this.IsCollapsed(true);

		const searchLabel = Search.Query().Label;

		if (searchLabel != null && searchLabel === label.Identifier)
			Search.ClearLabel();

		EZArchivePortal.EZLabel.Delete(label.Identifier).WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error("Failed to delete label: " + response.Error.Message);
				this.HandleLabel(label);
			}
		});
	}

	private ExpandIfProjectHasSelectedLabel(query:Query):void
	{
		if (query.Label === null)
			return;

		if (this.Labels().some(f => this.ExpandFolderIfHasLabel(f, query.Label)))
			this.IsCollapsed(false);
	}

	private ExpandFolderIfHasLabel(folder: LabelFolderData, labelIdentifier: string): boolean
	{
		if (folder.HasLabel() && folder.Label().Identifier === labelIdentifier) {
			return true;
		}

		if (folder.ExtraLabels().some(l => l.Identifier === labelIdentifier) ||
			folder.Folders().some(f => this.ExpandFolderIfHasLabel(f, labelIdentifier))) {
			folder.TriggerExpand();
			return true;
		}

		return false;
	}
}

export = Project;