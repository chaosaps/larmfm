﻿import knockout = require("knockout");
import Search = require("Managers/Search/Search");
import DialogState = require("Managers/Dialog/DialogState");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Configuration = require("Managers/Configuration");
import Notification = require("Managers/Notification");

type State = { Project: EZArchivePortal.IEZProject, Name: KnockoutObservable<string>, Delete: (clientSideOnly: boolean) => void };

class EditDialog
{
	public Name: KnockoutObservable<string>;
	public NewName: KnockoutObservable<string>;
	public Project: EZArchivePortal.IEZProject;
	public NumberOfLabels:number;
	public HasLabels:boolean;
	public HasMultipleLabels: boolean;
	public Leave:()=>void;

	private _state: DialogState<State>;

	constructor(state: DialogState<State>)
	{
		this._state = state;
		this.Leave = () =>
		{
			this._state.Data.Delete(true);
			this._state.Close();
		};

		this.Name = this._state.Data.Name;
		this.NewName = knockout.observable(this.Name());
		this.Project = this._state.Data.Project;
		this.NumberOfLabels = this._state.Data.Project.Labels ? this._state.Data.Project.Labels.length : 0;
		this.HasLabels = this.NumberOfLabels !== 0;
		this.HasMultipleLabels = this.NumberOfLabels > 1;
	}

	public UpdateName():void
	{
		this.Name(this.NewName());

		this.Project.Name = this.Name();

		EZArchivePortal.EZProject.Set(this.Project).WithCallback(response =>
		{
			if (response.Error != null)
				Notification.Error("Failed to update project: " + response.Error.Message);
		});
	}

	public CancelUpdateName():void
	{
		this.NewName(this.Name());
	}

	public Delete():void
	{
		this._state.Data.Delete(false);
		this._state.Close();
	}
}

export = EditDialog;