var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Search/Search", "Managers/Portal/EZArchivePortal", "Managers/Notification", "Managers/Dialog/Dialog", "Components/DisposableComponent", "Managers/Dragging", "Managers/ProjectManager", "./LabelFolderData"], function (require, exports, knockout, Search, EZArchivePortal, Notification, DialogManager, DisposableComponent, Dragging, ProjectManager, LabelFolderData_1) {
    "use strict";
    var Project = (function (_super) {
        __extends(Project, _super);
        function Project(data) {
            var _this = _super.call(this) || this;
            _this.IsDeleted = knockout.observable(false);
            _this.IsEditing = knockout.observable(false);
            _this.IsCollapsed = knockout.observable(true);
            _this.CanEdit = knockout.observable(false);
            _this.HasFocus = knockout.observable(false);
            _this.Name = knockout.observable("");
            _this.Labels = knockout.observableArray();
            _this._project = data.Project;
            _this._createdCallback = data.Created;
            _this.DragStartHandle = function (event) { return _this.HandleStartDrag(event); };
            _this.DragEnterHandle = function (event) { return _this.HandleEnterDrag(event); };
            _this.DragOverHandle = function (event) { return _this.HandleOverDrag(event); };
            _this.DragDropHandle = function (event) { return _this.HandleDragDrop(event); };
            _this.Identifier = _this._project.Identifier;
            _this.Name(_this._project.Name);
            _this.IsEditing(!_this.Identifier);
            _this.HasFocus(_this.IsEditing());
            _this.CanEdit(!_this.IsEditing());
            _this.IsDefaultHeaderVisible = _this.PureComputed(function () { return !_this.IsEditing(); });
            if (_this._project.Labels && _this._project.Labels.length > 0)
                for (var _i = 0, _a = _this._project.Labels; _i < _a.length; _i++) {
                    var label = _a[_i];
                    _this.HandleLabel(label);
                }
            _this.ExpandIfProjectHasSelectedLabel(Search.Query());
            _this.Subscribe(Search.Query, function (q) { return _this.ExpandIfProjectHasSelectedLabel(q); });
            return _this;
        }
        Project.prototype.Delete = function (clientSideOnly) {
            this.IsDeleted(true);
            if (!clientSideOnly) {
                EZArchivePortal.EZProject.Delete(this.Identifier).WithCallback(function (response) {
                    if (response.Error != null) {
                        Notification.Error("Failed to delete project: " + response.Error.Message);
                        return;
                    }
                });
            }
        };
        Project.prototype.CreateLabel = function () {
            this.IsCollapsed(false);
            this.HandleLabel({ Name: "" });
        };
        Project.prototype.Save = function () {
            var _this = this;
            this.IsEditing(false);
            this._project.Name = this.Name();
            EZArchivePortal.EZProject.Set({ Identifier: this.Identifier, Name: this.Name() }).WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to set project: " + response.Error.Message);
                    return;
                }
                var project = response.Body.Results[0];
                _this._project.Identifier = project.Identifier;
                _this._project.Users = project.Users;
                _this._project.Name = project.Name;
                _this.Identifier = _this._project.Identifier;
                if (_this._createdCallback != null)
                    _this._createdCallback(_this._project);
                _this.CanEdit(true);
            });
        };
        Project.prototype.Edit = function () {
            var _this = this;
            DialogManager.Show("Menu/Projects/Project/EditDialog", { Project: this._project, Name: this.Name, Delete: function (clientSideOnly) { return _this.Delete(clientSideOnly); } });
        };
        Project.prototype.ToggleCollapsed = function () {
            this.IsCollapsed(!this.IsCollapsed());
        };
        Project.prototype.HandleLabel = function (label) {
            var _this = this;
            var labelDatas = this.Labels();
            for (var _i = 0, labelDatas_1 = labelDatas; _i < labelDatas_1.length; _i++) {
                var data = labelDatas_1[_i];
                if (data.AddLabel(label))
                    return;
            }
            var labelFolder = new LabelFolderData_1.default(this.Identifier, function (l, f, r) { return _this.InnerDeleteLabel(l, f, r); }, label);
            LabelFolderData_1.default.AlphabeticAddFolder(this.Labels, labelFolder);
        };
        Project.prototype.HandleStartDrag = function (event) {
            Dragging.StartProjectDrag(event, this.Identifier);
            return true;
        };
        Project.prototype.HandleEnterDrag = function (event) {
            if (Dragging.IsDraggingSearchResult())
                this.IsCollapsed(false);
            else if (Dragging.IsDraggingProject() && Dragging.DragData() !== this.Identifier)
                return false;
            return true;
        };
        Project.prototype.HandleOverDrag = function (event) {
            return !Dragging.IsDraggingProject() || Dragging.DragData() === this.Identifier;
        };
        Project.prototype.HandleDragDrop = function (event) {
            if (Dragging.IsDraggingProject() && Dragging.DragData() !== this.Identifier) {
                ProjectManager.MoveProjectNextTo(this.Identifier, Dragging.DragData());
                return false;
            }
            return true;
        };
        Project.prototype.InnerDeleteLabel = function (label, labelFolder, reAdd) {
            var _this = this;
            if (labelFolder !== null)
                this.Labels.remove(labelFolder);
            if (reAdd) {
                this.HandleLabel(label);
                return;
            }
            for (var i = 0; i < this._project.Labels.length; i++)
                if (this._project.Labels[i].Identifier === label.Identifier) {
                    this._project.Labels.splice(i, 1);
                    break;
                }
            if (this.Labels().length === 0)
                this.IsCollapsed(true);
            var searchLabel = Search.Query().Label;
            if (searchLabel != null && searchLabel === label.Identifier)
                Search.ClearLabel();
            EZArchivePortal.EZLabel.Delete(label.Identifier).WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to delete label: " + response.Error.Message);
                    _this.HandleLabel(label);
                }
            });
        };
        Project.prototype.ExpandIfProjectHasSelectedLabel = function (query) {
            var _this = this;
            if (query.Label === null)
                return;
            if (this.Labels().some(function (f) { return _this.ExpandFolderIfHasLabel(f, query.Label); }))
                this.IsCollapsed(false);
        };
        Project.prototype.ExpandFolderIfHasLabel = function (folder, labelIdentifier) {
            var _this = this;
            if (folder.HasLabel() && folder.Label().Identifier === labelIdentifier) {
                return true;
            }
            if (folder.ExtraLabels().some(function (l) { return l.Identifier === labelIdentifier; }) ||
                folder.Folders().some(function (f) { return _this.ExpandFolderIfHasLabel(f, labelIdentifier); })) {
                folder.TriggerExpand();
                return true;
            }
            return false;
        };
        return Project;
    }(DisposableComponent));
    return Project;
});
