var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Dragging", "Components/DisposableComponent"], function (require, exports, knockout, Dragging, DisposableComponent) {
    "use strict";
    var LabelFolder = (function (_super) {
        __extends(LabelFolder, _super);
        function LabelFolder(data) {
            var _this = _super.call(this) || this;
            _this.IsCollapsed = knockout.observable(true);
            _this.Data = data.Data;
            _this.CanExpand = _this.PureComputed(function () { return _this.Data.Folders().length !== 0; });
            _this.Subscribe(_this.Data.ExpandTrigger, function () { _this.IsCollapsed(false); });
            if (_this.Data.ExpandTrigger() !== 0)
                _this.IsCollapsed(false);
            return _this;
        }
        LabelFolder.prototype.ToggleCollapsed = function () {
            if (this.CanExpand())
                this.IsCollapsed(!this.IsCollapsed());
        };
        LabelFolder.prototype.DragEnter = function (target, event) {
            if (Dragging.IsDraggingSearchResult() && this.CanExpand())
                this.IsCollapsed(false);
            return true;
        };
        return LabelFolder;
    }(DisposableComponent));
    return LabelFolder;
});
