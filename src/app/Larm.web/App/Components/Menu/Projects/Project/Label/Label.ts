﻿import knockout = require("knockout");
import jquery = require("jquery");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Notification = require("Managers/Notification");
import DisposableComponent = require("Components/DisposableComponent");
import Search = require("Managers/Search/Search");
import DialogManager = require("Managers/Dialog/Dialog");
import Dragging = require("Managers/Dragging");
import LabelFolderData from "../LabelFolderData";

class Label extends DisposableComponent
{
	private _projectId: string;
	private _label: EZArchivePortal.IEZLabel;
	private _identifier: KnockoutObservable<string> = knockout.observable(null);
	private _deleteCallback: (label: EZArchivePortal.IEZLabel, reAdd: boolean) => void;

	public Name = knockout.observable("");
	public Link:KnockoutComputed<string>;
	public DisplayName:KnockoutComputed<string>;
	public IsEditing = knockout.observable(false);
	public HasFocus = knockout.observable(false);
	public HasAssociated = knockout.observable(false);
	public IsActive: KnockoutComputed<boolean>;
	public CanEdit = knockout.observable(false);
	public IsHoveringDrop = knockout.observable(false);

	constructor(data: { ProjectId: string, Label: EZArchivePortal.IEZLabel, Delete: (label: EZArchivePortal.IEZLabel, reAdd: boolean)=>void })
	{
		super();

		this._projectId = data.ProjectId;
		this._label = data.Label;
		this._deleteCallback = data.Delete;

		if (this._label.Identifier)
			this._identifier(this._label.Identifier);

		this.Name(this._label.Name);
		this.IsEditing(!this._label.Identifier);
		this.HasFocus(this.IsEditing());
		this.CanEdit(!this.IsEditing());

		this.DisplayName = this.PureComputed(() =>
		{
			const levels = LabelFolderData.GetLevels(this._label);
			return levels[levels.length - 1];
		});

		this.Link = this.PureComputed(() =>
		{
			if (this._identifier() == null) return null;

			var query = Search.Clone();
			var fullId = this._identifier();

			query.Label = (query.Label === fullId ? null : fullId);
			query.PageNumber = null;
			query.From = null;
			query.To = null;

			return `/Search${query.ToString()}`;
		});

		this.IsActive = this.PureComputed(() =>
		{
			if (this._identifier() == null) return false;

			return this._identifier() === Search.Query().Label;
		});

		this.Subscribe(Dragging.IsDraggingSearchResult, v =>
		{
			if (!v) this.IsHoveringDrop(false);
		});
	}

	public Save():void
	{
		this.IsEditing(false);
		this._label.Name = this.Name();

		EZArchivePortal.EZLabel.Set(this._projectId, this._label).WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error(`Failed to set Label: ${response.Error.Message}`);
				return;
			}

			var newLabel = response.Body.Results[0];

			this._label.Identifier = newLabel.Identifier;

			this._identifier(this._label.Identifier);
			this.CanEdit(true);
			this._deleteCallback(this._label, true);
		});
	}

	public Edit():void
	{
		DialogManager.Show("Menu/Projects/Project/Label/EditDialog", { Label: this._label, Save: () =>
		{
			this.Name(this._label.Name);
			this.Save();
		},
		Delete: () =>
		{
			this._deleteCallback(this._label, false);
		}});
	}

	public DragEnter(target: any, event: JQueryEventObject): boolean
	{
		if (!Dragging.IsDraggingSearchResult())
			return true;

		this.IsHoveringDrop(true);

		return false;
	}

	public DragLeave(target: any, event: JQueryEventObject): boolean
	{
		if (jquery.contains(<Element>event.currentTarget, event.relatedTarget))
			return true;

		this.IsHoveringDrop(false);

		return false;
	}

	public DragOver(target:any, event:JQueryEventObject):boolean
	{
		return !Dragging.IsDraggingSearchResult();
	}

	public DragDrop(target: any, event: JQueryEventObject): boolean
	{
		if (!Dragging.IsDraggingSearchResult())
			return true;

		this.HasAssociated(false);
		this.HasAssociated(true);
		setTimeout(() => this.HasAssociated(false), 1000);

		EZArchivePortal.EZLabel.AssociateWith(this._label.Identifier, Dragging.DragData()).WithCallback(response =>
		{
			if (response.Error != null)
				Notification.Error("Failed to associate asset with label: " + response.Error.Message);
		});

		return false;
	}
}

export = Label;