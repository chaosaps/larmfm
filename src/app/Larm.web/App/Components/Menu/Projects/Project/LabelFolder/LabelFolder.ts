﻿import knockout = require("knockout");
import Dragging = require("Managers/Dragging");
import DisposableComponent = require("Components/DisposableComponent");
import LabelFolderData from "../LabelFolderData";

class LabelFolder extends DisposableComponent
{
	public readonly Data: LabelFolderData;

	public readonly IsCollapsed = knockout.observable(true);
	public readonly CanExpand: KnockoutComputed<boolean>;

	constructor(data: { Data: LabelFolderData })
	{
		super();

		this.Data = data.Data;
		this.CanExpand = this.PureComputed(() => this.Data.Folders().length !== 0);

		this.Subscribe(this.Data.ExpandTrigger, () => { this.IsCollapsed(false) });

		if (this.Data.ExpandTrigger() !== 0)
			this.IsCollapsed(false);
	}

	public ToggleCollapsed(): void
	{
		if (this.CanExpand())
			this.IsCollapsed(!this.IsCollapsed());
	}

	private DragEnter(target: this, event: JQueryEventObject): boolean
	{
		if (Dragging.IsDraggingSearchResult() && this.CanExpand())
			this.IsCollapsed(false);

		return true;
	}
}

export = LabelFolder;