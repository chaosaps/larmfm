define(["require", "exports", "knockout", "Managers/Portal/EZArchivePortal", "Managers/Notification"], function (require, exports, knockout, EZArchivePortal, Notification) {
    "use strict";
    var EditDialog = (function () {
        function EditDialog(state) {
            var _this = this;
            this._state = state;
            this.Leave = function () {
                _this._state.Data.Delete(true);
                _this._state.Close();
            };
            this.Name = this._state.Data.Name;
            this.NewName = knockout.observable(this.Name());
            this.Project = this._state.Data.Project;
            this.NumberOfLabels = this._state.Data.Project.Labels ? this._state.Data.Project.Labels.length : 0;
            this.HasLabels = this.NumberOfLabels !== 0;
            this.HasMultipleLabels = this.NumberOfLabels > 1;
        }
        EditDialog.prototype.UpdateName = function () {
            this.Name(this.NewName());
            this.Project.Name = this.Name();
            EZArchivePortal.EZProject.Set(this.Project).WithCallback(function (response) {
                if (response.Error != null)
                    Notification.Error("Failed to update project: " + response.Error.Message);
            });
        };
        EditDialog.prototype.CancelUpdateName = function () {
            this.NewName(this.Name());
        };
        EditDialog.prototype.Delete = function () {
            this._state.Data.Delete(false);
            this._state.Close();
        };
        return EditDialog;
    }());
    return EditDialog;
});
