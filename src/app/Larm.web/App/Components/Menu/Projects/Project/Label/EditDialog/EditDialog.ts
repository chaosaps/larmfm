﻿import knockout = require("knockout");
import DialogState = require("Managers/Dialog/DialogState");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");

type State = { Label: EZArchivePortal.IEZLabel, Delete: () => void, Save: () => void };

class EditDialog
{
	public Name: KnockoutObservable<string>;
	public NewName: KnockoutObservable<string>;
	public Label: EZArchivePortal.IEZLabel;

	private _state: DialogState<State>;

	constructor(state: DialogState<State>)
	{
		this.Label = state.Data.Label;
		this.Name = knockout.observable(this.Label.Name);
		this.NewName = knockout.observable(this.Label.Name);
		
		this._state = state;
	}

	public UpdateName():void
	{
		this.Name(this.NewName());

		this.Label.Name = this.Name();

		this._state.Data.Save();
		this._state.Close();
	}

	public Delete():void
	{
		this._state.Data.Delete();
		this._state.Close();
	}
}

export = EditDialog;