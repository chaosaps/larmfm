var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/DisposableComponent"], function (require, exports, knockout, DisposableComponent) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var LabelContent = (function (_super) {
        __extends(LabelContent, _super);
        function LabelContent(label, level) {
            if (level === void 0) { level = 0; }
            var _this = _super.call(this) || this;
            _this.Name = knockout.observable("");
            _this.Folders = knockout.observableArray();
            _this.Labels = knockout.observableArray();
            _this._level = level;
            return _this;
        }
        LabelContent.prototype.AddLabel = function (label) {
            return false;
        };
        LabelContent.prototype.GetLevels = function (label) {
            return label.Name.split("/");
        };
        return LabelContent;
    }(DisposableComponent));
    exports.default = LabelContent;
});
