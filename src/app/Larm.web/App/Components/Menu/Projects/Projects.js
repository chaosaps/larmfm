var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Search/Search", "Managers/ProjectManager", "Components/DisposableComponent"], function (require, exports, knockout, Search, ProjectManager, DisposableComponent) {
    "use strict";
    var Projects = (function (_super) {
        __extends(Projects, _super);
        function Projects() {
            var _this = _super.call(this) || this;
            _this.IsCollapsed = knockout.observable(false);
            _this.CanCreate = knockout.observable(true);
            _this.CreatedCallback = function () { return _this.CanCreate(true); };
            _this.DragEnterHandle = function (event) {
                _this.IsCollapsed(false);
                return true;
            };
            _this.Projects = ProjectManager.Projects;
            if (Search.Query().Label != null)
                _this.IsCollapsed(false);
            _this.Subscribe(Search.Query, function (q) {
                if (q.Label != null)
                    _this.IsCollapsed(false);
            });
            return _this;
        }
        Projects.prototype.Create = function () {
            this.Projects.push({ Name: "" });
            this.IsCollapsed(false);
            this.CanCreate(false);
            return true;
        };
        return Projects;
    }(DisposableComponent));
    return Projects;
});
