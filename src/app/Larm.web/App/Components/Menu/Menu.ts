﻿import knockout = require("knockout");
import Search = require("Managers/Search/Search");
import Query = require("Managers/Search/Query");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import LarmPortal = require("Managers/Portal/Portal");
import MenuFacets = require("Managers/Search/MenuFacets");
import Interface = require("Managers/Interface");
import DisposableComponent = require("Components/DisposableComponent");

class Menu extends DisposableComponent
{
	public Query: KnockoutObservable<string> = knockout.observable("");
	public QueryHasFocus:KnockoutObservable<boolean> = knockout.observable(true);
	public Facets:KnockoutObservable<EZArchivePortal.IEZFacetField[]>;
	public IsAuthenticated: KnockoutObservable<boolean>;
	public SearchIsCollapsed: KnockoutObservable<boolean> = knockout.observable(false);
	public IsCollapsed = Interface.IsMenuCollapsed;

	private _isUpdating = false;

	constructor()
	{
		super();

		this.Facets = MenuFacets.Facets;
		var queryUpdater = (query:Query) =>
		{
			this.LoadFromQuery(query);
		}

		this.IsAuthenticated = LarmPortal.IsAuthenticated;

		this.Subscribe(this.QueryHasFocus, v =>
		{
			if (!v && this.Query() !== Search.Query().Query)
				this.Search();
		});
		this.Subscribe(Search.Query, queryUpdater);

		queryUpdater(Search.Query());
	}

	public static Show(): void
	{
		Interface.Show();
	}

	public static Hide(): void
	{
		Interface.Hide();
	}

	public Search():void
	{
		var query = Search.Clone();
		query.Query = this.Query();
		query.PageNumber = null;

		Search.Search(query);
	}

	public ClearSearch():void
	{
		Search.Search(new Query());
	}

	public ToggleMenu(): void
	{
		this.IsCollapsed(!this.IsCollapsed());
	}

	private LoadFromQuery(query: Query):void
	{
		this._isUpdating = true;
		this.Query(query.Query);
		this._isUpdating = false;

		if ((query.From != null || query.To != null) || (query.Facets != null && query.Facets.length !== 0))
			this.SearchIsCollapsed(false);
	}
}

export = Menu;