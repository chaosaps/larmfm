﻿import knockout = require("knockout");
import Dragging = require("Managers/Dragging");

type DragHandler = (event: DragEvent) => boolean

class Group
{
	public Name: KnockoutObservable<string>;
	public HasCustomHeader:boolean;
	public CssClasses: KnockoutComputed<string>;
	public IsCollapsed: KnockoutObservable<boolean>;
	public IsDefaultHeaderVisible: KnockoutObservable<boolean>;
	public IsDragHighlighted: KnockoutObservable<boolean>;
	public IsDraggable: boolean;

	private readonly _dragEnterHandle?: DragHandler;
	private readonly _dragStartHandle?: DragHandler;
	private readonly _dragOverHandle?: DragHandler;
	private readonly _dragEndHandle?: DragHandler;
	private readonly _dragDropHandle?: DragHandler;

	constructor(data: { Name: string | KnockoutObservable<string>, CssClasses?: string, HasCustomHeader?: boolean, IsCollapsed?: KnockoutObservable<boolean>, IsDefaultHeaderVisible?: KnockoutObservable<boolean>, DragEnterHandle?: DragHandler, DragStartHandle?: DragHandler, DragOverHandle?: DragHandler, DragEndHandle?: DragHandler, DragDropHandle?: DragHandler})
	{
		const name = data.Name;
		this.Name = (typeof name === "string") ? knockout.observable(name) : name;

		this.HasCustomHeader = !!data.HasCustomHeader;
		this.IsCollapsed = data.IsCollapsed ? data.IsCollapsed : knockout.observable(false);
		this.IsDefaultHeaderVisible = data.IsDefaultHeaderVisible ? data.IsDefaultHeaderVisible : knockout.observable(true);

		var cssClasses = data.CssClasses || "";
		this.CssClasses = knockout.computed(() => { return (this.IsCollapsed() ? "Collapsed " : "") + cssClasses; });

		this.IsDragHighlighted = Dragging.IsDraggingSearchResult;
		this._dragStartHandle = data.DragStartHandle;
		this._dragEnterHandle = data.DragEnterHandle;
		this._dragOverHandle = data.DragOverHandle;
		this._dragEndHandle = data.DragEndHandle;
		this._dragDropHandle = data.DragDropHandle;

		this.IsDraggable = !!data.DragStartHandle;
	}

	public ToggleCollapsed(): void
	{
		this.IsCollapsed(!this.IsCollapsed());
	}

	public DragStart(target: Group, event: JQueryEventObject): boolean
	{
		if (!this.IsDraggable)
			return true;

		return this._dragStartHandle(event.originalEvent as DragEvent);
	}

	public DragEnter(target: Group, event: JQueryEventObject): boolean {
		if (!this._dragEnterHandle)
			return true;

		return this._dragEnterHandle(event.originalEvent as DragEvent);
	}

	public DragOver(target: Group, event: JQueryEventObject): boolean {
		if (!this._dragOverHandle)
			return true;

		return this._dragOverHandle(event.originalEvent as DragEvent);
	}

	public DragEnd(target: Group, event: JQueryEventObject): boolean {
		if (!this._dragEndHandle)
			return true;

		return this._dragEndHandle(event.originalEvent as DragEvent);
	}

	public DragDrop(target: Group, event: JQueryEventObject): boolean {
		if (!this._dragDropHandle)
			return true;

		return this._dragDropHandle(event.originalEvent as DragEvent);
	}
}

export = Group;