define(["require", "exports", "knockout", "Managers/Dragging"], function (require, exports, knockout, Dragging) {
    "use strict";
    var Group = (function () {
        function Group(data) {
            var _this = this;
            var name = data.Name;
            this.Name = (typeof name === "string") ? knockout.observable(name) : name;
            this.HasCustomHeader = !!data.HasCustomHeader;
            this.IsCollapsed = data.IsCollapsed ? data.IsCollapsed : knockout.observable(false);
            this.IsDefaultHeaderVisible = data.IsDefaultHeaderVisible ? data.IsDefaultHeaderVisible : knockout.observable(true);
            var cssClasses = data.CssClasses || "";
            this.CssClasses = knockout.computed(function () { return (_this.IsCollapsed() ? "Collapsed " : "") + cssClasses; });
            this.IsDragHighlighted = Dragging.IsDraggingSearchResult;
            this._dragStartHandle = data.DragStartHandle;
            this._dragEnterHandle = data.DragEnterHandle;
            this._dragOverHandle = data.DragOverHandle;
            this._dragEndHandle = data.DragEndHandle;
            this._dragDropHandle = data.DragDropHandle;
            this.IsDraggable = !!data.DragStartHandle;
        }
        Group.prototype.ToggleCollapsed = function () {
            this.IsCollapsed(!this.IsCollapsed());
        };
        Group.prototype.DragStart = function (target, event) {
            if (!this.IsDraggable)
                return true;
            return this._dragStartHandle(event.originalEvent);
        };
        Group.prototype.DragEnter = function (target, event) {
            if (!this._dragEnterHandle)
                return true;
            return this._dragEnterHandle(event.originalEvent);
        };
        Group.prototype.DragOver = function (target, event) {
            if (!this._dragOverHandle)
                return true;
            return this._dragOverHandle(event.originalEvent);
        };
        Group.prototype.DragEnd = function (target, event) {
            if (!this._dragEndHandle)
                return true;
            return this._dragEndHandle(event.originalEvent);
        };
        Group.prototype.DragDrop = function (target, event) {
            if (!this._dragDropHandle)
                return true;
            return this._dragDropHandle(event.originalEvent);
        };
        return Group;
    }());
    return Group;
});
