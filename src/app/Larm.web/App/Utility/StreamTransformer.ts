﻿class StreamTransformer
{
	private _rtmpMask = /^rtmps?(:\/\/.+?:)(\d{4})(\/chaos)(\?.+)\/(mp3:.+(?:\.mp3)?)$/;
	private _hlsMask = /^http(:\/\/.+?:)(\d{4})(\/chaos\/_definst_\/.+playlist\.m3u8.+)$/;

	public UpdateHls(url: string): string | null
	{
		const results = this._hlsMask.exec(url);

		if (results === null)
			return null;

		return "https" + results[1] + (parseInt(results[2]) + 10).toString() + results[3];
	}

	public GetRtmpParts(url: string): RtmpOptions | null
	{
		const results = this._rtmpMask.exec(url);

		if (results === null)
			return null;

		return {
			ServerUrl: "rtmps" + results[1] + results[2] + results[3] + results[4],
			Url: results[5]
		}
	}

	public Update(url: string): string | null
	{
		const results = this._rtmpMask.exec(url);

		if (results === null)
			return null;

		return "rtmp" + results[1] + (parseInt(results[2])).toString() + results[3] + results[4] + "/" + results[5];
	}

	public UpdateAndGetRtmpParts(url: string): RtmpOptions | null
	{
		const results = this._rtmpMask.exec(url);

		if (results === null)
			return null;

		return {
			ServerUrl: "rtmps" + results[1] + (parseInt(results[2]) + 10).toString() + results[3] + results[4],
			Url: results[5]
		}
	}

	public UpdateAndGetHls(url: string): string | null
	{
		const results = this._rtmpMask.exec(url);

		if (results === null)
			return null;

		return "https" + results[1] + parseInt(results[2]) + results[3] + "/_definst_/"  + results[5] + "/playlist.m3u8" + results[4];
	}
}

export type RtmpOptions = {Url: string, ServerUrl: string}

export default new StreamTransformer();
