﻿import moment = require("moment");
import DataFormat = require("Utility/DataFormat");

class DateParser
{
	public TypeName = "Datetime";
	public DateTypeName = "Date";
	private DateRange = /^(.+)? - (.+)?$/;
	private DateMask = /^(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)(?:\.(\d+))?Z$/;

	public ParseLocalUTCString(data:string):Date {
		const dateData = this.DateMask.exec(data);

		if (dateData === null)
			throw new Error("Failed to parse date: " + data);

		return new Date(parseInt(dateData[1]),
			parseInt(dateData[2]) - 1,
			parseInt(dateData[3]),
			parseInt(dateData[4]),
			parseInt(dateData[5]),
			parseInt(dateData[6]),
			dateData[7] !== undefined ? parseInt(dateData[7]) : null);
	}

	public ToLocalUTCString(date:Date, includeMilliseconds:boolean = false):string
	{
		if (date == null) return "";

		return moment(date).format("YYYY-MM-DDTHH:mm:ss" + (includeMilliseconds ? ".SSS" : "")) + "Z";
	}

	public ToFormattedStringByAssetType(date: Date, assetTypeId: string = null, isUserData: boolean = false): string
	{
		return this.ToFormattedString(date, DataFormat.GetFormat(this.TypeName, assetTypeId, isUserData));
	}

	public ToFormattedDateString(date: Date): string
	{
		return this.ToFormattedString(date, DataFormat.GetFormat(this.DateTypeName, null));
	}

	public ToFormattedString(date: Date, format: string): string
	{
		return moment(date).format(format);
	}

	public ToFormattedDateRange(value: string | null, assetTypeId: string): string {
		if (value === null)
			return "";

		const results = this.DateRange.exec(value);

		if (results === null)
			return "";

		const from = results[1] ? this.GetFormattedDateFromString(results[1], assetTypeId) : "";
		const to = results[2] ? this.GetFormattedDateFromString(results[2], assetTypeId) : "";

		return from + " - " + to;
	}

	private GetFormattedDateFromString(value: string, assetTypeId: string): string {
		return value !== null
			? moment(this.ParseLocalUTCString(value)).format(DataFormat.GetFormat(this.TypeName, assetTypeId))
			: "";
	}
}

var instance = new DateParser();

export = instance;