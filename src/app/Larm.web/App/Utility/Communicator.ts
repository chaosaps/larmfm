﻿import Storage = require("Managers/Storage");

class Communicator
{
	private _key:string;
	private _callback: (value: string) => void;

	constructor(key:string, callback:(value:string)=>void)
	{
		this._key = key;
		this._callback = v =>
		{
			if (v != null) callback;
		};

		Storage.Listen(key, callback);
	}

	public Dispose():void
	{
		Storage.Unlisten(this._key, this._callback);
	}

	public Send(value:string):void
	{
		Storage.Set(this._key, value);
		Storage.Remove(this._key);
	}
}

export = Communicator;