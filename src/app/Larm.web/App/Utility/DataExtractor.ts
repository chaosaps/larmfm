﻿import moment = require("moment");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Configuration = require("Managers/Configuration");
import DateParser = require("Utility/DateParser");

class DataExtractor
{
	private _id: string;
	private _data: KnockoutObservableArray<EZArchivePortal.IData>;
	private _typeId:KnockoutObservable<string>;

	private _title: string;

	constructor(id: string, typeId:KnockoutObservable<string>, data: KnockoutObservableArray<EZArchivePortal.IData>)
	{
		this._id = id;
		this._typeId = typeId;
		this._data = data;
		this.Update();
	}

	public Update():void
	{
		this._title = this.GetTitle();
	}

	public get Title(): string { return this._title; }

	public get BroadcastWalltimeStart(): Date
	{
		for (var walltime of Configuration.BroadcastWallTime)
		{
			for (var data of this._data())
			{
				if (data.Name !== walltime.Schema) continue;
				var value = data.Fields[walltime.Field];

				if (value == null || value === "") break;

				return DateParser.ParseLocalUTCString(value);
			}
		}

		return null;

	}

	private GetTitle():string
	{
		var datas = this._data.slice(0).reverse();

		for (let data of datas)
		{
            if (this.HasValue("Title", data.Fields)) return data.Fields["Title"];
            if (this.HasValue("title", data.Fields)) return data.Fields["title"];
			if (this.HasValue("Date", data.Fields)) return DateParser.ToFormattedStringByAssetType(DateParser.ParseLocalUTCString(data.Fields["Date"]), this._typeId());
		}

		return this._id;
    }

    private HasValue(name: string, fields: { [key: string]: string }): boolean
    {
        return fields.hasOwnProperty(name) && fields[name] != null && fields[name] !== "";
    }
}

export = DataExtractor;