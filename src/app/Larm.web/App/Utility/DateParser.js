define(["require", "exports", "moment", "Utility/DataFormat"], function (require, exports, moment, DataFormat) {
    "use strict";
    var DateParser = (function () {
        function DateParser() {
            this.TypeName = "Datetime";
            this.DateTypeName = "Date";
            this.DateRange = /^(.+)? - (.+)?$/;
            this.DateMask = /^(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)(?:\.(\d+))?Z$/;
        }
        DateParser.prototype.ParseLocalUTCString = function (data) {
            var dateData = this.DateMask.exec(data);
            if (dateData === null)
                throw new Error("Failed to parse date: " + data);
            return new Date(parseInt(dateData[1]), parseInt(dateData[2]) - 1, parseInt(dateData[3]), parseInt(dateData[4]), parseInt(dateData[5]), parseInt(dateData[6]), dateData[7] !== undefined ? parseInt(dateData[7]) : null);
        };
        DateParser.prototype.ToLocalUTCString = function (date, includeMilliseconds) {
            if (includeMilliseconds === void 0) { includeMilliseconds = false; }
            if (date == null)
                return "";
            return moment(date).format("YYYY-MM-DDTHH:mm:ss" + (includeMilliseconds ? ".SSS" : "")) + "Z";
        };
        DateParser.prototype.ToFormattedStringByAssetType = function (date, assetTypeId, isUserData) {
            if (assetTypeId === void 0) { assetTypeId = null; }
            if (isUserData === void 0) { isUserData = false; }
            return this.ToFormattedString(date, DataFormat.GetFormat(this.TypeName, assetTypeId, isUserData));
        };
        DateParser.prototype.ToFormattedDateString = function (date) {
            return this.ToFormattedString(date, DataFormat.GetFormat(this.DateTypeName, null));
        };
        DateParser.prototype.ToFormattedString = function (date, format) {
            return moment(date).format(format);
        };
        DateParser.prototype.ToFormattedDateRange = function (value, assetTypeId) {
            if (value === null)
                return "";
            var results = this.DateRange.exec(value);
            if (results === null)
                return "";
            var from = results[1] ? this.GetFormattedDateFromString(results[1], assetTypeId) : "";
            var to = results[2] ? this.GetFormattedDateFromString(results[2], assetTypeId) : "";
            return from + " - " + to;
        };
        DateParser.prototype.GetFormattedDateFromString = function (value, assetTypeId) {
            return value !== null
                ? moment(this.ParseLocalUTCString(value)).format(DataFormat.GetFormat(this.TypeName, assetTypeId))
                : "";
        };
        return DateParser;
    }());
    var instance = new DateParser();
    return instance;
});
