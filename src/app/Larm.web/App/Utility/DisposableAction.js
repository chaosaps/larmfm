define(["require", "exports", "knockout"], function (require, exports, knockout) {
    "use strict";
    var DisposableAction = (function () {
        function DisposableAction(condition, action, timeout) {
            var _this = this;
            if (condition == null)
                throw new Error("Condition must be none null");
            if (action == null)
                throw new Error("Action must be none null");
            this._condition = condition;
            this._action = action;
            this._observableCondition = knockout.computed(condition);
            this._observableCondition.subscribe(function (v) { return _this.Check(); });
            this.Check();
            if (timeout && !this.IsDisposed)
                this._timeoutHandle = setTimeout(function () { return _this.Run(); }, timeout);
        }
        Object.defineProperty(DisposableAction.prototype, "IsDisposed", {
            get: function () { return this._condition == null; },
            enumerable: true,
            configurable: true
        });
        DisposableAction.prototype.Check = function () {
            if (this._condition == null || !this._condition())
                return;
            this.Run();
        };
        DisposableAction.prototype.Run = function () {
            if (this._condition == null)
                return;
            var action = this._action;
            this.Dispose();
            action();
        };
        DisposableAction.prototype.Dispose = function () {
            if (this._condition == null)
                return;
            this._condition = null;
            this._action = null;
            this._observableCondition.dispose();
            this._observableCondition = null;
            clearTimeout(this._timeoutHandle);
        };
        return DisposableAction;
    }());
    return DisposableAction;
});
