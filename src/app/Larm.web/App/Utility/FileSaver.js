define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var quoteMask = /"/g;
    function CsvEscape(value) {
        return "\"" + value.replace(quoteMask, "\"\"") + "\"";
    }
    function SaveAsCsv(data, fileName) {
        var csvData = data.map(function (line) { return line.map(CsvEscape)
            .join(","); })
            .join("\r\n");
        SaveAs(csvData, fileName, "text/csv");
    }
    exports.SaveAsCsv = SaveAsCsv;
    function SaveAs(data, fileName, type) {
        var file = new Blob([data], { type: type });
        if (window.navigator.msSaveOrOpenBlob)
            window.navigator.msSaveOrOpenBlob(file, fileName);
        else {
            var a_1 = document.createElement("a");
            if (URL && "download" in a_1) {
                var url_1 = URL.createObjectURL(file);
                a_1.href = url_1;
                a_1.download = fileName;
                a_1.style.setProperty("visibility", "hidden");
                document.body.appendChild(a_1);
                a_1.click();
                setTimeout(function () {
                    document.body.removeChild(a_1);
                    window.URL.revokeObjectURL(url_1);
                }, 0);
            }
            else {
                window.open("data:" + type + "," + encodeURIComponent(data), "_blank");
            }
        }
    }
    exports.default = SaveAs;
});
