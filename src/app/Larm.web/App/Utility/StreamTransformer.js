define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var StreamTransformer = (function () {
        function StreamTransformer() {
            this._rtmpMask = /^rtmps?(:\/\/.+?:)(\d{4})(\/chaos)(\?.+)\/(mp3:.+(?:\.mp3)?)$/;
            this._hlsMask = /^http(:\/\/.+?:)(\d{4})(\/chaos\/_definst_\/.+playlist\.m3u8.+)$/;
        }
        StreamTransformer.prototype.UpdateHls = function (url) {
            var results = this._hlsMask.exec(url);
            if (results === null)
                return null;
            return "https" + results[1] + (parseInt(results[2]) + 10).toString() + results[3];
        };
        StreamTransformer.prototype.GetRtmpParts = function (url) {
            var results = this._rtmpMask.exec(url);
            if (results === null)
                return null;
            return {
                ServerUrl: "rtmps" + results[1] + results[2] + results[3] + results[4],
                Url: results[5]
            };
        };
        StreamTransformer.prototype.Update = function (url) {
            var results = this._rtmpMask.exec(url);
            if (results === null)
                return null;
            return "rtmp" + results[1] + (parseInt(results[2])).toString() + results[3] + results[4] + "/" + results[5];
        };
        StreamTransformer.prototype.UpdateAndGetRtmpParts = function (url) {
            var results = this._rtmpMask.exec(url);
            if (results === null)
                return null;
            return {
                ServerUrl: "rtmps" + results[1] + (parseInt(results[2]) + 10).toString() + results[3] + results[4],
                Url: results[5]
            };
        };
        StreamTransformer.prototype.UpdateAndGetHls = function (url) {
            var results = this._rtmpMask.exec(url);
            if (results === null)
                return null;
            return "https" + results[1] + parseInt(results[2]) + results[3] + "/_definst_/" + results[5] + "/playlist.m3u8" + results[4];
        };
        return StreamTransformer;
    }());
    exports.default = new StreamTransformer();
});
