﻿const quoteMask = /"/g;

function CsvEscape(value: string): string {
	return `"${value.replace(quoteMask, "\"\"")}"`;
}

export function SaveAsCsv(data: string[][], fileName: string): void {
	const csvData = data.map(line => line.map(CsvEscape)
			.join(","))
		.join("\r\n");

	SaveAs(csvData, fileName, "text/csv");
}

export default function SaveAs(data: string, fileName: string, type: string): void {
    const file = new Blob([data], { type: type });

    if (window.navigator.msSaveOrOpenBlob)
        window.navigator.msSaveOrOpenBlob(file, fileName);
	else {
		const a = document.createElement("a") as HTMLAnchorElement;

		if (URL && "download" in a) {
			const url = URL.createObjectURL(file);
			a.href = url;
			a.download = fileName;
			a.style.setProperty("visibility", "hidden");
			document.body.appendChild(a);
			a.click();
			setTimeout(() => {
					document.body.removeChild(a);
					window.URL.revokeObjectURL(url);
				},
				0);
		} else {
			window.open(`data:${type},${encodeURIComponent(data)}`, "_blank");
		}
    }
}
