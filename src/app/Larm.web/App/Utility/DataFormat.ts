﻿import Configuration = require("Managers/Configuration");

class DataFormat
{
	public GetFormat(type: string, assetTypeId: string = null, isUserData: boolean = false):string
	{
		if (assetTypeId != null) {
			if (isUserData && Configuration.UserAssetTypeFormat.hasOwnProperty(assetTypeId) && Configuration.UserAssetTypeFormat[assetTypeId].hasOwnProperty(type))
				return Configuration.UserAssetTypeFormat[assetTypeId][type];
			if (Configuration.AssetTypeFormat.hasOwnProperty(assetTypeId) && Configuration.AssetTypeFormat[assetTypeId].hasOwnProperty(type))
				return Configuration.AssetTypeFormat[assetTypeId][type];
		}

		return  Configuration.DefaultTypeFormat[type];
	}

	public PrettyNumber(value:number):string
	{
		return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, v => v + "." );
	}

	public MillieSecondsToPrettyTime(milliseconds: number): string
	{
		const date = new Date(milliseconds);
		return `${this.GetTwoDigits(date.getUTCHours() + (date.getUTCDate() - 1) * 24)}:${this.GetTwoDigits(date.getUTCMinutes())}:${this.GetTwoDigits(Math.round(date.getUTCSeconds() + (date.getUTCMilliseconds() / 1000)))}`;
	}

	public PrettyTimeToMillieSeconds(prettyTime: string): number | null
	{
		const mask = /^(\d+):(\d\d):(\d\d)(?:\.(\d+))?$/;

		const result = mask.exec(prettyTime);

		if (result === null)
			throw new Error("Failed to parse time string");

		return parseInt(result[1]) * 3600000 +
			parseInt(result[2]) * 60000 +
			parseInt(result[3]) * 1000 +
			(result[4] ? parseInt(result[4]) : 0);
	}

	private GetTwoDigits(value: number): string {
		return value < 10 ? "0" + value : value.toString();
	}
}

var instance = new DataFormat();

export = instance;