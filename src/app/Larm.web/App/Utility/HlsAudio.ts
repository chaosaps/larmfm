﻿import knockout = require("knockout");
import Notification = require("Managers/Notification");
import DisposableComponent = require("Components/DisposableComponent");
import StreamTransformer from "Utility/StreamTransformer"
import Hls = require("Hls");

export default class HlsAudio extends DisposableComponent
{
	private static readonly ResetTimeOut = 1000;
	private static readonly LoadTimeOut = 5000;

	public IsReady = knockout.observable(false);
	public IsPlaying = knockout.observable(false);
	public IsFinished = knockout.observable(false);
	public HasErrorOccured = knockout.observable(false);
	public IsMediaUnsupported = knockout.observable(false);
	public Position = knockout.observable(0);
	public Duration = knockout.observable(0);
	public Volume = knockout.observable(100);

	private _element: HTMLAudioElement;
	private _hls: Hls | null = null;


	constructor(url: string)
	{
		super();
		this.Initialize(url);
	}

	public TogglePlay():void
	{
		if (this.IsPlaying())
			this.Pause();
		else
			this.Play();
	}

	public Play(): void
	{
		this._element.play();
	}

	public Pause(): void
	{
		this._element.pause();
	}

	public dispose(): void
	{
		super.dispose();
		if (this._hls !== null)
			this._hls.destroy();
		window.document.body.removeChild(this._element);
	}

	private Initialize(url: string): void
	{
		const format = "application/vnd.apple.mpegURL; codecs=\"mp3\"";
		url = StreamTransformer.UpdateAndGetHls(url);

		if (url === null) {
			Notification.Error("Failed to transform url");
			return;
		}

		this._element = window.document.createElement("Audio") as HTMLAudioElement;
		this._element.setAttribute("style", "display: none;");

		this.InitializeEvents();

		if (Hls.isSupported())
		{
			this._hls = new Hls();
			url = url.replace("/chaos/", "/chaosnonapple/"); // Switch to alternative stream to avoid problems.
			this._hls.loadSource(url);
			this._hls.attachMedia(this._element as any);
		} else if (this._element.canPlayType(format) !== "") {
			const sourceElement = window.document.createElement("source");
			sourceElement.src = url;
			sourceElement.type = format;
			this._element.appendChild(sourceElement);
		} else {
			this.HasErrorOccured(true);
			this.IsMediaUnsupported(true);
			Notification.Debug("Audio codec not supported");
		}

		window.document.body.appendChild(this._element);
	}

	private InitializeEvents(): void
	{
		this._element.addEventListener("loadeddata", event => {
			this.IsReady(true);
		});

		let isResetting = false;

		const reset = () =>
		{
			if (isResetting) {
				Notification.Error("Playback failed; Failed to reset");
				isResetting = false;
				this.HasErrorOccured(true);
				return;
			}

			isResetting = true;
			const position = this.Position();
			this.TogglePlay();

			setTimeout(() =>
				{
					this.TogglePlay();
					this.Position(position);

					setTimeout(() => isResetting = false, HlsAudio.ResetTimeOut);
				},
				10);
		}

		this._element.addEventListener("error", event =>
		{
			if (this._element.error !== null)
			{
				if (this._element.error.code === 3) {
					reset();
					Notification.Debug("Audio playback failed, code 3");
				} else if (this._element.error.code === 4)
				{
					this.HasErrorOccured(true);
					this.IsMediaUnsupported(true);
					Notification.Debug("Audio codec not supported");
				} else
				{
					this.HasErrorOccured(true);
					Notification.Error(`Playback failed: ${this._element.error.message !== undefined ? this._element.error.message : `code ${this._element.error.code}`}`);
				}
			}
		});

		setTimeout(() =>
		{
			if (this.IsReady() || this.HasErrorOccured() || isResetting || this._element === null)
				return;

			if (this._element.error !== null) {
				console.log("Audio error: " + this._element.error.message);
				this.HasErrorOccured(true);
				return;
			}

			setTimeout(() =>
			{
				if (this.IsReady() || this.HasErrorOccured() || isResetting || this._element === null)
					return;

				if (this._element.readyState === 4)
					this.IsReady(true);
				else {
					console.log("Audio error: ready state timeout");
					this.HasErrorOccured(true);
				}
			}, HlsAudio.LoadTimeOut);
		}, HlsAudio.LoadTimeOut);

		if (this._hls !== null) {
			this._hls.on(Hls.Events.ERROR, (event, data) =>
			{
				if (data.fatal) {
					switch (data.type) {
					case Hls.ErrorTypes.NETWORK_ERROR:
						console.log("Fatal audio network error encountered, try to recover");
						this._hls.startLoad(); // try to recover network error
						break;
					case Hls.ErrorTypes.MEDIA_ERROR:
						console.log("Fatal audio media error encountered, try to recover");
						this._hls.recoverMediaError();
						break;
					default:
						this.HasErrorOccured(true);
					}
				} else
					console.log("None fatal audio error: " + data.type + " " + data.details);
			});
		}

		this.InitializePosition();
		this.InitializeDuration();
		this.InitializeVolume();
		this.InitializeState();
	}

	private InitializePosition(): void
	{
		let isUpdatingPosition = false;

		this._element.addEventListener("timeupdate", event => {
			isUpdatingPosition = true;
			this.Position(this._element.currentTime * 1000);
			isUpdatingPosition = false;
		});

		this.Subscribe(this.Position,
			v => {
				if (isUpdatingPosition)
					return;
				this._element.currentTime = v / 1000;
			});
	}

	private InitializeDuration(): void
	{
		this._element.addEventListener("durationchange", event => {
			this.Duration(this._element.duration * 1000);
		});
	}

	private InitializeVolume(): void
	{
		this.Subscribe(this.Volume, value => {
			this._element.volume = value / 100;
		});
	}

	private InitializeState(): void
	{
		let isChanging = false;

		this._element.addEventListener("playing", event => {
			isChanging = true;
			this.IsPlaying(true);
			isChanging = false;
		});
		this._element.addEventListener("pause", event => {
			isChanging = true;
			this.IsPlaying(false);
			isChanging = false;
		});
		this._element.addEventListener("ended", event => {
			isChanging = true;
			this.IsPlaying(false);
			isChanging = false;
		});

		this.Subscribe(this.IsPlaying, v => {
			if (isChanging)
				return;

			if (v)
				this._element.play();
			else
				this._element.pause();
		});
	}
}
