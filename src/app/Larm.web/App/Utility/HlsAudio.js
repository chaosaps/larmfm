var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Notification", "Components/DisposableComponent", "Utility/StreamTransformer", "Hls"], function (require, exports, knockout, Notification, DisposableComponent, StreamTransformer_1, Hls) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var HlsAudio = (function (_super) {
        __extends(HlsAudio, _super);
        function HlsAudio(url) {
            var _this = _super.call(this) || this;
            _this.IsReady = knockout.observable(false);
            _this.IsPlaying = knockout.observable(false);
            _this.IsFinished = knockout.observable(false);
            _this.HasErrorOccured = knockout.observable(false);
            _this.IsMediaUnsupported = knockout.observable(false);
            _this.Position = knockout.observable(0);
            _this.Duration = knockout.observable(0);
            _this.Volume = knockout.observable(100);
            _this._hls = null;
            _this.Initialize(url);
            return _this;
        }
        HlsAudio.prototype.TogglePlay = function () {
            if (this.IsPlaying())
                this.Pause();
            else
                this.Play();
        };
        HlsAudio.prototype.Play = function () {
            this._element.play();
        };
        HlsAudio.prototype.Pause = function () {
            this._element.pause();
        };
        HlsAudio.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            if (this._hls !== null)
                this._hls.destroy();
            window.document.body.removeChild(this._element);
        };
        HlsAudio.prototype.Initialize = function (url) {
            var format = "application/vnd.apple.mpegURL; codecs=\"mp3\"";
            url = StreamTransformer_1.default.UpdateAndGetHls(url);
            if (url === null) {
                Notification.Error("Failed to transform url");
                return;
            }
            this._element = window.document.createElement("Audio");
            this._element.setAttribute("style", "display: none;");
            this.InitializeEvents();
            if (Hls.isSupported()) {
                this._hls = new Hls();
                url = url.replace("/chaos/", "/chaosnonapple/");
                this._hls.loadSource(url);
                this._hls.attachMedia(this._element);
            }
            else if (this._element.canPlayType(format) !== "") {
                var sourceElement = window.document.createElement("source");
                sourceElement.src = url;
                sourceElement.type = format;
                this._element.appendChild(sourceElement);
            }
            else {
                this.HasErrorOccured(true);
                this.IsMediaUnsupported(true);
                Notification.Debug("Audio codec not supported");
            }
            window.document.body.appendChild(this._element);
        };
        HlsAudio.prototype.InitializeEvents = function () {
            var _this = this;
            this._element.addEventListener("loadeddata", function (event) {
                _this.IsReady(true);
            });
            var isResetting = false;
            var reset = function () {
                if (isResetting) {
                    Notification.Error("Playback failed; Failed to reset");
                    isResetting = false;
                    _this.HasErrorOccured(true);
                    return;
                }
                isResetting = true;
                var position = _this.Position();
                _this.TogglePlay();
                setTimeout(function () {
                    _this.TogglePlay();
                    _this.Position(position);
                    setTimeout(function () { return isResetting = false; }, HlsAudio.ResetTimeOut);
                }, 10);
            };
            this._element.addEventListener("error", function (event) {
                if (_this._element.error !== null) {
                    if (_this._element.error.code === 3) {
                        reset();
                        Notification.Debug("Audio playback failed, code 3");
                    }
                    else if (_this._element.error.code === 4) {
                        _this.HasErrorOccured(true);
                        _this.IsMediaUnsupported(true);
                        Notification.Debug("Audio codec not supported");
                    }
                    else {
                        _this.HasErrorOccured(true);
                        Notification.Error("Playback failed: " + (_this._element.error.message !== undefined ? _this._element.error.message : "code " + _this._element.error.code));
                    }
                }
            });
            setTimeout(function () {
                if (_this.IsReady() || _this.HasErrorOccured() || isResetting || _this._element === null)
                    return;
                if (_this._element.error !== null) {
                    console.log("Audio error: " + _this._element.error.message);
                    _this.HasErrorOccured(true);
                    return;
                }
                setTimeout(function () {
                    if (_this.IsReady() || _this.HasErrorOccured() || isResetting || _this._element === null)
                        return;
                    if (_this._element.readyState === 4)
                        _this.IsReady(true);
                    else {
                        console.log("Audio error: ready state timeout");
                        _this.HasErrorOccured(true);
                    }
                }, HlsAudio.LoadTimeOut);
            }, HlsAudio.LoadTimeOut);
            if (this._hls !== null) {
                this._hls.on(Hls.Events.ERROR, function (event, data) {
                    if (data.fatal) {
                        switch (data.type) {
                            case Hls.ErrorTypes.NETWORK_ERROR:
                                console.log("Fatal audio network error encountered, try to recover");
                                _this._hls.startLoad();
                                break;
                            case Hls.ErrorTypes.MEDIA_ERROR:
                                console.log("Fatal audio media error encountered, try to recover");
                                _this._hls.recoverMediaError();
                                break;
                            default:
                                _this.HasErrorOccured(true);
                        }
                    }
                    else
                        console.log("None fatal audio error: " + data.type + " " + data.details);
                });
            }
            this.InitializePosition();
            this.InitializeDuration();
            this.InitializeVolume();
            this.InitializeState();
        };
        HlsAudio.prototype.InitializePosition = function () {
            var _this = this;
            var isUpdatingPosition = false;
            this._element.addEventListener("timeupdate", function (event) {
                isUpdatingPosition = true;
                _this.Position(_this._element.currentTime * 1000);
                isUpdatingPosition = false;
            });
            this.Subscribe(this.Position, function (v) {
                if (isUpdatingPosition)
                    return;
                _this._element.currentTime = v / 1000;
            });
        };
        HlsAudio.prototype.InitializeDuration = function () {
            var _this = this;
            this._element.addEventListener("durationchange", function (event) {
                _this.Duration(_this._element.duration * 1000);
            });
        };
        HlsAudio.prototype.InitializeVolume = function () {
            var _this = this;
            this.Subscribe(this.Volume, function (value) {
                _this._element.volume = value / 100;
            });
        };
        HlsAudio.prototype.InitializeState = function () {
            var _this = this;
            var isChanging = false;
            this._element.addEventListener("playing", function (event) {
                isChanging = true;
                _this.IsPlaying(true);
                isChanging = false;
            });
            this._element.addEventListener("pause", function (event) {
                isChanging = true;
                _this.IsPlaying(false);
                isChanging = false;
            });
            this._element.addEventListener("ended", function (event) {
                isChanging = true;
                _this.IsPlaying(false);
                isChanging = false;
            });
            this.Subscribe(this.IsPlaying, function (v) {
                if (isChanging)
                    return;
                if (v)
                    _this._element.play();
                else
                    _this._element.pause();
            });
        };
        HlsAudio.ResetTimeOut = 1000;
        HlsAudio.LoadTimeOut = 5000;
        return HlsAudio;
    }(DisposableComponent));
    exports.default = HlsAudio;
});
