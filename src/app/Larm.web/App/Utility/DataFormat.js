define(["require", "exports", "Managers/Configuration"], function (require, exports, Configuration) {
    "use strict";
    var DataFormat = (function () {
        function DataFormat() {
        }
        DataFormat.prototype.GetFormat = function (type, assetTypeId, isUserData) {
            if (assetTypeId === void 0) { assetTypeId = null; }
            if (isUserData === void 0) { isUserData = false; }
            if (assetTypeId != null) {
                if (isUserData && Configuration.UserAssetTypeFormat.hasOwnProperty(assetTypeId) && Configuration.UserAssetTypeFormat[assetTypeId].hasOwnProperty(type))
                    return Configuration.UserAssetTypeFormat[assetTypeId][type];
                if (Configuration.AssetTypeFormat.hasOwnProperty(assetTypeId) && Configuration.AssetTypeFormat[assetTypeId].hasOwnProperty(type))
                    return Configuration.AssetTypeFormat[assetTypeId][type];
            }
            return Configuration.DefaultTypeFormat[type];
        };
        DataFormat.prototype.PrettyNumber = function (value) {
            return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, function (v) { return v + "."; });
        };
        DataFormat.prototype.MillieSecondsToPrettyTime = function (milliseconds) {
            var date = new Date(milliseconds);
            return this.GetTwoDigits(date.getUTCHours() + (date.getUTCDate() - 1) * 24) + ":" + this.GetTwoDigits(date.getUTCMinutes()) + ":" + this.GetTwoDigits(Math.round(date.getUTCSeconds() + (date.getUTCMilliseconds() / 1000)));
        };
        DataFormat.prototype.PrettyTimeToMillieSeconds = function (prettyTime) {
            var mask = /^(\d+):(\d\d):(\d\d)(?:\.(\d+))?$/;
            var result = mask.exec(prettyTime);
            if (result === null)
                throw new Error("Failed to parse time string");
            return parseInt(result[1]) * 3600000 +
                parseInt(result[2]) * 60000 +
                parseInt(result[3]) * 1000 +
                (result[4] ? parseInt(result[4]) : 0);
        };
        DataFormat.prototype.GetTwoDigits = function (value) {
            return value < 10 ? "0" + value : value.toString();
        };
        return DataFormat;
    }());
    var instance = new DataFormat();
    return instance;
});
