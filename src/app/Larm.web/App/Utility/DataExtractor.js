define(["require", "exports", "Managers/Configuration", "Utility/DateParser"], function (require, exports, Configuration, DateParser) {
    "use strict";
    var DataExtractor = (function () {
        function DataExtractor(id, typeId, data) {
            this._id = id;
            this._typeId = typeId;
            this._data = data;
            this.Update();
        }
        DataExtractor.prototype.Update = function () {
            this._title = this.GetTitle();
        };
        Object.defineProperty(DataExtractor.prototype, "Title", {
            get: function () { return this._title; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataExtractor.prototype, "BroadcastWalltimeStart", {
            get: function () {
                for (var _i = 0, _a = Configuration.BroadcastWallTime; _i < _a.length; _i++) {
                    var walltime = _a[_i];
                    for (var _b = 0, _c = this._data(); _b < _c.length; _b++) {
                        var data = _c[_b];
                        if (data.Name !== walltime.Schema)
                            continue;
                        var value = data.Fields[walltime.Field];
                        if (value == null || value === "")
                            break;
                        return DateParser.ParseLocalUTCString(value);
                    }
                }
                return null;
            },
            enumerable: true,
            configurable: true
        });
        DataExtractor.prototype.GetTitle = function () {
            var datas = this._data.slice(0).reverse();
            for (var _i = 0, datas_1 = datas; _i < datas_1.length; _i++) {
                var data = datas_1[_i];
                if (this.HasValue("Title", data.Fields))
                    return data.Fields["Title"];
                if (this.HasValue("title", data.Fields))
                    return data.Fields["title"];
                if (this.HasValue("Date", data.Fields))
                    return DateParser.ToFormattedStringByAssetType(DateParser.ParseLocalUTCString(data.Fields["Date"]), this._typeId());
            }
            return this._id;
        };
        DataExtractor.prototype.HasValue = function (name, fields) {
            return fields.hasOwnProperty(name) && fields[name] != null && fields[name] !== "";
        };
        return DataExtractor;
    }());
    return DataExtractor;
});
