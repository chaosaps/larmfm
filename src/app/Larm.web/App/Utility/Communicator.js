define(["require", "exports", "Managers/Storage"], function (require, exports, Storage) {
    "use strict";
    var Communicator = (function () {
        function Communicator(key, callback) {
            this._key = key;
            this._callback = function (v) {
                if (v != null)
                    callback;
            };
            Storage.Listen(key, callback);
        }
        Communicator.prototype.Dispose = function () {
            Storage.Unlisten(this._key, this._callback);
        };
        Communicator.prototype.Send = function (value) {
            Storage.Set(this._key, value);
            Storage.Remove(this._key);
        };
        return Communicator;
    }());
    return Communicator;
});
