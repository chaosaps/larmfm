﻿import knockout = require("knockout");

class DisposableAction
{
	private _condition: () => boolean;
	private _action: () => void;
	private _observableCondition: KnockoutComputed<boolean>;
	private _timeoutHandle: number;

	public get IsDisposed() { return this._condition == null; }

	constructor(condition: () => boolean, action: () => void, timeout?: number)
	{
		if (condition == null)
			throw new Error("Condition must be none null");

		if (action == null)
			throw new Error("Action must be none null");

		this._condition = condition;
		this._action = action;
		this._observableCondition = knockout.computed(condition);
		this._observableCondition.subscribe(v => this.Check());

		this.Check();

		if (timeout && !this.IsDisposed)
			this._timeoutHandle = setTimeout(() => this.Run(), timeout);
	}

	public Check(): void
	{
		if (this._condition == null || !this._condition()) return;

		this.Run();
	}

	public Run(): void
	{
		if (this._condition == null) return;

		let action = this._action;
		this.Dispose();
		action();
	}

	public Dispose(): void
	{
		if (this._condition == null) return;

		this._condition = null;
		this._action = null;
		this._observableCondition.dispose();
		this._observableCondition = null;
		clearTimeout(this._timeoutHandle);
	}
}

export = DisposableAction;