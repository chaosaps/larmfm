define(["require", "exports", "knockout", "Components/NameConventionLoader"], function (require, exports, knockout, NameConventionLoader) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    knockout.components.loaders.push(new NameConventionLoader());
    knockout.applyBindings();
});
