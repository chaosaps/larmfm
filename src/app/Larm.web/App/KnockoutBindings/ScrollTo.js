define(["require", "exports", "knockout", "jquery"], function (require, exports, knockout, jquery) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    knockout.bindingHandlers["ScrollTo"] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var $element = jquery(element);
            var options = knockout.utils.unwrapObservable(valueAccessor());
            if (options.Trigger == undefined)
                throw new Error("Trigger not set for ScrollTo binding");
            var trigger = options.Trigger;
            var duration = options.Duration == undefined ? 500 : options.Duration;
            var containerName = options.Container == undefined ? "html, body" : options.Container;
            var container = $element.parent().closest(containerName);
            if (container.length === 0)
                throw new Error("No container mathcing \"" + containerName + "\" found");
            var subscription = trigger.subscribe(function (v) {
                if (v)
                    container.animate({ scrollTop: $element.offset().top - container.offset().top + container.scrollTop() }, duration);
            });
            knockout.utils.domNodeDisposal.addDisposeCallback(element, function () {
                subscription.dispose();
            });
        }
    };
});
