﻿import knockout = require("knockout");
import jquery = require("jquery");

knockout.bindingHandlers["ScrollTo"] = {
	init: (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) =>
	{
		var $element = jquery(element);
		var options: any = knockout.utils.unwrapObservable(valueAccessor());

		if (options.Trigger == undefined)
			throw new Error("Trigger not set for ScrollTo binding");

		var trigger = <KnockoutObservable<boolean>>options.Trigger;
		var duration = options.Duration == undefined ? 500 : options.Duration;
		var containerName = options.Container == undefined ? "html, body" : options.Container;
		var container = <JQuery><any>$element.parent().closest(containerName);

		if (container.length === 0)
			throw new Error(`No container mathcing "${containerName}" found`);

		var subscription = trigger.subscribe(v =>
		{
			if (v)
				container.animate({ scrollTop: $element.offset().top - container.offset().top + container.scrollTop() }, duration);
		});

		knockout.utils.domNodeDisposal.addDisposeCallback(element, () =>
		{
			subscription.dispose();
		});
	}
};