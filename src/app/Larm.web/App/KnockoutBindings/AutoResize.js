define(["require", "exports", "knockout"], function (require, exports, knockout) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    knockout.bindingHandlers["autoResize"] = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            var valueObservable = knockout.computed(function () { return valueAccessor()(); });
            var resize = function () {
                element.style.height = "auto";
                element.style.height = element.scrollHeight + "px";
            };
            var delayedResize = function () {
                window.setTimeout(resize, 0);
            };
            knockout.utils.registerEventHandler(element, "change", resize);
            knockout.utils.registerEventHandler(element, "cut", delayedResize);
            knockout.utils.registerEventHandler(element, "paste", delayedResize);
            knockout.utils.registerEventHandler(element, "drop", delayedResize);
            knockout.utils.registerEventHandler(element, "keydown", delayedResize);
            var subscription = valueObservable.subscribe(function () {
                delayedResize();
            });
            knockout.utils.domNodeDisposal.addDisposeCallback(element, function () {
                subscription.dispose();
            });
            resize();
        }
    };
});
