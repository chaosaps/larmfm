define(["require", "exports", "knockout", "jquery", "bootstrapDatetimepicker"], function (require, exports, knockout, jquery, bootstrapDatetimepicker) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    bootstrapDatetimepicker;
    knockout.bindingHandlers["dateTimePicker"] = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            var options = allBindingsAccessor().dateTimePickerOptions || {};
            jquery(element).datetimepicker(options);
            knockout.utils.registerEventHandler(element, "dp.change", function (event) {
                var value = valueAccessor();
                if (knockout.isObservable(value)) {
                    if (event.date === false) {
                        value(null);
                    }
                    else if (event.date != null && !(event.date instanceof Date)) {
                        value(event.date.toDate());
                    }
                    else {
                        value(event.date);
                    }
                }
            });
            knockout.utils.domNodeDisposal.addDisposeCallback(element, function () {
                var picker = jquery(element).data("DateTimePicker");
                if (picker)
                    picker.destroy();
            });
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var picker = jquery(element).data("DateTimePicker");
            if (picker) {
                var koDate = knockout.utils.unwrapObservable(valueAccessor());
                koDate = (typeof (koDate) !== "object") ? new Date(parseFloat(koDate.replace(/[^0-9]/g, ""))) : koDate;
                picker.date(koDate);
            }
        }
    };
});
