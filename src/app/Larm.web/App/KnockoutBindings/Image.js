define(["require", "exports", "knockout", "jquery"], function (require, exports, knockout, jquery) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    knockout.bindingHandlers["Image"] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var $element = jquery(element);
            var options = knockout.utils.unwrapObservable(valueAccessor());
            if (options.Path == null)
                $element.text(options.Fallback);
            else {
                jquery("<img draggable=\"false\" src=\"" + options.Path + "\">").appendTo($element).error(function () {
                    $element.text(options.Fallback);
                });
            }
        }
    };
});
