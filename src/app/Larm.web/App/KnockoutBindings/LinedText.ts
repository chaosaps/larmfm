﻿import knockout = require("knockout");
import jquery = require("jquery");

knockout.bindingHandlers["linedText"] = {
	update: (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) =>
	{
		var $element = jquery(element);
		var value: any = knockout.utils.unwrapObservable(valueAccessor());

		if (value != null)
		{
			value = value
				.replace(/&/g, "&amp;")
				.replace(/"/g, "&quot;")
				.replace(/'/g, "&#39;")
				.replace(/</g, "&lt;")
				.replace(/>/g, "&gt;")
				.replace(/[\n\r]/g, "<br/>");
		}

		$element.html(value);
	}
};