define(["require", "exports", "bootstrap", "knockout", "jquery"], function (require, exports, bootstrap, knockout, jquery) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    bootstrap;
    knockout.bindingHandlers["Tooltip"] = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var $element, options, tooltip;
            options = knockout.utils.unwrapObservable(valueAccessor());
            $element = jquery(element);
            tooltip = $element.data('tooltip');
            if (options) {
                if (tooltip)
                    $.extend(tooltip.options, options);
                else
                    $element.tooltip(options);
            }
        }
    };
});
