﻿import knockout = require("knockout");

knockout.bindingHandlers["autoResize"] = {
    init: function (element, valueAccessor, allBindingsAccessor)
    {
	    var valueObservable = knockout.computed(() => valueAccessor()());

		var resize = () =>
		{
			element.style.height = "auto";
			element.style.height = element.scrollHeight + "px";
		}
		var delayedResize = ()=>
		{
			window.setTimeout(resize, 0);
		}
		knockout.utils.registerEventHandler(element, "change", resize);
		knockout.utils.registerEventHandler(element, "cut", delayedResize);
		knockout.utils.registerEventHandler(element, "paste", delayedResize);
		knockout.utils.registerEventHandler(element, "drop", delayedResize);
		knockout.utils.registerEventHandler(element, "keydown", delayedResize);

		var subscription = valueObservable.subscribe(() =>
		{
			 delayedResize();
		});

		knockout.utils.domNodeDisposal.addDisposeCallback(element, () =>
		{
			subscription.dispose();
		});

		resize();
    }
};