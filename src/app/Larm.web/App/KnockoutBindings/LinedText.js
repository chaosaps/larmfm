define(["require", "exports", "knockout", "jquery"], function (require, exports, knockout, jquery) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    knockout.bindingHandlers["linedText"] = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var $element = jquery(element);
            var value = knockout.utils.unwrapObservable(valueAccessor());
            if (value != null) {
                value = value
                    .replace(/&/g, "&amp;")
                    .replace(/"/g, "&quot;")
                    .replace(/'/g, "&#39;")
                    .replace(/</g, "&lt;")
                    .replace(/>/g, "&gt;")
                    .replace(/[\n\r]/g, "<br/>");
            }
            $element.html(value);
        }
    };
});
