﻿import knockout = require("knockout");
import jquery = require("jquery");

knockout.bindingHandlers["Image"] = {
	init: (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) =>
	{
		var $element = jquery(element);
		var options: any = knockout.utils.unwrapObservable(valueAccessor());

		if (options.Path == null)
			$element.text(options.Fallback);
		else
		{
			jquery(`<img draggable="false" src="${options.Path}">`).appendTo($element).error(() =>
			{
				$element.text(options.Fallback);
			});
		}
	}
};