﻿import knockout = require("knockout");
import jquery = require("jquery");
import bootstrapDatetimepicker = require("bootstrapDatetimepicker"); bootstrapDatetimepicker;

knockout.bindingHandlers["dateTimePicker"] = {
	init(element, valueAccessor, allBindingsAccessor)
	{
		//initialize datepicker with some optional options
		var options = allBindingsAccessor().dateTimePickerOptions || {};
		(<any>jquery(element)).datetimepicker(options);

		//when a user changes the date, update the view model
		knockout.utils.registerEventHandler(element, "dp.change", (event:any) => {
			var value = valueAccessor();
			if (knockout.isObservable(value))
			{
				if (event.date === false)
				{
					value(null);
				}
				else if (event.date != null && !(event.date instanceof Date))
				{
					value(event.date.toDate());
				} else
				{
					value(event.date);
				}
			}
		});

		knockout.utils.domNodeDisposal.addDisposeCallback(element, () => {
			var picker = jquery(element).data("DateTimePicker");
			if (picker) (<any>picker).destroy();
		});
	},
	update(element, valueAccessor, allBindings, viewModel, bindingContext)
	{
		var picker = jquery(element).data("DateTimePicker");
		//when the view model is updated, update the widget
		if (picker)
		{
			var koDate = knockout.utils.unwrapObservable(valueAccessor());

			//in case return from server datetime i am get in this form for example /Date(93989393)/ then fomat this
			koDate = (typeof (koDate) !== "object") ? new Date(parseFloat(koDate.replace(/[^0-9]/g, ""))) : koDate;

			(<any>picker).date(koDate);
		}
	}
};