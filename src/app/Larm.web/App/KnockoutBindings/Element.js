define(["require", "exports", "knockout"], function (require, exports, knockout) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    knockout.bindingHandlers["Element"] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            value(element);
        }
    };
});
