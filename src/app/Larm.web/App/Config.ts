﻿declare var CacheBuster: number;
declare module "PortalClient" { export = CHAOS };

var isLive = location.hostname.replace("www2.", "").replace("www.", "") === "larm.fm" || location.hostname === "prod.larm.fm";

requirejs.config({
	config: {
		"Managers/Configuration": {
			PortalPath: isLive ? "https://api.prod.larm.fm" : "https://api.stage.larm.fm",
			TrackingId: isLive ? "UA-70747180-1" : "UA-70747180-2",
			WayfPath: "https://wayf.larm.fm",
			AnonymousEmail: "anon@ymo.us",
			SchemaOrder: [
				"Arkiv Metadata", "TV Arkiv metadata", "Radio Arkiv metadata", "Programoversigt Arkiv metadata", "Radiooversigt Arkiv metadata", "Manuskript Arkiv metadata"
			],
			SchemaTableSort: {
				"Programoversigt Arkiv metadata": {
					"Programmes": [5, 3, 1]
				}
			},
			SchemaTableCollapse: {
				"Programoversigt Arkiv metadata": {
					"Programmes": {
						4: 270,
						5: 0
					}
				}
			},
			SchemaListField: {
				TypeFacet: "{Document}.TypeId",
				Schemas: {
					"TV LARM metadata": {
						Field: "Channel",
						Facet: "{Search}.Kanal"
					},
					"Radio LARM metadata": {
						Field: "Channel",
						Facet: "{Search}.Kanal"
					}
				}
			},
			BroadcastWallTime: [
				{
					Schema: "Offset",
					Field: "Walltime"
				},
				{
					Schema: "Radio Arkiv metadata",
					Field: "PublicationStartDate"
				}
			],
			HiddenSchemas: ["Offset"],
			OffsetSchema: "Offset",
			HiddenAnnotationProperties: ["IsPositionAdjusted"],
			DefaultPageSize: 20,
			MaxNumberOfPages: 9,
			PageSizes: [20, 50, 100],
			SearchImagePath: "/App/Images/SearchFields/",
			SearchImageMaps: ["Kanal"],
			SearchResultReplaceMap: {
				"Datospænd": "Udsendelsesdato",
				"Udsendelsesdato_ddmmyyyy": "Udsendelsesdato"
			},
			SearchResultReplaceValueMap: {
				"Udsendelsesdato": {
					"01/01/1900": "Ukendt"
				}
			},
			SearchResultDefaultValue: {
				"24": { "Titel": "Radioprogram" },
				"25": { "Titel": "TV-program" },
				"26": { "Titel": "Radioavisrapport" },
				"27": { "Titel": "Manuskript" },
				"28": { "Titel": "Radiooversigt" },
				"86": { "Titel": "Programoversigt" },
				"124": { "Titel": "DAT bånd" }
			},
			AssetDataReplaceValues: {
				"Datetime": {
					"01/01/1900": "Ukendt"
				}
			},
			DefaultTypeFormat: {
				 "Datetime": "DD/MM/YYYY HH:mm",
				 "Date": "DD/MM/YYYY"
			},
			AssetTypeFormat: {
				"24": { "Datetime": "DD/MM/YYYY HH:mm" },
				"25": { "Datetime": "DD/MM/YYYY HH:mm" },
				"26": { "Datetime": "MM/YYYY" },
				"27": { "Datetime": "DD/MM/YYYY" },
				"28": { "Datetime": "DD/MM/YYYY" },
				"86": { "Datetime": "DD/MM/YYYY" },
				"124": { "Datetime": "DD/MM/YYYY HH:mm" }
			},
			UserAssetTypeFormat: {
				"26": { "Datetime": "DD/MM/YYYY" }
			},
			AssetUseDateFormatFieldNames: ["Dato"],
			TimedToolbar: {
				JumpLengths: [5000, 10000, 30000, 60000, 300000, 1800000],
				DefaultJumpLength: 10000,
				JumpLengthSecondText: " sek",
				JumpLengthMinuteText: " min"
			},
			Copyright: {
				TvSchemaName: "TV Arkiv metadata"
			},
			DrillDownFacet: {
				Header: "Date drill down",
				Field: "{Search}.Udsendelsesdato"
			},
			LabelFilter: "{Label}",
			MenuFacetHeader: "Facetter",
			FacetValuesVisible: 6,
			FacetKeys: {
				"{Search}.Kanal": {
					Name: "Kanal",
					HasIcon: false,
					Values: null
				},
				"{Document}.TypeId": {
					Name: "Type",
					HasIcon: true,
					Values: {
						"24": "Radioprogrammer",
						"25": "TV-Programmer",
						"26": "Radioavisrapporter",
						"27": "Manuskripter",
						"28": "Radiooversigter",
						"86": "Programoversigter",
						"124": "DAT bånd"
					}
				}
			},
			TrackingType: {
				"24": "Radioprogram",
				"25": "TV-program",
				"26": "Radioavisrapport",
				"27": "Manuskript",
				"28": "Radiooversigt",
				"86": "Programoversigt",
				"124": "DAT bånd"
			},
			PortChecker: {
				Url: "https://ports.chaosinsight.com:{port}/test.txt",
				PortToken: "{port}",
				Ports: [1944, 1945, 1948, 1949],
				Expected: "It worked!"
			}
		}
	},
	paths: {
		text: "../Lib/text/text",
		jquery: "../Lib/jquery/jquery.min",
		knockout: "../Lib/knockout/knockout",
		bootstrap: "../Lib/bootstrap/js/bootstrap.min",
		crossroads: "../Lib/crossroads/crossroads.min",
		signals: "../Lib/signals/signals.min",
		PortalClient: "../Lib/Portal/PortalClient.min",
		bootstrapDatetimepicker: "../Lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min",
		moment: "../Lib/moment/moment-with-locales.min",
		vis: "../Lib/vis/vis.min",
		Hls: "../Lib/hls.js/hls.min",
		bloodhound: "../Lib/typeahead.js/dist/bloodhound",
		typeahead: "../Lib/typeahead.js/dist/typeahead.jquery",
		analytics: "//www.google-analytics.com/analytics",
	},
	shim: {
		bootstrap: {
			deps: [
				"jquery"
			]
		},
		PortalClient: {
			exports: "CHAOS"
		},
		analytics: {
			exports: "ga"
		}
	},
	deps: ["bootstrap", "KnockoutBindings/DateTimePicker", "KnockoutBindings/AutoResize", "KnockoutBindings/Element", "KnockoutBindings/ScrollTo", "KnockoutBindings/Image", "KnockoutBindings/LinedText", "KnockoutBindings/Tooltip", "Main"],
	waitSeconds: 7,
	urlArgs: "bust=" + CacheBuster
});