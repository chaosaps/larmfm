﻿import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Configuration = require("Managers/Configuration");

type Fields = { [key: string]: any };

class Copyright
{
	public IsCleared(data:EZArchivePortal.IData[]): boolean
	{
		const tvDataList = data.filter(d => d.Name === Configuration.Copyright.TvSchemaName);

		if (tvDataList.length === 0) return true;

		const fields = tvDataList[0].Fields;

		return this.CheckMovies(fields) && this.CheckSports(fields);
	}

	private CheckMovies(fields: Fields): boolean
	{
		if (!fields.hasOwnProperty("MajorGenre")) return true;

		const major = fields["MajorGenre"].toLocaleLowerCase();

		if (major !== "film")
			return true;
		else if (!fields.hasOwnProperty("MinorGenre"))
			return false;

		const minor = fields["MinorGenre"].toLocaleLowerCase();

		return minor === "miniserie" || minor === "thrillerserie" || minor === "thrillerdrama" || minor.indexOf("krimi") === 0;
	}

	private CheckSports(fields: Fields): boolean
	{
		return !fields.hasOwnProperty("Channel") || fields["Channel"].toLocaleLowerCase() !== "tv2 sport";
	}
}

var instance = new Copyright();

export = instance;