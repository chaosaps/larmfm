var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Components/DisposableComponent", "Managers/Navigation/Navigation"], function (require, exports, DisposableComponent, Navigation) {
    "use strict";
    var Title = (function (_super) {
        __extends(Title, _super);
        function Title() {
            var _this = _super.call(this) || this;
            _this.Subscribe(Navigation.Current, function (p) { return _this.Set(); });
            return _this;
        }
        Title.prototype.Set = function (newTitle) {
            if (newTitle === void 0) { newTitle = null; }
            document.title = "LARM" + (newTitle == null ? "" : " - " + newTitle);
        };
        return Title;
    }(DisposableComponent));
    var instance = new Title();
    return instance;
});
