﻿import knockout = require("knockout");
import crossroads = require("crossroads");
import Address = require("Managers/Navigation/Address");
import Tracking = require("Managers/Tracking");

class Navigation
{
	public Current: KnockoutObservable<IPage> = knockout.observable<IPage>(null);

	private stateHandler: Address;

	constructor()
	{
		this.stateHandler = new Address(address =>
		{
			crossroads.parse(address);
			Tracking.TrackPage(`/${address}`);
		});

		crossroads.addRoute("",() => this.LoadSearchPage());
		crossroads.addRoute("Search",() => this.LoadSearchPage());
		crossroads.addRoute("Search{?query}", (query: any) => this.LoadSearchPage(this.DecodeSpacesInQuery(query)));
		crossroads.addRoute("Asset/{id}", (id: string) => this.LoadAssetPage(id));
		crossroads.addRoute("Asset/{id}/{annotationId}", (id: string, annotationId: string) => this.LoadAssetPage(id, annotationId));
		crossroads.addRoute("#!object/id={id}",(id: string) => this.Navigate(`Asset/${id}`));
		crossroads.addRoute("#MCM/Object?guid={id}",(id: string) => this.Navigate(`Asset/${this.GuidToUId(id)}`));
		crossroads.addRoute("Login",() => this.LoadPage("Login"));
		crossroads.addRoute("Terms", () => this.LoadPage("Terms"));
		crossroads.addRoute("CookiePolicy", () => this.LoadPage("CookiePolicy"));
		crossroads.addRoute("Stats", () => this.LoadPage("Stats"));
		crossroads.addRoute("Analyse", () => this.LoadPage("Analyse"));
		crossroads.addRoute("Test", () => this.LoadPage("Test"));
		crossroads.addRoute("Tools", () => this.LoadPage("Tools"));
		crossroads.addRoute("{address*}", (address: string) => this.LoadPage("NotFound", address));
		crossroads.shouldTypecast = true;

		crossroads.parse(this.stateHandler.Current);
		Tracking.TrackPage(`/${this.stateHandler.Current}`);
	}

	public Navigate(path: string): void
	{
		this.stateHandler.Navigate(path);
	}

	private LoadPage(name: string, data?: any): void
	{
		if (this.Current() != null && this.Current().Name === name)
			this.Current().Data(data);
		else
			this.Current(this.CreatePage(name, data));
	}

	private LoadSearchPage(query?: {[key:string]:any}): void
	{
		if (query === undefined)
			query = {}

		if (this.Current() != null && this.Current().Name === "Search")
			this.Current().Data().Query(query);
		else
			this.Current(this.CreatePage("Search", {Query: knockout.observable(query)}));
	}

	private LoadAssetPage(id: string, annotationId: string = null): void
	{
		if (this.Current() == null || this.Current().Name !== "Asset" || this.Current().Data().Id !== id)
			this.LoadPage("Asset", { Id: id, AnnotationId: knockout.observable(annotationId) });
		else
			this.Current().Data().AnnotationId(annotationId);
	}

	private CreatePage(name: string, data?: any): IPage
	{
		return new Page(name, data);
	}

	private DecodeSpacesInQuery(query: { [key: string]: (string|boolean|number) }): { [key:string]:(string | boolean | number) }
	{
		for (let key in query)
		{
			if (query.hasOwnProperty(key) && typeof query[key] !== "string") continue;
			query[key] = (<string>query[key]).replace(/\+/g, " ");
		}

		return query;
	}

	private GuidToUId(id: string): string
	{
		return id.substr(6, 2) +
			id.substr(4, 2) +
			id.substr(2, 2) +
			id.substr(0, 2) +
			"-" +
			id.substr(11, 2) +
			id.substr(9, 2) +
			"-" +
			id.substr(16, 2) +
			id.substr(14, 2) +
			id.substr(18);
	}
}

class Page implements IPage
{
	public Name: string;
	public Data: KnockoutObservable<any>;

	constructor(name: string, data?: any)
	{
		this.Name = name;
		this.Data = knockout.observable(data);
	}
}

var instance = new Navigation();

export = instance;