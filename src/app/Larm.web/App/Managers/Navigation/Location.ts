﻿import jquery = require("jquery");

class Location
{
	public AddChangedCallback(changedCallback:(url:string) => void):void
	{
		jquery(window).on("popstate",() => changedCallback(this.Path));
	} 

	public AddLinkCallback(linkCallback: (url: string, absoluteUrl: string) => boolean): void
	{
		jquery(document).on("click", "a[href]",(event: JQueryMouseEventObject) =>
		{
			const element = event.currentTarget as HTMLAnchorElement;

			if (element.download && element.download !== "")
		        return true;

			var $currentTarget = jquery(element);
			return linkCallback($currentTarget.attr("href"), $currentTarget.prop("href"));
		});
	} 

	public get Path(): string
	{
		return document.location.pathname.toString() + document.location.search.toString() + document.location.hash.toString();
	}

	public set Path(path: string)
	{
		history.pushState(null, "", path);
	}
}

var instance = new Location();

export = instance;