﻿import Location = require("Managers/Navigation/Location");

class Address
{
	private callback: (address: string) => void;
	private basePath: string;
	private basePathReg:RegExp;
	private baseUrlReg: RegExp;

	constructor(callback: (address: string) => void, basePath?: string)
	{
		this.basePath = basePath == null ? "/" : `/${basePath}/`;
		this.basePathReg = new RegExp(`^${this.basePath}`);
		this.baseUrlReg = new RegExp(`^https?://.*?${this.basePath}`);
		this.callback = callback;

		Location.AddChangedCallback(url => this.callback(this.Current));
		Location.AddLinkCallback((url, absolute) => this.LinkClicked(url, absolute));
	}

	public get Current(): string
	{
		return Location.Path.replace(this.basePathReg, "");
	}

	public Navigate(path: string): void
	{
		Location.Path = this.basePath + path;
		this.callback(path);
	}

	private LinkClicked(url:string, absoluteUrl:string): boolean
	{
		if (url.indexOf("#") === 0 || url.indexOf("http") === 0 || url.indexOf("mailto:") === 0) return true;

		this.Navigate(absoluteUrl.replace(this.baseUrlReg, ""));

		return false;
	}
}

export = Address; 