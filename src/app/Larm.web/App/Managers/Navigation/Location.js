define(["require", "exports", "jquery"], function (require, exports, jquery) {
    "use strict";
    var Location = (function () {
        function Location() {
        }
        Location.prototype.AddChangedCallback = function (changedCallback) {
            var _this = this;
            jquery(window).on("popstate", function () { return changedCallback(_this.Path); });
        };
        Location.prototype.AddLinkCallback = function (linkCallback) {
            jquery(document).on("click", "a[href]", function (event) {
                var element = event.currentTarget;
                if (element.download && element.download !== "")
                    return true;
                var $currentTarget = jquery(element);
                return linkCallback($currentTarget.attr("href"), $currentTarget.prop("href"));
            });
        };
        Object.defineProperty(Location.prototype, "Path", {
            get: function () {
                return document.location.pathname.toString() + document.location.search.toString() + document.location.hash.toString();
            },
            set: function (path) {
                history.pushState(null, "", path);
            },
            enumerable: true,
            configurable: true
        });
        return Location;
    }());
    var instance = new Location();
    return instance;
});
