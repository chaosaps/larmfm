define(["require", "exports", "Managers/Navigation/Location"], function (require, exports, Location) {
    "use strict";
    var Address = (function () {
        function Address(callback, basePath) {
            var _this = this;
            this.basePath = basePath == null ? "/" : "/" + basePath + "/";
            this.basePathReg = new RegExp("^" + this.basePath);
            this.baseUrlReg = new RegExp("^https?://.*?" + this.basePath);
            this.callback = callback;
            Location.AddChangedCallback(function (url) { return _this.callback(_this.Current); });
            Location.AddLinkCallback(function (url, absolute) { return _this.LinkClicked(url, absolute); });
        }
        Object.defineProperty(Address.prototype, "Current", {
            get: function () {
                return Location.Path.replace(this.basePathReg, "");
            },
            enumerable: true,
            configurable: true
        });
        Address.prototype.Navigate = function (path) {
            Location.Path = this.basePath + path;
            this.callback(path);
        };
        Address.prototype.LinkClicked = function (url, absoluteUrl) {
            if (url.indexOf("#") === 0 || url.indexOf("http") === 0 || url.indexOf("mailto:") === 0)
                return true;
            this.Navigate(absoluteUrl.replace(this.baseUrlReg, ""));
            return false;
        };
        return Address;
    }());
    return Address;
});
