define(["require", "exports", "knockout", "crossroads", "Managers/Navigation/Address", "Managers/Tracking"], function (require, exports, knockout, crossroads, Address, Tracking) {
    "use strict";
    var Navigation = (function () {
        function Navigation() {
            var _this = this;
            this.Current = knockout.observable(null);
            this.stateHandler = new Address(function (address) {
                crossroads.parse(address);
                Tracking.TrackPage("/" + address);
            });
            crossroads.addRoute("", function () { return _this.LoadSearchPage(); });
            crossroads.addRoute("Search", function () { return _this.LoadSearchPage(); });
            crossroads.addRoute("Search{?query}", function (query) { return _this.LoadSearchPage(_this.DecodeSpacesInQuery(query)); });
            crossroads.addRoute("Asset/{id}", function (id) { return _this.LoadAssetPage(id); });
            crossroads.addRoute("Asset/{id}/{annotationId}", function (id, annotationId) { return _this.LoadAssetPage(id, annotationId); });
            crossroads.addRoute("#!object/id={id}", function (id) { return _this.Navigate("Asset/" + id); });
            crossroads.addRoute("#MCM/Object?guid={id}", function (id) { return _this.Navigate("Asset/" + _this.GuidToUId(id)); });
            crossroads.addRoute("Login", function () { return _this.LoadPage("Login"); });
            crossroads.addRoute("Terms", function () { return _this.LoadPage("Terms"); });
            crossroads.addRoute("CookiePolicy", function () { return _this.LoadPage("CookiePolicy"); });
            crossroads.addRoute("Stats", function () { return _this.LoadPage("Stats"); });
            crossroads.addRoute("Analyse", function () { return _this.LoadPage("Analyse"); });
            crossroads.addRoute("Test", function () { return _this.LoadPage("Test"); });
            crossroads.addRoute("Tools", function () { return _this.LoadPage("Tools"); });
            crossroads.addRoute("{address*}", function (address) { return _this.LoadPage("NotFound", address); });
            crossroads.shouldTypecast = true;
            crossroads.parse(this.stateHandler.Current);
            Tracking.TrackPage("/" + this.stateHandler.Current);
        }
        Navigation.prototype.Navigate = function (path) {
            this.stateHandler.Navigate(path);
        };
        Navigation.prototype.LoadPage = function (name, data) {
            if (this.Current() != null && this.Current().Name === name)
                this.Current().Data(data);
            else
                this.Current(this.CreatePage(name, data));
        };
        Navigation.prototype.LoadSearchPage = function (query) {
            if (query === undefined)
                query = {};
            if (this.Current() != null && this.Current().Name === "Search")
                this.Current().Data().Query(query);
            else
                this.Current(this.CreatePage("Search", { Query: knockout.observable(query) }));
        };
        Navigation.prototype.LoadAssetPage = function (id, annotationId) {
            if (annotationId === void 0) { annotationId = null; }
            if (this.Current() == null || this.Current().Name !== "Asset" || this.Current().Data().Id !== id)
                this.LoadPage("Asset", { Id: id, AnnotationId: knockout.observable(annotationId) });
            else
                this.Current().Data().AnnotationId(annotationId);
        };
        Navigation.prototype.CreatePage = function (name, data) {
            return new Page(name, data);
        };
        Navigation.prototype.DecodeSpacesInQuery = function (query) {
            for (var key in query) {
                if (query.hasOwnProperty(key) && typeof query[key] !== "string")
                    continue;
                query[key] = query[key].replace(/\+/g, " ");
            }
            return query;
        };
        Navigation.prototype.GuidToUId = function (id) {
            return id.substr(6, 2) +
                id.substr(4, 2) +
                id.substr(2, 2) +
                id.substr(0, 2) +
                "-" +
                id.substr(11, 2) +
                id.substr(9, 2) +
                "-" +
                id.substr(16, 2) +
                id.substr(14, 2) +
                id.substr(18);
        };
        return Navigation;
    }());
    var Page = (function () {
        function Page(name, data) {
            this.Name = name;
            this.Data = knockout.observable(data);
        }
        return Page;
    }());
    var instance = new Navigation();
    return instance;
});
