﻿import knockout = require("knockout");
import Search = require("Managers/Search/Search");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import LarmPortal = require("Managers/Portal/Portal");
import Notification = require("Managers/Notification");
import Profile = require("Managers/Profile");
import DisposableComponent = require("Components/DisposableComponent");
import IEZProject = EZArchivePortal.IEZProject;

class ProjectManager extends DisposableComponent
{
	public Projects = knockout.observableArray<IEZProject>();
	public SearchLabel: KnockoutComputed<EZArchivePortal.IEZLabel>;

	constructor()
	{
		super();

		this.AddAction(Profile.IsReady, () => this.LoadProjects());

		this.SearchLabel = knockout.pureComputed(() => this.GetSearchLabel());
	}

	public AddUser(project: IEZProject, user: EZArchivePortal.IEZUser): void
	{
		project.Users.push(user);

		EZArchivePortal.EZProject.AssociateWith(project.Identifier, user.Identifier).WithCallback(response =>
		{
			if (response.Error != null) 
			{
				Notification.Error("Failed to add user to project: " + response.Error.Message);

				if (project.Users[project.Users.length - 1] === user)
					project.Users.splice(project.Users.length - 1, 1);
			}
		});
	}

	public RemoveUser(project: IEZProject, userIdentifier: string): void 
	{
		var user: EZArchivePortal.IEZUser;
		for (let i = 0; i < project.Users.length; i++)
		{
			if (project.Users[i].Identifier === userIdentifier) 
			{
				user = project.Users.splice(i, 1)[0];
				break;
			}
		}

		EZArchivePortal.EZProject.DisassociateWith(project.Identifier, userIdentifier).WithCallback(response =>
		{
			if (response.Error != null) 
			{
				Notification.Error("Failed to add user to project: " + response.Error.Message);

				project.Users.push(user);
			}
		});
	}

	public MoveProjectNextTo(idA: string, idB: string): void
	{
		const projects = this.Projects();
		let indexA = -1;
		let indexB = -1;
		for (let i = 0; i < projects.length; i++)
			if (projects[i].Identifier === idA)
				indexA = i;
			else if (projects[i].Identifier === idB)
				indexB = i;

		if (indexA === -1)
			throw new Error("Could not find project: " + idA);

		if (indexB === -1)
			throw new Error("Could not find project: " + idB);

		const projectB = this.Projects.splice(indexB, 1)[0];
		this.Projects.splice(indexA, 0, projectB);

		Profile.SetProjectOrder(this.Projects().map(p => p.Identifier));
	}

	private LoadProjects():void
	{
		EZArchivePortal.EZProject.Get().WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error("Failed to get projects: " + response.Error.Message);
				return;
			}

			if (response.Body.Results.length > 0)
				this.InitializeProjects(response.Body.Results);
				
		});
	}

	private InitializeProjects(projects: IEZProject[]): void
	{
		let projectOrder = Profile.ProjectOrder();
		let hasChanges = false;
		const result: IEZProject[] = [];

		if (projectOrder === null)
			projectOrder = [];

		for (let order of projectOrder)
			for (let i = 0; i < projects.length; i++)
				if (projects[i].Identifier === order) {
					result.push(projects.splice(i, 1)[0]);
					break;
				}

		if (projectOrder.length > result.length)
			hasChanges = true;

		if (projects.length !== 0) {
			hasChanges = true;
			result.push(...projects);
		}
			

		if (hasChanges)
			Profile.SetProjectOrder(result.map(p => p.Identifier));

		this.Projects.push(...result);
	}

	private GetSearchLabel(): EZArchivePortal.IEZLabel
	{
		var query = Search.Query();
		var projects = this.Projects();

		if (query.Label == null || projects.length === 0) return null;

		for (let project of projects)
		{
			if (project.Labels == null) continue;
			for (let label of project.Labels)
			{
				if (label.Identifier === query.Label) return label;
			}
		}

		return null;
	}
}

var instance = new ProjectManager();

export = instance;