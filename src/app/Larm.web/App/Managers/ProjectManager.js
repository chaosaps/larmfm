var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Search/Search", "Managers/Portal/EZArchivePortal", "Managers/Notification", "Managers/Profile", "Components/DisposableComponent"], function (require, exports, knockout, Search, EZArchivePortal, Notification, Profile, DisposableComponent) {
    "use strict";
    var ProjectManager = (function (_super) {
        __extends(ProjectManager, _super);
        function ProjectManager() {
            var _this = _super.call(this) || this;
            _this.Projects = knockout.observableArray();
            _this.AddAction(Profile.IsReady, function () { return _this.LoadProjects(); });
            _this.SearchLabel = knockout.pureComputed(function () { return _this.GetSearchLabel(); });
            return _this;
        }
        ProjectManager.prototype.AddUser = function (project, user) {
            project.Users.push(user);
            EZArchivePortal.EZProject.AssociateWith(project.Identifier, user.Identifier).WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to add user to project: " + response.Error.Message);
                    if (project.Users[project.Users.length - 1] === user)
                        project.Users.splice(project.Users.length - 1, 1);
                }
            });
        };
        ProjectManager.prototype.RemoveUser = function (project, userIdentifier) {
            var user;
            for (var i = 0; i < project.Users.length; i++) {
                if (project.Users[i].Identifier === userIdentifier) {
                    user = project.Users.splice(i, 1)[0];
                    break;
                }
            }
            EZArchivePortal.EZProject.DisassociateWith(project.Identifier, userIdentifier).WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to add user to project: " + response.Error.Message);
                    project.Users.push(user);
                }
            });
        };
        ProjectManager.prototype.MoveProjectNextTo = function (idA, idB) {
            var projects = this.Projects();
            var indexA = -1;
            var indexB = -1;
            for (var i = 0; i < projects.length; i++)
                if (projects[i].Identifier === idA)
                    indexA = i;
                else if (projects[i].Identifier === idB)
                    indexB = i;
            if (indexA === -1)
                throw new Error("Could not find project: " + idA);
            if (indexB === -1)
                throw new Error("Could not find project: " + idB);
            var projectB = this.Projects.splice(indexB, 1)[0];
            this.Projects.splice(indexA, 0, projectB);
            Profile.SetProjectOrder(this.Projects().map(function (p) { return p.Identifier; }));
        };
        ProjectManager.prototype.LoadProjects = function () {
            var _this = this;
            EZArchivePortal.EZProject.Get().WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to get projects: " + response.Error.Message);
                    return;
                }
                if (response.Body.Results.length > 0)
                    _this.InitializeProjects(response.Body.Results);
            });
        };
        ProjectManager.prototype.InitializeProjects = function (projects) {
            var _a;
            var projectOrder = Profile.ProjectOrder();
            var hasChanges = false;
            var result = [];
            if (projectOrder === null)
                projectOrder = [];
            for (var _i = 0, projectOrder_1 = projectOrder; _i < projectOrder_1.length; _i++) {
                var order = projectOrder_1[_i];
                for (var i = 0; i < projects.length; i++)
                    if (projects[i].Identifier === order) {
                        result.push(projects.splice(i, 1)[0]);
                        break;
                    }
            }
            if (projectOrder.length > result.length)
                hasChanges = true;
            if (projects.length !== 0) {
                hasChanges = true;
                result.push.apply(result, projects);
            }
            if (hasChanges)
                Profile.SetProjectOrder(result.map(function (p) { return p.Identifier; }));
            (_a = this.Projects).push.apply(_a, result);
        };
        ProjectManager.prototype.GetSearchLabel = function () {
            var query = Search.Query();
            var projects = this.Projects();
            if (query.Label == null || projects.length === 0)
                return null;
            for (var _i = 0, projects_1 = projects; _i < projects_1.length; _i++) {
                var project = projects_1[_i];
                if (project.Labels == null)
                    continue;
                for (var _a = 0, _b = project.Labels; _a < _b.length; _a++) {
                    var label = _b[_a];
                    if (label.Identifier === query.Label)
                        return label;
                }
            }
            return null;
        };
        return ProjectManager;
    }(DisposableComponent));
    var instance = new ProjectManager();
    return instance;
});
