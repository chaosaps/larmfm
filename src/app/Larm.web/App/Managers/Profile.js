define(["require", "exports", "knockout", "Managers/Portal/EZArchivePortal", "Managers/Notification", "Managers/Authorization", "Managers/Dialog/Dialog"], function (require, exports, knockout, EZArchivePortal, Notification, Authorization, DialogManager) {
    "use strict";
    var Profile = (function () {
        function Profile() {
            var _this = this;
            this.Identifier = knockout.observable(null);
            this.Email = knockout.observable("");
            this.Name = knockout.observable("");
            this.Preferences = knockout.observable(null);
            this.Permission = knockout.observable(null);
            this.IsAdministrator = knockout.observable(false);
            this._user = null;
            this.IsReady = knockout.pureComputed(function () { return _this.Identifier() !== null; });
            this.HasAcceptedTerms = knockout.pureComputed(function () {
                var preferences = _this.Preferences();
                return preferences !== null && preferences.Terms === 1;
            });
            this.ProjectOrder = knockout.pureComputed(function () {
                var preferences = _this.Preferences();
                return preferences !== null && preferences.ProjectOrder ? preferences.ProjectOrder : null;
            });
            Authorization.IsAuthenticated.subscribe(function (isAuthenticated) {
                if (isAuthenticated)
                    _this.GetProfile();
                else
                    _this.Reset();
            });
        }
        Profile.prototype.AcceptTerms = function () {
            if (!this.IsReady())
                throw new Error("Not ready to update preferences");
            var preferences = this.Preferences();
            if (preferences === null)
                preferences = { Terms: 1 };
            else
                preferences.Terms = 1;
            this._user.Preferences = JSON.stringify(preferences);
            this.Preferences(preferences);
            this.SetProfile(function (success) {
            });
        };
        Profile.prototype.SetProjectOrder = function (projectOrder) {
            if (!this.IsReady())
                throw new Error("Not ready to update preferences");
            var preferences = this.Preferences();
            if (preferences === null)
                preferences = { ProjectOrder: projectOrder };
            else
                preferences.ProjectOrder = projectOrder;
            this._user.Preferences = JSON.stringify(preferences);
            this.Preferences(preferences);
            this.SetProfile(function (success) {
            });
        };
        Profile.prototype.Update = function (name, email, callback) {
            var _this = this;
            var originalName = this._user.Name;
            var originalEmail = this._user.Email;
            this._user.Name = name;
            this._user.Email = email !== "" ? email : this.Identifier();
            this.Name(name);
            this.Email(email);
            this.SetProfile(function (success) {
                if (!success) {
                    _this._user.Name = originalName;
                    _this._user.Email = originalEmail;
                    _this.Name(originalName);
                    _this.Email(originalEmail);
                }
                callback(success);
            });
        };
        Profile.prototype.EditProfile = function () {
            DialogManager.Show("Profile/EditProfileDialog", false);
        };
        Profile.prototype.SetProfile = function (callback) {
            EZArchivePortal.EZUser.Set(this._user).WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Error updating profile: " + response.Error.Message);
                    callback(false);
                    return;
                }
                callback(true);
            });
        };
        Profile.prototype.GetProfile = function () {
            var _this = this;
            EZArchivePortal.EZUser.Me().WithCallback(function (ezUserResponse) {
                if (ezUserResponse.Error != null) {
                    Notification.Error("Error getting profile: " + ezUserResponse.Error.Message);
                    return;
                }
                if (ezUserResponse.Body.Results.length !== 0)
                    _this.LoadProfile(ezUserResponse.Body.Results[0]);
                else
                    Notification.Error("Error getting profile, profile matching userid not found");
            });
        };
        Profile.prototype.Reset = function () {
            this.Name("");
            this.Email("");
            this.Preferences(null);
        };
        Profile.prototype.LoadProfile = function (user) {
            this._user = user;
            this.Identifier(user.Identifier);
            this.Name(user.Name);
            this.Email(user.Email !== user.Identifier ? user.Email : "");
            this.Preferences(user.Preferences === null ? null : JSON.parse(user.Preferences));
            this.Permission(user.Permission);
            this.IsAdministrator(user.IsAdministrator);
            if (this.ShowTermsDialogIfNeeded())
                this.ShowDialogIfProfileIsIncomplete(false);
        };
        Profile.prototype.ShowTermsDialogIfNeeded = function () {
            if (this.HasAcceptedTerms())
                return true;
            DialogManager.Show("Profile/Terms", true, false, true);
            return false;
        };
        Profile.prototype.ShowDialogIfProfileIsIncomplete = function (showWelcome) {
            if (this.Name() === null || this.Name().trim() === "")
                DialogManager.Show("Profile/EditProfileDialog", showWelcome, false);
        };
        return Profile;
    }());
    var instance = new Profile();
    return instance;
});
