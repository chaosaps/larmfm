define(["require", "exports", "knockout", "Managers/Portal/Portal", "Managers/Notification", "Managers/Portal/Wayf", "PortalClient", "Managers/Tracking", "Managers/Dialog/Dialog"], function (require, exports, knockout, Portal, Notification, Wayf, PortalClient, Tracking, DialogManager) {
    "use strict";
    var Authorization = (function () {
        function Authorization() {
            var _this = this;
            this.IsLoggingIn = knockout.observable(false);
            this.IsAuthenticated = Portal.IsAuthenticated;
            this.CanLogin = knockout.computed(function () { return Portal.IsReady() && !_this.IsAuthenticated(); });
            this.CanLogOut = knockout.computed(function () { return Portal.IsReady() && _this.IsAuthenticated(); });
        }
        Authorization.prototype.Login = function (email, password, storeLogin, callback) {
            var _this = this;
            if (!this.CanLogin())
                return;
            this.IsLoggingIn(true);
            PortalClient.Portal.Client.EmailPassword.Login(email, password).WithCallback(function (response) {
                _this.IsLoggingIn(false);
                if (response.Error != null) {
                    if (response.Error.Fullname === "Chaos.Portal.Authentication.Exception.LoginException")
                        callback(false);
                    else
                        Notification.Error("Failed to login: " + response.Error.Message);
                }
                else {
                    Portal.StoreLogin(storeLogin);
                    callback(true);
                }
            });
        };
        Authorization.prototype.WayfLogin = function () {
            var _this = this;
            this.IsLoggingIn(true);
            Wayf.Login(function (success, notAproved) {
                _this.IsLoggingIn(false);
                if (!success && notAproved) {
                    DialogManager.Show("Profile/NotApprovedDialog", {});
                    Tracking.TrackEvent("Login", "Wayf", "User not approved");
                }
            });
        };
        Authorization.prototype.LogOut = function () {
            Portal.LogOut();
        };
        return Authorization;
    }());
    var instance = new Authorization();
    return instance;
});
