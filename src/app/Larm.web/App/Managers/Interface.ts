﻿import knockout = require("knockout");
import DisposableComponent = require("Components/DisposableComponent");

class Interface extends DisposableComponent
{
	public IsMenuCollapsed = knockout.observable(false)

	public Show(): void
	{
		this.IsMenuCollapsed(false);
	}

	public Hide(): void
	{
		this.IsMenuCollapsed(true);
	}
}

var instance = new Interface();

export = instance;