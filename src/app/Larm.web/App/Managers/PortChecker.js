define(["require", "exports", "jquery", "knockout", "Managers/Configuration", "Managers/Tracking"], function (require, exports, jquery, knockout, Configuration, Tracking) {
    "use strict";
    var PortChecker = (function () {
        function PortChecker() {
            var _this = this;
            this.IsChecking = knockout.observable(false);
            this.FailedPorts = knockout.observableArray();
            this.HasErrors = knockout.pureComputed(function () { return _this.FailedPorts().length !== 0; });
            this.IsChecking.subscribe(function (v) {
                if (!v && _this.HasErrors()) {
                    var ports = _this.FailedPorts();
                    ports.sort(function (a, b) { return a - b; });
                    Tracking.TrackEvent("Port Checking", "Failed", ports.join());
                }
            });
            this.Run();
        }
        PortChecker.prototype.Run = function () {
            var _this = this;
            if (this.IsChecking())
                return;
            this.IsChecking(true);
            this.FailedPorts.removeAll();
            jquery.when.apply(jquery, Configuration.PortChecker.Ports.map(function (p) { return _this.CheckPort(p); }).map(function (check) {
                var deferred = jquery.Deferred();
                check.always(function () { return deferred.resolve(); });
                return deferred.promise();
            })).always(function () { return _this.IsChecking(false); });
        };
        PortChecker.prototype.CheckPort = function (port) {
            var _this = this;
            var url = Configuration.PortChecker.Url.replace(Configuration.PortChecker.PortToken, port.toString());
            return jquery.ajax(url, { cache: false }).done(function (data) {
                if (data.replace(/\r?\n/g, "") !== Configuration.PortChecker.Expected)
                    _this.FailedPorts.push(port);
            }).fail(function () { return _this.FailedPorts.push(port); });
        };
        return PortChecker;
    }());
    var instance = new PortChecker();
    return instance;
});
