define(["require", "exports", "module"], function (require, exports, Module) {
    "use strict";
    var Configuration = (function () {
        function Configuration() {
            this.PortalPath = Module.config().PortalPath;
            this.TrackingId = Module.config().TrackingId;
            this.WayfPath = Module.config().WayfPath;
            this.AnonymousEmail = Module.config().AnonymousEmail;
            this.SchemaOrder = Module.config().SchemaOrder;
            this.SchemaTableSort = Module.config().SchemaTableSort;
            this.SchemaTableCollapse = Module.config().SchemaTableCollapse;
            this.SchemaListField = Module.config().SchemaListField;
            this.BroadcastWallTime = Module.config().BroadcastWallTime;
            this.HiddenSchemas = Module.config().HiddenSchemas;
            this.OffsetSchema = Module.config().OffsetSchema;
            this.HiddenAnnotationProperties = Module.config().HiddenAnnotationProperties;
            this.DefaultPageSize = Module.config().DefaultPageSize;
            this.MaxNumberOfPages = Module.config().MaxNumberOfPages;
            this.PageSizes = Module.config().PageSizes;
            this.SearchImagePath = Module.config().SearchImagePath;
            this.SearchImageMaps = Module.config().SearchImageMaps;
            this.SearchResultReplaceMap = Module.config().SearchResultReplaceMap;
            this.SearchResultReplaceValueMap = Module.config().SearchResultReplaceValueMap;
            this.SearchResultDefaultValue = Module.config().SearchResultDefaultValue;
            this.AssetDataReplaceValues = Module.config().AssetDataReplaceValues;
            this.DefaultTypeFormat = Module.config().DefaultTypeFormat;
            this.AssetTypeFormat = Module.config().AssetTypeFormat;
            this.UserAssetTypeFormat = Module.config().UserAssetTypeFormat;
            this.AssetUseDateFormatFieldNames = Module.config().AssetUseDateFormatFieldNames;
            this.TimedToolbar = Module.config().TimedToolbar;
            this.Copyright = Module.config().Copyright;
            this.DrillDownFacet = Module.config().DrillDownFacet;
            this.MenuFacetHeader = Module.config().MenuFacetHeader;
            this.LabelFilter = Module.config().LabelFilter;
            this.FacetValuesVisible = Module.config().FacetValuesVisible;
            this.FacetKeys = Module.config().FacetKeys;
            this.TrackingType = Module.config().TrackingType;
            this.PortChecker = Module.config().PortChecker;
        }
        Configuration.prototype.IsSchemaVisible = function (name, includeOffset) {
            if (includeOffset && name === this.OffsetSchema)
                return true;
            for (var _i = 0, _a = this.HiddenSchemas; _i < _a.length; _i++) {
                var hidden = _a[_i];
                if (hidden === name)
                    return false;
            }
            return true;
        };
        return Configuration;
    }());
    var instance = new Configuration();
    return instance;
});
