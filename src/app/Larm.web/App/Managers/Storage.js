define(["require", "exports", "jquery"], function (require, exports, jquery) {
    "use strict";
    var Storage = (function () {
        function Storage() {
            var _this = this;
            this._canUseStorage = false;
            this._listeners = {};
            try {
                this._canUseStorage = window.localStorage != undefined;
            }
            catch (e) { }
            if (this._canUseStorage) {
                jquery(window).on("storage", function (e) {
                    var event = e.originalEvent;
                    _this.Changed(event.key, event.newValue);
                });
            }
        }
        Object.defineProperty(Storage.prototype, "CanUseStorage", {
            get: function () {
                return this._canUseStorage;
            },
            enumerable: true,
            configurable: true
        });
        Storage.prototype.Get = function (key) {
            return this._canUseStorage ? window.localStorage.getItem(key) : null;
        };
        Storage.prototype.Set = function (key, value) {
            if (this._canUseStorage)
                window.localStorage.setItem(key, value);
        };
        Storage.prototype.SetAndRemove = function (key, value) {
            this.Set(key, value);
            this.Remove(key);
        };
        Storage.prototype.Remove = function (key) {
            if (this._canUseStorage)
                window.localStorage.removeItem(key);
        };
        Storage.prototype.Listen = function (key, callback) {
            if (this._listeners.hasOwnProperty(key))
                this._listeners[key].push(callback);
            else
                this._listeners[key] = [callback];
        };
        Storage.prototype.Unlisten = function (key, callback) {
            if (!this._listeners.hasOwnProperty(key) || this._listeners[key].indexOf(callback) === -1)
                return;
            this._listeners[key].splice(this._listeners[key].indexOf(callback), 1);
        };
        Storage.prototype.Changed = function (key, value) {
            if (!this._listeners.hasOwnProperty(key) || this._listeners[key].length === 0)
                return;
            this._listeners[key].forEach(function (v) { return v(value); });
        };
        return Storage;
    }());
    var instance = new Storage();
    return instance;
});
