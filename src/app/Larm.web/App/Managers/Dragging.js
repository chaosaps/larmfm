define(["require", "exports", "jquery", "knockout", "Managers/Interface"], function (require, exports, jquery, knockout, Interface) {
    "use strict";
    var Dragging = (function () {
        function Dragging() {
            var _this = this;
            this.DragType = "text";
            this.IsDraggingSearchResult = knockout.observable(false);
            this.IsDraggingProject = knockout.observable(false);
            this.DragData = knockout.observable(null);
            jquery("html").on("dragend", function () {
                _this.IsDraggingSearchResult(false);
                _this.IsDraggingProject(false);
                _this.DragData(null);
            });
        }
        Dragging.prototype.StartSearchResultDrag = function (event, data) {
            this.IsDraggingSearchResult(true);
            this.StartDrag(event, data);
            Interface.Show();
        };
        Dragging.prototype.StartProjectDrag = function (event, data) {
            this.IsDraggingProject(true);
            this.StartDrag(event, data);
        };
        Dragging.prototype.StartDrag = function (event, data) {
            event.dataTransfer.effectAllowed = "move";
            event.dataTransfer.clearData();
            event.dataTransfer.setData(this.DragType, data);
            this.DragData(data);
        };
        return Dragging;
    }());
    var instance = new Dragging();
    return instance;
});
