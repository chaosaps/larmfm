﻿import knockout = require("knockout");
import Tracking = require("Managers/Tracking");

class Notification
{
	public Notifications: KnockoutObservableArray<INotification> = knockout.observableArray<INotification>();

	private static instance: Notification;

	public Error(message: string): void
	{
		this.Notifications.push({ Message: message, Level: "Error" });
		Tracking.TrackEvent("Notification", "Error", message);
	}

	public Warning(message: string): void
	{
		this.Notifications.push({ Message: message, Level: "Warning" });
		Tracking.TrackEvent("Notification", "Warning", message);
	}

	public Debug(message: string): void
	{
		console.log(message);
		Tracking.TrackEvent("Notification", "Debug", message);
	}
}

var instance = new Notification();

export = instance; 