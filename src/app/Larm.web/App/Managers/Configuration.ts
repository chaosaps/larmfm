﻿import Module = require("module");

class Configuration
{
	public PortalPath: string;
	public TrackingId: string;
	public WayfPath: string;
	public AnonymousEmail: string;
	public SchemaOrder: string[];
	public SchemaTableSort: {[schemaName:string]:{[fieldName:string]:number[]}};
	public SchemaTableCollapse: {[schemaName:string]:{[fieldName:string]:{[index:number]:number}}};
	public SchemaListField: { TypeFacet: string, Schemas: { [schemaName: string]: {Field: string, Facet: string}}}
	public BroadcastWallTime: {Schema:string, Field:string}[];
	public HiddenSchemas: string[];
	public OffsetSchema: string;
	public HiddenAnnotationProperties: string;
	public DefaultPageSize: number;
	public MaxNumberOfPages: number;
	public PageSizes: number[];
	public SearchImagePath: string;
	public SearchImageMaps: string[];
	public SearchResultReplaceMap: { [type: string]: string };
	public SearchResultReplaceValueMap: { [type: string]: { [type: string]: string } };
	public SearchResultDefaultValue: { [assetType: string]: { [fieldName: string]: string } };
	public AssetDataReplaceValues: { [assetType: string]: { [fieldName: string]: string } };
	public DefaultTypeFormat: { [fieldType: string]: string }
	public AssetTypeFormat: { [assetType: string]: { [fieldType: string]: string } };
	public UserAssetTypeFormat: { [assetType: string]: { [fieldType: string]: string } };
	public AssetUseDateFormatFieldNames: string[];
	public TimedToolbar: { JumpLengths: number[], DefaultJumpLength: number, JumpLengthSecondText: string, JumpLengthMinuteText:string };
	public Copyright: { TvSchemaName: string };
	public DrillDownFacet: { Header: string, Field:string};
	public LabelFilter: string;
	public MenuFacetHeader: string;
	public FacetValuesVisible: number;
	public FacetKeys: { [key: string]: { Name: string, HasIcon:boolean, Values: { [key: string]: string } } };
	public TrackingType: { [key: string]: string }
	public PortChecker: { Url: string, PortToken: string, Ports: number[], Expected: string };

	constructor()
	{
		this.PortalPath = Module.config().PortalPath;
		this.TrackingId = Module.config().TrackingId;
		this.WayfPath = Module.config().WayfPath;
		this.AnonymousEmail = Module.config().AnonymousEmail;
		this.SchemaOrder = Module.config().SchemaOrder;
		this.SchemaTableSort = Module.config().SchemaTableSort;
		this.SchemaTableCollapse = Module.config().SchemaTableCollapse;
		this.SchemaListField = Module.config().SchemaListField;
		this.BroadcastWallTime = Module.config().BroadcastWallTime;
		this.HiddenSchemas = Module.config().HiddenSchemas;
		this.OffsetSchema = Module.config().OffsetSchema;
		this.HiddenAnnotationProperties = Module.config().HiddenAnnotationProperties;
		this.DefaultPageSize = Module.config().DefaultPageSize;
		this.MaxNumberOfPages = Module.config().MaxNumberOfPages;
		this.PageSizes = Module.config().PageSizes;
		this.SearchImagePath = Module.config().SearchImagePath;
		this.SearchImageMaps = Module.config().SearchImageMaps;
		this.SearchResultReplaceMap = Module.config().SearchResultReplaceMap;
		this.SearchResultReplaceValueMap = Module.config().SearchResultReplaceValueMap;
		this.SearchResultDefaultValue = Module.config().SearchResultDefaultValue;
		this.AssetDataReplaceValues = Module.config().AssetDataReplaceValues;
		this.DefaultTypeFormat = Module.config().DefaultTypeFormat;
		this.AssetTypeFormat = Module.config().AssetTypeFormat;
		this.UserAssetTypeFormat = Module.config().UserAssetTypeFormat;
		this.AssetUseDateFormatFieldNames = Module.config().AssetUseDateFormatFieldNames;
		this.TimedToolbar = Module.config().TimedToolbar;
		this.Copyright = Module.config().Copyright;
		this.DrillDownFacet = Module.config().DrillDownFacet;
		this.MenuFacetHeader = Module.config().MenuFacetHeader;
		this.LabelFilter = Module.config().LabelFilter;
		this.FacetValuesVisible = Module.config().FacetValuesVisible;
		this.FacetKeys = Module.config().FacetKeys;
		this.TrackingType = Module.config().TrackingType;
		this.PortChecker = Module.config().PortChecker;
	}

	public IsSchemaVisible(name: string, includeOffset: boolean): boolean
	{
		if (includeOffset && name === this.OffsetSchema)
			return true;

		for (let hidden of this.HiddenSchemas)
			if (hidden === name) return false;

		return true;
	}
}

var instance = new Configuration();

export = instance;