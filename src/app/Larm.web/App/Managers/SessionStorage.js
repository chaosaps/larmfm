define(["require", "exports"], function (require, exports) {
    "use strict";
    var SessionStorage = (function () {
        function SessionStorage() {
            this._canUseStorage = false;
            try {
                this._canUseStorage = window.sessionStorage != undefined;
            }
            catch (e) { }
        }
        Object.defineProperty(SessionStorage.prototype, "CanUseStorage", {
            get: function () {
                return this._canUseStorage;
            },
            enumerable: true,
            configurable: true
        });
        SessionStorage.prototype.Get = function (key) {
            return this._canUseStorage ? window.sessionStorage.getItem(key) : null;
        };
        SessionStorage.prototype.Set = function (key, value) {
            if (this._canUseStorage)
                window.sessionStorage.setItem(key, value);
        };
        SessionStorage.prototype.Remove = function (key) {
            if (this._canUseStorage)
                window.sessionStorage.removeItem(key);
        };
        return SessionStorage;
    }());
    var instance = new SessionStorage();
    return instance;
});
