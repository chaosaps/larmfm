define(["require", "exports", "PortalClient", "Managers/Notification", "Managers/SessionStorage"], function (require, exports, PortalClient, Notification, SessionStorage) {
    "use strict";
    var SessionRecover = (function () {
        function SessionRecover(client) {
            var _this = this;
            this._recallers = [];
            this.STORAGE_SECURE_COOKIE_GUID = "SecureCookieGuid";
            this.STORAGE_SECURE_COOKIE_PASSWORD = "SecureCookiePasswordGuid";
            this.TIMEOUT_DURATION = 10000;
            this._cookieGuid = null;
            this._cookiePassword = null;
            this._isOnTimeout = false;
            this.IsEnabled = true;
            this._cookieGuid = SessionStorage.Get(this.STORAGE_SECURE_COOKIE_GUID);
            this._cookiePassword = SessionStorage.Get(this.STORAGE_SECURE_COOKIE_PASSWORD);
            client.SetCallHandler({ ProcessResponse: function (response, recaller) { return _this.ProcessPortalResponse(response, recaller); } });
            client.SessionAuthenticated().Add(function (v) { return _this.AuthenticatedChanged(v); });
        }
        SessionRecover.prototype.Reset = function () {
            this._cookieGuid = null;
            this._cookiePassword = null;
            SessionStorage.Remove(this.STORAGE_SECURE_COOKIE_GUID);
            SessionStorage.Remove(this.STORAGE_SECURE_COOKIE_PASSWORD);
        };
        Object.defineProperty(SessionRecover.prototype, "CanRecover", {
            get: function () {
                return this._cookieGuid != null;
            },
            enumerable: true,
            configurable: true
        });
        SessionRecover.prototype.ProcessPortalResponse = function (response, recaller) {
            var _this = this;
            if (!this.IsEnabled || this._isOnTimeout || response.Error == null || (response.Error.Fullname !== "Chaos.Portal.Core.Exceptions.SessionDoesNotExistException" && response.Error.Fullname !== "Chaos.Portal.Core.Exceptions.InsufficientPermissionsException"))
                return true;
            Notification.Debug("Recovering session");
            this._recallers.push(recaller);
            if (this._recallers.length === 1) {
                PortalClient.Portal.Client.Session.Create().WithCallback(function (recreateResponse) {
                    if (recreateResponse.Error != null) {
                        Notification.Error("Failed to recreate session after old one had expired: " + recreateResponse.Error.Message);
                        return;
                    }
                    if (_this.CanRecover) {
                        _this.Recover(function (success) {
                            if (success)
                                _this.CallRecallers();
                        });
                    }
                    else
                        _this.CallRecallers();
                });
            }
            return false;
        };
        SessionRecover.prototype.Recover = function (callback) {
            var _this = this;
            PortalClient.Portal.Client.SecureCookie.Login(this._cookieGuid, this._cookiePassword).WithCallback(function (loginResponse) {
                if (loginResponse.Error != null) {
                    _this.Reset();
                    Notification.Debug("Failed to reauthenticate: " + loginResponse.Error.Message);
                    callback(false);
                    return;
                }
                var cookie = loginResponse.Body.Results[0];
                _this._cookiePassword = cookie.PasswordGuid;
                SessionStorage.Set(_this.STORAGE_SECURE_COOKIE_PASSWORD, cookie.PasswordGuid);
                callback(true);
            });
        };
        SessionRecover.prototype.CallRecallers = function () {
            var _this = this;
            this._isOnTimeout = true;
            while (this._recallers.length !== 0)
                this._recallers.pop()(true);
            setTimeout(function () { return (_this._isOnTimeout = false); }, this.TIMEOUT_DURATION);
        };
        SessionRecover.prototype.AuthenticatedChanged = function (authenticationType) {
            var _this = this;
            if (authenticationType == null) {
                this.Reset();
                return;
            }
            if (this._cookieGuid != null)
                return;
            PortalClient.Portal.Client.SecureCookie.Create().WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Debug("Failed to create recovery secure cookie: " + response.Error.Message);
                    return;
                }
                var cookie = response.Body.Results[0];
                _this._cookieGuid = cookie.Guid;
                _this._cookiePassword = cookie.PasswordGuid;
                SessionStorage.Set(_this.STORAGE_SECURE_COOKIE_GUID, cookie.Guid);
                SessionStorage.Set(_this.STORAGE_SECURE_COOKIE_PASSWORD, cookie.PasswordGuid);
            });
        };
        return SessionRecover;
    }());
    return SessionRecover;
});
