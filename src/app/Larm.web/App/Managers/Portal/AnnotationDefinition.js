define(["require", "exports", "knockout", "Managers/Portal/EZArchivePortal", "Managers/Portal/Portal", "Managers/Notification", "Utility/DisposableAction"], function (require, exports, knockout, EZArchivePortal, Portal, Notification, DisposableAction) {
    "use strict";
    var AnnotationDefinition = (function () {
        function AnnotationDefinition() {
            var _this = this;
            this.StartField = "StartTime";
            this.EndField = "EndTime";
            this.IsReady = knockout.observable(false);
            this.AnnotationDefinitions = knockout.observableArray();
            new DisposableAction(Portal.IsReady, function () { return _this.Initialize(); });
        }
        AnnotationDefinition.prototype.Initialize = function () {
            var _this = this;
            EZArchivePortal.EZAnnotationDefinition.Get().WithCallback(function (response) {
                var _a;
                if (response.Error != null) {
                    Notification.Error("Failed to load AnnotationDefinition: " + response.Error.Message);
                    return;
                }
                if (response.Body.Results.length > 0)
                    (_a = _this.AnnotationDefinitions).push.apply(_a, response.Body.Results);
                _this.IsReady(true);
            });
        };
        AnnotationDefinition.prototype.IsTimed = function (definition) {
            return definition.Fields.hasOwnProperty(this.StartField) && definition.Fields.hasOwnProperty(this.EndField);
        };
        AnnotationDefinition.prototype.GetByAssetType = function (type) {
            if (!this.IsReady())
                throw new Error("AnnotationDefinition not ready");
            var result = new Array();
            for (var _i = 0, _a = this.AnnotationDefinitions(); _i < _a.length; _i++) {
                var definition = _a[_i];
                if (definition.TypeId == type)
                    result.push(definition);
            }
            return result;
        };
        AnnotationDefinition.prototype.GetById = function (id) {
            if (!this.IsReady())
                throw new Error("AnnotationDefinition not ready");
            for (var _i = 0, _a = this.AnnotationDefinitions(); _i < _a.length; _i++) {
                var definition = _a[_i];
                if (definition.Identifier === id)
                    return definition;
            }
            throw new Error("AnnotationDefinition not found, id: " + id);
        };
        AnnotationDefinition.prototype.Get = function (name) {
            if (!this.IsReady())
                throw new Error("AnnotationDefinition not ready");
            for (var _i = 0, _a = this.AnnotationDefinitions(); _i < _a.length; _i++) {
                var definition = _a[_i];
                if (definition.Name === name)
                    return definition;
            }
            throw new Error("AnnotationDefinition not found: " + name);
        };
        return AnnotationDefinition;
    }());
    var instance = new AnnotationDefinition();
    return instance;
});
