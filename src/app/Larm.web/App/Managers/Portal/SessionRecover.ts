﻿import PortalClient = require("PortalClient");
import Notification = require("Managers/Notification");
import SessionStorage = require("Managers/SessionStorage");

class SessionRecover
{
	private _recallers: { (resetSession: boolean): void }[] = [];

	private STORAGE_SECURE_COOKIE_GUID: string = "SecureCookieGuid";
	private STORAGE_SECURE_COOKIE_PASSWORD: string = "SecureCookiePasswordGuid";
	private TIMEOUT_DURATION:number = 10000;

	private _cookieGuid:string = null;
	private _cookiePassword: string = null;
	private _isOnTimeout:boolean = false;

	public IsEnabled: boolean = true;

	constructor(client: PortalClient.Portal.Client.IPortalClient)
	{
		this._cookieGuid = SessionStorage.Get(this.STORAGE_SECURE_COOKIE_GUID);
		this._cookiePassword = SessionStorage.Get(this.STORAGE_SECURE_COOKIE_PASSWORD);

		client.SetCallHandler({ ProcessResponse: (response: any, recaller: (resetSession: boolean) => void) => this.ProcessPortalResponse(response, recaller) });
		client.SessionAuthenticated().Add(v => this.AuthenticatedChanged(v));
	}

	public Reset():void
	{
		this._cookieGuid = null;
		this._cookiePassword = null;
		SessionStorage.Remove(this.STORAGE_SECURE_COOKIE_GUID);
		SessionStorage.Remove(this.STORAGE_SECURE_COOKIE_PASSWORD);
	}

	public get CanRecover():boolean
	{
		return this._cookieGuid != null;
	}

	private ProcessPortalResponse<T>(response: CHAOS.Portal.Client.IPortalResponse<T>, recaller: (resetSession: boolean) => void): boolean
	{
		if (!this.IsEnabled || this._isOnTimeout || response.Error == null || (response.Error.Fullname !== "Chaos.Portal.Core.Exceptions.SessionDoesNotExistException" && response.Error.Fullname !== "Chaos.Portal.Core.Exceptions.InsufficientPermissionsException")) return true;

		Notification.Debug("Recovering session");

		this._recallers.push(recaller);

		if (this._recallers.length === 1)
		{
			PortalClient.Portal.Client.Session.Create().WithCallback(recreateResponse =>
			{
				if (recreateResponse.Error != null)
				{
					Notification.Error("Failed to recreate session after old one had expired: " + recreateResponse.Error.Message);
					return;
				}

				if (this.CanRecover)
				{
					this.Recover(success =>
					{
						if (success)
							this.CallRecallers();
					});
				}
				else
					this.CallRecallers();
			});
		}

		return false;
	}

	public Recover(callback:(success:boolean)=>void):void
	{
		PortalClient.Portal.Client.SecureCookie.Login(this._cookieGuid, this._cookiePassword).WithCallback(loginResponse =>
		{
			if (loginResponse.Error != null)
			{
				this.Reset();

				Notification.Debug("Failed to reauthenticate: " + loginResponse.Error.Message);
				callback(false);
				return;
			}

			var cookie = loginResponse.Body.Results[0];

			this._cookiePassword = cookie.PasswordGuid;
			SessionStorage.Set(this.STORAGE_SECURE_COOKIE_PASSWORD, cookie.PasswordGuid);
			
			callback(true);
		});
	}

	private CallRecallers(): void
	{
		this._isOnTimeout = true;

		while (this._recallers.length !== 0)
			this._recallers.pop()(true);

		setTimeout(() => (this._isOnTimeout = false), this.TIMEOUT_DURATION);
	}

	private AuthenticatedChanged(authenticationType:string):void
	{
		if (authenticationType == null)
		{
			this.Reset();
			return;
		}
		if (this._cookieGuid != null) return;

		PortalClient.Portal.Client.SecureCookie.Create().WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Debug("Failed to create recovery secure cookie: " + response.Error.Message);
				return;
			}

			var cookie = response.Body.Results[0];

			this._cookieGuid = cookie.Guid;
			this._cookiePassword = cookie.PasswordGuid;
			SessionStorage.Set(this.STORAGE_SECURE_COOKIE_GUID, cookie.Guid);
			SessionStorage.Set(this.STORAGE_SECURE_COOKIE_PASSWORD, cookie.PasswordGuid);
		});
	}
}

export = SessionRecover;