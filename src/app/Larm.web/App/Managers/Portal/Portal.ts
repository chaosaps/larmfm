﻿import PortalClient = require("PortalClient");
import Configuration = require("Managers/Configuration");
import SessionHandler = require("Managers/Portal/SessionHandler");

class Portal
{
	public Client: CHAOS.Portal.Client.IPortalClient;
	public IsReady: KnockoutObservable<boolean>;
	public IsAuthenticated: KnockoutObservable<boolean>;
	public HasCheckedAuthorization: KnockoutObservable<boolean>;
	public StoreLogin: KnockoutObservable<boolean>;

	private _sessionHandler: SessionHandler;

	constructor()
	{
		this.Client = PortalClient.Portal.Client.Initialize(Configuration.PortalPath, null, false);
		this._sessionHandler = new SessionHandler(this.Client, Configuration.AnonymousEmail);
		this.IsReady = this._sessionHandler.HasCheckedAuthorization;
		this.IsAuthenticated = this._sessionHandler.IsAuthenticated;
		this.HasCheckedAuthorization = this._sessionHandler.HasCheckedAuthorization;
		this.StoreLogin = this._sessionHandler.StoreLogin;
	}

	public LogOut(): void
	{
		this._sessionHandler.LogOut();
	}
}

var instance = new Portal();

export = instance;