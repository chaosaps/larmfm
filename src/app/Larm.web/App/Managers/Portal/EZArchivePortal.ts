﻿import PortalClient = require("PortalClient");
import Portal = require("Managers/Portal/Portal");
import User = CHAOS.Portal.Client.User;

export var ServiceCaller = (Portal.Client as any) as PortalClient.Portal.Client.IServiceCaller;

export class Search
{
	public static Get(query: string, filter: string, between: string, tag: string, facets: string, sort: string, pageIndex: number, pageSize: number): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<ISearchResult>>
	{
		return ServiceCaller.CallService("EZSearch/Get", PortalClient.Portal.Client.HttpMethod.Get, { q: query, filter: filter, between, tag: tag, facets: facets, sort: sort, pageIndex: pageIndex, pageSize: pageSize }, true);
	}
}

export class Asset
{
	public static Get(id: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IAsset>>
	{
		return ServiceCaller.CallService("EZAsset/Get", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
	}

	public static Set(fields: { [key: string]: string }, identifier?: string, typeId?: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IAsset>>
	{
		if (!fields) fields = {};
		if (identifier) fields["Identifier"] = identifier;
		if (typeId) fields["TypeId"] = typeId;

		return ServiceCaller.CallService("EZAsset/Set", PortalClient.Portal.Client.HttpMethod.Post, { data: JSON.stringify(fields) }, true);
	}

	public static SetTag(id: string, tags: string[]): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IAsset>>
	{
		return ServiceCaller.CallService("EZAsset/SetTag", PortalClient.Portal.Client.HttpMethod.Post, { id: id, tags: tags.join(",") }, true);
	}

	public static Delete(id: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IAsset>>
	{
		return ServiceCaller.CallService("EZAsset/Delete", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
	}

	public static SetFileAccess(id: string, requireLogin: boolean): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZAsset/SetFileAccess", PortalClient.Portal.Client.HttpMethod.Get, { id: id, requireLogin: requireLogin }, true);
	}
}

export class Annotation
{
	public static Set(assetId: string, definitionId: string, data: IAnnotation, visibility?: IAnnotationVisibility): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IAnnotationSetResult>>
	{
		return ServiceCaller.CallService("EZAnnotation/Set", PortalClient.Portal.Client.HttpMethod.Post, { assetId, definitionId, data: JSON.stringify(data), visibility: visibility ? JSON.stringify(visibility) : undefined}, true);
	}

	public static SetPermission(assetId: string, id: string, permission: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZAnnotation/SetPermission", PortalClient.Portal.Client.HttpMethod.Post, { assetId: assetId, id: id, permission: permission }, true);
	}

	public static Delete(assetId: string, id: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IAsset>>
	{
		return ServiceCaller.CallService("EZAnnotation/Delete", PortalClient.Portal.Client.HttpMethod.Get, { assetId: assetId, id: id }, true);
	}
}

export class Settings
{
	public static Get(): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<ISettings>>
	{
		return ServiceCaller.CallService("EZSettings/Get", PortalClient.Portal.Client.HttpMethod.Get, null, false);
	}
}

export class EZUser
{
	public static Me(): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZUser>>
	{
		return ServiceCaller.CallService("EZUser/Me", PortalClient.Portal.Client.HttpMethod.Get, null, true);
	}

	public static Get(): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZUser>>
	{
		return ServiceCaller.CallService("EZUser/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
	}

	public static Search(query: string, pageSize:number = 10): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZUser>>
	{
		return ServiceCaller.CallService("EZUser/Search", PortalClient.Portal.Client.HttpMethod.Get, { q: query, pageSize: pageSize }, true);
	}

	public static Set(user: IEZUser, password?: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZUser>>
	{
		return ServiceCaller.CallService("EZUser/Set", PortalClient.Portal.Client.HttpMethod.Post, { data: JSON.stringify(user), password: password }, true);
	}

	public static Delete(id: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZUser>>
	{
		return ServiceCaller.CallService("EZUser/Delete", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
	}
}

export class EZFile
{
	public static Upload(assetId: string, type: number, file: File): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZFile/Upload", PortalClient.Portal.Client.HttpMethod.Post, { assetId: assetId, type: type, file: file }, true);
	}

	public static Set(id: string, name:string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZFile/Set", PortalClient.Portal.Client.HttpMethod.Post, { id: id, name: name }, true);
	}

	public static Delete(id: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZFile/Delete", PortalClient.Portal.Client.HttpMethod.Post, { id: id }, true);
	}
}

export class EZDataDefinition
{
	public static Get(): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZDataDefinition>>
	{
		return ServiceCaller.CallService("EZDataDefinition/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
	}
}

export class EZAnnotationDefinition
{
	public static Get(): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZAnnotationDefinition>>
	{
		return ServiceCaller.CallService("EZAnnotationDefinition/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
	}
}

export class EZSearchDefinition
{
	public static Get(): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZSearchDefinition>>
	{
		return ServiceCaller.CallService("EZSearchDefinition/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
	}
}

export class EZTag
{
	public static Get(query?: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZTag>>
	{
		return ServiceCaller.CallService("EZTag/Get", PortalClient.Portal.Client.HttpMethod.Get, { q: query || "" }, true);
	}
}

export class EZFacet
{
	public static Get(query: string, filter: string, filter2: string, tag: string, rangeStart?: Date, rangeEnd?: Date, rangeGap?: string, pageIndex?: number, pageSize?: number): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZFacet>>
	{
		var parameters: any = { q: query || "", filter: filter, filter2: filter2, tag: tag, rangeGap: rangeGap, pageIndex: pageIndex, pageSize: pageSize };

		if (rangeStart) parameters.rangeStart = rangeStart.toISOString();
		if (rangeEnd) parameters.rangeEnd = rangeEnd.toISOString();

		return ServiceCaller.CallService("EZFacet/Get", PortalClient.Portal.Client.HttpMethod.Get, parameters, true);
	}
}

export class EZProject
{
	public static Get(): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZProject>>
	{
		return ServiceCaller.CallService("EZProject/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
	}

	public static Set(project: IEZProject): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZProject>>
	{
		return ServiceCaller.CallService("EZProject/Set", PortalClient.Portal.Client.HttpMethod.Post, { project: JSON.stringify(project) }, true);
	}

	public static AssociateWith(id: string, userId: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZProject/AssociateWith", PortalClient.Portal.Client.HttpMethod.Get, { id: id, userId: userId }, true);
	}

	public static DisassociateWith(id: string, userId: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZProject/DisassociateWith", PortalClient.Portal.Client.HttpMethod.Get, { id: id, userId: userId }, true);
	}

	public static Delete(id: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZProject/Delete", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
	}
}

export class EZLabel
{
	public static Set(projectId: string, label: IEZLabel): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<IEZLabel>>
	{
		return ServiceCaller.CallService("EZLabel/Set", PortalClient.Portal.Client.HttpMethod.Post, { projectId: projectId, label: JSON.stringify(label) }, true);
	}

	public static Delete(id: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZLabel/Delete", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
	}

	public static AssociateWith(id: string, assetId: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZLabel/AssociateWith", PortalClient.Portal.Client.HttpMethod.Get, { id: id, assetId: assetId }, true);
	}

	public static DisassociateWith(id: string, assetId: string): PortalClient.Portal.Client.ICallState<PortalClient.Portal.Client.IPagedPortalResult<any>>
	{
		return ServiceCaller.CallService("EZLabel/DisassociateWith", PortalClient.Portal.Client.HttpMethod.Get, { id: id, assetId: assetId }, true);
	}
}

export class FileType
{
	public static Video: number = 1;
	public static Audio: number = 2;
	public static Image: number = 3;
	public static Other: number = 4;
}

export enum UserPermissions
{
	Administrator = "Administrator",
	Contributor = "Contributor",
	User = "User"
}

export class AnnotationPermission
{
	public static Owner: string = "2147483648";
	public static All: string = "0";
}

export type AnnotationVisibility = "Public" | "Private";

export interface IAnnotationVisibility
{
	Scope: AnnotationVisibility
}

export interface ISearchResult
{
	Identifier: string;
	TypeId: string;
	Fields: { Key: string; Value: string }[];
}

export interface IAsset
{
	Identifier: string;
	TypeId: string;
	DoFilesRequireLogin: boolean;
	Data: IData[];
	Files: IFile[];
	Annotations: IAnnotationGroup[];
	Tags: string[];
}

export interface IAnnotationGroup
{
	DefinitionId: string;
	Name: string;
	Data: IAnnotation[];
}

export interface IData
{
	Name: string;
	Fields: { [key: string]: any };
}

export interface IFile
{
	Identifier: string;
	Type: string;
	Name: string;
	Destinations: IDestination[];
}

export interface IDestination
{
	Type: string;
	Url: string;
}

export interface IAnnotation
{
	Identifier?: string;
	__OwnerId?: string;
	__Owner?: string;
	__Permission?: string;
	__Visibility?: AnnotationVisibility;
	[key: string]: any;
}

export interface IAnnotationSetResult
{
	Identifier: string;
}

export interface ISettings
{
	[key: string]: any;
	DisplayNotificationsLevel: string;
	RequiresLogin: boolean;
}

export interface IEZUser
{
	Identifier?: string;
	Email: string;
	Name: string;
	Preferences: string;
	Permission: UserPermissions;
	IsAdministrator: boolean;
}

export interface IEZDataDefinition
{
	Identifier: string;
	IsEditable: boolean;
	Name: string;
	Type: string;
	TypeId: string;
	Description: string;
	Fields: { [key: string]: IEZDataDefinitionType };
}

export interface IEZAnnotationDefinition
{
	Identifier: string;
	Name: string;
	Type: string;
	TypeId: string;
	Description: string;
	Fields: { [key: string]: IEZDataDefinitionType };
}

export interface IEZDataDefinitionType
{
	DisplayName: string;
	Type: string;
	Options: any;
	IsRequired: boolean;
	Placeholder: string;
}

export interface IEZSearchDefinition
{
	Fields: IEZSearchDefinitionField[];
}

export interface IEZSearchDefinitionField
{
	DisplayName: string;
	IsSortable: boolean;
	Type: string;
}

export interface IEZTag
{
	Key: string;
	Count: number;
}

export interface IEZFacet
{
	Header: string;
	Position: string;
	Fields: IEZFacetField[];
}

export interface IEZFacetField
{
	Value: string;
	Facets: IEZFacetFieldValue[];
}

export interface IEZFacetFieldValue
{
	Key: string;
	Count: number;
}

export interface IEZProject
{
	Identifier?: string;
	Name: string;
	Users?: IEZUser[];
	Labels?: IEZLabel[];
}

export interface IEZLabel
{
	Identifier?: string;
	Name: string;
}