﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Portal = require("Managers/Portal/Portal");
import Notification = require("Managers/Notification");
import Authorization = require("Managers/Authorization");
import DisposableComponent = require("Components/DisposableComponent");

class DataDefinition extends DisposableComponent
{
	public IsReady: KnockoutObservable<boolean> = knockout.observable(false);
	public DataDefinitions: KnockoutObservableArray<EZArchivePortal.IEZDataDefinition> = knockout.observableArray<EZArchivePortal.IEZDataDefinition>();

	constructor()
	{
		super();

		this.Subscribe(Portal.IsReady, () => this.GetDefinitions());
		this.Subscribe(Authorization.IsAuthenticated, () => this.GetDefinitions());

		this.GetDefinitions();
	}

	private GetDefinitions():void
	{
		if (!Portal.IsReady())
			return;

		EZArchivePortal.EZDataDefinition.Get().WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error("Failed to load DataDefinition: " + response.Error.Message);
				return;
			}

			this.DataDefinitions.removeAll();

			if (response.Body.Results.length > 0)
				this.DataDefinitions.push(...response.Body.Results);

			this.IsReady(true);
		});
	}

	public GetByAssetType(type: string): EZArchivePortal.IEZDataDefinition[]
	{
		if (!this.IsReady()) throw new Error("DataDefinition not ready");

		var result = new Array<EZArchivePortal.IEZDataDefinition>();

		for (let definition of this.DataDefinitions())
		{
			if (definition.TypeId == type)
				result.push(definition);
		}

		return result;
	}

	public Get(name:string):EZArchivePortal.IEZDataDefinition
	{
		if (!this.IsReady()) throw new Error("DataDefinition not ready");

		for (let definition of this.DataDefinitions())
		{
			if (definition.Name === name) return definition;
		}

		return null;
	}
}

var instance = new DataDefinition();

export = instance;