﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Portal = require("Managers/Portal/Portal");
import Notification = require("Managers/Notification");
import DisposableAction = require("Utility/DisposableAction");

class AnnotationDefinition
{
	public StartField = "StartTime";
	public EndField = "EndTime";

	public IsReady: KnockoutObservable<boolean> = knockout.observable(false);
	public AnnotationDefinitions: KnockoutObservableArray<EZArchivePortal.IEZAnnotationDefinition> = knockout.observableArray<EZArchivePortal.IEZAnnotationDefinition>();

	constructor()
	{
		new DisposableAction(Portal.IsReady, () => this.Initialize());
	}

	private Initialize():void
	{
		EZArchivePortal.EZAnnotationDefinition.Get().WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error("Failed to load AnnotationDefinition: " + response.Error.Message);
				return;
			}

			if (response.Body.Results.length > 0)
				this.AnnotationDefinitions.push(...response.Body.Results);

			this.IsReady(true);
		});
	}

	public IsTimed(definition:EZArchivePortal.IEZAnnotationDefinition):boolean
	{
		return definition.Fields.hasOwnProperty(this.StartField) && definition.Fields.hasOwnProperty(this.EndField);
	}

	public GetByAssetType(type: string):EZArchivePortal.IEZAnnotationDefinition[]
	{
		if (!this.IsReady()) throw new Error("AnnotationDefinition not ready");

		var result = new Array<EZArchivePortal.IEZAnnotationDefinition>();

		for (let definition of this.AnnotationDefinitions())
		{
			if (definition.TypeId == type)
				result.push(definition);
		}

		return result;
	}

	public GetById(id: string): EZArchivePortal.IEZAnnotationDefinition
	{
		if (!this.IsReady()) throw new Error("AnnotationDefinition not ready");

		for (let definition of this.AnnotationDefinitions())
		{
			if (definition.Identifier === id) return definition;
		}

		throw new Error(`AnnotationDefinition not found, id: ${id}`);
	}

	public Get(name: string): EZArchivePortal.IEZAnnotationDefinition
	{
		if (!this.IsReady()) throw new Error("AnnotationDefinition not ready");

		for (let definition of this.AnnotationDefinitions())
		{
			if (definition.Name === name) return definition;
		}

		throw new Error(`AnnotationDefinition not found: ${name}`);
	}
}

var instance = new AnnotationDefinition();

export = instance;