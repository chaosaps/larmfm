define(["require", "exports", "PortalClient", "Managers/Configuration", "Managers/Portal/SessionHandler"], function (require, exports, PortalClient, Configuration, SessionHandler) {
    "use strict";
    var Portal = (function () {
        function Portal() {
            this.Client = PortalClient.Portal.Client.Initialize(Configuration.PortalPath, null, false);
            this._sessionHandler = new SessionHandler(this.Client, Configuration.AnonymousEmail);
            this.IsReady = this._sessionHandler.HasCheckedAuthorization;
            this.IsAuthenticated = this._sessionHandler.IsAuthenticated;
            this.HasCheckedAuthorization = this._sessionHandler.HasCheckedAuthorization;
            this.StoreLogin = this._sessionHandler.StoreLogin;
        }
        Portal.prototype.LogOut = function () {
            this._sessionHandler.LogOut();
        };
        return Portal;
    }());
    var instance = new Portal();
    return instance;
});
