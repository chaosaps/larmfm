var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Portal/EZArchivePortal", "Managers/Portal/Portal", "Managers/Notification", "Managers/Authorization", "Components/DisposableComponent"], function (require, exports, knockout, EZArchivePortal, Portal, Notification, Authorization, DisposableComponent) {
    "use strict";
    var DataDefinition = (function (_super) {
        __extends(DataDefinition, _super);
        function DataDefinition() {
            var _this = _super.call(this) || this;
            _this.IsReady = knockout.observable(false);
            _this.DataDefinitions = knockout.observableArray();
            _this.Subscribe(Portal.IsReady, function () { return _this.GetDefinitions(); });
            _this.Subscribe(Authorization.IsAuthenticated, function () { return _this.GetDefinitions(); });
            _this.GetDefinitions();
            return _this;
        }
        DataDefinition.prototype.GetDefinitions = function () {
            var _this = this;
            if (!Portal.IsReady())
                return;
            EZArchivePortal.EZDataDefinition.Get().WithCallback(function (response) {
                var _a;
                if (response.Error != null) {
                    Notification.Error("Failed to load DataDefinition: " + response.Error.Message);
                    return;
                }
                _this.DataDefinitions.removeAll();
                if (response.Body.Results.length > 0)
                    (_a = _this.DataDefinitions).push.apply(_a, response.Body.Results);
                _this.IsReady(true);
            });
        };
        DataDefinition.prototype.GetByAssetType = function (type) {
            if (!this.IsReady())
                throw new Error("DataDefinition not ready");
            var result = new Array();
            for (var _i = 0, _a = this.DataDefinitions(); _i < _a.length; _i++) {
                var definition = _a[_i];
                if (definition.TypeId == type)
                    result.push(definition);
            }
            return result;
        };
        DataDefinition.prototype.Get = function (name) {
            if (!this.IsReady())
                throw new Error("DataDefinition not ready");
            for (var _i = 0, _a = this.DataDefinitions(); _i < _a.length; _i++) {
                var definition = _a[_i];
                if (definition.Name === name)
                    return definition;
            }
            return null;
        };
        return DataDefinition;
    }(DisposableComponent));
    var instance = new DataDefinition();
    return instance;
});
