define(["require", "exports", "PortalClient", "Managers/Portal/Portal"], function (require, exports, PortalClient, Portal) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.ServiceCaller = Portal.Client;
    var Search = (function () {
        function Search() {
        }
        Search.Get = function (query, filter, between, tag, facets, sort, pageIndex, pageSize) {
            return exports.ServiceCaller.CallService("EZSearch/Get", PortalClient.Portal.Client.HttpMethod.Get, { q: query, filter: filter, between: between, tag: tag, facets: facets, sort: sort, pageIndex: pageIndex, pageSize: pageSize }, true);
        };
        return Search;
    }());
    exports.Search = Search;
    var Asset = (function () {
        function Asset() {
        }
        Asset.Get = function (id) {
            return exports.ServiceCaller.CallService("EZAsset/Get", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
        };
        Asset.Set = function (fields, identifier, typeId) {
            if (!fields)
                fields = {};
            if (identifier)
                fields["Identifier"] = identifier;
            if (typeId)
                fields["TypeId"] = typeId;
            return exports.ServiceCaller.CallService("EZAsset/Set", PortalClient.Portal.Client.HttpMethod.Post, { data: JSON.stringify(fields) }, true);
        };
        Asset.SetTag = function (id, tags) {
            return exports.ServiceCaller.CallService("EZAsset/SetTag", PortalClient.Portal.Client.HttpMethod.Post, { id: id, tags: tags.join(",") }, true);
        };
        Asset.Delete = function (id) {
            return exports.ServiceCaller.CallService("EZAsset/Delete", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
        };
        Asset.SetFileAccess = function (id, requireLogin) {
            return exports.ServiceCaller.CallService("EZAsset/SetFileAccess", PortalClient.Portal.Client.HttpMethod.Get, { id: id, requireLogin: requireLogin }, true);
        };
        return Asset;
    }());
    exports.Asset = Asset;
    var Annotation = (function () {
        function Annotation() {
        }
        Annotation.Set = function (assetId, definitionId, data, visibility) {
            return exports.ServiceCaller.CallService("EZAnnotation/Set", PortalClient.Portal.Client.HttpMethod.Post, { assetId: assetId, definitionId: definitionId, data: JSON.stringify(data), visibility: visibility ? JSON.stringify(visibility) : undefined }, true);
        };
        Annotation.SetPermission = function (assetId, id, permission) {
            return exports.ServiceCaller.CallService("EZAnnotation/SetPermission", PortalClient.Portal.Client.HttpMethod.Post, { assetId: assetId, id: id, permission: permission }, true);
        };
        Annotation.Delete = function (assetId, id) {
            return exports.ServiceCaller.CallService("EZAnnotation/Delete", PortalClient.Portal.Client.HttpMethod.Get, { assetId: assetId, id: id }, true);
        };
        return Annotation;
    }());
    exports.Annotation = Annotation;
    var Settings = (function () {
        function Settings() {
        }
        Settings.Get = function () {
            return exports.ServiceCaller.CallService("EZSettings/Get", PortalClient.Portal.Client.HttpMethod.Get, null, false);
        };
        return Settings;
    }());
    exports.Settings = Settings;
    var EZUser = (function () {
        function EZUser() {
        }
        EZUser.Me = function () {
            return exports.ServiceCaller.CallService("EZUser/Me", PortalClient.Portal.Client.HttpMethod.Get, null, true);
        };
        EZUser.Get = function () {
            return exports.ServiceCaller.CallService("EZUser/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
        };
        EZUser.Search = function (query, pageSize) {
            if (pageSize === void 0) { pageSize = 10; }
            return exports.ServiceCaller.CallService("EZUser/Search", PortalClient.Portal.Client.HttpMethod.Get, { q: query, pageSize: pageSize }, true);
        };
        EZUser.Set = function (user, password) {
            return exports.ServiceCaller.CallService("EZUser/Set", PortalClient.Portal.Client.HttpMethod.Post, { data: JSON.stringify(user), password: password }, true);
        };
        EZUser.Delete = function (id) {
            return exports.ServiceCaller.CallService("EZUser/Delete", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
        };
        return EZUser;
    }());
    exports.EZUser = EZUser;
    var EZFile = (function () {
        function EZFile() {
        }
        EZFile.Upload = function (assetId, type, file) {
            return exports.ServiceCaller.CallService("EZFile/Upload", PortalClient.Portal.Client.HttpMethod.Post, { assetId: assetId, type: type, file: file }, true);
        };
        EZFile.Set = function (id, name) {
            return exports.ServiceCaller.CallService("EZFile/Set", PortalClient.Portal.Client.HttpMethod.Post, { id: id, name: name }, true);
        };
        EZFile.Delete = function (id) {
            return exports.ServiceCaller.CallService("EZFile/Delete", PortalClient.Portal.Client.HttpMethod.Post, { id: id }, true);
        };
        return EZFile;
    }());
    exports.EZFile = EZFile;
    var EZDataDefinition = (function () {
        function EZDataDefinition() {
        }
        EZDataDefinition.Get = function () {
            return exports.ServiceCaller.CallService("EZDataDefinition/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
        };
        return EZDataDefinition;
    }());
    exports.EZDataDefinition = EZDataDefinition;
    var EZAnnotationDefinition = (function () {
        function EZAnnotationDefinition() {
        }
        EZAnnotationDefinition.Get = function () {
            return exports.ServiceCaller.CallService("EZAnnotationDefinition/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
        };
        return EZAnnotationDefinition;
    }());
    exports.EZAnnotationDefinition = EZAnnotationDefinition;
    var EZSearchDefinition = (function () {
        function EZSearchDefinition() {
        }
        EZSearchDefinition.Get = function () {
            return exports.ServiceCaller.CallService("EZSearchDefinition/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
        };
        return EZSearchDefinition;
    }());
    exports.EZSearchDefinition = EZSearchDefinition;
    var EZTag = (function () {
        function EZTag() {
        }
        EZTag.Get = function (query) {
            return exports.ServiceCaller.CallService("EZTag/Get", PortalClient.Portal.Client.HttpMethod.Get, { q: query || "" }, true);
        };
        return EZTag;
    }());
    exports.EZTag = EZTag;
    var EZFacet = (function () {
        function EZFacet() {
        }
        EZFacet.Get = function (query, filter, filter2, tag, rangeStart, rangeEnd, rangeGap, pageIndex, pageSize) {
            var parameters = { q: query || "", filter: filter, filter2: filter2, tag: tag, rangeGap: rangeGap, pageIndex: pageIndex, pageSize: pageSize };
            if (rangeStart)
                parameters.rangeStart = rangeStart.toISOString();
            if (rangeEnd)
                parameters.rangeEnd = rangeEnd.toISOString();
            return exports.ServiceCaller.CallService("EZFacet/Get", PortalClient.Portal.Client.HttpMethod.Get, parameters, true);
        };
        return EZFacet;
    }());
    exports.EZFacet = EZFacet;
    var EZProject = (function () {
        function EZProject() {
        }
        EZProject.Get = function () {
            return exports.ServiceCaller.CallService("EZProject/Get", PortalClient.Portal.Client.HttpMethod.Get, null, true);
        };
        EZProject.Set = function (project) {
            return exports.ServiceCaller.CallService("EZProject/Set", PortalClient.Portal.Client.HttpMethod.Post, { project: JSON.stringify(project) }, true);
        };
        EZProject.AssociateWith = function (id, userId) {
            return exports.ServiceCaller.CallService("EZProject/AssociateWith", PortalClient.Portal.Client.HttpMethod.Get, { id: id, userId: userId }, true);
        };
        EZProject.DisassociateWith = function (id, userId) {
            return exports.ServiceCaller.CallService("EZProject/DisassociateWith", PortalClient.Portal.Client.HttpMethod.Get, { id: id, userId: userId }, true);
        };
        EZProject.Delete = function (id) {
            return exports.ServiceCaller.CallService("EZProject/Delete", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
        };
        return EZProject;
    }());
    exports.EZProject = EZProject;
    var EZLabel = (function () {
        function EZLabel() {
        }
        EZLabel.Set = function (projectId, label) {
            return exports.ServiceCaller.CallService("EZLabel/Set", PortalClient.Portal.Client.HttpMethod.Post, { projectId: projectId, label: JSON.stringify(label) }, true);
        };
        EZLabel.Delete = function (id) {
            return exports.ServiceCaller.CallService("EZLabel/Delete", PortalClient.Portal.Client.HttpMethod.Get, { id: id }, true);
        };
        EZLabel.AssociateWith = function (id, assetId) {
            return exports.ServiceCaller.CallService("EZLabel/AssociateWith", PortalClient.Portal.Client.HttpMethod.Get, { id: id, assetId: assetId }, true);
        };
        EZLabel.DisassociateWith = function (id, assetId) {
            return exports.ServiceCaller.CallService("EZLabel/DisassociateWith", PortalClient.Portal.Client.HttpMethod.Get, { id: id, assetId: assetId }, true);
        };
        return EZLabel;
    }());
    exports.EZLabel = EZLabel;
    var FileType = (function () {
        function FileType() {
        }
        FileType.Video = 1;
        FileType.Audio = 2;
        FileType.Image = 3;
        FileType.Other = 4;
        return FileType;
    }());
    exports.FileType = FileType;
    var UserPermissions;
    (function (UserPermissions) {
        UserPermissions["Administrator"] = "Administrator";
        UserPermissions["Contributor"] = "Contributor";
        UserPermissions["User"] = "User";
    })(UserPermissions = exports.UserPermissions || (exports.UserPermissions = {}));
    var AnnotationPermission = (function () {
        function AnnotationPermission() {
        }
        AnnotationPermission.Owner = "2147483648";
        AnnotationPermission.All = "0";
        return AnnotationPermission;
    }());
    exports.AnnotationPermission = AnnotationPermission;
});
