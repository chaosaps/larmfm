define(["require", "exports", "knockout", "PortalClient", "Managers/Notification", "Managers/Storage", "Managers/Portal/SessionRecover"], function (require, exports, knockout, PortalClient, Notification, Storage, SessionRecover) {
    "use strict";
    var SessionHandler = (function () {
        function SessionHandler(client, anonymousEmail) {
            var _this = this;
            this.HasCheckedAuthorization = knockout.observable(false);
            this.IsAuthenticated = knockout.observable(false);
            this.StoreLogin = knockout.observable(false);
            this.AUTHENTICATION_TYPE_REUSED = "ReusedSession";
            this.AUTHENTICATION_TYPE_CROSS_WINDOW = "CrossWindow";
            this.STORAGE_IS_AUTHENTICATED = "IsAuthenticated";
            this.STORAGE_PORTAL_SESSION = "PortalSession";
            this.STORAGE_SECURE_COOKIE_GUID = "SecureCookieGuid";
            this.STORAGE_SECURE_COOKIE_PASSWORD = "SecureCookiePasswordGuid";
            this._client = client;
            this._serviceCaller = client;
            this._anonymousEmail = anonymousEmail;
            this.StoreLogin.subscribe(function (v) { return _this.StoreLoginChanged(v); });
            this._client.SessionAuthenticated().Add(function (v) { return _this.AuthenticatedChanged(v); });
            this._sessionRecover = new SessionRecover(this._client);
            this._sessionRecover.IsEnabled = false;
            Storage.Listen(this.STORAGE_IS_AUTHENTICATED, function (v) { return _this.CrossWindowAuthenticationChanged(v); });
            Storage.Listen(this.STORAGE_PORTAL_SESSION, function (v) { return _this.CrossWindowPortalSessionChanged(v); });
            this.ReuseSession();
        }
        SessionHandler.prototype.LogOut = function () {
            var _this = this;
            this.HasCheckedAuthorization(false);
            this.RemoveSecureCookie();
            this.RemoveStoredSession();
            this._sessionRecover.Reset();
            this._serviceCaller.UpdateSession(null);
            this.IsAuthenticated(false);
            Storage.SetAndRemove(this.STORAGE_IS_AUTHENTICATED, "false");
            this.CreateNewSession(function () { return _this.HasCheckedAuthorization(true); });
        };
        SessionHandler.prototype.CrossWindowAuthenticationChanged = function (value) {
            if (value === "true")
                this._serviceCaller.SetSessionAuthenticated(this.AUTHENTICATION_TYPE_CROSS_WINDOW);
            else if (value === "false") {
                this.HasCheckedAuthorization(false);
                this.IsAuthenticated(false);
                this._serviceCaller.UpdateSession(null);
            }
        };
        SessionHandler.prototype.CrossWindowPortalSessionChanged = function (guid) {
            if (guid == null)
                return;
            Notification.Debug("Got session from another window");
            this._serviceCaller.UpdateSession({ Guid: guid, UserGuid: null, DateCreated: null, DateModified: null });
            this.HasCheckedAuthorization(true);
        };
        SessionHandler.prototype.StoreLoginChanged = function (shouldRemember) {
            if (shouldRemember) {
                if (this._client.IsAuthenticated())
                    this.CreateSecureCookie();
            }
            else
                this.RemoveSecureCookie();
        };
        SessionHandler.prototype.AuthenticatedChanged = function (authenticationType) {
            Notification.Debug("AuthenticatedChanged: " + authenticationType);
            this.IsAuthenticated(authenticationType != null);
            if (authenticationType === this.AUTHENTICATION_TYPE_REUSED || authenticationType === this.AUTHENTICATION_TYPE_CROSS_WINDOW)
                return;
            if (this.StoreLogin())
                this.CreateSecureCookie();
            Storage.SetAndRemove(this.STORAGE_IS_AUTHENTICATED, "true");
        };
        SessionHandler.prototype.ReuseSession = function () {
            var _this = this;
            if (Storage.Get(this.STORAGE_PORTAL_SESSION) != null) {
                Notification.Debug("Reusing session");
                this._sessionRecover.IsEnabled = false;
                this._serviceCaller.UpdateSession({ Guid: Storage.Get(this.STORAGE_PORTAL_SESSION), UserGuid: null, DateCreated: null, DateModified: null });
                PortalClient.Portal.Client.User.Get(null, null, this._serviceCaller).WithCallback(function (response) {
                    _this._sessionRecover.IsEnabled = true;
                    if (response.Error != null) {
                        Notification.Debug("Failed to reuse session");
                        Storage.Remove(_this.STORAGE_PORTAL_SESSION);
                        if (response.Error.Fullname !== "Chaos.Portal.Core.Exceptions.SessionDoesNotExistException" && response.Error.Fullname !== "Chaos.Portal.Core.Exceptions.InsufficientPermissionsException")
                            Notification.Debug("Portal error reusing session: " + response.Error.Message);
                        _this.CreateNewSession(function () {
                            _this.SessionRecover(function (success) {
                                if (success)
                                    _this.HasCheckedAuthorization(true);
                                else
                                    _this.SecureCookieLogin();
                            });
                        });
                    }
                    else if (response.Body.Results[0].Email === _this._anonymousEmail) {
                        Notification.Debug("Reused session is not authenticated, trying to recover authentication");
                        _this.SessionRecover(function (success) {
                            if (success)
                                _this.HasCheckedAuthorization(true);
                            else {
                                Notification.Debug("Failed to recover authentication");
                                _this.SecureCookieLogin();
                            }
                        });
                    }
                    else {
                        _this._serviceCaller.SetSessionAuthenticated(_this.AUTHENTICATION_TYPE_REUSED);
                        _this.HasCheckedAuthorization(true);
                    }
                });
            }
            else
                this.CreateNewSession(function () { return _this.SecureCookieLogin(); });
        };
        SessionHandler.prototype.CreateNewSession = function (nextStep) {
            var _this = this;
            if (nextStep === void 0) { nextStep = null; }
            PortalClient.Portal.Client.Session.Create(this._serviceCaller).WithCallback(function (response) {
                if (response.Error != null)
                    Notification.Error("Portal error creating session: " + response.Error.Message);
                else {
                    Storage.Set(_this.STORAGE_PORTAL_SESSION, _this._client.GetCurrentSession().Guid);
                    if (nextStep != null)
                        nextStep();
                }
            });
        };
        SessionHandler.prototype.SessionRecover = function (nextStep) {
            if (nextStep === void 0) { nextStep = null; }
            if (this._sessionRecover.CanRecover)
                this._sessionRecover.Recover(nextStep);
            else
                nextStep(false);
        };
        SessionHandler.prototype.SecureCookieLogin = function () {
            var _this = this;
            if (Storage.Get(this.STORAGE_SECURE_COOKIE_GUID) != null && Storage.Get(this.STORAGE_SECURE_COOKIE_PASSWORD) != null) {
                Notification.Debug("Trying saved login");
                PortalClient.Portal.Client.SecureCookie.Login(Storage.Get(this.STORAGE_SECURE_COOKIE_GUID), Storage.Get(this.STORAGE_SECURE_COOKIE_PASSWORD), this._serviceCaller).WithCallback(function (response) {
                    if (response.Error != null)
                        Notification.Warning("Portal error loggining in with cookie: " + response.Error.Message);
                    else
                        Storage.Set(_this.STORAGE_SECURE_COOKIE_PASSWORD, response.Body.Results[0].PasswordGuid);
                    _this.HasCheckedAuthorization(true);
                });
            }
            else
                this.HasCheckedAuthorization(true);
        };
        SessionHandler.prototype.CreateSecureCookie = function () {
            var _this = this;
            if (!Storage.CanUseStorage)
                return;
            PortalClient.Portal.Client.SecureCookie.Create(this._serviceCaller).WithCallback(function (response) {
                if (response.Error != null)
                    Notification.Warning("Portal error creating secure cookie: " + response.Error.Message);
                else {
                    Storage.Set(_this.STORAGE_SECURE_COOKIE_GUID, response.Body.Results[0].Guid);
                    Storage.Set(_this.STORAGE_SECURE_COOKIE_PASSWORD, response.Body.Results[0].PasswordGuid);
                }
            });
        };
        SessionHandler.prototype.RemoveSecureCookie = function () {
            Storage.Remove(this.STORAGE_SECURE_COOKIE_GUID);
            Storage.Remove(this.STORAGE_SECURE_COOKIE_PASSWORD);
        };
        SessionHandler.prototype.RemoveStoredSession = function () {
            Storage.Remove(this.STORAGE_PORTAL_SESSION);
        };
        return SessionHandler;
    }());
    return SessionHandler;
});
