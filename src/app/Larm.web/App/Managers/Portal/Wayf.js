define(["require", "exports", "PortalClient", "Managers/Notification", "Managers/Configuration"], function (require, exports, PortalClient, Notification, Configuration) {
    "use strict";
    var Wayf = (function () {
        function Wayf() {
        }
        Wayf.prototype.GetParameterByName = function (name, query) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(query);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };
        Wayf.prototype.Login = function (callback) {
            var _this = this;
            var callbackUrl = window.location.protocol + "//" + window.location.host + "/WayfCallback.html";
            var wayfInfo = PortalClient.Portal.Client.Wayf.LogIn(Configuration.WayfPath, callbackUrl);
            var listener = function (event) {
                if (event.originalEvent.key === "WayfStatus") {
                    $(window).off("storage", listener);
                    var status = parseInt(_this.GetParameterByName("status", event.originalEvent.newValue));
                    var message = _this.GetParameterByName("message", event.originalEvent.newValue);
                    wayfInfo.Callback(status);
                    switch (status) {
                        case 0:
                            callback(true, false);
                            break;
                        case 1:
                        case 3:
                            Notification.Error("Wayf login failed: " + message);
                            callback(false, false);
                            break;
                        case 2:
                            callback(false, true);
                            break;
                    }
                }
            };
            $(window).on("storage", listener);
            window.open(wayfInfo.Path, "WayfLogin", "width=800,height=800,menubar=no,status=no,toolbar=no,resizable=yes");
        };
        return Wayf;
    }());
    var instance = new Wayf();
    return instance;
});
