﻿import knockout = require("knockout");
import PortalClient = require("PortalClient");
import Notification = require("Managers/Notification");
import Storage = require("Managers/Storage");
import SessionRecover = require("Managers/Portal/SessionRecover");

class SessionHandler
{
	public HasCheckedAuthorization: KnockoutObservable<boolean> = knockout.observable(false);
	public IsAuthenticated: KnockoutObservable<boolean> = knockout.observable<boolean>(false);
	public StoreLogin:KnockoutObservable<boolean> = knockout.observable(false);

	private _client:CHAOS.Portal.Client.IPortalClient;
	private _serviceCaller: CHAOS.Portal.Client.IServiceCaller;
	private _sessionRecover: SessionRecover;
	private _anonymousEmail: string;

	private AUTHENTICATION_TYPE_REUSED: string = "ReusedSession";
	private AUTHENTICATION_TYPE_CROSS_WINDOW: string = "CrossWindow";

	private STORAGE_IS_AUTHENTICATED:string = "IsAuthenticated";
	private STORAGE_PORTAL_SESSION:string = "PortalSession";
	private STORAGE_SECURE_COOKIE_GUID: string = "SecureCookieGuid";
	private STORAGE_SECURE_COOKIE_PASSWORD: string = "SecureCookiePasswordGuid";

	constructor(client: CHAOS.Portal.Client.IPortalClient, anonymousEmail:string)
	{
		this._client = client;
		this._serviceCaller = <CHAOS.Portal.Client.IServiceCaller><any>client;
		this._anonymousEmail = anonymousEmail;

		this.StoreLogin.subscribe(v => this.StoreLoginChanged(v));
		this._client.SessionAuthenticated().Add(v => this.AuthenticatedChanged(v));
		this._sessionRecover = new SessionRecover(this._client);
		this._sessionRecover.IsEnabled = false;
		Storage.Listen(this.STORAGE_IS_AUTHENTICATED, v => this.CrossWindowAuthenticationChanged(v));
		Storage.Listen(this.STORAGE_PORTAL_SESSION, v => this.CrossWindowPortalSessionChanged(v));

		this.ReuseSession();
	}

	public LogOut():void
	{
		this.HasCheckedAuthorization(false);
		this.RemoveSecureCookie();
		this.RemoveStoredSession();
		this._sessionRecover.Reset();
		this._serviceCaller.UpdateSession(null);
		this.IsAuthenticated(false);
		Storage.SetAndRemove(this.STORAGE_IS_AUTHENTICATED, "false");
		this.CreateNewSession(() => this.HasCheckedAuthorization(true));
	}

	private CrossWindowAuthenticationChanged(value: string): void
	{
		if (value === "true")
			this._serviceCaller.SetSessionAuthenticated(this.AUTHENTICATION_TYPE_CROSS_WINDOW);
		else if(value === "false")
		{
			this.HasCheckedAuthorization(false);
			this.IsAuthenticated(false);
			this._serviceCaller.UpdateSession(null);
		}
	}

	private CrossWindowPortalSessionChanged(guid: string): void
	{
		if (guid == null) return;

		Notification.Debug("Got session from another window");
		
		this._serviceCaller.UpdateSession({ Guid: guid, UserGuid: null, DateCreated: null, DateModified: null });

		this.HasCheckedAuthorization(true);
	}

	private StoreLoginChanged(shouldRemember:boolean):void
	{
		if (shouldRemember)
		{
			if (this._client.IsAuthenticated())
				this.CreateSecureCookie();
		}
		else
			this.RemoveSecureCookie();
	}

	private AuthenticatedChanged(authenticationType:string):void
	{
		Notification.Debug(`AuthenticatedChanged: ${authenticationType}`);

		this.IsAuthenticated(authenticationType != null);

		if (authenticationType === this.AUTHENTICATION_TYPE_REUSED || authenticationType === this.AUTHENTICATION_TYPE_CROSS_WINDOW) return;
		if (this.StoreLogin()) this.CreateSecureCookie();
		
		Storage.SetAndRemove(this.STORAGE_IS_AUTHENTICATED, "true");
	}

	private ReuseSession():void
	{
		if (Storage.Get(this.STORAGE_PORTAL_SESSION) != null)
		{
			Notification.Debug("Reusing session");
			this._sessionRecover.IsEnabled = false;
			this._serviceCaller.UpdateSession({ Guid: Storage.Get(this.STORAGE_PORTAL_SESSION), UserGuid: null, DateCreated: null, DateModified: null });
			PortalClient.Portal.Client.User.Get(null, null, this._serviceCaller).WithCallback(response =>
			{
				this._sessionRecover.IsEnabled = true;
				if (response.Error != null)
				{
					Notification.Debug("Failed to reuse session");
					Storage.Remove(this.STORAGE_PORTAL_SESSION);

					if (response.Error.Fullname !== "Chaos.Portal.Core.Exceptions.SessionDoesNotExistException" && response.Error.Fullname !== "Chaos.Portal.Core.Exceptions.InsufficientPermissionsException")
						Notification.Debug("Portal error reusing session: " + response.Error.Message);

					this.CreateNewSession(() =>
					{
						this.SessionRecover(success =>
						{
							if (success)
								this.HasCheckedAuthorization(true);
							else
								this.SecureCookieLogin();
						});
					});
				}
				else if (response.Body.Results[0].Email === this._anonymousEmail)
				{
					Notification.Debug("Reused session is not authenticated, trying to recover authentication");
					this.SessionRecover(success =>
					{
						if (success)
							this.HasCheckedAuthorization(true);
						else
						{
							Notification.Debug("Failed to recover authentication");
							this.SecureCookieLogin();
						}
					});
				}
				else
				{
					this._serviceCaller.SetSessionAuthenticated(this.AUTHENTICATION_TYPE_REUSED);
					this.HasCheckedAuthorization(true);
				}
			});
		} else
			this.CreateNewSession(() => this.SecureCookieLogin());
	}

	private CreateNewSession(nextStep: () => void = null): void
	{
		PortalClient.Portal.Client.Session.Create(this._serviceCaller).WithCallback(response =>
		{
			if (response.Error != null)
				Notification.Error("Portal error creating session: " + response.Error.Message);
			else
			{
				Storage.Set(this.STORAGE_PORTAL_SESSION, this._client.GetCurrentSession().Guid);
				if(nextStep != null) nextStep();
			}
		});
	}

	private SessionRecover(nextStep:(success:boolean) => void = null):void
	{
		if (this._sessionRecover.CanRecover)
			this._sessionRecover.Recover(nextStep);
		else
			nextStep(false);
	}

	private SecureCookieLogin(): void
	{
		if (Storage.Get(this.STORAGE_SECURE_COOKIE_GUID) != null && Storage.Get(this.STORAGE_SECURE_COOKIE_PASSWORD) != null)
		{
			Notification.Debug("Trying saved login");

			PortalClient.Portal.Client.SecureCookie.Login(Storage.Get(this.STORAGE_SECURE_COOKIE_GUID), Storage.Get(this.STORAGE_SECURE_COOKIE_PASSWORD), this._serviceCaller).WithCallback(response =>
			{
				if (response.Error != null)
					Notification.Warning("Portal error loggining in with cookie: " + response.Error.Message);
				else
					Storage.Set(this.STORAGE_SECURE_COOKIE_PASSWORD, response.Body.Results[0].PasswordGuid);

				this.HasCheckedAuthorization(true);
			});
		} else
			this.HasCheckedAuthorization(true);
	}

	private CreateSecureCookie(): void
	{
		if (!Storage.CanUseStorage) return;

		PortalClient.Portal.Client.SecureCookie.Create(this._serviceCaller).WithCallback(response =>
		{
			if (response.Error != null)
				Notification.Warning("Portal error creating secure cookie: " + response.Error.Message);
			else
			{
				Storage.Set(this.STORAGE_SECURE_COOKIE_GUID, response.Body.Results[0].Guid);
				Storage.Set(this.STORAGE_SECURE_COOKIE_PASSWORD, response.Body.Results[0].PasswordGuid);
			}
		});
	}

	private RemoveSecureCookie():void
	{
		Storage.Remove(this.STORAGE_SECURE_COOKIE_GUID);
		Storage.Remove(this.STORAGE_SECURE_COOKIE_PASSWORD);
	}

	private RemoveStoredSession():void
	{
		Storage.Remove(this.STORAGE_PORTAL_SESSION);
	}
}

export = SessionHandler;