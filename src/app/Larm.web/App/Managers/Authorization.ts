﻿import knockout = require("knockout");
import Portal = require("Managers/Portal/Portal");
import Notification = require("Managers/Notification");
import Wayf = require("Managers/Portal/Wayf");
import PortalClient = require("PortalClient");
import Tracking = require("Managers/Tracking");
import DialogManager = require("Managers/Dialog/Dialog");

class Authorization
{
	public IsAuthenticated:KnockoutObservable<boolean>;
	public IsLoggingIn:KnockoutObservable<boolean> = knockout.observable(false);
	public CanLogin:KnockoutComputed<boolean>;
	public CanLogOut:KnockoutComputed<boolean>;
	
	constructor()
	{
		this.IsAuthenticated = Portal.IsAuthenticated;
		this.CanLogin = knockout.computed(() => Portal.IsReady() && !this.IsAuthenticated());
		this.CanLogOut = knockout.computed(() => Portal.IsReady() && this.IsAuthenticated());
	}

	public Login(email:string, password:string, storeLogin:boolean, callback:(success:boolean)=>void):void
	{
		if (!this.CanLogin()) return;
		this.IsLoggingIn(true);

		PortalClient.Portal.Client.EmailPassword.Login(email, password).WithCallback(response =>
		{
			this.IsLoggingIn(false);

			if (response.Error != null)
			{
				if (response.Error.Fullname === "Chaos.Portal.Authentication.Exception.LoginException")
					callback(false);
				else
					Notification.Error("Failed to login: " + response.Error.Message);
			} else
			{
				Portal.StoreLogin(storeLogin);
				callback(true);
			}
		});
	}

	public WayfLogin():void
	{
		this.IsLoggingIn(true);
		Wayf.Login((success, notAproved) =>
		{
			this.IsLoggingIn(false);

			if (!success && notAproved)
			{
				DialogManager.Show("Profile/NotApprovedDialog", {});
				Tracking.TrackEvent("Login", "Wayf", "User not approved");
			}
		});
	}

	public LogOut():void
	{
		Portal.LogOut();
	}
}

var instance = new Authorization();

export = instance;