﻿import Configuration = require("Managers/Configuration");

class Tracking
{
	private _tracker: AnalyticsTracker | null = null;
	private _trackingBackLog: Array<()=>void> = [];

	public get IsTracking(): boolean
	{
		return this._tracker !== null;
	}

	public StartTracking(): void
	{
		if (this.IsTracking)
			return;
        try {
            require(["analytics"], (a: AnalyticsStatic) => {
                a("set", "anonymizeIp", true);
                this._tracker = a.create(Configuration.TrackingId);
                this._trackingBackLog.forEach(call => call());
                this._trackingBackLog.splice(0, this._trackingBackLog.length);
            });
		} catch (e) {console.log("Tracking failed", e)} 
	}

	public TrackPage(address: string): void
	{
		this.Track(() => this._tracker.send("pageview", address));
	}

	public TrackEvent(category: string, action: string, label: string): void
	{
		this.Track(() => this._tracker.send("event", category, action, label));
	}

	private Track(trackCall: () => void): void
	{
		if (this.IsTracking)
		{
			try
			{
				trackCall();
			} catch (e) {} 
		}
		else
			this._trackingBackLog.push(trackCall);
	}
}

var instance = new Tracking();

export = instance;