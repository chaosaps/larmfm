﻿class DialogState<T>
{
	private readonly _name:string;
	private readonly _data: T;
	private readonly _closeCallback:(state:DialogState<T>) => JQueryPromise<void>;
	
	constructor(name: string, data: T, closeCallback: (state: DialogState<T>) => JQueryPromise<void>)
	{
		this._name = name;
		this._data = data;
		this._closeCallback = closeCallback;
	}

	public get Name(): string
	{
		return this._name;
	}

	public get Data(): T
	{
		 return this._data;
	}

	public Close(): JQueryPromise<void>
	{
		return this._closeCallback(this);
	}
}

export = DialogState;