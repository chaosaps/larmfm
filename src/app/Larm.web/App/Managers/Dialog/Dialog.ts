﻿import jquery = require("jquery");
import knockout = require("knockout");
import DialogState = require("Managers/Dialog/DialogState");

class Dialog
{
	public Current: KnockoutObservable<DialogState<any>> = knockout.observable<any>(null);
	public IsWide = knockout.observable(false);

	public Show<T>(name: string, data: T, canClose:boolean = true, isWide:boolean = false): DialogState<T>
	{
		this.IsWide(isWide);
		var dialogState = new DialogState<T>(name, data, s => this.Close(s));

		this.Current(<any>dialogState);

		jquery("body > .modal").modal({ show: true, backdrop: (!canClose ? "static" : true), keyboard: canClose });
		jquery("body > .modal").on("hidden.bs.modal", () => this.Current(null));

		return dialogState;
	}

	private Close<T>(dialogState: DialogState<T>): JQueryPromise<void>
	{
		const deferred = jquery.Deferred<void>();
		if (this.Current() === dialogState) {
			const $dialog = jquery("body > .modal");
			const handle = () =>
			{
				$dialog.off("hidden.bs.modal", handle);
				deferred.resolve();
			};

			$dialog.on("hidden.bs.modal", handle);
			$dialog.modal("hide");
		} else deferred.resolve();

		return deferred.promise();
	}
}

var instance = new Dialog();

export = instance;