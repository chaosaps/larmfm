define(["require", "exports", "jquery", "knockout", "Managers/Dialog/DialogState"], function (require, exports, jquery, knockout, DialogState) {
    "use strict";
    var Dialog = (function () {
        function Dialog() {
            this.Current = knockout.observable(null);
            this.IsWide = knockout.observable(false);
        }
        Dialog.prototype.Show = function (name, data, canClose, isWide) {
            var _this = this;
            if (canClose === void 0) { canClose = true; }
            if (isWide === void 0) { isWide = false; }
            this.IsWide(isWide);
            var dialogState = new DialogState(name, data, function (s) { return _this.Close(s); });
            this.Current(dialogState);
            jquery("body > .modal").modal({ show: true, backdrop: (!canClose ? "static" : true), keyboard: canClose });
            jquery("body > .modal").on("hidden.bs.modal", function () { return _this.Current(null); });
            return dialogState;
        };
        Dialog.prototype.Close = function (dialogState) {
            var deferred = jquery.Deferred();
            if (this.Current() === dialogState) {
                var $dialog_1 = jquery("body > .modal");
                var handle_1 = function () {
                    $dialog_1.off("hidden.bs.modal", handle_1);
                    deferred.resolve();
                };
                $dialog_1.on("hidden.bs.modal", handle_1);
                $dialog_1.modal("hide");
            }
            else
                deferred.resolve();
            return deferred.promise();
        };
        return Dialog;
    }());
    var instance = new Dialog();
    return instance;
});
