define(["require", "exports"], function (require, exports) {
    "use strict";
    var DialogState = (function () {
        function DialogState(name, data, closeCallback) {
            this._name = name;
            this._data = data;
            this._closeCallback = closeCallback;
        }
        Object.defineProperty(DialogState.prototype, "Name", {
            get: function () {
                return this._name;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DialogState.prototype, "Data", {
            get: function () {
                return this._data;
            },
            enumerable: true,
            configurable: true
        });
        DialogState.prototype.Close = function () {
            return this._closeCallback(this);
        };
        return DialogState;
    }());
    return DialogState;
});
