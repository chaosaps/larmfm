﻿import jquery = require("jquery");
import knockout = require("knockout");
import Interface = require("Managers/Interface");

class Dragging
{
	public readonly DragType = "text";
	public IsDraggingSearchResult = knockout.observable(false);
	public IsDraggingProject = knockout.observable(false);
	public DragData = knockout.observable<null | string>(null);

	constructor()
	{
		jquery("html").on("dragend", () => {
			this.IsDraggingSearchResult(false);
			this.IsDraggingProject(false);
			this.DragData(null);
		});
	}

	public StartSearchResultDrag(event: DragEvent, data: string): void
	{
		this.IsDraggingSearchResult(true);
		this.StartDrag(event, data);
		Interface.Show();
	}

	public StartProjectDrag(event: DragEvent, data: string): void
	{
		this.IsDraggingProject(true);
		this.StartDrag(event, data);
	}

	private StartDrag(event: DragEvent, data: string): void
	{
		event.dataTransfer.effectAllowed = "move";
		event.dataTransfer.clearData();
		event.dataTransfer.setData(this.DragType, data);
		this.DragData(data);
	}
}

var instance = new Dragging();

export = instance;