﻿import jquery = require("jquery");
import knockout = require("knockout");
import Configuration = require("Managers/Configuration");
import Tracking = require("Managers/Tracking");

class PortChecker
{
	public IsChecking = knockout.observable(false);
	public FailedPorts = knockout.observableArray<number>();
	public HasErrors:KnockoutComputed<boolean>;

	constructor()
	{
		this.HasErrors = knockout.pureComputed(() => this.FailedPorts().length !== 0);
		this.IsChecking.subscribe(v =>
		{
			if (!v && this.HasErrors())
			{
				var ports = this.FailedPorts();
				ports.sort((a, b) => a - b);
				Tracking.TrackEvent("Port Checking", "Failed", ports.join());
			}
		});

		this.Run();
	}

	public Run():void
	{
		if (this.IsChecking()) return;

		this.IsChecking(true);

		this.FailedPorts.removeAll();

		jquery.when(...Configuration.PortChecker.Ports.map(p => this.CheckPort(p)).map(check =>
			{
				var deferred = jquery.Deferred(); 
				check.always(() => deferred.resolve()); //Catch success or failure so combined resolve will be when all checks are completed, not just first fail
				return deferred.promise();
			}))
			.always(() => this.IsChecking(false));
	}

	private CheckPort(port:number):JQueryPromise<string>
	{
		const url = Configuration.PortChecker.Url.replace(Configuration.PortChecker.PortToken, port.toString());

		return jquery.ajax(url, { cache: false }).done(data =>
		{
			if (data.replace(/\r?\n/g, "") !== Configuration.PortChecker.Expected)
				this.FailedPorts.push(port);
		}).fail(() => this.FailedPorts.push(port));
	}
}

var instance = new PortChecker();

export = instance;