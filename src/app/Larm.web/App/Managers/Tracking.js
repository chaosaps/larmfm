define(["require", "exports", "Managers/Configuration"], function (require, exports, Configuration) {
    "use strict";
    var Tracking = (function () {
        function Tracking() {
            this._tracker = null;
            this._trackingBackLog = [];
        }
        Object.defineProperty(Tracking.prototype, "IsTracking", {
            get: function () {
                return this._tracker !== null;
            },
            enumerable: true,
            configurable: true
        });
        Tracking.prototype.StartTracking = function () {
            var _this = this;
            if (this.IsTracking)
                return;
            try {
                require(["analytics"], function (a) {
                    a("set", "anonymizeIp", true);
                    _this._tracker = a.create(Configuration.TrackingId);
                    _this._trackingBackLog.forEach(function (call) { return call(); });
                    _this._trackingBackLog.splice(0, _this._trackingBackLog.length);
                });
            }
            catch (e) {
                console.log("Tracking failed", e);
            }
        };
        Tracking.prototype.TrackPage = function (address) {
            var _this = this;
            this.Track(function () { return _this._tracker.send("pageview", address); });
        };
        Tracking.prototype.TrackEvent = function (category, action, label) {
            var _this = this;
            this.Track(function () { return _this._tracker.send("event", category, action, label); });
        };
        Tracking.prototype.Track = function (trackCall) {
            if (this.IsTracking) {
                try {
                    trackCall();
                }
                catch (e) { }
            }
            else
                this._trackingBackLog.push(trackCall);
        };
        return Tracking;
    }());
    var instance = new Tracking();
    return instance;
});
