define(["require", "exports", "knockout", "Managers/Tracking"], function (require, exports, knockout, Tracking) {
    "use strict";
    var Notification = (function () {
        function Notification() {
            this.Notifications = knockout.observableArray();
        }
        Notification.prototype.Error = function (message) {
            this.Notifications.push({ Message: message, Level: "Error" });
            Tracking.TrackEvent("Notification", "Error", message);
        };
        Notification.prototype.Warning = function (message) {
            this.Notifications.push({ Message: message, Level: "Warning" });
            Tracking.TrackEvent("Notification", "Warning", message);
        };
        Notification.prototype.Debug = function (message) {
            console.log(message);
            Tracking.TrackEvent("Notification", "Debug", message);
        };
        return Notification;
    }());
    var instance = new Notification();
    return instance;
});
