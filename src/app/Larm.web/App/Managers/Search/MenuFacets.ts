﻿import knockout = require("knockout");
import Search = require("Managers/Search/Search");
import Query = require("Managers/Search/Query");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import LarmPortal = require("../Portal/Portal");
import Notification = require("Managers/Notification");
import Configuration = require("Managers/Configuration");
import DisposableComponent = require("Components/DisposableComponent");
import DateParser = require("Utility/DateParser");

type SearchFacet = { Facet: EZArchivePortal.IEZFacetField, Value: EZArchivePortal.IEZFacetFieldValue }

class MenuFacets extends DisposableComponent
{
	public Facets = knockout.observable<EZArchivePortal.IEZFacetField[]>(null);
	public SearchFacet:KnockoutComputed<SearchFacet>;

	constructor()
	{
		super();

		this.AddAction(LarmPortal.IsReady, () =>
		{
			var previousQuery: Query = Search.Query();

			Search.Query.subscribe(q =>
			{
				var comparison = q.Compare(previousQuery);
				previousQuery = q;

				if (comparison.Query || comparison.Facets || comparison.From || comparison.To)
					this.LoadFacets(q);
			});
			this.LoadFacets(previousQuery);
		});

		this.SearchFacet = knockout.pureComputed(() => this.GetSearchFacet());
	}

	private GetSearchFacet(): SearchFacet
	{
		var query = Search.Query();
		var facets = this.Facets();

		if (query.Facets == null || query.Facets.length === 0 || facets == null || facets.length === 0) return null;
		var facet = query.Facets[0];

		for (let field of facets)
		{
			if (field.Value !== facet.Key) continue;

			for (let value of field.Facets)
			{
				if (value.Key !== facet.Value) continue;

				return { Facet: field, Value: value };
			}

			break;
		}

		return null;
	}

	public GetFacetValueName(key:string, value:string):string
	{
		return Configuration.FacetKeys.hasOwnProperty(key) && Configuration.FacetKeys[key].Values != null && Configuration.FacetKeys[key].Values.hasOwnProperty(value) ? Configuration.FacetKeys[key].Values[value] : value;
	}

	private LoadFacets(query: Query): any
	{
		let filteredFacets: EZArchivePortal.IEZFacetField[] | null = null;
		let unfilteredFacets: EZArchivePortal.IEZFacetField[] | null = null;

		const updateFacets = () =>
		{
			if (unfilteredFacets === null)
				return;

			if (query.Facets === null)
				this.Facets(unfilteredFacets);
			else if (filteredFacets !== null)
			{
				this.Facets(filteredFacets.map(f =>
				{
					if (f.Value !== query.Facets[0].Key)
						return f;

					return unfilteredFacets.filter(filtered => filtered.Value === query.Facets[0].Key)[0];
				}));
			}
		}

		const dateFilter = query.From !== null && query.To !== null ? "{Search}.Udsendelsesdato:BETWEEN " + DateParser.ToLocalUTCString(query.From) + " " + DateParser.ToLocalUTCString(query.To) : null;

		EZArchivePortal.EZFacet.Get(query.SafeQuery, dateFilter, null, null).WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error(`Failed to get facets: ${response.Error.Message}`);
				return;
			}

			var facets = response.Body.Results.filter(f => f.Header === Configuration.MenuFacetHeader);
			if (facets.length === 0)
				return;

			unfilteredFacets = facets[0].Fields;
			updateFacets();
		});

		if (query.Facets !== null)
		{
			const filter = `${query.Facets[0].Key}:"${query.Facets[0].Value}"`;
			EZArchivePortal.EZFacet.Get(query.SafeQuery, filter, dateFilter, null)
				.WithCallback(response =>
				{
					if (response.Error != null)
					{
						Notification.Error(`Failed to get facets: ${response.Error.Message}`);
						return;
					}

					var facets = response.Body.Results.filter(f => f.Header === Configuration.MenuFacetHeader);

					if (facets.length === 0)
						return;
					filteredFacets = facets[0].Fields;
					updateFacets();
				});
		}
	}

	private GetUTCDate(date: Date): Date
	{
		if (date == null) return null;

		return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours()));
	}
}

var instance = new MenuFacets();

export = instance;