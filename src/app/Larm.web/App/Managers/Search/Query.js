define(["require", "exports", "Utility/DateParser"], function (require, exports, DateParser) {
    "use strict";
    var Query = (function () {
        function Query() {
            this.From = null;
            this.To = null;
            this.SortField = null;
            this.IsSortDirectionDescending = true;
            this.Facets = null;
            this.PageSize = null;
            this.PageNumber = null;
            this._query = "";
        }
        Object.defineProperty(Query.prototype, "Query", {
            get: function () {
                return this._query;
            },
            set: function (value) {
                this._query = value == null ? null : value.toString();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Query.prototype, "SafeQuery", {
            get: function () {
                return this.Query.replace(/\:/g, "");
            },
            enumerable: true,
            configurable: true
        });
        Query.prototype.Parse = function (query) {
            this.Query = query.hasOwnProperty(Query.ParameterNameQuery) ? query[Query.ParameterNameQuery] : "";
            this.SortField = query.hasOwnProperty(Query.ParameterNameSort) ? query[Query.ParameterNameSort] : null;
            this.IsSortDirectionDescending = query.hasOwnProperty(Query.ParameterNameSortDirecton) ? query[Query.ParameterNameSortDirecton] : true;
            this.From = query.hasOwnProperty(Query.ParameterNameFrom) ? DateParser.ParseLocalUTCString(query[Query.ParameterNameFrom]) : null;
            this.To = query.hasOwnProperty(Query.ParameterNameTo) ? DateParser.ParseLocalUTCString(query[Query.ParameterNameTo]) : null;
            this.Facets = query.hasOwnProperty(Query.ParameterNameFacets) ? this.ParseFacets(query[Query.ParameterNameFacets]) : null;
            this.Label = query.hasOwnProperty(Query.ParameterNameLabel) ? query[Query.ParameterNameLabel].toString() : null;
            this.PageSize = query.hasOwnProperty(Query.ParameterNamePageSize) ? query[Query.ParameterNamePageSize] : null;
            this.PageNumber = query.hasOwnProperty(Query.ParameterNamePageNumber) ? query[Query.ParameterNamePageNumber] : null;
        };
        Query.prototype.ToString = function () {
            var query = [];
            if (this.Query != null && this.Query !== "")
                query.push(this.GetQueryPair(Query.ParameterNameQuery, this.Query));
            if (this.From != null)
                query.push(this.GetQueryPair(Query.ParameterNameFrom, DateParser.ToLocalUTCString(this.From, false)));
            if (this.To != null)
                query.push(this.GetQueryPair(Query.ParameterNameTo, DateParser.ToLocalUTCString(this.To, false)));
            if (this.Facets != null)
                query.push(this.GetQueryPair(Query.ParameterNameFacets, this.ToFacetsString(this.Facets)));
            if (this.Label != null)
                query.push(this.GetQueryPair(Query.ParameterNameLabel, this.Label));
            if (this.PageSize != null)
                query.push(this.GetQueryPair(Query.ParameterNamePageSize, this.PageSize.toString()));
            if (this.PageNumber != null)
                query.push(this.GetQueryPair(Query.ParameterNamePageNumber, this.PageNumber.toString()));
            if (this.SortField != null) {
                query.push(this.GetQueryPair(Query.ParameterNameSort, this.SortField));
                if (!this.IsSortDirectionDescending)
                    query.push(Query.ParameterNameSortDirecton + "=false");
            }
            return query.length === 0 ? "" : "?" + query.join("&");
        };
        Query.prototype.Clone = function () {
            var query = new Query();
            query.Query = this.Query;
            if (this.From != null)
                query.From = new Date(this.From.getTime());
            if (this.To != null)
                query.To = new Date(this.To.getTime());
            if (this.Facets != null)
                query.Facets = this.Facets.map(function (f) { return ({ Key: f.Key, Value: f.Value }); });
            query.Label = this.Label;
            query.SortField = this.SortField;
            query.IsSortDirectionDescending = this.IsSortDirectionDescending;
            query.PageSize = this.PageSize;
            query.PageNumber = this.PageNumber;
            return query;
        };
        Query.prototype.Compare = function (query) {
            return query == null ? { Query: true, Facets: true, From: true, To: true, SortField: true, IsSortDirectionDescending: true, Label: true, PageSize: true, PageNumber: true } : {
                Query: this.Query !== query.Query,
                Facets: this.AreFacetsEqual(this.Facets, query.Facets),
                From: !this.AreDatesEqual(this.From, query.From),
                To: !this.AreDatesEqual(this.To, query.To),
                SortField: this.SortField !== query.SortField,
                IsSortDirectionDescending: this.IsSortDirectionDescending !== query.IsSortDirectionDescending,
                Label: this.Label !== query.Label,
                PageSize: this.PageSize !== query.PageSize,
                PageNumber: this.PageNumber !== query.PageNumber
            };
        };
        Query.prototype.AreFacetsEqual = function (facets1, facets2) {
            if (facets1 === null || facets2 === null)
                return facets1 === facets2;
            return facets1.every(function (f1) { return facets2.some(function (f2) { return f1.Key === f2.Key && f1.Value === f2.Value; }); });
        };
        Query.prototype.AreDatesEqual = function (date1, date2) {
            return date1 == null || date2 == null ?
                date1 === date2 :
                date1.getTime() !== date2.getTime();
        };
        Query.prototype.GetQueryPair = function (key, value) {
            return key + "=" + this.GetEncodedValue(value);
        };
        Query.prototype.GetEncodedValue = function (value) {
            return encodeURIComponent(value).replace(/%20/g, "+");
        };
        Query.prototype.ParseFacets = function (facetString) {
            return facetString.split(",").map(function (s) { return s.split(":"); }).map(function (f) { return ({ Key: f[0], Value: f[1] }); });
        };
        Query.prototype.ToFacetsString = function (facets) {
            return facets.map(function (f) { return f.Key + ":" + f.Value; }).join(",");
        };
        Query.ParameterNameQuery = "query";
        Query.ParameterNameSort = "sort";
        Query.ParameterNameSortDirecton = "direction";
        Query.ParameterNameFrom = "from";
        Query.ParameterNameTo = "to";
        Query.ParameterNameFacets = "facets";
        Query.ParameterNameLabel = "label";
        Query.ParameterNamePageSize = "pageSize";
        Query.ParameterNamePageNumber = "pageNumber";
        return Query;
    }());
    return Query;
});
