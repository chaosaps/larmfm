﻿import DateParser = require("Utility/DateParser");

type Facet = { Key: string; Value: string };
type Comparison = { Query: boolean; Facets: boolean; From: boolean; To: boolean; SortField: boolean; IsSortDirectionDescending: boolean; Label: boolean; PageSize: boolean; PageNumber: boolean; }

class Query
{
	private static ParameterNameQuery:string = "query";
	private static ParameterNameSort:string = "sort";
	private static ParameterNameSortDirecton: string = "direction";
	private static ParameterNameFrom: string = "from";
	private static ParameterNameTo: string = "to";
	private static ParameterNameFacets: string = "facets";
	private static ParameterNameLabel: string = "label";
	private static ParameterNamePageSize: string = "pageSize";
	private static ParameterNamePageNumber: string = "pageNumber";

	public From: Date = null;
	public To: Date = null;
	public SortField:string = null;
	public IsSortDirectionDescending: boolean = true;
	public Facets: Facet[] = null;
	public Label:string;
	public PageSize:number = null;
	public PageNumber: number = null;

	private _query: string = "";
	public get Query(): string
	{
		return this._query;
	}
	public set Query(value:string)
	{
		this._query = value == null ? null : value.toString();
	}

	public get SafeQuery(): string
	{
		return this.Query.replace(/\:/g, "");
	}

	public Parse(query:{[key:string]:(string|boolean|number)}):void
	{
		this.Query = query.hasOwnProperty(Query.ParameterNameQuery) ? <string>query[Query.ParameterNameQuery] : "";
		this.SortField = query.hasOwnProperty(Query.ParameterNameSort) ? <string>query[Query.ParameterNameSort] : null;
		this.IsSortDirectionDescending = query.hasOwnProperty(Query.ParameterNameSortDirecton) ? <boolean>query[Query.ParameterNameSortDirecton] : true;
		this.From = query.hasOwnProperty(Query.ParameterNameFrom) ? DateParser.ParseLocalUTCString(<string>query[Query.ParameterNameFrom]) : null;
		this.To = query.hasOwnProperty(Query.ParameterNameTo) ? DateParser.ParseLocalUTCString(<string>query[Query.ParameterNameTo]) : null;
		this.Facets = query.hasOwnProperty(Query.ParameterNameFacets) ? this.ParseFacets(<string>query[Query.ParameterNameFacets]) : null;
		this.Label = query.hasOwnProperty(Query.ParameterNameLabel) ? <string>query[Query.ParameterNameLabel].toString() : null;
		this.PageSize = query.hasOwnProperty(Query.ParameterNamePageSize) ? <number>query[Query.ParameterNamePageSize] : null;
		this.PageNumber = query.hasOwnProperty(Query.ParameterNamePageNumber) ? <number>query[Query.ParameterNamePageNumber] : null;
	}

	public ToString():string
	{
		var query: string[] = [];

		if (this.Query != null && this.Query !== "") query.push(this.GetQueryPair(Query.ParameterNameQuery, this.Query));
		if (this.From != null) query.push(this.GetQueryPair(Query.ParameterNameFrom, DateParser.ToLocalUTCString(this.From, false)));
		if (this.To != null) query.push(this.GetQueryPair(Query.ParameterNameTo, DateParser.ToLocalUTCString(this.To, false)));
		if (this.Facets != null) query.push(this.GetQueryPair(Query.ParameterNameFacets, this.ToFacetsString(this.Facets)));
		if (this.Label != null) query.push(this.GetQueryPair(Query.ParameterNameLabel, this.Label));
		if (this.PageSize != null) query.push(this.GetQueryPair(Query.ParameterNamePageSize, this.PageSize.toString()));
		if (this.PageNumber != null) query.push(this.GetQueryPair(Query.ParameterNamePageNumber, this.PageNumber.toString()));

		if (this.SortField != null)
		{
			query.push(this.GetQueryPair(Query.ParameterNameSort, this.SortField));

			if (!this.IsSortDirectionDescending)
				query.push(`${Query.ParameterNameSortDirecton}=false`);
		}

		return query.length === 0 ? "" : `?${query.join("&")}`;
	}

	public Clone():Query
	{
		var query = new Query();

		query.Query = this.Query;
		if (this.From != null) query.From = new Date(this.From.getTime());
		if (this.To != null) query.To = new Date(this.To.getTime());
		if (this.Facets != null) query.Facets = this.Facets.map(f => ({Key: f.Key, Value: f.Value}));

		query.Label = this.Label;
		query.SortField = this.SortField;
		query.IsSortDirectionDescending = this.IsSortDirectionDescending;
		query.PageSize = this.PageSize;
		query.PageNumber = this.PageNumber;

		return query;
	}

	public Compare(query:Query):Comparison
	{
		return query == null ? {Query: true, Facets: true, From: true, To:true, SortField: true, IsSortDirectionDescending: true, Label: true, PageSize: true, PageNumber: true} : {
			Query: this.Query !== query.Query,
			Facets: this.AreFacetsEqual(this.Facets, query.Facets),
			From: !this.AreDatesEqual(this.From, query.From),
			To: !this.AreDatesEqual(this.To, query.To),
			SortField: this.SortField !== query.SortField,
			IsSortDirectionDescending: this.IsSortDirectionDescending !== query.IsSortDirectionDescending,
			Label: this.Label !== query.Label,
			PageSize: this.PageSize !== query.PageSize,
			PageNumber: this.PageNumber !== query.PageNumber
		}
	}

	private AreFacetsEqual(facets1: Facet[] | null, facets2: Facet[] | null): boolean
	{
		if (facets1 === null || facets2 === null)
			return facets1 === facets2;

		return facets1.every(f1 => facets2.some(f2 => f1.Key === f2.Key && f1.Value === f2.Value));
	}

	private AreDatesEqual(date1:Date, date2:Date):boolean
	{
		return date1 == null || date2 == null ?
				date1 === date2 :
				date1.getTime() !== date2.getTime();
	}

	private GetQueryPair(key:string, value:string):string
	{
		return `${key}=${this.GetEncodedValue(value)}`;
	}

	private GetEncodedValue(value:string):string
	{
		return encodeURIComponent(value).replace(/%20/g, "+");
	}

	private ParseFacets(facetString: string): Facet[]
	{
		return facetString.split(",").map(s => s.split(":")).map(f => ({ Key: f[0], Value: f[1] }));
	}

	private ToFacetsString(facets: Facet[]): string
	{
		return facets.map(f => `${f.Key }:${f.Value }`).join(",");
	}
}

export = Query;