var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Managers/Search/Search", "Managers/Portal/EZArchivePortal", "../Portal/Portal", "Managers/Notification", "Managers/Configuration", "Components/DisposableComponent", "Utility/DateParser"], function (require, exports, knockout, Search, EZArchivePortal, LarmPortal, Notification, Configuration, DisposableComponent, DateParser) {
    "use strict";
    var MenuFacets = (function (_super) {
        __extends(MenuFacets, _super);
        function MenuFacets() {
            var _this = _super.call(this) || this;
            _this.Facets = knockout.observable(null);
            _this.AddAction(LarmPortal.IsReady, function () {
                var previousQuery = Search.Query();
                Search.Query.subscribe(function (q) {
                    var comparison = q.Compare(previousQuery);
                    previousQuery = q;
                    if (comparison.Query || comparison.Facets || comparison.From || comparison.To)
                        _this.LoadFacets(q);
                });
                _this.LoadFacets(previousQuery);
            });
            _this.SearchFacet = knockout.pureComputed(function () { return _this.GetSearchFacet(); });
            return _this;
        }
        MenuFacets.prototype.GetSearchFacet = function () {
            var query = Search.Query();
            var facets = this.Facets();
            if (query.Facets == null || query.Facets.length === 0 || facets == null || facets.length === 0)
                return null;
            var facet = query.Facets[0];
            for (var _i = 0, facets_1 = facets; _i < facets_1.length; _i++) {
                var field = facets_1[_i];
                if (field.Value !== facet.Key)
                    continue;
                for (var _a = 0, _b = field.Facets; _a < _b.length; _a++) {
                    var value = _b[_a];
                    if (value.Key !== facet.Value)
                        continue;
                    return { Facet: field, Value: value };
                }
                break;
            }
            return null;
        };
        MenuFacets.prototype.GetFacetValueName = function (key, value) {
            return Configuration.FacetKeys.hasOwnProperty(key) && Configuration.FacetKeys[key].Values != null && Configuration.FacetKeys[key].Values.hasOwnProperty(value) ? Configuration.FacetKeys[key].Values[value] : value;
        };
        MenuFacets.prototype.LoadFacets = function (query) {
            var _this = this;
            var filteredFacets = null;
            var unfilteredFacets = null;
            var updateFacets = function () {
                if (unfilteredFacets === null)
                    return;
                if (query.Facets === null)
                    _this.Facets(unfilteredFacets);
                else if (filteredFacets !== null) {
                    _this.Facets(filteredFacets.map(function (f) {
                        if (f.Value !== query.Facets[0].Key)
                            return f;
                        return unfilteredFacets.filter(function (filtered) { return filtered.Value === query.Facets[0].Key; })[0];
                    }));
                }
            };
            var dateFilter = query.From !== null && query.To !== null ? "{Search}.Udsendelsesdato:BETWEEN " + DateParser.ToLocalUTCString(query.From) + " " + DateParser.ToLocalUTCString(query.To) : null;
            EZArchivePortal.EZFacet.Get(query.SafeQuery, dateFilter, null, null).WithCallback(function (response) {
                if (response.Error != null) {
                    Notification.Error("Failed to get facets: " + response.Error.Message);
                    return;
                }
                var facets = response.Body.Results.filter(function (f) { return f.Header === Configuration.MenuFacetHeader; });
                if (facets.length === 0)
                    return;
                unfilteredFacets = facets[0].Fields;
                updateFacets();
            });
            if (query.Facets !== null) {
                var filter = query.Facets[0].Key + ":\"" + query.Facets[0].Value + "\"";
                EZArchivePortal.EZFacet.Get(query.SafeQuery, filter, dateFilter, null)
                    .WithCallback(function (response) {
                    if (response.Error != null) {
                        Notification.Error("Failed to get facets: " + response.Error.Message);
                        return;
                    }
                    var facets = response.Body.Results.filter(function (f) { return f.Header === Configuration.MenuFacetHeader; });
                    if (facets.length === 0)
                        return;
                    filteredFacets = facets[0].Fields;
                    updateFacets();
                });
            }
        };
        MenuFacets.prototype.GetUTCDate = function (date) {
            if (date == null)
                return null;
            return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours()));
        };
        return MenuFacets;
    }(DisposableComponent));
    var instance = new MenuFacets();
    return instance;
});
