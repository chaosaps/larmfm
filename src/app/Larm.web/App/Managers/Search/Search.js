define(["require", "exports", "knockout", "Managers/Navigation/Navigation", "Managers/Portal/Portal", "Managers/Portal/EZArchivePortal", "Managers/Notification", "Managers/Search/Query", "Utility/DisposableAction"], function (require, exports, knockout, Navigation, LarmPortal, EZArchivePortal, Notification, Query, DisposableAction) {
    "use strict";
    var Search = (function () {
        function Search() {
            var _this = this;
            this.IsReady = knockout.observable(false);
            this.Query = knockout.observable(new Query());
            this._isParsed = false;
            this.Query.extend({ rateLimit: 50 });
            this.Query.subscribe(function (q) {
                if (_this._isParsed)
                    _this._isParsed = false;
                else
                    Navigation.Navigate("Search" + q.ToString());
            });
            new DisposableAction(LarmPortal.IsReady, function () { return _this.Initialize(); });
        }
        Search.prototype.ClearLabel = function () {
            var query = this.Clone();
            query.Label = null;
            query.PageNumber = null;
            this.Search(query);
        };
        Search.prototype.Search = function (query) {
            this.Query(query);
        };
        Search.prototype.Clone = function () {
            return this.Query().Clone();
        };
        Search.prototype.Parse = function (query) {
            this._isParsed = true;
            var q = new Query();
            q.Parse(query);
            this.Query(q);
        };
        Search.prototype.Initialize = function () {
            var _this = this;
            EZArchivePortal.EZSearchDefinition.Get().WithCallback(function (response) {
                if (response.Error !== null) {
                    Notification.Error("Failed to get search definition: " + response.Error.Message);
                    return;
                }
                _this.SearchFields = response.Body.Results[0].Fields;
                _this.IsReady(true);
            });
        };
        return Search;
    }());
    var instance = new Search();
    return instance;
});
