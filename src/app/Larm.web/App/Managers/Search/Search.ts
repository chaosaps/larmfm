﻿import knockout = require("knockout");
import Navigation = require("Managers/Navigation/Navigation");
import LarmPortal = require("Managers/Portal/Portal");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Notification = require("Managers/Notification");
import Query = require("Managers/Search/Query");
import DisposableAction = require("Utility/DisposableAction");

class Search
{
	public IsReady:KnockoutObservable<boolean> = knockout.observable(false);
	public Query: KnockoutObservable<Query> = knockout.observable(new Query());
	public SearchFields: EZArchivePortal.IEZSearchDefinitionField[];

	private _isParsed = false;

	constructor()
	{
		this.Query.extend({ rateLimit: 50 });
		this.Query.subscribe(q => {
			if (this._isParsed)
				this._isParsed = false;
			else
				Navigation.Navigate("Search" + q.ToString());
		});
		new DisposableAction( LarmPortal.IsReady, () => this.Initialize());
	}

	public ClearLabel():void
	{
		var query = this.Clone();

		query.Label = null;
		query.PageNumber = null;

		this.Search(query);
	}

	public Search(query: Query):void
	{
		this.Query(query);
	}

	public Clone():Query
	{
		return this.Query().Clone();
	}

	public Parse(query: { [key: string]: string }):void
	{
		this._isParsed = true;

		var q = new Query();
		q.Parse(query);

		this.Query(q);
	}

	private Initialize():void
	{
		EZArchivePortal.EZSearchDefinition.Get().WithCallback(response =>
		{
			if (response.Error !== null)
			{
				Notification.Error("Failed to get search definition: " + response.Error.Message);
				return;
			}

			this.SearchFields = response.Body.Results[0].Fields;
			this.IsReady(true);
		});
	}
}

var instance = new Search();

export = instance;