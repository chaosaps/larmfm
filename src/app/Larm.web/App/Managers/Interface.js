var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "knockout", "Components/DisposableComponent"], function (require, exports, knockout, DisposableComponent) {
    "use strict";
    var Interface = (function (_super) {
        __extends(Interface, _super);
        function Interface() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.IsMenuCollapsed = knockout.observable(false);
            return _this;
        }
        Interface.prototype.Show = function () {
            this.IsMenuCollapsed(false);
        };
        Interface.prototype.Hide = function () {
            this.IsMenuCollapsed(true);
        };
        return Interface;
    }(DisposableComponent));
    var instance = new Interface();
    return instance;
});
