﻿import knockout = require("knockout");
import Authorization = require("Managers/Authorization");
import Profile = require("Managers/Profile");
import EzArchivePortal = require("./Portal/EZArchivePortal");
import UserPermissions = EzArchivePortal.UserPermissions;

class Permissions
{
	public CanEditMetadata:KnockoutComputed<boolean>;
	public CanEditAnnotations: KnockoutComputed<boolean>;
	public CanStreamContent: KnockoutComputed<boolean>;
	public CanAssociateWithLabel: KnockoutComputed<boolean>;
	public CanSeeOffsetMetadata: KnockoutComputed<boolean>;
	public CanEditOffsetMetadata: KnockoutComputed<boolean>;

	constructor()
	{
		this.CanEditMetadata = knockout.computed(() => Authorization.IsAuthenticated());
		this.CanEditAnnotations = knockout.computed(() => Authorization.IsAuthenticated());
		this.CanStreamContent = knockout.computed(() => Authorization.IsAuthenticated());
		this.CanAssociateWithLabel = knockout.computed(() => Authorization.IsAuthenticated());
		this.CanSeeOffsetMetadata = knockout.computed(() => Profile.IsAdministrator() || Profile.Permission() === UserPermissions.Administrator);
		this.CanEditOffsetMetadata = this.CanSeeOffsetMetadata;
	}
}

var instance = new Permissions();

export = instance;