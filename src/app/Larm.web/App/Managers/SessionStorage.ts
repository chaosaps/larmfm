﻿class SessionStorage
{
	private _canUseStorage: boolean = false;

	constructor()
	{
		try { this._canUseStorage = window.sessionStorage != undefined; } catch (e) { }
	}

	public get CanUseStorage(): boolean
	{
		return this._canUseStorage;
	}

	public Get(key: string): string | null
	{
		return this._canUseStorage ? window.sessionStorage.getItem(key) : null;
	}

	public Set(key: string, value: string): void
	{
		if (this._canUseStorage) window.sessionStorage.setItem(key, value);
	}

	public Remove(key: string): void
	{
		if (this._canUseStorage) window.sessionStorage.removeItem(key);
	}
}

var instance = new SessionStorage();

export = instance;