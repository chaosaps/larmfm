﻿import jquery = require("jquery");

class Storage
{
	private _canUseStorage: boolean = false;
	private _listeners:{[key:string]:((value:string)=>void)[]} = {};

	constructor()
	{
		try { this._canUseStorage = window.localStorage != undefined; } catch (e) { }

		if (this._canUseStorage)
		{
			jquery(window).on("storage", e =>
			{
				var event = <StorageEvent>e.originalEvent;
				this.Changed(event.key, event.newValue);
			});
		}
	}

	public get CanUseStorage():boolean
	{
		return this._canUseStorage;
	}

	public Get(key: string): string
	{
		return this._canUseStorage ? window.localStorage.getItem(key) : null;
	}

	public Set(key: string, value: string): void
	{
		if (this._canUseStorage) window.localStorage.setItem(key, value);
	}

	public SetAndRemove(key:string, value:string):void
	{
		this.Set(key, value);
		this.Remove(key);
	}

	public Remove(key: string): void
	{
		if (this._canUseStorage) window.localStorage.removeItem(key);
	}

	public Listen(key:string, callback:(value:string) => void):void
	{
		if (this._listeners.hasOwnProperty(key))
			this._listeners[key].push(callback);
		else
			this._listeners[key] = [callback];
	}

	public Unlisten(key:string, callback:(value:string) => void):void
	{
		if (!this._listeners.hasOwnProperty(key) || this._listeners[key].indexOf(callback) === -1) return;

		this._listeners[key].splice(this._listeners[key].indexOf(callback), 1);
	}

	private Changed(key:string, value:string):void
	{
		if (!this._listeners.hasOwnProperty(key) || this._listeners[key].length === 0) return;

		this._listeners[key].forEach(v => v(value));
	}
}

var instance = new Storage();

export = instance;