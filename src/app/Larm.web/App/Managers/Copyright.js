define(["require", "exports", "Managers/Configuration"], function (require, exports, Configuration) {
    "use strict";
    var Copyright = (function () {
        function Copyright() {
        }
        Copyright.prototype.IsCleared = function (data) {
            var tvDataList = data.filter(function (d) { return d.Name === Configuration.Copyright.TvSchemaName; });
            if (tvDataList.length === 0)
                return true;
            var fields = tvDataList[0].Fields;
            return this.CheckMovies(fields) && this.CheckSports(fields);
        };
        Copyright.prototype.CheckMovies = function (fields) {
            if (!fields.hasOwnProperty("MajorGenre"))
                return true;
            var major = fields["MajorGenre"].toLocaleLowerCase();
            if (major !== "film")
                return true;
            else if (!fields.hasOwnProperty("MinorGenre"))
                return false;
            var minor = fields["MinorGenre"].toLocaleLowerCase();
            return minor === "miniserie" || minor === "thrillerserie" || minor === "thrillerdrama" || minor.indexOf("krimi") === 0;
        };
        Copyright.prototype.CheckSports = function (fields) {
            return !fields.hasOwnProperty("Channel") || fields["Channel"].toLocaleLowerCase() !== "tv2 sport";
        };
        return Copyright;
    }());
    var instance = new Copyright();
    return instance;
});
