﻿import knockout = require("knockout");
import DisposableComponent = require("Components/DisposableComponent");
import Navigation = require("Managers/Navigation/Navigation");

class Title extends DisposableComponent
{
	constructor()
	{
		super();
		this.Subscribe(Navigation.Current, p => this.Set());
	}

	public Set(newTitle: string = null): void
	{
		document.title = `LARM${newTitle == null ? "" : ` - ${newTitle}`}`;
	}
}

var instance = new Title();

export = instance;