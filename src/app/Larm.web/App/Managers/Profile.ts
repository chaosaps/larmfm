﻿import knockout = require("knockout");
import EZArchivePortal = require("Managers/Portal/EZArchivePortal");
import Notification = require("Managers/Notification");
import Authorization = require("Managers/Authorization");
import DialogManager = require("Managers/Dialog/Dialog");

class Profile
{
	public Identifier = knockout.observable<string>(null);

	public Email: KnockoutObservable<string> = knockout.observable("");
	public Name: KnockoutObservable<string> = knockout.observable("");
	public Preferences = knockout.observable<any | null>(null);
	public IsReady: KnockoutComputed<boolean>;
	public HasAcceptedTerms: KnockoutComputed<boolean>;
	public ProjectOrder: KnockoutComputed<string[] | null>;

	public readonly Permission = knockout.observable<EZArchivePortal.UserPermissions | null>(null);
	public readonly IsAdministrator = knockout.observable<boolean>(false);
	
	private _user: EZArchivePortal.IEZUser = null;

	constructor()
	{
		this.IsReady = knockout.pureComputed(() => this.Identifier() !== null);
		this.HasAcceptedTerms = knockout.pureComputed(() =>
		{
			const preferences = this.Preferences();

			return preferences !== null && preferences.Terms === 1;
		});
		this.ProjectOrder = knockout.pureComputed(() => {
			const preferences = this.Preferences();

			return preferences !== null && preferences.ProjectOrder ? preferences.ProjectOrder : null;
		});
		Authorization.IsAuthenticated.subscribe(isAuthenticated =>
		{
			if (isAuthenticated)
				this.GetProfile();
			else
				this.Reset();
		});
	}

	public AcceptTerms(): void
	{
		if (!this.IsReady())
			throw new Error("Not ready to update preferences")

		let preferences = this.Preferences();

		if (preferences === null)
			preferences = { Terms: 1 }
		else
			preferences.Terms = 1;

		this._user.Preferences = JSON.stringify(preferences);
		this.Preferences(preferences);

		this.SetProfile(success =>
		{

		});
	}

	public SetProjectOrder(projectOrder: string[]): void
	{
		if (!this.IsReady())
			throw new Error("Not ready to update preferences")

		let preferences = this.Preferences();

		if (preferences === null)
			preferences = { ProjectOrder: projectOrder }
		else
			preferences.ProjectOrder = projectOrder;

		this._user.Preferences = JSON.stringify(preferences);
		this.Preferences(preferences);

		this.SetProfile(success => {

		});
	}

	public Update(name:string, email:string, callback:(success:boolean)=>void):void
	{
		var originalName = this._user.Name;
		var originalEmail = this._user.Email;

		this._user.Name = name;
		this._user.Email = email !== "" ? email : this.Identifier();
		this.Name(name);
		this.Email(email);

		this.SetProfile(success =>
		{
			if (!success)
			{
				this._user.Name = originalName;
				this._user.Email = originalEmail;
				this.Name(originalName);
				this.Email(originalEmail);
			}
			callback(success);
		});
	}

	public EditProfile(): void
	{
		DialogManager.Show("Profile/EditProfileDialog", false);
	}

	private SetProfile(callback: (success: boolean) => void): void
	{
		EZArchivePortal.EZUser.Set(this._user).WithCallback(response =>
		{
			if (response.Error != null)
			{
				Notification.Error("Error updating profile: " + response.Error.Message);
				callback(false);
				return;
			}

			callback(true);
		});
	}

	private GetProfile():any
	{
		EZArchivePortal.EZUser.Me().WithCallback(ezUserResponse =>
		{
			if (ezUserResponse.Error != null)
			{
				Notification.Error("Error getting profile: " + ezUserResponse.Error.Message);
				return;
			}

			if (ezUserResponse.Body.Results.length !== 0)
				this.LoadProfile(ezUserResponse.Body.Results[0]);
			else
				Notification.Error("Error getting profile, profile matching userid not found");
		});
	}

	private Reset():any
	{
		this.Name("");
		this.Email("");
		this.Preferences(null);
	}

	private LoadProfile(user: EZArchivePortal.IEZUser):any
	{
		this._user = user;

		this.Identifier(user.Identifier);
		this.Name(user.Name);
		this.Email(user.Email !== user.Identifier ? user.Email : "");
		this.Preferences(user.Preferences === null ? null : JSON.parse(user.Preferences));
		this.Permission(user.Permission);
		this.IsAdministrator(user.IsAdministrator);

		if (this.ShowTermsDialogIfNeeded())
			this.ShowDialogIfProfileIsIncomplete(false);
	}

	private ShowTermsDialogIfNeeded(): boolean
	{
		if (this.HasAcceptedTerms())
			return true;

		DialogManager.Show("Profile/Terms", true, false, true);
		return false;
	}

	public ShowDialogIfProfileIsIncomplete(showWelcome: boolean):void
	{
		if (this.Name() === null || this.Name().trim() === "")
			DialogManager.Show("Profile/EditProfileDialog", showWelcome, false);
	}
}

var instance = new Profile();

export = instance;