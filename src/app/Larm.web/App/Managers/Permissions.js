define(["require", "exports", "knockout", "Managers/Authorization", "Managers/Profile", "./Portal/EZArchivePortal"], function (require, exports, knockout, Authorization, Profile, EzArchivePortal) {
    "use strict";
    var UserPermissions = EzArchivePortal.UserPermissions;
    var Permissions = (function () {
        function Permissions() {
            this.CanEditMetadata = knockout.computed(function () { return Authorization.IsAuthenticated(); });
            this.CanEditAnnotations = knockout.computed(function () { return Authorization.IsAuthenticated(); });
            this.CanStreamContent = knockout.computed(function () { return Authorization.IsAuthenticated(); });
            this.CanAssociateWithLabel = knockout.computed(function () { return Authorization.IsAuthenticated(); });
            this.CanSeeOffsetMetadata = knockout.computed(function () { return Profile.IsAdministrator() || Profile.Permission() === UserPermissions.Administrator; });
            this.CanEditOffsetMetadata = this.CanSeeOffsetMetadata;
        }
        return Permissions;
    }());
    var instance = new Permissions();
    return instance;
});
