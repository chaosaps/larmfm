﻿declare module "vis"
{
	class Timeline
	{
		constructor(container: Element, Data: DataSet, options: ITimelineOptions);
		constructor(container: Element, Data: DataSet, groups: IGroup[], options: ITimelineOptions);

		setOptions(options: ITimelineOptions): void;
		addCustomTime(time: Date|any, id:string): void;
		setCustomTime(time: Date|any, id:string): void;

		getSelection():number[];
		setSelection(id: any, options?: { focus:boolean}):void;

		on(event:string, callback:(event:any)=>void):void;
		off(event:string, callback:()=>void):void;
	}

	class DataSet
	{
		constructor(data?: any[], options?: IDataSetOptions);

		add(data:any, senderId?:string):number[];
		add(data: any[], senderId?: string): number[];

		remove(id: any, senderId?: string): number[];

		update(data: any, senderId?: string): number[];
		update(data: any[], senderId?: string): number[];

		update(id: any, senderId?: string): number[];
		update(ids: any[], senderId?: string): number[];
		update(item: any, senderId?: string): number[];
		update(items: any[], senderId?: string): number[];

		getIds():number[];
	}

	interface IGroup
	{
		
	}

	interface ITimelineOptions
	{
		autoResize?:boolean;
		clickToUse?:boolean;
		start?: number;
		end?: number;
		min?: number;
		max?:number;
		editable?: boolean;
		selectable?: boolean;
		showCurrentTime?: boolean;
		maxHeight:string;
		zoomMin?: number;
		zoomMax?: number;
		showMajorLabels?:boolean;
		showMinorLabels?: boolean;
		snap: any;
		moment: (data:Date)=>moment.Moment;
		format:{
			minorLabels?: ITimelineLabelOptions;
			majorLabels?: ITimelineLabelOptions;
		}
		onAdd?(item:any, callback:(item:any)=>void):void;
		onUpdate?(item:any, callback:(item:any)=>void):void;
		onMove?(item:any, callback:(item:any)=>void):void;
		onRemove?(item: any, callback: (item: any) => void): void;
	}

	interface IDataSetOptions
	{
		fieldId?: string;
		type?: { [key: string]: string };
		queue?: any;
	}

	interface ITimelineLabelOptions
	{
		millisecond:string;
		second: string;
		minute: string;
		hour: string;
		weekday: string;
		day: string;
		month: string;
		year: string;
	}
} 