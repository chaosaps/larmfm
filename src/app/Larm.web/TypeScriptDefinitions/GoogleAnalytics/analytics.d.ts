﻿interface AnalyticsStatic
{
	(method:string, trackingId:string, configuration?:any):void;
	create(trackingId: string, configuration?: any): AnalyticsTracker;
}

interface AnalyticsTracker
{
	send(hitType:string, parameters?:any):void;
	send(hitType:string, ... parameters:any[]):void;
}

declare module "analytics" {
	export = ga;
}

declare var ga:AnalyticsStatic;