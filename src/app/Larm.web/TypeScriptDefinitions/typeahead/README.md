# Installation
> `npm install --save @types/typeahead`

# Summary
This package contains type definitions for typeahead.js (http://twitter.github.io/typeahead.js/).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/typeahead

Additional Details
 * Last updated: Mon, 05 Feb 2018 16:02:05 GMT
 * Dependencies: jquery
 * Global values: Bloodhound

# Credits
These definitions were written by Ivaylo Gochkov <https://github.com/igochkov>, Gidon Junge <https://github.com/gjunge>.
